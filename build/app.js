
/*
Ricochet Application
Author : James O'Brien
URL : http://www.doc.ic.ac.uk/~jo408/
*/

(function() {
  var API, R, UI, express, server;

  console.log("Starting Ricochet...");

  R = require('./engine/ricochet').Ricochet;

  console.log("Starting web server...");

  express = require('express');

  server = express.createServer();

  console.log("Starting API...");

  API = new (require('./api/http').HTTPAPI)(R);

  server.all('/api', API.respond);

  console.log("Starting UI...");

  UI = new (require('./ui/ui').UI);

  console.log("Establishing routes...");

  server.get('/*', UI.request);

  if (UI.viewEngine) server.set('view engine', UI.viewEngine);

  console.log("Ready.");

  server.listen(process.env.PORT || 3000);

  console.log("Listening...");

}).call(this);
