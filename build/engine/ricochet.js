(function() {
  var Ricochet;

  Ricochet = (function() {

    Ricochet.prototype._languages = ['ml'];

    function Ricochet() {
      var name, _i, _len, _ref, _ref2;
      if (!(((_ref = this._languages) != null ? _ref.length : void 0) > 0)) {
        throw "No languages defined for Ricochet.";
      }
      this.languages || (this.languages = {});
      _ref2 = this._languages;
      for (_i = 0, _len = _ref2.length; _i < _len; _i++) {
        name = _ref2[_i];
        console.log(" -> Loading language '" + name + "'");
        this.languages[name] = require("../lang/" + name + "/lang");
      }
    }

    Ricochet.prototype.parse = function(code, callback, language, bullet) {
      if (language == null) language = this._languages[0];
      if (bullet == null) bullet = false;
      return this.languages[language].parser.parse(code, callback, bullet);
    };

    return Ricochet;

  })();

  exports.Ricochet = new Ricochet();

}).call(this);
