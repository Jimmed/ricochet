
/*
HTTP API for Ricochet
Send code to be parsed as a POST request body, or as the parameter 'code' in a GET
*/

(function() {
  var HTTPAPI, parseQuery,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  parseQuery = require('url').parse;

  HTTPAPI = (function() {

    function HTTPAPI(engine, port, ip) {
      this.engine = engine;
      this.respond = __bind(this.respond, this);
      if (!this.engine) throw "No engine provided.";
    }

    HTTPAPI.prototype.respond = function(req, res) {
      var method;
      method = req.method.toLowerCase();
      if (method in this) {
        try {
          return this[method](req, res);
        } catch (e) {
          return this.error(req, res, e);
        }
      } else {
        return this.error(req, res, "Unsupported method '" + req.method + "'");
      }
    };

    HTTPAPI.prototype.get = function(req, res) {
      var query;
      query = parseQuery(req.url, true).query;
      if (!('code' in query)) {
        throw "You must supply code through a query parameter";
      }
      return this.engine.parse(query.code, function(data) {
        return res.json(data, query.lang, query.bullet);
      });
    };

    HTTPAPI.prototype.post = function(req, res) {
      var body, query,
        _this = this;
      query = parseQuery(req.url, true).query;
      body = '';
      req.on('data', function(data) {
        return body += data;
      });
      return req.on('end', (function() {
        return _this.engine.parse(body, (function(data) {
          return res.json(data);
        }), query.lang, query.bullet !== 'false');
      }));
    };

    HTTPAPI.prototype.error = function(req, res, error) {
      console.log(error);
      return res.send(error.toString(), 500);
    };

    return HTTPAPI;

  })();

  exports.HTTPAPI = HTTPAPI;

}).call(this);
