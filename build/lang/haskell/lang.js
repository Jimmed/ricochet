(function() {
  var parser;

  parser = {
    parse: function(code, callback) {
      return callback(require('./JSHCParser.js').JSHC.parse(code));
    }
  };

  exports.parser = parser;

}).call(this);
