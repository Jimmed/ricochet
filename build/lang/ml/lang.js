(function() {
  var decodeError, decodeOutput, execFile, fs, parser, temp, util,
    __slice = Array.prototype.slice;

  util = require('util');

  execFile = require('child_process').execFile;

  fs = require('fs');

  temp = require('temp');

  decodeOutput = function(text, path) {
    var output, stripped;
    stripped = text.replace(new RegExp(path.replace(/\//g, '\\\/'), 'g'), '');
    output = stripped.replace(/^\s?(\[[^\]]+\]\s?)*/g, '');
    return [
      {
        success: output
      }
    ];
  };

  decodeError = function(error) {
    var description, ec, el, filename, line, lines, out, range, sc, sl, x, _i, _len, _ref, _ref2, _ref3, _results;
    _ref = error.split("\n"), lines = 1 <= _ref.length ? __slice.call(_ref, 0) : [];
    _results = [];
    for (_i = 0, _len = lines.length; _i < _len; _i++) {
      line = lines[_i];
      if (!(line.length !== 0)) continue;
      _ref2 = line.split(':'), filename = _ref2[0], range = _ref2[1], description = 3 <= _ref2.length ? __slice.call(_ref2, 2) : [];
      if (filename === 'hamlet') {
        out = {
          internalError: range
        };
      } else {
        _ref3 = (function() {
          var _j, _len2, _ref3, _results2;
          _ref3 = range.split(/[^0-9]/g);
          _results2 = [];
          for (_j = 0, _len2 = _ref3.length; _j < _len2; _j++) {
            x = _ref3[_j];
            _results2.push(Number(x));
          }
          return _results2;
        })(), sl = _ref3[0], sc = _ref3[1], el = _ref3[2], ec = _ref3[3];
        out = {
          error: {
            range: {
              start: {
                row: sl - 1,
                column: sc
              },
              end: {
                row: el - 1,
                column: ec
              }
            },
            description: description.join(': ')
          }
        };
      }
      _results.push(out);
    }
    return _results;
  };

  parser = {
    parse: function(code, callback, bullet) {
      var finished, hamletpath, out, _callback;
      hamletpath = 'lib/hamlet-1.3.1';
      if (bullet) hamletpath += '-bullet';
      hamletpath += '/hamlet';
      console.log('{Processing API call for Standard ML...' + (bullet ? ' (with Bullet Types)' : void 0));
      out = {};
      finished = 0;
      _callback = function(job, output) {
        console.log(' -> ', job, 'finished.');
        out[job] = output;
        finished++;
        if (finished === 2) callback(out);
        if (finished === 2) return console.log(' -> Response sent.}');
      };
      return temp.open('hamlettmp', function(err, info) {
        fs.write(info.fd, code);
        return fs.close(info.fd, function(err) {
          execFile(hamletpath, ['-p', info.path], {}, function(err, stdout, stderr) {
            if (stderr.length > 0) {
              console.log(' -> ', stderr);
              return _callback('AST', decodeError(stderr, info.path));
            } else {
              return _callback('AST', decodeOutput(stdout, info.path));
            }
          });
          return execFile(hamletpath, ['-l', info.path], {}, function(err, stdout, stderr) {
            if (stderr.length > 0) {
              console.log(' -> Error:', stderr);
              return _callback('Type', decodeError(stderr));
            } else {
              return _callback('Type', decodeOutput(stdout, info.path));
            }
          });
        });
      });
    }
  };

  exports.parser = parser;

}).call(this);
