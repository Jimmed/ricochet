(function() {
  var RicochetUI;

  RicochetUI = (function() {

    function RicochetUI() {}

    RicochetUI.prototype.request = function(req, res) {
      var path;
      path = "./src/ui" + (req.path === '/' ? '/index.html' : req.path);
      return res.sendfile(path, function(err) {
        if (err) return res.send(404);
      });
    };

    return RicochetUI;

  })();

  exports.UI = RicochetUI;

}).call(this);
