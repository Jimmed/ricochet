# Ricochet

## What is Ricochet?

Ricochet is a test-bed implementation of the type system developed by Nakano and extended by Rowe, on top of the vanilla Standard ML type system.

For more information, please read the full report, contained in the 'report' folder.

## Installation

Firstly, you will need [node.js](http://nodejs.org), [CoffeeScript](http://coffeescript.org) and a copy of this repository. 

You will probably need to compile HaMLet if your target machine does not match mine. 

* Install an ML compiler - [SML-NJ](http://www.smlnj.org) is recommended.
* Navigate to `lib/hamlet-1.3.1`
* Run `make with-smlnj` (if using SML-NJ)
* Navigate to `lib/hamlet-1.3.1-bullet`
* Run `make with-smlnj`

With any luck, you will now have two `hamlet` executables - one vanilla, and one bulleted.

Return to Ricochet's root directory.
You can start Ricochet by running `node build/app.js`.

If you have completed the above, you should now be able to access `http://localhost:3000` using your web browser. It is strongly recommended that you use Google Chrome, Safari or another up-to-date webkit-based browser. Note that you can set the environment variable `PORT`, and Ricochet will server content via that port instead.

This *has* worked perfectly using Ubuntu 12.04 LTS.
