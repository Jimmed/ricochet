(function($) {
  function prettyBox(contents, title, cssClass) {
      return '<div class="prettyBox ' + cssClass + '"><h3>' + title
           + '</h3><div class="output">' + contents + '</div></div>'; 
  }
  function formatAST(result) {
    response = [];
    for (i = 0; i < result.length; i++) {
      if (result[i].success) {
        var ast = result[i].success.replace(/[0-9]+\.[0-9]+-[0-9]+\.[0-9]+/g, '<a class="rangeref" href="#$&">$&</a>');
        var html = '<pre class="ast">' + ast + '</pre>';
        response.push(prettyBox(html, 'Abstract Syntax Tree', 'success'))
      }
    }
    return response.join('');
  }
  function formatType(result) {
    function internalError(text) {
      html = '<p class="description">' + text + '</p>';
      return prettyBox(html, 'Internal Error', 'error');
    }
    function error(e) {
      e.description[0] = 
      html = '<p class="description">' + e.description 
           + '</p><p class="range">'
      if (e.range.start.row == e.range.end.row && e.range.start.column == e.range.end.column) {
        html += 'At row ' + e.range.start.row + ', column ' + e.range.start.column;
      } else {
        html += 'From row ' + e.range.start.row + ', column ' + e.range.start.column
             + ' to row ' + e.range.end.row + ', column ' + e.range.end.column;
      }
      html += '</p>';
      return prettyBox(html, 'Error', 'error');
    }
    function success(types) {
      html = '<pre>' + types + '</pre>';
      return prettyBox(html, 'Typeable!', 'success');
    }

    var response;
    response = [];
    if (!result) response.push(internalError('Invalid response from server.'));
    for (i = 0; i < result.length; i++) {
      console.log(result[i]);
      if (result[i].internalError) response.push(internalError(result[i].internalError));
      if (result[i].error) response.push(error(result[i].error));
      if (result[i].success) response.push(success(result[i].success));
    }
    response = [response.join('')];
    if (result[0] && result[0].error && result[0].error.range) response.push(result[0].error.range);
    return response;
  }

  $(function(){
    /* Load ace.js */
    var buttons = $('#buttons').text(' ');
    var input = ace.edit('input');
    input.getSession().setMode("ace/mode/ocaml");
    input.setTheme("ace/theme/solarized_light");
    var output = $('#output');

    /* Method for making a selection in ace.js */
    var makeSelection = function(range) {
	    if(range && range.start && range.end) {
	      var selection = input.getSelection();
	      var sRange = selection.getRange();
        if(range.start.row && range.start.column)
    	    sRange.setStart(range.start.row, range.start.column);
        if(range.end.row && range.end.column)
    	    sRange.setEnd(range.end.row, range.end.column);
	      selection.setSelectionRange(sRange);
	    }
    };

    /* Translates a string range to a range object */
    var decodeRange = function(strRange) {
      var end, start, strEnd, strStart, x, _ref, _ref1, _ref2;
      _ref = strRange.split('-'), strStart = _ref[0], strEnd = _ref[1];
      start = {};
      end = {};
      _ref1 = (function() {
        var _i, _len, _ref1, _results;
        _ref1 = strStart.split('.');
        _results = [];
        for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
          x = _ref1[_i];
          _results.push(Number(x));
        }
        return _results;
      })(), start.row = _ref1[0] - 1, start.column = _ref1[1];
      _ref2 = (function() {
        var _i, _len, _ref2, _results;
        _ref2 = strEnd.split('.');
        _results = [];
        for (_i = 0, _len = _ref2.length; _i < _len; _i++) {
          x = _ref2[_i];
          _results.push(Number(x));
        }
        return _results;
      })(), end.row = _ref2[0] - 1, end.column = _ref2[1];
      return {start: start, end: end};
    };
    
    /* Build buttons bar */
    var language = $('<select/>', {'name':'language'}).appendTo(buttons);
    language.append($('<option/>',{value:'ml', text:'Standard ML'}));
    var bullet = $('<input/>',{type:'checkbox','name':'bullet'}).appendTo(buttons);
    bullet.after($('<label/>',{'for':'bullet',text:'Bullet-Type'}));
    
    /* Drag & Drop support */
    var drop = document.getElementById('input');
    var noop = function(evt) { evt.stopPropagation(); evt.preventDefault(); };
    drop.addEventListener('dragenter', noop, false);
    drop.addEventListener('dragexit', noop, false);
    drop.addEventListener('dragover', noop, false);
    drop.addEventListener('drop', function(evt) {
      noop(evt);
      var files = evt.dataTransfer.files;
      console.log('Drag & drop: ', files);
      if (files.length > 0) {
        var file = files[0];
        var reader = new FileReader();
        reader.onload = function(event) {
          input.setValue(event.target.result);
        };
        reader.readAsText(file);
      }
    }, false);
    
    /* Handle submission */
    var typeIt = $('<input/>', {type:'button',value:'Type It!'}).appendTo(buttons).click(function(ev) {
      var _this = $(this);
      _this.attr('disabled', 'disabled');
      var spinner = $('<img/>',{src:'theme/spinner.gif'});
      output.removeClass('init done').addClass('loading').html(spinner);
      $.ajax({
        url: './api?lang=' + language.val() + '&bullet=' + bullet.is(':checked'),
        type: 'POST', 
        data: input.getValue(), 
        complete: function() {
          _this.removeAttr('disabled');
          spinner.detach();
        },
        success: function(result) {
          var html;
          if(result && result.Type) {
            data = formatType(result.Type);
            if(data[0]) html = data[0];
              makeSelection(data[1]);
          }
          if(result && result.AST) {
            html += formatAST(result.AST);
          }
          output.removeClass('loading').addClass('done').html(html);
          $('.ast .rangeref').click(function(event) {
            var range = decodeRange($(this).text());
            makeSelection(range);
            event.preventDefault();
          });
        },
				error: function(result) {
					html = formatType([{internalError:'No response from server.'}])[0];
          output.removeClass('loading').addClass('done').html(html);
				},
        timeout: 30000
      });
    });
  });
})(jQuery);
