# Define UI
class RicochetUI
  request: (req, res) ->
    path = "./src/ui#{if req.path == '/' then '/index.html' else req.path}"
    res.sendfile path, (err) -> res.send 404 if err

# Start application
exports.UI = RicochetUI