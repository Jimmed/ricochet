###
HTTP API for Ricochet
Send code to be parsed as a POST request body, or as the parameter 'code' in a GET
###

parseQuery = require('url').parse;

class HTTPAPI
  constructor: (@engine, port, ip) ->        
    throw "No engine provided." unless @engine

  respond: (req, res) =>
    method = req.method.toLowerCase()
    if method of this
      try 
        @[method] req, res 
      catch e 
        @error req, res, e
    else
      @error req, res, "Unsupported method '#{req.method}'"
  
  get: (req, res) ->
    query = parseQuery(req.url, true).query
    throw "You must supply code through a query parameter" unless 'code' of query
    @engine.parse query.code, (data) -> res.json data, query.lang, query.bullet
    
  post: (req, res) ->
    query = parseQuery(req.url, true).query
    body = ''
    req.on 'data', (data) -> body += data
    req.on 'end', (=> @engine.parse body, ((data) -> res.json data), query.lang, query.bullet isnt 'false')
    
  error: (req, res, error) ->
    console.log error
    res.send error.toString(), 500
    
exports.HTTPAPI = HTTPAPI
