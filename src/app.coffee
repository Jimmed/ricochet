###
Ricochet Application
Author : James O'Brien
URL : http://www.doc.ic.ac.uk/~jo408/
###

# Start the engine
console.log "Starting Ricochet..."
R = require('./engine/ricochet').Ricochet

# Start a web server
console.log "Starting web server..."
express = require 'express'
server = express.createServer()

# Start an HTTP API
console.log "Starting API..."
API = new (require('./api/http').HTTPAPI) R
server.all '/api', API.respond

# Start a UI
console.log "Starting UI..."
UI = new (require('./ui/ui').UI)
console.log "Establishing routes..."
server.get '/*', UI.request
server.set 'view engine', UI.viewEngine if UI.viewEngine

# Start listening
console.log "Ready."
server.listen process.env.PORT or 3000
console.log "Listening..."
