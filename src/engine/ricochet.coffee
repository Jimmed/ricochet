# Define app engine
class Ricochet
  _languages: ['ml']
  
  constructor: ->
    throw "No languages defined for Ricochet." unless @_languages?.length > 0
    @languages or= {}
    for name in @_languages
      console.log " -> Loading language '#{name}'"
      @languages[name] = require "../lang/#{name}/lang"
      
  parse: (code, callback, language=@_languages[0], bullet=false) ->
    @languages[language].parser.parse code, callback, bullet

# Start application
exports.Ricochet = new Ricochet()
