parser =
  # Acts as a wrapper for JSHC
  parse: (code, callback) -> callback require('./JSHCParser.js').JSHC.parse code
    
exports.parser = parser
