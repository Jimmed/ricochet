util = require 'util'
execFile = require('child_process').execFile
fs = require 'fs'
temp = require 'temp'

decodeOutput = (text, path) ->
  # Strip out instances of the temporary file's path
  stripped = text.replace new RegExp(path.replace(/\//g, '\\\/'), 'g'), ''
  
  # Remove bracketed text from start of output
  output = stripped.replace /^\s?(\[[^\]]+\]\s?)*/g, ''
  
  [{success: output}]

decodeError = (error) ->
  # Split apart the error
  [lines...] = error.split "\n"
  for line in lines when line.length isnt 0
    [filename,range,description...] = line.split ':'
    if filename is 'hamlet'
      out = {internalError: range}
    else 
      [sl,sc,el,ec] = (Number(x) for x in range.split /[^0-9]/g)
      out = error:
        range: 
          start: {row: sl - 1, column: sc}, 
          end: {row: el - 1, column: ec}
        description: description.join ': '
    out

parser =
  # Acts as a wrapper for HaMLet
  parse: (code, callback, bullet) ->
    hamletpath = 'lib/hamlet-1.3.1'
    hamletpath += '-bullet' if bullet
    hamletpath += '/hamlet'
    console.log '{Processing API call for Standard ML...' + (' (with Bullet Types)' if bullet)
    out = {}
    finished = 0
    _callback = (job, output) ->
      console.log ' -> ', job, 'finished.'
      out[job] = output
      finished++
      callback out if finished == 2
      console.log ' -> Response sent.}' if finished == 2
    temp.open 'hamlettmp', (err, info) ->
      fs.write info.fd, code
      fs.close info.fd, (err) ->
        execFile hamletpath, ['-p', info.path], {}, (err, stdout, stderr) ->
          if stderr.length > 0
            console.log ' -> ', stderr
            _callback 'AST', decodeError stderr, info.path 
          else
            _callback 'AST', decodeOutput stdout, info.path
        execFile hamletpath, ['-l', info.path], {}, (err, stdout, stderr) ->
          if stderr.length > 0
            console.log ' -> Error:', stderr
            _callback 'Type', decodeError stderr 
          else
            _callback 'Type', decodeOutput stdout, info.path

exports.parser = parser
