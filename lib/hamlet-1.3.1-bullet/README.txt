All HaMLet sources (c) 1999-2007 Andreas Rossberg.

Please see INSTALL.txt for brief instructions and the doc directory for
the full documentation. See LICENSE.txt for licensing information.

The HaMLet homepage is:

	http://www.ps.uni-sb.de/hamlet/
