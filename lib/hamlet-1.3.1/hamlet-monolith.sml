(* DO NOT EDIT! *)
(* Generated from sources.cm (Fri Jun 15 13:09:49 BST 2012) *)
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML implementation main structure
 *)

signature SML =
sig
    val basisPath :	string ref

    val parseString :	string -> unit	(* Parse only *)
    val elabString :	string -> unit	(* Parse and elaborate *)
    val evalString :	string -> unit	(* Parse and evaluate *)
    val execString :	string -> unit	(* Parse, elaborate, and evaluate *)

    val parseFile :	string -> unit
    val elabFile :	string -> unit
    val evalFile :	string -> unit
    val execFile :	string -> unit

    val parseFiles :	string list -> unit
    val elabFiles :	string list -> unit
    val evalFiles :	string list -> unit
    val execFiles :	string list -> unit

    val parseSession :	unit -> unit
    val elabSession :	unit -> unit
    val evalSession :	unit -> unit
    val execSession :	unit -> unit
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Helpers for handling source strings
 *)

signature SOURCE =
sig
    type source = string
    type pos    = int * int
    type region = pos * pos
    type info   = {file : string option, region : region}

    exception Error of (int * int) * string

    val nowhere :  info
    val over :     info * info -> info
    val between :  info * info -> info
    val compare :  info * info -> order
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Helpers for handling source strings
 *)

structure Source :> SOURCE =
struct
    type source = string
    type pos    = int * int
    type region = pos * pos
    type info   = {file : string option, region : region}

    exception Error of (int * int) * string

    val nowhere = {file = NONE, region = ((0,0), (0,0))}

    fun over'(r1 : region, r2 : region)    = (#1 r1, #2 r2)
    fun between'(r1 : region, r2 : region) = (#2 r1, #1 r2)

    fun transform f (i1 : info, i2 : info) = 
	{file = #file i1, region = f(#region i1, #region i2)}

    val over    = transform over'
    val between = transform between'

    fun comparePair compare1 ((x1,y1), (x2,y2)) =
	case compare1(x1, x2)
	  of EQUAL => compare1(y1, y2)
	   | order => order

    fun compare(i1 : info, i2 : info) =
	comparePair (comparePair Int.compare) (#region i1, #region i2)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Stamp generator.
 *)

signature STAMP =
sig
    eqtype stamp

    val stamp :		unit  -> stamp
    val toString :	stamp -> string

    val reset :		unit -> unit

    val compare :	stamp * stamp -> order
    val min :		stamp * stamp -> stamp
end;
(* ord-key-sig.sml
 *
 * COPYRIGHT (c) 1993 by AT&T Bell Laboratories.  See COPYRIGHT file for details.
 *
 * Abstract linearly ordered keys.
 *
 *)

signature ORD_KEY =
  sig
    type ord_key

    val compare : ord_key * ord_key -> order

  end (* ORD_KEY *)
;
(* ord-map-sig.sml
 *
 * COPYRIGHT (c) 1996 by AT&T Research.  See COPYRIGHT file for details.
 *
 * Abstract signature of an applicative-style finite maps (dictionaries)
 * structure over ordered monomorphic keys.
 *)

signature ORD_MAP =
  sig

    structure Key : ORD_KEY

    type 'a map

    val empty : 'a map
	(* The empty map *)

    val isEmpty : 'a map -> bool
	(* Return true if and only if the map is empty *)

    val singleton : (Key.ord_key * 'a) -> 'a map
	(* return the specified singleton map *)

    val insert  : 'a map * Key.ord_key * 'a -> 'a map
    val insert' : ((Key.ord_key * 'a) * 'a map) -> 'a map
	(* Insert an item. *)

    val find : 'a map * Key.ord_key -> 'a option
	(* Look for an item, return NONE if the item doesn't exist *)

    val inDomain : ('a map * Key.ord_key) -> bool
	(* return true, if the key is in the domain of the map *)

    val remove : 'a map * Key.ord_key -> 'a map * 'a
	(* Remove an item, returning new map and value removed.
         * Raises LibBase.NotFound if not found.
	 *)

    val first : 'a map -> 'a option
    val firsti : 'a map -> (Key.ord_key * 'a) option
	(* return the first item in the map (or NONE if it is empty) *)

    val numItems : 'a map ->  int
	(* Return the number of items in the map *)

    val listItems  : 'a map -> 'a list
    val listItemsi : 'a map -> (Key.ord_key * 'a) list
	(* Return an ordered list of the items (and their keys) in the map. *)

    val listKeys : 'a map -> Key.ord_key list
	(* return an ordered list of the keys in the map. *)

    val collate : ('a * 'a -> order) -> ('a map * 'a map) -> order
	(* given an ordering on the map's range, return an ordering
	 * on the map.
	 *)

    val unionWith  : ('a * 'a -> 'a) -> ('a map * 'a map) -> 'a map
    val unionWithi : (Key.ord_key * 'a * 'a -> 'a) -> ('a map * 'a map) -> 'a map
	(* return a map whose domain is the union of the domains of the two input
	 * maps, using the supplied function to define the map on elements that
	 * are in both domains.
	 *)

    val intersectWith  : ('a * 'b -> 'c) -> ('a map * 'b map) -> 'c map
    val intersectWithi : (Key.ord_key * 'a * 'b -> 'c) -> ('a map * 'b map) -> 'c map
	(* return a map whose domain is the intersection of the domains of the
	 * two input maps, using the supplied function to define the range.
	 *)

    val app  : ('a -> unit) -> 'a map -> unit
    val appi : ((Key.ord_key * 'a) -> unit) -> 'a map -> unit
	(* Apply a function to the entries of the map in map order. *)

    val map  : ('a -> 'b) -> 'a map -> 'b map
    val mapi : (Key.ord_key * 'a -> 'b) -> 'a map -> 'b map
	(* Create a new map by applying a map function to the
         * name/value pairs in the map.
         *)

    val foldl  : ('a * 'b -> 'b) -> 'b -> 'a map -> 'b
    val foldli : (Key.ord_key * 'a * 'b -> 'b) -> 'b -> 'a map -> 'b
	(* Apply a folding function to the entries of the map
         * in increasing map order.
         *)

    val foldr  : ('a * 'b -> 'b) -> 'b -> 'a map -> 'b
    val foldri : (Key.ord_key * 'a * 'b -> 'b) -> 'b -> 'a map -> 'b
	(* Apply a folding function to the entries of the map
         * in decreasing map order.
         *)

    val filter  : ('a -> bool) -> 'a map -> 'a map
    val filteri : (Key.ord_key * 'a -> bool) -> 'a map -> 'a map
	(* Filter out those elements of the map that do not satisfy the
	 * predicate.  The filtering is done in increasing map order.
	 *)

    val mapPartial  : ('a -> 'b option) -> 'a map -> 'b map
    val mapPartiali : (Key.ord_key * 'a -> 'b option) -> 'a map -> 'b map
	(* map a partial function over the elements of a map in increasing
	 * map order.
	 *)

  end (* ORD_MAP *)
;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML finite maps
 *
 * Definition, Section 4.2
 *
 * Note:
 *     This signature just extends the one available in the SML/NJ lib.
 *     Actually, the operation added here would be general purpose and useful enough
 *     (and more efficient) to be in the lib. Also see FIN_SET.
 *)

signature FIN_MAP =
sig
    include ORD_MAP

    exception NotFound
    exception Duplicate

    val fromList :	(Key.ord_key * 'a) list -> 'a map

    val delete :	'a map * Key.ord_key -> 'a map
    val difference :	'a map * 'a map -> 'a map

    val all :		('a -> bool) -> 'a map -> bool
    val exists :	('a -> bool) -> 'a map -> bool
    val alli :		(Key.ord_key * 'a -> bool) -> 'a map -> bool
    val existsi :	(Key.ord_key * 'a -> bool) -> 'a map -> bool

    val disjoint :	'a map * 'a map -> bool
end;
(* lib-base-sig.sml
 *
 * COPYRIGHT (c) 1993 by AT&T Bell Laboratories.  See COPYRIGHT file for details.
 *)

signature LIB_BASE =
  sig

    exception Unimplemented of string
	(* raised to report unimplemented features *)
    exception Impossible of string
	(* raised to report internal errors *)

    exception NotFound
	(* raised by searching operations *)

    val failure : {module : string, func : string, msg : string} -> 'a
	(* raise the exception Fail with a standard format message. *)

    val version : {date : string, system : string, version_id : int list}
    val banner : string

  end (* LIB_BASE *)
;
(* lib-base.sml
 *
 * COPYRIGHT (c) 1993 by AT&T Bell Laboratories.  See COPYRIGHT file for details.
 *)

structure LibBase : LIB_BASE =
  struct

  (* raised to report unimplemented features *)
    exception Unimplemented of string

  (* raised to report internal errors *)
    exception Impossible of string

  (* raised by searching operations *)
    exception NotFound

  (* raise the exception Fail with a standard format message. *)
    fun failure {module, func, msg} =
	  raise (Fail(concat[module, ".", func, ": ", msg]))

    val version = {
	    date = "June 1, 1996", 
	    system = "SML/NJ Library",
	    version_id = [1, 0]
	  }

    fun f ([], l) = l
      | f ([x : int], l) = (Int.toString x)::l
      | f (x::r, l) = (Int.toString x) :: "." :: f(r, l)

    val banner = concat (
	    #system version :: ", Version " ::
	    f (#version_id version, [", ", #date version]))

  end (* LibBase *)
;
(* binary-map-fn.sml
 *
 * COPYRIGHT (c) 1993 by AT&T Bell Laboratories.  See COPYRIGHT file for details.
 *
 * This code was adapted from Stephen Adams' binary tree implementation
 * of applicative integer sets.
 *
 *   Copyright 1992 Stephen Adams.
 *
 *    This software may be used freely provided that:
 *      1. This copyright notice is attached to any copy, derived work,
 *         or work including all or part of this software.
 *      2. Any derived work must contain a prominent notice stating that
 *         it has been altered from the original.
 *
 *
 *   Name(s): Stephen Adams.
 *   Department, Institution: Electronics & Computer Science,
 *      University of Southampton
 *   Address:  Electronics & Computer Science
 *             University of Southampton
 *	     Southampton  SO9 5NH
 *	     Great Britian
 *   E-mail:   sra@ecs.soton.ac.uk
 *
 *   Comments:
 *
 *     1.  The implementation is based on Binary search trees of Bounded
 *         Balance, similar to Nievergelt & Reingold, SIAM J. Computing
 *         2(1), March 1973.  The main advantage of these trees is that
 *         they keep the size of the tree in the node, giving a constant
 *         time size operation.
 *
 *     2.  The bounded balance criterion is simpler than N&R's alpha.
 *         Simply, one subtree must not have more than `weight' times as
 *         many elements as the opposite subtree.  Rebalancing is
 *         guaranteed to reinstate the criterion for weight>2.23, but
 *         the occasional incorrect behaviour for weight=2 is not
 *         detrimental to performance.
 *
 *)

functor BinaryMapFn (K : ORD_KEY) : ORD_MAP =
  struct

    structure Key = K

    (*
    **  val weight = 3
    **  fun wt i = weight * i
    *)
    fun wt (i : int) = i + i + i

    datatype 'a map
      = E 
      | T of {
          key : K.ord_key, 
          value : 'a, 
          cnt : int, 
          left : 'a map, 
          right : 'a map
	}

    val empty = E

    fun isEmpty E = true
      | isEmpty _ = false

    fun numItems E = 0
      | numItems (T{cnt,...}) = cnt

  (* return the first item in the map (or NONE if it is empty) *)
    fun first E = NONE
      | first (T{value, left=E, ...}) = SOME value
      | first (T{left, ...}) = first left

  (* return the first item in the map and its key (or NONE if it is empty) *)
    fun firsti E = NONE
      | firsti (T{key, value, left=E, ...}) = SOME(key, value)
      | firsti (T{left, ...}) = firsti left

local
    fun N(k,v,E,E) = T{key=k,value=v,cnt=1,left=E,right=E}
      | N(k,v,E,r as T n) = T{key=k,value=v,cnt=1+(#cnt n),left=E,right=r}
      | N(k,v,l as T n,E) = T{key=k,value=v,cnt=1+(#cnt n),left=l,right=E}
      | N(k,v,l as T n,r as T n') = 
          T{key=k,value=v,cnt=1+(#cnt n)+(#cnt n'),left=l,right=r}

    fun single_L (a,av,x,T{key=b,value=bv,left=y,right=z,...}) = 
          N(b,bv,N(a,av,x,y),z)
      | single_L _ = raise Match
    fun single_R (b,bv,T{key=a,value=av,left=x,right=y,...},z) = 
          N(a,av,x,N(b,bv,y,z))
      | single_R _ = raise Match
    fun double_L (a,av,w,T{key=c,value=cv,left=T{key=b,value=bv,left=x,right=y,...},right=z,...}) =
          N(b,bv,N(a,av,w,x),N(c,cv,y,z))
      | double_L _ = raise Match
    fun double_R (c,cv,T{key=a,value=av,left=w,right=T{key=b,value=bv,left=x,right=y,...},...},z) = 
          N(b,bv,N(a,av,w,x),N(c,cv,y,z))
      | double_R _ = raise Match

    fun T' (k,v,E,E) = T{key=k,value=v,cnt=1,left=E,right=E}
      | T' (k,v,E,r as T{right=E,left=E,...}) =
          T{key=k,value=v,cnt=2,left=E,right=r}
      | T' (k,v,l as T{right=E,left=E,...},E) =
          T{key=k,value=v,cnt=2,left=l,right=E}

      | T' (p as (_,_,E,T{left=T _,right=E,...})) = double_L p
      | T' (p as (_,_,T{left=E,right=T _,...},E)) = double_R p

        (* these cases almost never happen with small weight*)
      | T' (p as (_,_,E,T{left=T{cnt=ln,...},right=T{cnt=rn,...},...})) =
          if ln < rn then single_L p else double_L p
      | T' (p as (_,_,T{left=T{cnt=ln,...},right=T{cnt=rn,...},...},E)) =
          if ln > rn then single_R p else double_R p

      | T' (p as (_,_,E,T{left=E,...})) = single_L p
      | T' (p as (_,_,T{right=E,...},E)) = single_R p

      | T' (p as (k,v,l as T{cnt=ln,left=ll,right=lr,...},
                      r as T{cnt=rn,left=rl,right=rr,...})) =
          if rn >= wt ln then (*right is too big*)
            let val rln = numItems rl
                val rrn = numItems rr
            in
              if rln < rrn then  single_L p  else  double_L p
            end
        
          else if ln >= wt rn then  (*left is too big*)
            let val lln = numItems ll
                val lrn = numItems lr
            in
              if lrn < lln then  single_R p  else  double_R p
            end
    
          else T{key=k,value=v,cnt=ln+rn+1,left=l,right=r}

    local
      fun min (T{left=E,key,value,...}) = (key,value)
        | min (T{left,...}) = min left
        | min _ = raise Match
  
      fun delmin (T{left=E,right,...}) = right
        | delmin (T{key,value,left,right,...}) = T'(key,value,delmin left,right)
        | delmin _ = raise Match
    in
      fun delete' (E,r) = r
        | delete' (l,E) = l
        | delete' (l,r) = let val (mink,minv) = min r in
            T'(mink,minv,l,delmin r)
          end
    end
in
    fun mkDict () = E
    
    fun singleton (x,v) = T{key=x,value=v,cnt=1,left=E,right=E}

    fun insert (E,x,v) = T{key=x,value=v,cnt=1,left=E,right=E}
      | insert (T(set as {key,left,right,value,...}),x,v) =
          case K.compare (key,x) of
            GREATER => T'(key,value,insert(left,x,v),right)
          | LESS => T'(key,value,left,insert(right,x,v))
          | _ => T{key=x,value=v,left=left,right=right,cnt= #cnt set}
    fun insert' ((k, x), m) = insert(m, k, x)

    fun inDomain (set, x) = let 
	  fun mem E = false
	    | mem (T(n as {key,left,right,...})) = (case K.compare (x,key)
		 of GREATER => mem right
		  | EQUAL => true
		  | LESS => mem left
		(* end case *))
	  in
	    mem set
	  end

    fun find (set, x) = let 
	  fun mem E = NONE
	    | mem (T(n as {key,left,right,...})) = (case K.compare (x,key)
		 of GREATER => mem right
		  | EQUAL => SOME(#value n)
		  | LESS => mem left
		(* end case *))
	  in
	    mem set
	  end

    fun remove (E,x) = raise LibBase.NotFound
      | remove (set as T{key,left,right,value,...},x) = (
          case K.compare (key,x)
	   of GREATER => let
		val (left', v) = remove(left, x)
		in
		  (T'(key, value, left', right), v)
		end
            | LESS => let
		val (right', v) = remove (right, x)
		in
		  (T'(key, value, left, right'), v)
		end
            | _ => (delete'(left,right),value)
	  (* end case *))

    fun listItems d = let
	  fun d2l (E, l) = l
	    | d2l (T{key,value,left,right,...}, l) =
		d2l(left, value::(d2l(right,l)))
	  in
	    d2l (d,[])
	  end

    fun listItemsi d = let
	  fun d2l (E, l) = l
	    | d2l (T{key,value,left,right,...}, l) =
		d2l(left, (key,value)::(d2l(right,l)))
	  in
	    d2l (d,[])
	  end

    fun listKeys d = let
	  fun d2l (E, l) = l
	    | d2l (T{key,left,right,...}, l) = d2l(left, key::(d2l(right,l)))
	  in
	    d2l (d,[])
	  end

    local
      fun next ((t as T{right, ...})::rest) = (t, left(right, rest))
	| next _ = (E, [])
      and left (E, rest) = rest
	| left (t as T{left=l, ...}, rest) = left(l, t::rest)
    in
    fun collate cmpRng (s1, s2) = let
	  fun cmp (t1, t2) = (case (next t1, next t2)
		 of ((E, _), (E, _)) => EQUAL
		  | ((E, _), _) => LESS
		  | (_, (E, _)) => GREATER
		  | ((T{key=x1, value=y1, ...}, r1), (T{key=x2, value=y2, ...}, r2)) => (
		      case Key.compare(x1, x2)
		       of EQUAL => (case cmpRng(y1, y2)
			     of EQUAL => cmp (r1, r2)
			      | order => order
			    (* end case *))
			| order => order
		      (* end case *))
		(* end case *))
	  in
	    cmp (left(s1, []), left(s2, []))
	  end
    end (* local *)

    fun appi f d = let
	  fun app' E = ()
	    | app' (T{key,value,left,right,...}) = (
		app' left; f(key, value); app' right)
	  in
	    app' d
	  end
    fun app f d = let
	  fun app' E = ()
	    | app' (T{value,left,right,...}) = (
		app' left; f value; app' right)
	  in
	    app' d
	  end

    fun mapi f d = let
	  fun map' E = E
	    | map' (T{key,value,left,right,cnt}) = let
		val left' = map' left
		val value' = f(key, value)
		val right' = map' right
		in
		  T{cnt=cnt, key=key, value=value', left = left', right = right'}
		end
	  in
	    map' d
	  end
    fun map f d = mapi (fn (_, x) => f x) d

    fun foldli f init d = let
	  fun fold (E, v) = v
	    | fold (T{key,value,left,right,...}, v) =
		fold (right, f(key, value, fold(left, v)))
	  in
	    fold (d, init)
	  end
    fun foldl f init d = foldli (fn (_, v, accum) => f (v, accum)) init d

    fun foldri f init d = let
	  fun fold (E,v) = v
	    | fold (T{key,value,left,right,...},v) =
		fold (left, f(key, value, fold(right, v)))
	  in
	    fold (d, init)
	  end
    fun foldr f init d = foldri (fn (_, v, accum) => f (v, accum)) init d

(** To be implemented **
    val filter  : ('a -> bool) -> 'a map -> 'a map
    val filteri : (Key.ord_key * 'a -> bool) -> 'a map -> 'a map
**)

    end (* local *)

(* the following are generic implementations of the unionWith and intersectWith
 * operetions.  These should be specialized for the internal representations
 * at some point.
 *)
    fun unionWith f (m1, m2) = let
	  fun ins  f (key, x, m) = (case find(m, key)
		 of NONE => insert(m, key, x)
		  | (SOME x') => insert(m, key, f(x, x'))
		(* end case *))
	  in
	    if (numItems m1 > numItems m2)
	      then foldli (ins (fn (a, b) => f (b, a))) m1 m2
	      else foldli (ins f) m2 m1
	  end
    fun unionWithi f (m1, m2) = let
	  fun ins f (key, x, m) = (case find(m, key)
		 of NONE => insert(m, key, x)
		  | (SOME x') => insert(m, key, f(key, x, x'))
		(* end case *))
	  in
	    if (numItems m1 > numItems m2)
	      then foldli (ins (fn (k, a, b) => f (k, b, a))) m1 m2
	      else foldli (ins f) m2 m1
	  end

    fun intersectWith f (m1, m2) = let
	(* iterate over the elements of m1, checking for membership in m2 *)
	  fun intersect f (m1, m2) = let
		fun ins (key, x, m) = (case find(m2, key)
		       of NONE => m
			| (SOME x') => insert(m, key, f(x, x'))
		      (* end case *))
		in
		  foldli ins empty m1
		end
	  in
	    if (numItems m1 > numItems m2)
	      then intersect f (m1, m2)
	      else intersect (fn (a, b) => f(b, a)) (m2, m1)
	  end
    fun intersectWithi f (m1, m2) = let
	(* iterate over the elements of m1, checking for membership in m2 *)
	  fun intersect f (m1, m2) = let
		fun ins (key, x, m) = (case find(m2, key)
		       of NONE => m
			| (SOME x') => insert(m, key, f(key, x, x'))
		      (* end case *))
		in
		  foldli ins empty m1
		end
	  in
	    if (numItems m1 > numItems m2)
	      then intersect f (m1, m2)
	      else intersect (fn (k, a, b) => f(k, b, a)) (m2, m1)
	  end

  (* this is a generic implementation of filter.  It should
   * be specialized to the data-structure at some point.
   *)
    fun filter predFn m = let
	  fun f (key, item, m) = if predFn item
		then insert(m, key, item)
		else m
	  in
	    foldli f empty m
	  end
    fun filteri predFn m = let
	  fun f (key, item, m) = if predFn(key, item)
		then insert(m, key, item)
		else m
	  in
	    foldli f empty m
	  end

  (* this is a generic implementation of mapPartial.  It should
   * be specialized to the data-structure at some point.
   *)
    fun mapPartial f m = let
	  fun g (key, item, m) = (case f item
		 of NONE => m
		  | (SOME item') => insert(m, key, item')
		(* end case *))
	  in
	    foldli g empty m
	  end
    fun mapPartiali f m = let
	  fun g (key, item, m) = (case f(key, item)
		 of NONE => m
		  | (SOME item') => insert(m, key, item')
		(* end case *))
	  in
	    foldli g empty m
	  end

  end (* functor BinaryMapFn *)
;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML finite maps
 *
 * Definition, Section 4.2
 *
 * Note:
 *     This functor just extends the one available in the SML/NJ lib.
 *     Actually, the operation added here would be general purpose and useful enough
 *     (and more efficient) to be in the lib. Also see FinSetFn.
 *)

functor FinMapFn(Key : ORD_KEY) :>
FIN_MAP where type Key.ord_key = Key.ord_key =
struct
    structure BinaryMap	= BinaryMapFn(Key)
    open BinaryMap

    exception NotFound	= LibBase.NotFound
    exception Duplicate

    fun fromList kvs	= List.foldl (fn((k,v), m) =>
					if inDomain(m, k) then raise Duplicate
					else insert(m, k, v)) empty kvs

    fun delete(m, k)	= #1(remove(m, k)) handle LibBase.NotFound => m

    fun difference(m,n)	= filteri (fn(k, _) => not(inDomain(n, k))) m

    fun all p		= foldl (fn(v, b) => b andalso p v) true
    fun exists p	= foldl (fn(v, b) => b orelse p v) false
    fun alli p		= foldli (fn(k, v, b) => b andalso p(k, v)) true
    fun existsi p	= foldli (fn(k, v, b) => b orelse p(k, v)) false

    fun disjoint(m1,m2)	= isEmpty(intersectWith #2 (m1, m2))
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Stamp generator.
 *)

structure Stamp :> STAMP =
struct
    type stamp = int

    val r = ref 0

    fun reset()  =  r := 0
    fun stamp()  = (r := !r + 1; !r)

    val toString = Int.toString
    val compare  = Int.compare
    val min      = Int.min
end

structure StampMap = FinMapFn(type ord_key = Stamp.stamp
			      val  compare = Stamp.compare);
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML type names
 *
 * Definition, Section 4.1
 *
 * Notes:
 * - Equality is not a boolean attribute. We distinguish a 3rd kind of special
 *   type names which have equality regardless of the types applied. This
 *   implements ref, array, and equivalent types.
 * - For easy checking of pattern exhaustiveness we add an attribute
 *   `span' counting the number of constructors of the type.
 * - For checking of declaration orders etc we provide access to a time stamp.
 *)

signature TYNAME =
sig
    (* Type [Section 4.1] *)

    eqtype TyName					(* [t] *)


    (* Operations *)

    val tyname :		string * int * bool * int -> TyName
    val invent :		int * bool -> TyName
    val rename :		TyName -> TyName
    val removeEquality :	TyName -> TyName
    val Abs :			TyName -> TyName

    val arity :			TyName -> int
    val admitsEquality :	TyName -> bool
    val span :			TyName -> int
    val toString :		TyName -> string
    val time :			TyName -> Stamp.stamp

    val compare :		TyName * TyName -> order
end;
(* ordset-sig.sml
 *
 * COPYRIGHT (c) 1993 by AT&T Bell Laboratories.  See COPYRIGHT file for details.
 *
 * Signature for a set of values with an order relation.
 *)

signature ORD_SET =
  sig

    structure Key : ORD_KEY

    type item = Key.ord_key
    type set

    val empty : set
	(* The empty set *)

    val singleton : item -> set
	(* Create a singleton set *)

    val add  : set * item -> set
    val add' : (item * set) -> set
	(* Insert an item. *)

    val addList : set * item list -> set
	(* Insert items from list. *)

    val delete : set * item -> set
	(* Remove an item. Raise NotFound if not found. *)

    val member : set * item -> bool
	(* Return true if and only if item is an element in the set *)

    val isEmpty : set -> bool
	(* Return true if and only if the set is empty *)

    val equal : (set * set) -> bool
	(* Return true if and only if the two sets are equal *)

    val compare : (set * set) -> order
	(* does a lexical comparison of two sets *)

    val isSubset : (set * set) -> bool
	(* Return true if and only if the first set is a subset of the second *)

    val numItems : set ->  int
	(* Return the number of items in the table *)

    val listItems : set -> item list
	(* Return an ordered list of the items in the set *)

    val union : set * set -> set
        (* Union *)

    val intersection : set * set -> set
        (* Intersection *)

    val difference : set * set -> set
        (* Difference *)

    val map : (item -> item) -> set -> set
	(* Create a new set by applying a map function to the elements
	 * of the set.
         *)
     
    val app : (item -> unit) -> set -> unit
	(* Apply a function to the entries of the set 
         * in decreasing order
         *)

    val foldl : (item * 'b -> 'b) -> 'b -> set -> 'b
	(* Apply a folding function to the entries of the set 
         * in increasing order
         *)

    val foldr : (item * 'b -> 'b) -> 'b -> set -> 'b
	(* Apply a folding function to the entries of the set 
         * in decreasing order
         *)

    val filter : (item -> bool) -> set -> set

    val exists : (item -> bool) -> set -> bool

    val find : (item -> bool) -> set -> item option

  end (* ORD_SET *)
;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML finite sets
 *
 * Definition, Section 4.2
 *
 * Note:
 *     This signature just extends the one available in the SML/NJ lib.
 *     Actually, the operation added here would be general purpose and useful enough
 *     to be in the lib. Also see FIN_MAP.
 *)

signature FIN_SET =
sig
    include ORD_SET

    exception NotFound

    val fromList : item list -> set
end;
(* binary-set-fn.sml
 *
 * COPYRIGHT (c) 1993 by AT&T Bell Laboratories.  See COPYRIGHT file for details.
 *
 * This code was adapted from Stephen Adams' binary tree implementation
 * of applicative integer sets.
 *
 *    Copyright 1992 Stephen Adams.
 *
 *    This software may be used freely provided that:
 *      1. This copyright notice is attached to any copy, derived work,
 *         or work including all or part of this software.
 *      2. Any derived work must contain a prominent notice stating that
 *         it has been altered from the original.
 *
 *   Name(s): Stephen Adams.
 *   Department, Institution: Electronics & Computer Science,
 *      University of Southampton
 *   Address:  Electronics & Computer Science
 *             University of Southampton
 *         Southampton  SO9 5NH
 *         Great Britian
 *   E-mail:   sra@ecs.soton.ac.uk
 *
 *   Comments:
 *
 *     1.  The implementation is based on Binary search trees of Bounded
 *         Balance, similar to Nievergelt & Reingold, SIAM J. Computing
 *         2(1), March 1973.  The main advantage of these trees is that
 *         they keep the size of the tree in the node, giving a constant
 *         time size operation.
 *
 *     2.  The bounded balance criterion is simpler than N&R's alpha.
 *         Simply, one subtree must not have more than `weight' times as
 *         many elements as the opposite subtree.  Rebalancing is
 *         guaranteed to reinstate the criterion for weight>2.23, but
 *         the occasional incorrect behaviour for weight=2 is not
 *         detrimental to performance.
 *
 *     3.  There are two implementations of union.  The default,
 *         hedge_union, is much more complex and usually 20% faster.  I
 *         am not sure that the performance increase warrants the
 *         complexity (and time it took to write), but I am leaving it
 *         in for the competition.  It is derived from the original
 *         union by replacing the split_lt(gt) operations with a lazy
 *         version. The `obvious' version is called old_union.
 *
 *     4.  Most time is spent in T', the rebalancing constructor.  If my
 *         understanding of the output of *<file> in the sml batch
 *         compiler is correct then the code produced by NJSML 0.75
 *         (sparc) for the final case is very disappointing.  Most
 *         invocations fall through to this case and most of these cases
 *         fall to the else part, i.e. the plain contructor,
 *         T(v,ln+rn+1,l,r).  The poor code allocates a 16 word vector
 *         and saves lots of registers into it.  In the common case it
 *         then retrieves a few of the registers and allocates the 5
 *         word T node.  The values that it retrieves were live in
 *         registers before the massive save.
 *
 *   Modified to functor to support general ordered values
 *)

functor BinarySetFn (K : ORD_KEY) : ORD_SET =
  struct

    structure Key = K

    type item = K.ord_key

    datatype set
      = E 
      | T of {
	  elt : item, 
          cnt : int, 
          left : set,
          right : set
	}

    fun numItems E = 0
      | numItems (T{cnt,...}) = cnt
        
    fun isEmpty E = true
      | isEmpty _ = false

    fun mkT(v,n,l,r) = T{elt=v,cnt=n,left=l,right=r}

      (* N(v,l,r) = T(v,1+numItems(l)+numItems(r),l,r) *)
    fun N(v,E,E) = mkT(v,1,E,E)
      | N(v,E,r as T{cnt=n,...}) = mkT(v,n+1,E,r)
      | N(v,l as T{cnt=n,...}, E) = mkT(v,n+1,l,E)
      | N(v,l as T{cnt=n,...}, r as T{cnt=m,...}) = mkT(v,n+m+1,l,r)

    fun single_L (a,x,T{elt=b,left=y,right=z,...}) = N(b,N(a,x,y),z)
      | single_L _ = raise Match
    fun single_R (b,T{elt=a,left=x,right=y,...},z) = N(a,x,N(b,y,z))
      | single_R _ = raise Match
    fun double_L (a,w,T{elt=c,left=T{elt=b,left=x,right=y,...},right=z,...}) =
          N(b,N(a,w,x),N(c,y,z))
      | double_L _ = raise Match
    fun double_R (c,T{elt=a,left=w,right=T{elt=b,left=x,right=y,...},...},z) =
          N(b,N(a,w,x),N(c,y,z))
      | double_R _ = raise Match

    (*
    **  val weight = 3
    **  fun wt i = weight * i
    *)
    fun wt (i : int) = i + i + i

    fun T' (v,E,E) = mkT(v,1,E,E)
      | T' (v,E,r as T{left=E,right=E,...}) = mkT(v,2,E,r)
      | T' (v,l as T{left=E,right=E,...},E) = mkT(v,2,l,E)

      | T' (p as (_,E,T{left=T _,right=E,...})) = double_L p
      | T' (p as (_,T{left=E,right=T _,...},E)) = double_R p

        (* these cases almost never happen with small weight*)
      | T' (p as (_,E,T{left=T{cnt=ln,...},right=T{cnt=rn,...},...})) =
            if ln<rn then single_L p else double_L p
      | T' (p as (_,T{left=T{cnt=ln,...},right=T{cnt=rn,...},...},E)) =
            if ln>rn then single_R p else double_R p

      | T' (p as (_,E,T{left=E,...})) = single_L p
      | T' (p as (_,T{right=E,...},E)) = single_R p

      | T' (p as (v,l as T{elt=lv,cnt=ln,left=ll,right=lr},
              r as T{elt=rv,cnt=rn,left=rl,right=rr})) =
          if rn >= wt ln (*right is too big*)
            then
              let val rln = numItems rl
                  val rrn = numItems rr
              in
                if rln < rrn then single_L p else double_L p
              end
          else if ln >= wt rn (*left is too big*)
            then
              let val lln = numItems ll
                  val lrn = numItems lr
              in
                if lrn < lln then single_R p else double_R p
              end
          else mkT(v,ln+rn+1,l,r)

    fun add (E,x) = mkT(x,1,E,E)
      | add (set as T{elt=v,left=l,right=r,cnt},x) =
          case K.compare(x,v) of
            LESS => T'(v,add(l,x),r)
          | GREATER => T'(v,l,add(r,x))
          | EQUAL => mkT(x,cnt,l,r)
    fun add' (s, x) = add(x, s)

    fun concat3 (E,v,r) = add(r,v)
      | concat3 (l,v,E) = add(l,v)
      | concat3 (l as T{elt=v1,cnt=n1,left=l1,right=r1}, v, 
                  r as T{elt=v2,cnt=n2,left=l2,right=r2}) =
        if wt n1 < n2 then T'(v2,concat3(l,v,l2),r2)
        else if wt n2 < n1 then T'(v1,l1,concat3(r1,v,r))
        else N(v,l,r)

    fun split_lt (E,x) = E
      | split_lt (T{elt=v,left=l,right=r,...},x) =
          case K.compare(v,x) of
            GREATER => split_lt(l,x)
          | LESS => concat3(l,v,split_lt(r,x))
          | _ => l

    fun split_gt (E,x) = E
      | split_gt (T{elt=v,left=l,right=r,...},x) =
          case K.compare(v,x) of
            LESS => split_gt(r,x)
          | GREATER => concat3(split_gt(l,x),v,r)
          | _ => r

    fun min (T{elt=v,left=E,...}) = v
      | min (T{left=l,...}) = min l
      | min _ = raise Match
        
    fun delmin (T{left=E,right=r,...}) = r
      | delmin (T{elt=v,left=l,right=r,...}) = T'(v,delmin l,r)
      | delmin _ = raise Match

    fun delete' (E,r) = r
      | delete' (l,E) = l
      | delete' (l,r) = T'(min r,l,delmin r)

    fun concat (E, s) = s
      | concat (s, E) = s
      | concat (t1 as T{elt=v1,cnt=n1,left=l1,right=r1}, 
                  t2 as T{elt=v2,cnt=n2,left=l2,right=r2}) =
          if wt n1 < n2 then T'(v2,concat(t1,l2),r2)
          else if wt n2 < n1 then T'(v1,l1,concat(r1,t2))
          else T'(min t2,t1, delmin t2)


    local
      fun trim (lo,hi,E) = E
        | trim (lo,hi,s as T{elt=v,left=l,right=r,...}) =
            if K.compare(v,lo) = GREATER
              then if K.compare(v,hi) = LESS then s else trim(lo,hi,l)
              else trim(lo,hi,r)
                
      fun uni_bd (s,E,_,_) = s
        | uni_bd (E,T{elt=v,left=l,right=r,...},lo,hi) = 
             concat3(split_gt(l,lo),v,split_lt(r,hi))
        | uni_bd (T{elt=v,left=l1,right=r1,...}, 
                   s2 as T{elt=v2,left=l2,right=r2,...},lo,hi) =
            concat3(uni_bd(l1,trim(lo,v,s2),lo,v),
                v, 
                uni_bd(r1,trim(v,hi,s2),v,hi))
              (* inv:  lo < v < hi *)

        (* all the other versions of uni and trim are
         * specializations of the above two functions with
         *     lo=-infinity and/or hi=+infinity 
         *)

      fun trim_lo (_, E) = E
        | trim_lo (lo,s as T{elt=v,right=r,...}) =
            case K.compare(v,lo) of
              GREATER => s
            | _ => trim_lo(lo,r)

      fun trim_hi (_, E) = E
        | trim_hi (hi,s as T{elt=v,left=l,...}) =
            case K.compare(v,hi) of
              LESS => s
            | _ => trim_hi(hi,l)
                
      fun uni_hi (s,E,_) = s
        | uni_hi (E,T{elt=v,left=l,right=r,...},hi) = 
             concat3(l,v,split_lt(r,hi))
        | uni_hi (T{elt=v,left=l1,right=r1,...}, 
                   s2 as T{elt=v2,left=l2,right=r2,...},hi) =
            concat3(uni_hi(l1,trim_hi(v,s2),v),v,uni_bd(r1,trim(v,hi,s2),v,hi))

      fun uni_lo (s,E,_) = s
        | uni_lo (E,T{elt=v,left=l,right=r,...},lo) = 
             concat3(split_gt(l,lo),v,r)
        | uni_lo (T{elt=v,left=l1,right=r1,...}, 
                   s2 as T{elt=v2,left=l2,right=r2,...},lo) =
            concat3(uni_bd(l1,trim(lo,v,s2),lo,v),v,uni_lo(r1,trim_lo(v,s2),v))

      fun uni (s,E) = s
        | uni (E,s) = s
        | uni (T{elt=v,left=l1,right=r1,...}, 
                s2 as T{elt=v2,left=l2,right=r2,...}) =
            concat3(uni_hi(l1,trim_hi(v,s2),v), v, uni_lo(r1,trim_lo(v,s2),v))

    in
      val hedge_union = uni
    end

      (* The old_union version is about 20% slower than
       *  hedge_union in most cases 
       *)
    fun old_union (E,s2)  = s2
      | old_union (s1,E)  = s1
      | old_union (T{elt=v,left=l,right=r,...},s2) = 
          let val l2 = split_lt(s2,v)
              val r2 = split_gt(s2,v)
          in
            concat3(old_union(l,l2),v,old_union(r,r2))
          end

    val empty = E
    fun singleton x = T{elt=x,cnt=1,left=E,right=E}

    fun addList (s,l) = List.foldl (fn (i,s) => add(s,i)) s l

    val add = add

    fun member (set, x) = let
	  fun pk E = false
	    | pk (T{elt=v, left=l, right=r, ...}) = (
		case K.compare(x,v)
		 of LESS => pk l
		  | EQUAL => true
		  | GREATER => pk r
		(* end case *))
	  in
	    pk set
	  end

    local
        (* true if every item in t is in t' *)
      fun treeIn (t,t') = let
            fun isIn E = true
              | isIn (T{elt,left=E,right=E,...}) = member(t',elt)
              | isIn (T{elt,left,right=E,...}) = 
                  member(t',elt) andalso isIn left
              | isIn (T{elt,left=E,right,...}) = 
                  member(t',elt) andalso isIn right
              | isIn (T{elt,left,right,...}) = 
                  member(t',elt) andalso isIn left andalso isIn right
            in
              isIn t
            end
    in
    fun isSubset (E,_) = true
      | isSubset (_,E) = false
      | isSubset (t as T{cnt=n,...},t' as T{cnt=n',...}) =
          (n<=n') andalso treeIn (t,t')

    fun equal (E,E) = true
      | equal (t as T{cnt=n,...},t' as T{cnt=n',...}) =
          (n=n') andalso treeIn (t,t')
      | equal _ = false
    end

    local
      fun next ((t as T{right, ...})::rest) = (t, left(right, rest))
	| next _ = (E, [])
      and left (E, rest) = rest
	| left (t as T{left=l, ...}, rest) = left(l, t::rest)
    in
    fun compare (s1, s2) = let
	  fun cmp (t1, t2) = (case (next t1, next t2)
		 of ((E, _), (E, _)) => EQUAL
		  | ((E, _), _) => LESS
		  | (_, (E, _)) => GREATER
		  | ((T{elt=e1, ...}, r1), (T{elt=e2, ...}, r2)) => (
		      case Key.compare(e1, e2)
		       of EQUAL => cmp (r1, r2)
			| order => order
		      (* end case *))
		(* end case *))
	  in
	    cmp (left(s1, []), left(s2, []))
	  end
    end

    fun delete (E,x) = raise LibBase.NotFound
      | delete (set as T{elt=v,left=l,right=r,...},x) =
          case K.compare(x,v) of
            LESS => T'(v,delete(l,x),r)
          | GREATER => T'(v,l,delete(r,x))
          | _ => delete'(l,r)

    val union = hedge_union

    fun intersection (E, _) = E
      | intersection (_, E) = E
      | intersection (s, T{elt=v,left=l,right=r,...}) = let
	  val l2 = split_lt(s,v)
	  val r2 = split_gt(s,v)
          in
            if member(s,v)
	      then concat3(intersection(l2,l),v,intersection(r2,r))
	      else concat(intersection(l2,l),intersection(r2,r))
          end

    fun difference (E,s) = E
      | difference (s,E)  = s
      | difference (s, T{elt=v,left=l,right=r,...}) =
          let val l2 = split_lt(s,v)
              val r2 = split_gt(s,v)
          in
            concat(difference(l2,l),difference(r2,r))
          end

    fun map f set = let
	  fun map'(acc, E) = acc
	    | map'(acc, T{elt,left,right,...}) =
		map' (add (map' (acc, left), f elt), right)
	  in 
	    map' (E, set)
	  end

    fun app apf =
         let fun apply E = ()
               | apply (T{elt,left,right,...}) = 
                   (apply left;apf elt; apply right)
         in
           apply
         end

    fun foldl f b set = let
	  fun foldf (E, b) = b
	    | foldf (T{elt,left,right,...}, b) = 
		foldf (right, f(elt, foldf (left, b)))
          in
            foldf (set, b)
          end

    fun foldr f b set = let
	  fun foldf (E, b) = b
	    | foldf (T{elt,left,right,...}, b) = 
		foldf (left, f(elt, foldf (right, b)))
          in
            foldf (set, b)
          end

    fun listItems set = foldr (op::) [] set

    fun filter pred set =
	  foldl (fn (item, s) => if (pred item) then add(s, item) else s)
	    empty set

    fun find p E = NONE
      | find p (T{elt,left,right,...}) = (case find p left
	   of NONE => if (p elt)
		then SOME elt
		else find p right
	    | a => a
	  (* end case *))

    fun exists p E = false
      | exists p (T{elt, left, right,...}) =
	  (exists p left) orelse (p elt) orelse (exists p right)

  end (* BinarySetFn *)
;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML finite sets
 *
 * Definition, Section 4.2
 *
 * Note:
 *     This functor just extends the one available in the SML/NJ lib.
 *     Actually, the operation added here would be general purpose and useful enough
 *     to be in the lib. Also see FinMapFn.
 *)

functor FinSetFn(Key : ORD_KEY) :>
FIN_SET where type Key.ord_key = Key.ord_key =
struct
    structure BinarySet	= BinarySetFn(Key)
    open BinarySet

    exception NotFound	= LibBase.NotFound

    fun fromList xs	= addList(empty, xs)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML type names and sets and maps thereof
 *
 * Definition, Sections 4.1, 4.2, 5.1 and 5.2
 *
 * Notes:
 * - Equality is not a boolean attribute. We distinguish a 3rd kind of special
 *   type names which have equality regardless of the types applied. This
 *   implements ref, array, and equivalent types.
 * - For easy checking of pattern exhaustiveness we add an attribute
 *   `span' counting the number of constructors of the type.
 * - For checking of declaration orders etc we provide access to a time stamp.
 *)

structure TyName :> TYNAME =
struct
    (* Type [Section 4.1] *)

    type TyName =				      (* [t] *)
	 { tycon :	string
	 , stamp :	Stamp.stamp
	 , arity :	int
	 , equality :	bool
	 , span :	int
	 }


    (* Creation *)

    fun tyname(tycon, arity, equality, span) =
	{ tycon    = tycon
	, stamp    = Stamp.stamp()
	, arity    = arity
	, equality = equality
	, span     = span
	}

    fun invent(arity, equality) =
	tyname("_id" ^ Stamp.toString(Stamp.stamp()), arity, equality, 0)


    (* Creation from existing *)

    fun rename {tycon, stamp, arity, equality, span} =
	    tyname(tycon, arity, equality, span)

    fun removeEquality {tycon, stamp, arity, equality, span} =
	    tyname(tycon, arity, false, span)

    fun Abs {tycon, stamp, arity, equality, span} =
	    tyname(tycon, arity, false, 0)


    (* Attributes [Section 4.1] *)

    fun arity{tycon, stamp, arity, equality, span} = arity
    fun time {tycon, stamp, arity, equality, span} = stamp
    fun span {tycon, stamp, arity, equality, span} = span
    fun toString{tycon, stamp, arity, equality, span} = tycon
    fun admitsEquality{tycon, stamp, arity, equality, span} = equality


    (* Ordering *)

    fun compare(t1 : TyName, t2 : TyName) = Stamp.compare(#stamp t1, #stamp t2)
end

structure TyNameSet = FinSetFn(type ord_key = TyName.TyName
			       val  compare = TyName.compare)
structure TyNameMap = FinMapFn(type ord_key = TyName.TyName
			       val  compare = TyName.compare);
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML special constants
 *
 * Definition, Section 2.2
 *)

signature SCON =
sig
    (* Import *)

    type TyName = TyName.TyName

    (* Type [Section 2.2] *)

    datatype base = DEC | HEX

    datatype SCon =				(* [scon] *)
	  INT    of base * string * TyName option ref
	| WORD   of base * string * TyName option ref
	| STRING of string * TyName option ref
	| CHAR   of string * TyName option ref
	| REAL   of string * TyName option ref

    (* Operations *)

    val toString :	SCon -> string
    val tyname :	SCon -> TyName option
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML special constants
 *
 * Definition, Section 2.2
 *)

structure SCon :> SCON =
struct
    (* Import *)

    type TyName = TyName.TyName

    (* Type [Section 2.2] *)

    datatype base = DEC | HEX

    datatype SCon =				(* [scon] *)
	  INT    of base * string * TyName option ref
	| WORD   of base * string * TyName option ref
	| STRING of string * TyName option ref
	| CHAR   of string * TyName option ref
	| REAL   of string * TyName option ref


    (* Conversions *)

    fun toString(INT(base, s, _))  = if base = DEC then s else "0x" ^ s
      | toString(WORD(base, s, _)) = (if base = DEC then "0w" else "0wx") ^ s
      | toString(STRING(s, _))     = "\""  ^ s ^ "\""
      | toString(CHAR(s, _))       = "#\"" ^ s ^ "\""
      | toString(REAL(s, _))       = s

    fun tyname(INT(_, _, r))       = !r
      | tyname(WORD(_, _, r))      = !r
      | tyname(STRING(_, r))       = !r
      | tyname(CHAR(_, r))         = !r
      | tyname(REAL(_, r))         = !r
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML label identifiers
 *
 * Definition, Section 2.4
 *)

signature LAB =
sig
    (* Type [Section 2.4] *)

    eqtype Lab					(* [lab] *)

    (* Operations *)

    val fromString :	string -> Lab
    val fromInt :	int    -> Lab
    val toString :	Lab    -> string

    val compare :	Lab * Lab -> order
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML label identifiers and maps thereof
 *
 * Definition, Sections 2.4 and 4.2
 *)

structure Lab :> LAB =
struct
    (* Type [Section 2.4] *)

    type Lab = string					(* [lab] *)


    (* Conversions *)

    fun fromString s = s
    val fromInt      = Int.toString
    fun toString s   = s


    (* Ordering *)

    fun compare(lab1,lab2) =
	(case (Int.fromString lab1, Int.fromString lab2)
	   of (SOME i1, SOME i2) => Int.compare(i1,i2)
	    |     _              => String.compare(lab1,lab2)
	) handle Overflow => String.compare(lab1,lab2)
end

structure LabSet = FinSetFn(type ord_key = Lab.Lab
			    val  compare = Lab.compare);
structure LabMap = FinMapFn(type ord_key = Lab.Lab
			    val  compare = Lab.compare);
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML identifiers
 *
 * Definition, Section 2.4
 *
 * Note:
 *   This is a generic signature to represent all kinds of identifiers (except
 *   for labels and tyvars).
 *)

signature ID =
sig
    (* Type [Section 2.4] *)

    eqtype Id					(* [id] *)

    (* Operations *)

    val invent :	unit -> Id

    val fromString :	string -> Id
    val toString :	Id -> string

    val compare :	Id * Id -> order
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML identifiers
 *
 * Definition, Section 2.4
 *
 * Note:
 *   This is a generic functor to represent all kinds of identifiers (except
 *   labels and tyvars).
 *)


functor IdFn() :> ID =
struct
    (* Type [Section 2.4] *)

    type Id = string				(* [id] *)


    (* Creation *)

    fun invent()     = "_id" ^ Stamp.toString(Stamp.stamp())

    fun fromString s = s
    fun toString s   = s


    (* Ordering *)

    val compare = String.compare
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML long identifiers
 *
 * Definition, Section 2.4
 *
 * Note:
 *   This is a generic signature to represent all kinds of long identifiers.
 *)

signature LONGID =
sig
    (* Import *)

    structure Id :    ID
    structure StrId : ID

    type Id    = Id.Id
    type StrId = StrId.Id


    (* Type [Section 2.4] *)

    eqtype longId				(* [longid] *)


    (* Operations *)

    val invent :	unit   -> longId
    val fromId :	Id     -> longId
    val toId :		longId -> Id
    val toString :	longId -> string

    val strengthen :	StrId * longId -> longId
    val implode :	StrId list * Id -> longId
    val explode :	longId -> StrId list * Id

    val isUnqualified :	longId -> bool

    val compare :	longId * longId -> order
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML long identifiers
 *
 * Definition, Section 2.4
 *
 * Note:
 *   This is a generic functor that generates a long identifier type from a
 *   given identifier type and the StrId type.
 *)

functor LongIdFn(structure Id :    ID
		 structure StrId : ID
		) :> LONGID where type Id.Id    = Id.Id
			    and   type StrId.Id = StrId.Id
		  =
struct

    (* Import *)

    structure Id    = Id
    structure StrId = StrId

    type Id         = Id.Id
    type StrId      = StrId.Id


    (* Type [Section 2.4] *)

    type longId	= StrId list * Id			(* [longid] *)


    (* Conversions *)

    fun toId(strid, id) = id
    fun fromId id       = ([],id)
    fun invent()        = ([],Id.invent())

    fun toString(strids, id) =
	let
	    fun prefix   []     = Id.toString id
	      | prefix(id::ids) = StrId.toString id ^ "." ^ prefix ids
	in
	    prefix strids
	end

    fun strengthen(strid, (strids, id)) = (strid::strids, id)

    fun implode longid = longid
    fun explode longid = longid

    fun isUnqualified (strids,id) = List.null strids


    (* Ordering *)

    fun compare(longid1, longid2) =
	    String.compare(toString longid1, toString longid2)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML overloading classes
 *
 * Definition, Appendix E
 *
 * Note:
 *  Overloading -- and defaulting in particular -- is not well formalised in
 *  the Definition. We treat overloaded constants and identifiers
 *  uniformingly and tried to smoothly integrate overloading resolution into
 *  type inference by generalising the concept of overloading class a bit.
 *  We describe an overloading class as a pair (T,t) of a set
 *  of type names (like the definition does), plus the default type name t.
 *  For overloading to be sound some well-formedness properties have to be
 *  enforced for all existing overloading classes (T,t):
 *  (1) t elem T
 *  (2) Eq T = 0  \/  t admits equality
 *  (3) forall (T',t') . (TT' = 0  \/  |{t,t'} intersect TT'| = 1)
 *  where Eq T = {t elem T | t admits equality} and we write TT' for
 *  T intersect T' and 0 for the empty set.
 *  The reason for (1) is obvious. (2) guarantees that we do not loose the
 *  default if we enforce equality. (3) ensures a unique default whenever we
 *  have to unify two overloading classes. (2) and (3) also allow the
 *  resulting set to become empty which represents a type error.
 *)

signature OVERLOADINGCLASS =
sig
    (* Import *)

    type TyName    = TyName.TyName
    type TyNameSet = TyNameSet.set


    (* Type *)

    type OverloadingClass				(* [O] *)


    (* Operations *)

    val make :		TyNameSet * TyName -> OverloadingClass

    val isEmpty :	OverloadingClass -> bool
    val set :		OverloadingClass -> TyNameSet
    val member :	OverloadingClass * TyName -> bool
    val default :	OverloadingClass -> TyName

    val makeEquality :	OverloadingClass -> OverloadingClass option
    val intersection :	OverloadingClass * OverloadingClass ->
					   OverloadingClass option
    val union :		OverloadingClass * OverloadingClass ->
					   OverloadingClass
end;
(*
 * (c) Andreas Rossberg 2001-2007
 *
 * Standard ML overloading classes
 *
 * Definition, Appendix E
 *
 * Note:
 *  Overloading -- and defaulting in particular -- is not well formalised in
 *  the Definition. We treat overloaded constants and identifiers
 *  uniformingly and tried to smoothly integrate overloading resolution into
 *  type inference by generalising the concept of overloading class a bit.
 *  We describe an overloading class as a pair (T,t) of a set
 *  of type names (like the definition does), plus the default type name t.
 *  For overloading to be sound some well-formedness properties have to be
 *  enforced for all existing overloading classes (T,t):
 *  (1) t elem T
 *  (2) Eq T = 0  \/  t admits equality
 *  (3) forall (T',t') . (TT' = 0  \/  |{t,t'} intersect TT'| = 1)
 *  where Eq T = {t elem T | t admits equality} and we write TT' for
 *  T intersect T' and 0 for the empty set.
 *  The reason for (1) is obvious. (2) guarantees that we do not loose the
 *  default if we enforce equality. (3) ensures a unique default whenever we
 *  have to unify two overloading classes. (2) and (3) also allow the
 *  resulting set to become empty which represents a type error.
 *)

structure OverloadingClass :> OVERLOADINGCLASS =
struct
    (* Import types *)

    type TyName    = TyName.TyName
    type TyNameSet = TyNameSet.set


    (* Type *)

    type OverloadingClass = TyNameSet * TyName		(* [O] *)


    (* Simple operations *)

    fun make O            = O
    fun isEmpty (T,t)     = TyNameSet.isEmpty T
    fun set (T,t)         = T
    fun default (T,t)     = t
    fun member((T,t), t') = TyNameSet.member(T, t')


    (* Filter equality types *)

    fun makeEquality (T,t) =
	let
	    val T' = TyNameSet.filter TyName.admitsEquality T
	in
	    if TyNameSet.isEmpty T' then
		NONE
	    else if TyName.admitsEquality t then
		SOME (T',t)
	    else
		raise Fail "OverloadingClass.makeEquality: \
			   \inconsistent overloading class"
	end


    (* Intersection and union *)

    fun union((T1,t1), (T2,t2)) = ( TyNameSet.union(T1,T2), t2 )

    fun intersection((T1,t1), (T2,t2)) =
	let
	    val T' = TyNameSet.intersection(T1,T2)
	in
	    if TyNameSet.isEmpty T' then
		NONE
	    else if t1 = t2 then
		SOME (T',t1)
	    else case (TyNameSet.member(T',t1), TyNameSet.member(T',t2))
	      of (true, false) => SOME (T',t1)
	       | (false, true) => SOME (T',t2)
	       | _ => raise Fail "OverloadingClass.intersection: \
				 \inconsistent overloading classes"
	end
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML type variables
 *
 * Definition, Sections 2.4 and 4.1
 *)

signature TYVAR =
sig
    (* Import types *)

    type OverloadingClass = OverloadingClass.OverloadingClass

    (* Type [Sections 2.4 and 4.1]*)

    eqtype TyVar			(* [alpha] or [tyvar] *)

    (* Operations *)

    val invent :		bool -> TyVar
    val fromInt :		bool -> int -> TyVar
    val fromString :		string -> TyVar
    val fromOverloadingClass :	string * OverloadingClass -> TyVar
    val toString :		TyVar -> string

    val admitsEquality :	TyVar -> bool
    val overloadingClass :	TyVar -> OverloadingClass option

    val compare :		TyVar * TyVar -> order
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML type variables and sets and maps thereof
 *
 * Definition, Sections 2.4, 4.1, and 4.2
 *
 * Note:
 *  - Tvars can be annotated with an overloading class, to represent the
 *    type schemes of overloaded identifiers.
 *  - We wrap the overloading class into a reference so that TyVar is an eqtype.
 *  - Internally generated tyvars get names _'xxx, where xxx is a stamp number.
 *  - Tyvars generated from integers are mapped to 'a,'b,..,'z,'a1,'b1,..,'z1,
 *    'a2,...
 *)

structure TyVar :> TYVAR =
struct
    (* Import types *)

    type OverloadingClass = OverloadingClass.OverloadingClass

    (* Type [Sections 2.4 and 4.1]*)

    type TyVar = { name :        string		(* [alpha] or [tyvar] *)
		 , equality :    bool
		 , overloading : OverloadingClass ref option
		 }


    (* Creation *)

    fun invent equality =
	{ name = "_" ^ (if equality then "''" else "'") ^
		 Stamp.toString(Stamp.stamp())
	, equality = equality
	, overloading = NONE
	}

    fun fromInt equality n =
	let
	    val c    = String.str(Char.chr(Char.ord #"a" + n mod 26))
	    val i    = n div 26
	    val name = (if equality then "''" else "'") ^
		       (if i = 0 then c else c ^ Int.toString i)
	in
	    {name = name, equality = equality, overloading = NONE}
	end

    fun fromString s =
    	{ name        = s
    	, equality    = String.size(s) > 1 andalso String.sub(s,1) = #"'"
	, overloading = NONE
	}

    fun fromOverloadingClass(s, O) =
    	{ name        = s
    	, equality    = false
	, overloading = SOME(ref O)
	}


    (* Attributes [Section 4.1] *)

    fun toString {name, equality, overloading} = name

    fun admitsEquality {name, equality, overloading} = equality

    fun overloadingClass {name, equality, overloading} =
	Option.map op! overloading


    (* Ordering *)

    fun compare(alpha1 : TyVar, alpha2 : TyVar) =
	String.compare(#name alpha1, #name alpha2)
end

structure TyVarSet = FinSetFn(type ord_key = TyVar.TyVar
			      val  compare = TyVar.compare)
structure TyVarMap = FinMapFn(type ord_key = TyVar.TyVar
			      val  compare = TyVar.compare);
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML identifiers for the core and maps thereof
 *
 * Definition, Section 2.4
 *)

structure VId		= IdFn()
structure TyCon		= IdFn()
structure StrId		= IdFn()

structure LongVId	= LongIdFn(structure Id    = VId
				   structure StrId = StrId)
structure LongTyCon	= LongIdFn(structure Id    = TyCon
				   structure StrId = StrId)
structure LongStrId	= LongIdFn(structure Id    = StrId
				   structure StrId = StrId)

structure VIdMap	= FinMapFn(type ord_key = VId.Id
				   val  compare = VId.compare)
structure TyConMap	= FinMapFn(type ord_key = TyCon.Id
				   val  compare = TyCon.compare)
structure StrIdMap	= FinMapFn(type ord_key = StrId.Id
				   val  compare = StrId.compare)

structure IdsCore =
struct
    type VId		= VId.Id
    type TyVar		= TyVar.TyVar
    type TyCon		= TyCon.Id
    type Lab		= Lab.Lab
    type StrId		= StrId.Id

    type longVId	= LongVId.longId
    type longTyCon	= LongTyCon.longId
    type longStrId	= LongStrId.longId

    type 'a VIdMap	= 'a VIdMap.map
    type 'a TyVarMap	= 'a TyVarMap.map
    type 'a TyConMap	= 'a TyConMap.map
    type 'a LabMap	= 'a LabMap.map
    type 'a StrIdMap	= 'a StrIdMap.map
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML abstract core grammar
 *
 * Definition, Section 2.8
 *
 * Note:
 *   This is the syntax used in the inference rules for the core [Definition,
 *   Sections 4.10 and 6.7]. It omits almost anything having to do with infixed
 *   identifiers:
 *     - fixity directives
 *     - infixed application
 *     - infixed value construction
 *   However, op prefixes are kept, since they are required for rebuilding the
 *   syntax tree during fixity resolution.
 *   Optional semicolons are also omitted.
 *)

signature GRAMMAR_CORE =
sig
    (* Import *)

    type Info

    type SCon		= SCon.SCon
    type Lab		= Lab.Lab
    type VId		= VId.Id
    type TyCon		= TyCon.Id
    type TyVar		= TyVar.TyVar
    type StrId		= StrId.Id
    type longVId	= LongVId.longId
    type longTyCon	= LongTyCon.longId
    type longStrId	= LongStrId.longId


    (* Optional keyword `op' *)

    datatype Op = SANSOp | WITHOp


    (* Expressions [Figures 2 and 4] *)

    datatype AtExp =
	  SCONAtExp      of Info * SCon
	| IDAtExp        of Info * Op * longVId
	| RECORDAtExp    of Info * ExpRow option
	| LETAtExp       of Info * Dec * Exp
	| PARAtExp       of Info * Exp

    and ExpRow =
	  ExpRow         of Info * Lab * Exp * ExpRow option

    and Exp =
	  ATExp          of Info * AtExp
	| APPExp         of Info * Exp * AtExp
	| COLONExp       of Info * Exp * Ty
	| HANDLEExp      of Info * Exp * Match
	| RAISEExp       of Info * Exp
	| FNExp          of Info * Match

    (* Matches [Figures 2 and 4] *)

    and Match =
	  Match          of Info * Mrule * Match option

    and Mrule =
	  Mrule          of Info * Pat * Exp

    (* Declarations [Figures 2 and 4] *)

    and Dec =
	  VALDec         of Info * TyVarseq * ValBind
	| TYPEDec        of Info * TypBind
	| DATATYPEDec    of Info * DatBind
	| DATATYPE2Dec   of Info * TyCon * longTyCon
	| ABSTYPEDec     of Info * DatBind * Dec
	| EXCEPTIONDec   of Info * ExBind
	| LOCALDec       of Info * Dec * Dec
	| OPENDec        of Info * longStrId list
	| EMPTYDec       of Info
	| SEQDec         of Info * Dec * Dec

    (* Bindings [Figures 2 and 4] *)

    and ValBind =
	  PLAINValBind   of Info * Pat * Exp * ValBind option
	| RECValBind     of Info * ValBind

    and TypBind =
	  TypBind        of Info * TyVarseq * TyCon * Ty * TypBind option

    and DatBind =
	  DatBind        of Info * TyVarseq * TyCon * ConBind * DatBind option

    and ConBind =
	  ConBind        of Info * Op * VId * Ty option * ConBind option

    and ExBind =
	  NEWExBind      of Info * Op * VId * Ty option * ExBind option
	| EQUALExBind    of Info * Op * VId * Op * longVId * ExBind option

    (* Patterns [Figures 2 and 3] *)

    and AtPat =
	  WILDCARDAtPat  of Info
	| SCONAtPat      of Info * SCon
	| IDAtPat        of Info * Op * longVId
	| RECORDAtPat    of Info * PatRow option
	| PARAtPat       of Info * Pat

    and PatRow =
	  DOTSPatRow     of Info
	| FIELDPatRow    of Info * Lab * Pat * PatRow option

    and Pat =
	  ATPat          of Info * AtPat
	| CONPat         of Info * Op * longVId * AtPat
	| COLONPat       of Info * Pat * Ty
	| ASPat          of Info * Op * VId * Ty option * Pat

    (* Type expressions [Figures 2 and 3] *)

    and Ty =
	  VARTy          of Info * TyVar
	| RECORDTy       of Info * TyRow option
	| CONTy          of Info * Tyseq * longTyCon
	| ARROWTy        of Info * Ty * Ty
	| PARTy          of Info * Ty

    and TyRow =
	  TyRow          of Info * Lab * Ty * TyRow option

    (* Sequences [Section 2.8] *)

    and Tyseq =
	  Tyseq          of Info * Ty list

    and TyVarseq =
	  TyVarseq       of Info * TyVar list


    (* Operations *)

    val infoAtExp :	AtExp	-> Info
    val infoExpRow :	ExpRow	-> Info
    val infoExp :	Exp	-> Info
    val infoMatch :	Match	-> Info
    val infoMrule :	Mrule	-> Info
    val infoDec :	Dec	-> Info
    val infoValBind :	ValBind	-> Info
    val infoTypBind :	TypBind	-> Info
    val infoDatBind :	DatBind	-> Info
    val infoConBind :	ConBind	-> Info
    val infoExBind :	ExBind	-> Info
    val infoAtPat :	AtPat	-> Info
    val infoPatRow :	PatRow	-> Info
    val infoPat :	Pat	-> Info
    val infoTy :	Ty	-> Info
    val infoTyRow :	TyRow	-> Info
    val infoTyseq :	Tyseq	-> Info
    val infoTyVarseq :	TyVarseq -> Info
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML abstract core grammar
 *
 * Definition, Section 2.8
 *
 * Note:
 *   This is the syntax used in the inference rules for the core [Definition,
 *   Sections 4.10 and 6.7]. It omits almost anything having to do with infixed
 *   identifiers:
 *     - fixity directives
 *     - infixed application
 *     - infixed value construction
 *   However, op prefixes are kept, since they are required for rebuilding the
 *   syntax tree during fixity resolution.
 *   Optional semicolons are also omitted.
 *)

functor GrammarCoreFn(type Info) : GRAMMAR_CORE =
struct
    (* Import *)

    type Info		= Info

    type SCon		= SCon.SCon
    type Lab		= Lab.Lab
    type VId		= VId.Id
    type TyCon		= TyCon.Id
    type TyVar		= TyVar.TyVar
    type StrId		= StrId.Id
    type longVId	= LongVId.longId
    type longTyCon	= LongTyCon.longId
    type longStrId	= LongStrId.longId


    (* Optional keyword `op' *)

    datatype Op = SANSOp | WITHOp


    (* Expressions [Figures 2 and 4] *)

    datatype AtExp =
	  SCONAtExp      of Info * SCon
	| IDAtExp        of Info * Op * longVId
	| RECORDAtExp    of Info * ExpRow option
	| LETAtExp       of Info * Dec * Exp
	| PARAtExp       of Info * Exp

    and ExpRow =
	  ExpRow         of Info * Lab * Exp * ExpRow option

    and Exp =
	  ATExp          of Info * AtExp
	| APPExp         of Info * Exp * AtExp
	| COLONExp       of Info * Exp * Ty
	| HANDLEExp      of Info * Exp * Match
	| RAISEExp       of Info * Exp
	| FNExp          of Info * Match

    (* Matches [Figures 2 and 4] *)

    and Match =
	  Match          of Info * Mrule * Match option

    and Mrule =
	  Mrule          of Info * Pat * Exp

    (* Declarations [Figures 2 and 4] *)

    and Dec =
	  VALDec         of Info * TyVarseq * ValBind
	| TYPEDec        of Info * TypBind
	| DATATYPEDec    of Info * DatBind
	| DATATYPE2Dec   of Info * TyCon * longTyCon
	| ABSTYPEDec     of Info * DatBind * Dec
	| EXCEPTIONDec   of Info * ExBind
	| LOCALDec       of Info * Dec * Dec
	| OPENDec        of Info * longStrId list
	| EMPTYDec       of Info
	| SEQDec         of Info * Dec * Dec

    (* Bindings [Figures 2 and 4] *)

    and ValBind =
	  PLAINValBind   of Info * Pat * Exp * ValBind option
	| RECValBind     of Info * ValBind

    and TypBind =
	  TypBind        of Info * TyVarseq * TyCon * Ty * TypBind option

    and DatBind =
	  DatBind        of Info * TyVarseq * TyCon * ConBind * DatBind option

    and ConBind =
	  ConBind        of Info * Op * VId * Ty option * ConBind option

    and ExBind =
	  NEWExBind      of Info * Op * VId * Ty option * ExBind option
	| EQUALExBind    of Info * Op * VId * Op * longVId * ExBind option

    (* Patterns [Figures 2 and 3] *)

    and AtPat =
	  WILDCARDAtPat  of Info
	| SCONAtPat      of Info * SCon
	| IDAtPat        of Info * Op * longVId
	| RECORDAtPat    of Info * PatRow option
	| PARAtPat       of Info * Pat

    and PatRow =
	  DOTSPatRow     of Info
	| FIELDPatRow    of Info * Lab * Pat * PatRow option

    and Pat =
	  ATPat          of Info * AtPat
	| CONPat         of Info * Op * longVId * AtPat
	| COLONPat       of Info * Pat * Ty
	| ASPat          of Info * Op * VId * Ty option * Pat

    (* Type expressions [Figures 2 and 3] *)

    and Ty =
	  VARTy          of Info * TyVar
	| RECORDTy       of Info * TyRow option
	| CONTy          of Info * Tyseq * longTyCon
	| ARROWTy        of Info * Ty * Ty
	| PARTy          of Info * Ty

    and TyRow =
	  TyRow          of Info * Lab * Ty * TyRow option

    (* Sequences [Section 2.8] *)

    and Tyseq =
	  Tyseq          of Info * Ty list

    and TyVarseq =
	  TyVarseq       of Info * TyVar list



    (* Extracting info fields *)

    fun infoAtExp(SCONAtExp(I,_))		= I
      | infoAtExp(IDAtExp(I,_,_))		= I
      | infoAtExp(RECORDAtExp(I,_))		= I
      | infoAtExp(LETAtExp(I,_,_))		= I
      | infoAtExp(PARAtExp(I,_))		= I

    fun infoExpRow(ExpRow(I,_,_,_))		= I

    fun infoExp(ATExp(I,_))			= I
      | infoExp(APPExp(I,_,_))			= I
      | infoExp(COLONExp(I,_,_))		= I
      | infoExp(HANDLEExp(I,_,_))		= I
      | infoExp(RAISEExp(I,_))			= I
      | infoExp(FNExp(I,_))			= I

    fun infoMatch(Match(I,_,_))			= I

    fun infoMrule(Mrule(I,_,_))			= I

    fun infoDec(VALDec(I,_,_))			= I
      | infoDec(TYPEDec(I,_))			= I
      | infoDec(DATATYPEDec(I,_))		= I
      | infoDec(DATATYPE2Dec(I,_,_))		= I
      | infoDec(ABSTYPEDec(I,_,_))		= I
      | infoDec(EXCEPTIONDec(I,_))		= I
      | infoDec(LOCALDec(I,_,_))		= I
      | infoDec(OPENDec(I,_))			= I
      | infoDec(EMPTYDec(I))			= I
      | infoDec(SEQDec(I,_,_))			= I

    fun infoValBind(PLAINValBind(I,_,_,_))	= I
      | infoValBind(RECValBind(I,_))		= I

    fun infoTypBind(TypBind(I,_,_,_,_))		= I

    fun infoDatBind(DatBind(I,_,_,_,_))		= I

    fun infoConBind(ConBind(I,_,_,_,_))		= I

    fun infoExBind(NEWExBind(I,_,_,_,_))	= I
      | infoExBind(EQUALExBind(I,_,_,_,_,_))	= I

    fun infoAtPat(WILDCARDAtPat(I))		= I
      | infoAtPat(SCONAtPat(I,_))		= I
      | infoAtPat(IDAtPat(I,_,_))		= I
      | infoAtPat(RECORDAtPat(I,_))		= I
      | infoAtPat(PARAtPat(I,_))		= I

    fun infoPatRow(DOTSPatRow(I))		= I
      | infoPatRow(FIELDPatRow(I,_,_,_))	= I

    fun infoPat(ATPat(I,_))			= I
      | infoPat(CONPat(I,_,_,_))		= I
      | infoPat(COLONPat(I,_,_))		= I
      | infoPat(ASPat(I,_,_,_,_))		= I

    fun infoTy(VARTy(I,_))			= I
      | infoTy(RECORDTy(I,_))			= I
      | infoTy(CONTy(I,_,_))			= I
      | infoTy(ARROWTy(I,_,_))			= I
      | infoTy(PARTy(I,_))			= I

    fun infoTyRow(TyRow(I,_,_,_))		= I

    fun infoTyseq(Tyseq(I,_))			= I
    fun infoTyVarseq(TyVarseq(I,_))		= I
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML identifiers for modules and maps thereof
 *
 * Definition, Section 3.2
 *)

structure SigId		= IdFn()
structure FunId		= IdFn()

structure SigIdMap	= FinMapFn(type ord_key = SigId.Id
				   val  compare = SigId.compare)
structure FunIdMap	= FinMapFn(type ord_key = FunId.Id
				   val  compare = FunId.compare)

structure IdsModule =
struct
    type SigId		= SigId.Id
    type FunId		= FunId.Id

    type 'a SigIdMap	= 'a SigIdMap.map
    type 'a FunIdMap	= 'a FunIdMap.map
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML abstract module grammar
 *
 * Definition, Section 3.4
 *
 * Notes:
 *   This is the syntax used in the inference rules for modules [Definition,
 *   Sections 5.7 and 7.3]. Optional semicolons are omitted.
 *   The structure sharing derived form [Definition, Appendix A] has been added,
 *   because it cannot be derived purely syntactically.
 *)

signature GRAMMAR_MODULE =
sig
    (* Import *)

    structure Core : GRAMMAR_CORE

    type Info

    type VId		= Core.VId
    type TyCon		= Core.TyCon
    type TyVar		= Core.TyVar
    type StrId		= Core.StrId
    type longVId	= Core.longVId
    type longTyCon	= Core.longTyCon
    type longStrId	= Core.longStrId
    type Dec		= Core.Dec
    type Ty		= Core.Ty
    type TyVarseq	= Core.TyVarseq

    type SigId		= SigId.Id
    type FunId		= FunId.Id


    (* Structures [Figures 5 and 6] *)

    datatype StrExp =
	  STRUCTStrExp    of Info * StrDec
	| IDStrExp        of Info * longStrId
	| COLONStrExp     of Info * StrExp * SigExp
	| SEALStrExp      of Info * StrExp * SigExp
	| APPStrExp       of Info * FunId * StrExp
	| LETStrExp       of Info * StrDec * StrExp

    and StrDec =
          DECStrDec       of Info * Dec
        | STRUCTUREStrDec of Info * StrBind
        | LOCALStrDec     of Info * StrDec * StrDec
        | EMPTYStrDec     of Info
        | SEQStrDec       of Info * StrDec * StrDec

    and StrBind =
          StrBind         of Info * StrId * StrExp * StrBind option

    (* Signatures [Figures 5 and 6] *)

    and SigExp =
          SIGSigExp       of Info * Spec
        | IDSigExp        of Info * SigId
        | WHERETYPESigExp of Info * SigExp * TyVarseq * longTyCon * Ty

    and SigDec =
          SigDec          of Info * SigBind

    and SigBind =
          SigBind         of Info * SigId * SigExp * SigBind option

    (* Specifications [Figures 5 and 7] *)

    and Spec =
	  VALSpec         of Info * ValDesc
	| TYPESpec        of Info * TypDesc
	| EQTYPESpec      of Info * TypDesc
	| DATATYPESpec    of Info * DatDesc
	| DATATYPE2Spec   of Info * TyCon * longTyCon
	| EXCEPTIONSpec   of Info * ExDesc
	| STRUCTURESpec   of Info * StrDesc
	| INCLUDESpec     of Info * SigExp
	| EMPTYSpec       of Info
	| SEQSpec         of Info * Spec * Spec
	| SHARINGTYPESpec of Info * Spec * longTyCon list
	| SHARINGSpec     of Info * Spec * longStrId list

    and ValDesc =
	  ValDesc         of Info * VId * Ty * ValDesc option

    and TypDesc =
	  TypDesc         of Info * TyVarseq * TyCon * TypDesc option

    and DatDesc =
	  DatDesc         of Info * TyVarseq * TyCon * ConDesc * DatDesc option

    and ConDesc =
	  ConDesc         of Info * VId * Ty option * ConDesc option

    and ExDesc =
	  ExDesc          of Info * VId * Ty option * ExDesc option

    and StrDesc =
          StrDesc         of Info * StrId * SigExp * StrDesc option

    (* Functors [Figures 5 and 8] *)

    and FunDec =
          FunDec          of Info * FunBind

    and FunBind =
          FunBind         of Info * FunId * StrId * SigExp * StrExp
                                  * FunBind option

    (* Top-level declarations [Figures 5 and 8] *)

    and TopDec =
          STRDECTopDec    of Info * StrDec * TopDec option
        | SIGDECTopDec    of Info * SigDec * TopDec option
        | FUNDECTopDec    of Info * FunDec * TopDec option


    (* Operations *)

    val infoStrExp :	StrExp  -> Info
    val infoStrDec :	StrDec  -> Info
    val infoStrBind :	StrBind -> Info
    val infoSigExp :	SigExp  -> Info
    val infoSigDec :	SigDec  -> Info
    val infoSigBind :	SigBind -> Info
    val infoSpec :	Spec    -> Info
    val infoValDesc :	ValDesc -> Info
    val infoTypDesc :	TypDesc -> Info
    val infoDatDesc :	DatDesc -> Info
    val infoConDesc :	ConDesc -> Info
    val infoExDesc :	ExDesc  -> Info
    val infoStrDesc :	StrDesc -> Info
    val infoFunDec :	FunDec  -> Info
    val infoFunBind :	FunBind -> Info
    val infoTopDec :	TopDec  -> Info
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML abstract module grammar
 *
 * Definition, Section 3.4
 *
 * Notes:
 *   This is the syntax used in the inference rules for modules [Definition,
 *   Sections 5.7 and 7.3]. Optional semicolons are omitted.
 *   The structure sharing derived form [Definition, Appendix A] has been added,
 *   because it cannot be derived purely syntactically.
 *)

functor GrammarModuleFn(type Info
			structure Core : GRAMMAR_CORE
		       ) : GRAMMAR_MODULE =
struct
    (* Import *)

    structure Core = Core
    type      Info = Info

    open Core

    type SigId = SigId.Id
    type FunId = FunId.Id


    (* Structures [Figures 5 and 6] *)

    datatype StrExp =
	  STRUCTStrExp    of Info * StrDec
	| IDStrExp        of Info * longStrId
	| COLONStrExp     of Info * StrExp * SigExp
	| SEALStrExp      of Info * StrExp * SigExp
	| APPStrExp       of Info * FunId * StrExp
	| LETStrExp       of Info * StrDec * StrExp

    and StrDec =
          DECStrDec       of Info * Dec
        | STRUCTUREStrDec of Info * StrBind
        | LOCALStrDec     of Info * StrDec * StrDec
        | EMPTYStrDec     of Info
        | SEQStrDec       of Info * StrDec * StrDec

    and StrBind =
          StrBind         of Info * StrId * StrExp * StrBind option

    (* Signatures [Figures 5 and 6] *)

    and SigExp =
          SIGSigExp       of Info * Spec
        | IDSigExp        of Info * SigId
        | WHERETYPESigExp of Info * SigExp * TyVarseq * longTyCon * Ty

    and SigDec =
          SigDec          of Info * SigBind

    and SigBind =
          SigBind         of Info * SigId * SigExp * SigBind option

    (* Specifications [Figures 5 and 7] *)

    and Spec =
	  VALSpec         of Info * ValDesc
	| TYPESpec        of Info * TypDesc
	| EQTYPESpec      of Info * TypDesc
	| DATATYPESpec    of Info * DatDesc
	| DATATYPE2Spec   of Info * TyCon * longTyCon
	| EXCEPTIONSpec   of Info * ExDesc
	| STRUCTURESpec   of Info * StrDesc
	| INCLUDESpec     of Info * SigExp
	| EMPTYSpec       of Info
	| SEQSpec         of Info * Spec * Spec
	| SHARINGTYPESpec of Info * Spec * longTyCon list
	| SHARINGSpec     of Info * Spec * longStrId list

    and ValDesc =
	  ValDesc         of Info * VId * Ty * ValDesc option

    and TypDesc =
	  TypDesc         of Info * TyVarseq * TyCon * TypDesc option

    and DatDesc =
	  DatDesc         of Info * TyVarseq * TyCon * ConDesc * DatDesc option

    and ConDesc =
	  ConDesc         of Info * VId * Ty option * ConDesc option

    and ExDesc =
	  ExDesc          of Info * VId * Ty option * ExDesc option

    and StrDesc =
          StrDesc         of Info * StrId * SigExp * StrDesc option

    (* Functors [Figures 5 and 8] *)

    and FunDec =
          FunDec          of Info * FunBind

    and FunBind =
          FunBind         of Info * FunId * StrId * SigExp * StrExp
                                  * FunBind option

    (* Top-level declarations [Figures 5 and 8] *)

    and TopDec =
          STRDECTopDec    of Info * StrDec * TopDec option
        | SIGDECTopDec    of Info * SigDec * TopDec option
        | FUNDECTopDec    of Info * FunDec * TopDec option


    (* Extracting info fields *)

    fun infoStrExp(STRUCTStrExp(I,_))		= I
      | infoStrExp(IDStrExp(I,_))		= I
      | infoStrExp(COLONStrExp(I,_,_))		= I
      | infoStrExp(SEALStrExp(I,_,_))		= I
      | infoStrExp(APPStrExp(I,_,_))		= I
      | infoStrExp(LETStrExp(I,_,_))		= I

    fun infoStrDec(DECStrDec(I,_))		= I
      | infoStrDec(STRUCTUREStrDec(I,_))	= I
      | infoStrDec(LOCALStrDec(I,_,_))		= I
      | infoStrDec(EMPTYStrDec(I))		= I
      | infoStrDec(SEQStrDec(I,_,_))		= I

    fun infoStrBind(StrBind(I,_,_,_))		= I

    fun infoSigExp(SIGSigExp(I,_))		= I
      | infoSigExp(IDSigExp(I,_))		= I
      | infoSigExp(WHERETYPESigExp(I,_,_,_,_))	= I

    fun infoSigDec(SigDec(I,_))			= I

    fun infoSigBind(SigBind(I,_,_,_))		= I

    fun infoSpec(VALSpec(I,_))			= I
      | infoSpec(TYPESpec(I,_))			= I
      | infoSpec(EQTYPESpec(I,_))		= I
      | infoSpec(DATATYPESpec(I,_))		= I
      | infoSpec(DATATYPE2Spec(I,_,_))		= I
      | infoSpec(EXCEPTIONSpec(I,_))		= I
      | infoSpec(STRUCTURESpec(I,_))		= I
      | infoSpec(INCLUDESpec(I,_))		= I
      | infoSpec(EMPTYSpec(I))			= I
      | infoSpec(SEQSpec(I,_,_))		= I
      | infoSpec(SHARINGTYPESpec(I,_,_))	= I
      | infoSpec(SHARINGSpec(I,_,_))		= I

    fun infoValDesc(ValDesc(I,_,_,_))		= I
    fun infoTypDesc(TypDesc(I,_,_,_))		= I
    fun infoDatDesc(DatDesc(I,_,_,_,_))		= I
    fun infoConDesc(ConDesc(I,_,_,_))		= I
    fun infoExDesc(ExDesc(I,_,_,_))		= I
    fun infoStrDesc(StrDesc(I,_,_,_))		= I

    fun infoFunDec(FunDec(I,_))			= I

    fun infoFunBind(FunBind(I,_,_,_,_,_))	= I

    fun infoTopDec(STRDECTopDec(I,_,_))		= I
      | infoTopDec(SIGDECTopDec(I,_,_))		= I
      | infoTopDec(FUNDECTopDec(I,_,_))		= I
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML abstract program grammar
 *
 * Definition, Section 8
 *)

signature GRAMMAR_PROGRAM =
sig
    (* Import *)

    structure Module : GRAMMAR_MODULE

    type Info	= Module.Info

    type TopDec = Module.TopDec


    (* Programs *)

    datatype Program = Program of Info * TopDec * Program option


    (* Extracting the info field *)

    val infoProgram : Program -> Info
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML abstract program grammar
 *
 * Definition, Section 8
 *)


functor GrammarProgramFn(type Info
			 structure Module : GRAMMAR_MODULE
			) : GRAMMAR_PROGRAM =
struct
    (* Import *)

    structure Module = Module
    type      Info   = Info

    open Module


    (* Programs *)

    datatype Program = Program of Info * TopDec * Program option


    (* Extracting the info field *)

    fun infoProgram(Program(I,_,_)) = I
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML grammar
 *)

structure GrammarCore    = GrammarCoreFn(type Info = Source.info)

structure GrammarModule  = GrammarModuleFn(type Info = Source.info
					   structure Core = GrammarCore)

structure GrammarProgram = GrammarProgramFn(type Info = Source.info
					    structure Module = GrammarModule);
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML infix resolution
 *
 * Definition, Section 2.6
 *)

signature INFIX =
sig
    (* Import *)

    type Info		= GrammarCore.Info

    type Op		= GrammarCore.Op
    type VId		= GrammarCore.VId
    type longVId	= GrammarCore.longVId
    type Exp		= GrammarCore.Exp
    type Pat		= GrammarCore.Pat
    type AtExp		= GrammarCore.AtExp
    type AtPat		= GrammarCore.AtPat


    (* Modifying fixity status *)

    datatype Assoc	= LEFT | RIGHT

    type InfStatus	= Assoc * int
    type InfEnv		= InfStatus VIdMap.map		(* [J] *)

    val empty :		InfEnv
    val assign :	InfEnv * VId list * InfStatus -> InfEnv
    val cancel :	InfEnv * VId list -> InfEnv


    (* Resolving phrases containing infixed identifiers *)

    val parseExp :	InfEnv * AtExp list -> Exp
    val parsePat :	InfEnv * AtPat list -> Pat
    val parseFmrule :	InfEnv * AtPat list -> Op * VId * AtPat list
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Error handling.
 *)

signature ERROR =
sig
    (* Import *)

    type position		= Source.info


    (* Export *)

    exception Error

    val warning :		position * string -> unit
    val error :			position * string -> 'a
    val errorLab :		position * string * IdsCore.Lab -> 'a
    val errorVId :		position * string * IdsCore.VId -> 'a
    val errorTyCon :		position * string * IdsCore.TyCon -> 'a
    val errorTyVar :		position * string * IdsCore.TyVar -> 'a
    val errorStrId :		position * string * IdsCore.StrId -> 'a
    val errorSigId :		position * string * IdsModule.SigId -> 'a
    val errorFunId :		position * string * IdsModule.FunId -> 'a
    val errorLongVId :		position * string * IdsCore.longVId -> 'a
    val errorLongTyCon :	position * string * IdsCore.longTyCon -> 'a
    val errorLongStrId :	position * string * IdsCore.longStrId -> 'a
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Error handling.
 *)

structure Error : ERROR =
struct
    (* Import *)

    type position = Source.info


    (* Export *)

    exception Error

    val dir = OS.FileSys.getDir()

    fun print({file, region = ((line1,col1), (line2,col2))}, message) =
	( case file
	    of NONE      => ()
	     | SOME name => 
	       let
		   val name' = OS.Path.mkRelative{path=name, relativeTo=dir}
	       in
		   TextIO.output(TextIO.stdErr, name' ^ ":")
	       end
	; TextIO.output(TextIO.stdErr, Int.toString line1 ^ ".")
	; TextIO.output(TextIO.stdErr, Int.toString col1 ^ "-")
	; TextIO.output(TextIO.stdErr, Int.toString line2 ^ ".")
	; TextIO.output(TextIO.stdErr, Int.toString col2 ^ ": ")
	; TextIO.output(TextIO.stdErr, message ^ "\n")
	; TextIO.flushOut TextIO.stdErr
	)

    fun warning(pos, message) =
	print(pos, "warning: " ^ message)

    fun error(pos, message) =
	( print(pos, message)
	; raise Error
	)

    fun errorLab  (pos, s, lab)   = error(pos, s ^ Lab.toString lab)
    fun errorVId  (pos, s, vid)   = error(pos, s ^ VId.toString vid)
    fun errorTyCon(pos, s, tycon) = error(pos, s ^ TyCon.toString tycon)
    fun errorTyVar(pos, s, tyvar) = error(pos, s ^ TyVar.toString tyvar)
    fun errorStrId(pos, s, strid) = error(pos, s ^ StrId.toString strid)
    fun errorSigId(pos, s, sigid) = error(pos, s ^ SigId.toString sigid)
    fun errorFunId(pos, s, funid) = error(pos, s ^ FunId.toString funid)

    fun errorLongVId(pos, s, longvid) = error(pos, s ^ LongVId.toString longvid)
    fun errorLongTyCon(pos, s, longtycon) =
	    error(pos, s ^ LongTyCon.toString longtycon)
    fun errorLongStrId(pos, s, longstrid) =
	    error(pos, s ^ LongStrId.toString longstrid)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML infix resolution
 *
 * Definition, Section 2.6
 *)


structure Infix :> INFIX =
struct
    (* Import *)

    open GrammarCore
    open Error


    (* Type definitions *)

    datatype Assoc = LEFT | RIGHT

    type InfStatus = Assoc * int

    type InfEnv    = InfStatus VIdMap.map		(* [J] *)


    (* Modifying infix environments *)

    val empty = VIdMap.empty

    fun assign(J, vids, infstatus) =
	let
	    fun insert(vid, J) = VIdMap.insert(J, vid, infstatus)
	in
	    List.foldl insert J vids
	end

    fun cancel(J, vids) =
	let
	    fun remove(vid, J) = VIdMap.delete(J, vid)
	in
	    List.foldl remove J vids
	end


    (* Categorisation of atomic expressions and patterns *)

    datatype 'a FixityCategory = NONFIX of 'a
			       | INFIX  of InfStatus * VId * Info

    fun isInfix J (longvid) =
	LongVId.isUnqualified longvid andalso
	VIdMap.find(J, LongVId.toId longvid) <> NONE

    fun categoriseLongVId J (atomic, I, longvid) =
	if LongVId.isUnqualified longvid then
	    let
		val vid = LongVId.toId longvid
	    in
		case VIdMap.find(J, vid)
		  of NONE           => NONFIX(atomic)
		   | SOME infstatus => INFIX(infstatus, vid, I)
	    end
	else
	    NONFIX(atomic)

    fun categoriseAtExp J (atexp as IDAtExp(I, SANSOp, longvid)) =
	    categoriseLongVId J (atexp, I, longvid)
      | categoriseAtExp J (atexp) = NONFIX(atexp)

    fun categoriseAtPat J (atpat as IDAtPat(I, SANSOp, longvid)) =
	    categoriseLongVId J (atpat, I, longvid)
      | categoriseAtPat J (atpat) = NONFIX(atpat)



    (* Resolving infixing [Section 2.6] *)

    fun parse(app, infapp, es) =
	let
	    fun loop(NONFIX(e)::[], []) = e

	      | loop(NONFIX(e2)::NONFIX(e1)::s', i) =
		    (* reduce nonfix application *)
		    loop(NONFIX(app(e1, e2))::s', i)

	      | loop(s, NONFIX(e)::i') =
		    (* shift *)
		    loop(NONFIX(e)::s, i')

	      | loop(s as NONFIX(e)::[], INFIX(x)::i') =
		    (* shift *)
		    loop(INFIX(x)::s, i')

	      | loop(NONFIX(e2)::INFIX(_,vid,_)::NONFIX(e1)::s', []) =
		    (* reduce infix application *)
		    loop(NONFIX(infapp(e1, vid, e2))::s', [])

	      | loop(s as NONFIX(e2)::INFIX((a1,p1),vid1,I1)::NONFIX(e1)::s',
		       i as INFIX(x2 as ((a2,p2),vid2,I2))::i') =
		if p1 > p2 then
		    (* reduce infix application *)
		    loop(NONFIX(infapp(e1, vid1, e2))::s', i)
		else if p1 < p2 then
		    (* shift *)
		    loop(INFIX(x2)::s, i')
		else if a1 <> a2 then
		    error(Source.over(I1,I2), "conflicting infix associativity")
		else if a1 = LEFT then
		    (* reduce infix application *)
		    loop(NONFIX(infapp(e1, vid1, e2))::s', i)
		else (* a1 = RIGHT *)
		    (* shift *)
		    loop(INFIX(x2)::s, i')

	      | loop(INFIX(_, vid, I)::s, []) =
		    errorVId(I, "misplaced infix identifier ", vid)

	      | loop(INFIX(x)::s, INFIX(_, vid, I)::i) =
		    errorVId(I, "misplaced infix identifier ", vid)

	      | loop([], INFIX(_, vid, I)::i) =
		    errorVId(I, "misplaced infix identifier ", vid)

	      | loop _ = raise Fail "Infix.parse: inconsistency"
	in
	    loop([], es)
	end


    (* Resolving infixed expressions [Section 2.6] *)

    fun atExp atexp = ATExp(infoAtExp atexp, atexp)

    fun appExp(atexp1, atexp2) =
	let
	    val I1 = infoAtExp atexp1
	    val I2 = infoAtExp atexp2
	    val I  = Source.over(I1, I2)
	in
	    PARAtExp(I, APPExp(I, atExp atexp1, atexp2))
	end

    fun pairExp(atexp1, atexp2) =
	let
	    val I1	= infoAtExp atexp1
	    val I2	= infoAtExp atexp2
	    val lab1	= Lab.fromInt 1
	    val lab2	= Lab.fromInt 2
	    val exprow2	= ExpRow(I2, lab2, atExp atexp2, NONE)
	    val exprow1	= ExpRow(I1, lab1, atExp atexp1, SOME exprow2)
	in
	    RECORDAtExp(Source.over(I1,I2), SOME exprow1)
	end

    fun infExp(atexp1, vid, atexp2) =
	let
	    val Ivid	= Source.between(infoAtExp atexp1, infoAtExp atexp2)
	    val longvid	= LongVId.fromId vid
	    val atexp1'	= IDAtExp(Ivid, SANSOp, longvid)
	    val atexp2'	= pairExp(atexp1, atexp2)
	in
	    appExp(atexp1', atexp2')
	end


    fun parseExp(J, atexps) =
	let
	    val atexp = parse(appExp, infExp,
			      List.map (categoriseAtExp J) atexps)
	in
	    atExp atexp
	end


    (* Resolving infixed patterns [Section 2.6] *)

    fun atPat atpat = ATPat(infoAtPat atpat, atpat)

    fun conPat(IDAtPat(I1, op_opt, longvid), atpat) =
	let
	    val I2 = infoAtPat atpat
	    val I  = Source.over(I1, I2)
	in
	    PARAtPat(I, CONPat(I, op_opt, longvid, atpat))
	end

      | conPat(_, atpat) =
	    error(infoAtPat atpat, "misplaced atomic pattern")

    fun pairPat(atpat1, atpat2) =
	let
	    val I1	= infoAtPat atpat1
	    val I2	= infoAtPat atpat2
	    val lab1	= Lab.fromInt 1
	    val lab2	= Lab.fromInt 2
	    val patrow2	= FIELDPatRow(I2, lab2, atPat atpat2, NONE)
	    val patrow1	= FIELDPatRow(I1, lab1, atPat atpat1, SOME patrow2)
	in
	    RECORDAtPat(Source.over(I1,I2), SOME patrow1)
	end

    fun infPat(atpat1, vid, atpat2) =
	let
	    val Ivid	= Source.between(infoAtPat atpat1, infoAtPat atpat2)
	    val longvid	= LongVId.fromId vid
	    val atpat1'	= IDAtPat(Ivid, SANSOp, longvid)
	    val atpat2'	= pairPat(atpat1, atpat2)
	in
	    conPat(atpat1', atpat2')
	end


    fun parsePat(J, atpats) =
	let
	    val atpat = parse(conPat, infPat,
			      List.map (categoriseAtPat J) atpats)
	in
	    atPat atpat
	end


    (* Resolving fun match rules [Figure 21, note] *)

    fun parseFmrule(J, atpats) =
	(*
	 * Allowed is the following:
	 * (1) <op> vid atpat+
	 * (2) (atpat infix_vid atpat) atpat*
	 * (3) atpat infix_vid atpat
	 *)
	let
	    fun checkNonfixity []           = true
	      | checkNonfixity(NONFIX _::t) = checkNonfixity t
	      | checkNonfixity(INFIX(_, vid, I)::t) =
		    errorVId(I, "misplaced infix identifier ", vid)

	    fun maybeNonfixClause(ps) =
		case List.hd atpats
		  of IDAtPat(I, op_opt, longvid) =>
			if not(LongVId.isUnqualified longvid) then
			    errorLongVId(I, "misplaced long identifier ",
					 longvid)
			else if List.length atpats < 2 then
			    error(I, "missing function arguments")
			else
			    ( checkNonfixity ps	(* including 1st *)
			    ; ( op_opt, LongVId.toId longvid, List.tl atpats )
			    )
		   | WILDCARDAtPat(I) =>
			error(I, "misplaced wildcard pattern")
		   | SCONAtPat(I, _) =>
			error(I, "misplaced constant pattern")
		   | RECORDAtPat(I, _) =>
			error(I, "misplaced record or tuple pattern")
		   | PARAtPat(I, _) =>
			error(I, "misplaced parenthesised pattern")

	    fun maybeParenthesisedInfixClause(ps) =
	        case List.hd ps
		  of NONFIX(PARAtPat(_, CONPat(I, SANSOp, longvid, atpat))) =>
			if not(LongVId.isUnqualified longvid) then
			    errorLongVId(I, "misplaced long identifier ",
					 longvid)
			else if not(isInfix J longvid) then
			    error(I, "misplaced non-infix pattern")
			else
			    (* Now, longvid has infix status but is sans `op',
			       so it can only result from resolving an
			       appropriate infix construction. *)
			    ( checkNonfixity(List.tl ps)
			    ; ( SANSOp, LongVId.toId longvid,
				atpat::List.tl atpats )
			    )

	           | NONFIX(PARAtPat(_, ATPat(_, atpat as PARAtPat _))) =>
			maybeParenthesisedInfixClause(NONFIX(atpat)::List.tl ps)

	           | NONFIX(PARAtPat(_, pat)) =>
			error(infoPat pat, "misplaced non-infix pattern")

	 	   | _ => maybeNonfixClause(ps)

	    fun maybePlainInfixClause(ps) =
	        case ps
	          of [NONFIX atpat1, INFIX(_, vid, I), NONFIX atpat2] =>
			 ( SANSOp, vid, pairPat(atpat1, atpat2)::[] )

		   | _ => maybeParenthesisedInfixClause(ps)
	in
	    maybePlainInfixClause(List.map (categoriseAtPat J) atpats)
	end
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML initial infix environment
 *
 * Definition, Appendix C
 *)

signature INITIAL_INFIX_ENV =
sig
    (* Import type *)

    type InfEnv = Infix.InfEnv

    (* Export *)

    val J0 : InfEnv
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML initial infix environment
 *
 * Definition, Appendix C
 *)

structure InitialInfixEnv : INITIAL_INFIX_ENV =
struct
    (* Import type *)

    type InfEnv = Infix.InfEnv

    (* Value identifiers *)

    val vidCons   = VId.fromString "::"
    val vidEqual  = VId.fromString "="
    val vidAssign = VId.fromString ":="

    (* Export *)

    val J0 = VIdMap.fromList[(vidCons,   (Infix.RIGHT, 5)),
			     (vidEqual,  (Infix.LEFT,  4)),
			     (vidAssign, (Infix.LEFT,  3))]
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML identifier status
 *
 * Definition, Sections 4.1 and 5.5
 *)

signature IDSTATUS =
sig
    (* Type [Section 4.1] *)

    datatype IdStatus = c | e | v			(* [is] *)


    (* Operations *)

    val generalises : IdStatus * IdStatus -> bool
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML identifier status
 *
 * Definition, Sections 4.1 and 5.5
 *)

structure IdStatus :> IDSTATUS =
struct
    (* Type [Section 4.1] *)

    datatype IdStatus = c | e | v			(* [is] *)


    (* Generalisation [Section 5.5] *)

    fun generalises(is1,is2) = is1 = is2 orelse is2 = v
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML addresses
 *
 * Definition, Section 6.2
 *)

signature ADDR =
sig
    (* Type [Section 6.2] *)

    eqtype Addr					(* [a] *)


    (* Operations *)

    val addr :     unit -> Addr
    val compare :  Addr * Addr -> order
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML addresses and maps thereof
 *
 * Definition, Sections 6.2 and 6.3
 *)

structure Addr :> ADDR =
struct
    (* Type [Section 6.2] *)

    type Addr =	Stamp.stamp				(* [a] *)

    (* Operations *)

    val addr    = Stamp.stamp
    val compare = Stamp.compare
end

structure AddrMap = FinMapFn(type ord_key = Addr.Addr
			     val  compare = Addr.compare);
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML exception names
 *
 * Definition, Section 6.2
 *)

signature EXNAME =
sig
    (* Import *)

    type VId = VId.Id


    (* Type [Section 6.2] *)

    eqtype ExName					(* [en] *)


    (* Operations *)

    val exname :   VId -> ExName
    val toString : ExName -> string

    val compare :  ExName * ExName -> order
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML exception names and sets thereof
 *
 * Definition, Sections 6.2 and 6.3
 *)


structure ExName :> EXNAME =
struct
    (* Import *)

    type VId   = VId.Id
    type stamp = Stamp.stamp


    (* Type [Section 6.2] *)

    type ExName =				      (* [en] *)
	 { vid :   VId
	 , stamp : stamp
	 }


    (* Creation *)

    fun exname vid = {vid = vid, stamp = Stamp.stamp()}


    (* Conversion *)

    fun toString{vid, stamp} = VId.toString vid


    (* Ordering *)

    fun compare(en1 : ExName, en2 : ExName) =
	    Stamp.compare(#stamp en1, #stamp en2)
end

structure ExNameSet = FinSetFn(type ord_key = ExName.ExName
			       val  compare = ExName.compare);
(*
 * (c) Andreas Rossberg 2001-2007
 *
 * Standard ML Basis Library value representation
 *
 * Definition, Sections 6.2 and E.1
 * Standard Basis Specification
 *
 * Note:
 * - We have to separate this from the rest of the Library module in order
 *   to avoid cycles in our architecture.
 *)

signature LIBRARY_SVAL =
sig
    datatype IntSVal		= INT of int
    datatype WordSVal		= WORD of word | WORD8 of Word8.word
    datatype RealSVal		= REAL of real
    datatype CharSVal		= CHAR of char
    datatype StringSVal		= STRING of string

    val wordToWord8 :		word -> Word8.word
    val word8ToWord :		Word8.word -> word
    val word8ToWordX :		Word8.word -> word

    val intToString :		IntSVal -> string
    val wordToString :		WordSVal -> string
    val realToString :		RealSVal -> string
    val charToString :		CharSVal -> string
    val stringToString :	StringSVal -> string

    val compareInt :		IntSVal * IntSVal -> order
    val compareWord :		WordSVal * WordSVal -> order
    val compareChar :		CharSVal * CharSVal -> order
    val compareString :		StringSVal * StringSVal -> order
end;
(*
 * (c) Andreas Rossberg 2001-2007
 *
 * Standard ML Basis Library value representation
 *
 * Definition, Sections 6.2 and E.1
 * Standard Basis Specification
 *
 * Note:
 * - We have to separate this from the rest of the Library module in order
 *   to avoid cycles in our architecture.
 *)

structure LibrarySVal : LIBRARY_SVAL =
struct
    datatype IntSVal                        = INT of int
    datatype WordSVal                       = WORD of word | WORD8 of Word8.word
    datatype RealSVal                       = REAL of real
    datatype CharSVal                       = CHAR of char
    datatype StringSVal                     = STRING of string

    val wordToWord8                         = Word8.fromLarge o Word.toLarge
    val word8ToWord                         = Word.fromLarge o Word8.toLarge
    val word8ToWordX                        = Word.fromLarge o Word8.toLargeX

    fun intToString(INT i)                  = Int.toString i
    fun wordToString(WORD w)                = Word.toString w
      | wordToString(WORD8 w)               = Word8.toString w
    fun realToString(REAL r)                = Real.toString r
    fun charToString(CHAR c)                = Char.toString c
    fun stringToString(STRING s)            = String.toString s

    fun compareInt(INT i1, INT i2)          = Int.compare(i1, i2)
    fun compareWord(WORD w1, WORD w2)       = Word.compare(w1, w2)
      | compareWord(WORD w1, WORD8 w2)      = Word.compare(w1, word8ToWord w2)
      | compareWord(WORD8 w1, WORD w2)      = Word.compare(word8ToWord w1, w2)
      | compareWord(WORD8 w1, WORD8 w2)     = Word8.compare(w1, w2)
    fun compareReal(REAL r1, REAL r2)       = Real.compare(r1, r2)
    fun compareChar(CHAR c1, CHAR c2)       = Char.compare(c1, c2)
    fun compareString(STRING s1, STRING s2) = String.compare(s1, s2)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML special values
 *
 * Definition, Section 6.2
 *)

signature SVAL =
sig
    (* Type [Section 6.2] *)

    datatype SVal =				(* [sv] *)
	  INT    of LibrarySVal.IntSVal
	| WORD   of LibrarySVal.WordSVal
	| STRING of LibrarySVal.StringSVal
	| CHAR   of LibrarySVal.CharSVal
	| REAL   of LibrarySVal.RealSVal

    (* Operations *)

    val toString :  SVal -> string
    val equal :     SVal * SVal -> bool		(* may raise Domain *)
    val compare :   SVal * SVal -> order	(* may raise Domain *)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML special values
 *
 * Definition, Section 6.2
 *)

structure SVal :> SVAL =
struct
    (* Type [Section 6.2] *)

    datatype SVal =				(* [sv] *)
	  INT    of LibrarySVal.IntSVal
	| WORD   of LibrarySVal.WordSVal
	| STRING of LibrarySVal.StringSVal
	| CHAR   of LibrarySVal.CharSVal
	| REAL   of LibrarySVal.RealSVal


    (* Conversions *)

    fun toString(INT i)		= LibrarySVal.intToString i
      | toString(WORD w)	= "0wx" ^ LibrarySVal.wordToString w
      | toString(STRING s)	= "\"" ^ LibrarySVal.stringToString s ^ "\""
      | toString(CHAR c)	= "#\"" ^ LibrarySVal.charToString c ^ "\""
      | toString(REAL r)	= LibrarySVal.realToString r

    (* Comparison *)

    fun compare(INT i1,    INT i2)	= LibrarySVal.compareInt(i1, i2)
      | compare(WORD w1,   WORD w2)	= LibrarySVal.compareWord(w1, w2)
      | compare(STRING s1, STRING s2)	= LibrarySVal.compareString(s1, s2)
      | compare(CHAR c1,   CHAR c2)	= LibrarySVal.compareChar(c1, c2)
      | compare  _			= raise Domain

    fun equal(sv1, sv2)			= compare(sv1, sv2) = EQUAL
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML objects of the dynamic semantics of the core
 *
 * Definition, Sections 6.2 and 6.3
 *
 * Notes:
 *   - Basic values are just named by strings.
 *   - Env is modelled by a datatype to deal with type recursion.
 *   - We call the domain type of value environments ValStr.
 *)

structure DynamicObjectsCore =
struct
    (* Import *)

    type VId		= IdsCore.VId
    type 'a LabMap	= 'a IdsCore.LabMap
    type 'a VIdMap	= 'a IdsCore.VIdMap
    type 'a TyConMap	= 'a IdsCore.TyConMap
    type 'a StrIdMap	= 'a IdsCore.StrIdMap

    type 'a AddrMap	= 'a AddrMap.map

    type IdStatus	= IdStatus.IdStatus

    type Match		= GrammarCore.Match


    (* Simple objects [Section 6.2] *)

    type Addr		= Addr.Addr				(* [a] *)
    type ExName		= ExName.ExName				(* [en] *)
    type BasVal		= string				(* [b] *)
    type SVal		= SVal.SVal				(* [sv] *)

    exception FAIL

    (* Compound objects [Section 6.3] *)

    datatype Val	=					(* [v] *)
	  Assign
	| SVal		of SVal
	| BasVal	of BasVal
	| VId		of VId
	| VIdVal	of VId * Val
	| ExVal		of ExVal
	| Record	of Record
	| Addr		of Addr
	| FcnClosure	of FcnClosure

    and ExVal		=					(* [e] *)
	  ExName	of ExName
	| ExNameVal	of ExName * Val

    and Env		= Env of StrEnv * TyEnv * ValEnv	(* [E] *)

    withtype Record	= Val LabMap				(* [r] *)
    and      FcnClosure	= Match * Env * (Val * IdStatus) VIdMap
    and      StrEnv	= Env StrIdMap				(* [SE] *)
    and      TyEnv	= ((Val * IdStatus) VIdMap) TyConMap	(* [TE] *)
    and      ValEnv	= (Val * IdStatus) VIdMap		(* [VE] *)
    and      ValStr	= Val * IdStatus

    type     Pack	= ExVal					(* [p] *)
    type     Mem	= Val AddrMap				(* [mem] *)
    type     ExNameSet	= ExNameSet.set				(* [ens] *)
    type     State	= Mem * ExNameSet			(* [s] *)

    exception Pack	of Pack					(* [p] *)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML objects of the dynamic semantics of modules
 *
 * Definition, Section 7.2
 *)

structure DynamicObjectsModule =
struct
    (* Import *)

    type StrId		= IdsCore.StrId
    type 'a VIdMap	= 'a IdsCore.VIdMap
    type 'a TyConMap	= 'a IdsCore.TyConMap
    type 'a StrIdMap	= 'a IdsCore.StrIdMap
    type 'a SigIdMap	= 'a IdsModule.SigIdMap
    type 'a FunIdMap	= 'a IdsModule.FunIdMap

    type IdStatus	= DynamicObjectsCore.IdStatus
    type Env		= DynamicObjectsCore.Env

    type StrExp		= GrammarModule.StrExp


    (* Compound objects [Section 7.2] *)

    datatype Int	= Int of StrInt * TyInt * ValInt	(* [I] *)
    withtype StrInt	= Int StrIdMap				(* [SI] *)
    and      TyInt	= (IdStatus VIdMap) TyConMap		(* [TI] *)
    and      ValInt	= IdStatus VIdMap			(* [VI] *)

    datatype FunctorClosure =
	FunctorClosure of (StrId * Int) * StrExp * (FunEnv * SigEnv * Env)

    withtype SigEnv	= Int SigIdMap				(* [G] *)
    and      FunEnv	= FunctorClosure FunIdMap		(* [F] *)
    type     Basis	= FunEnv * SigEnv * Env			(* [B] *)
    type     IntBasis	= SigEnv * Int				(* [IB] *)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML initial dynamic basis
 *
 * Definition, Appendix D
 *
 * Note:
 *     The Definition does not specify what the initial state has to contain.
 *     This is a bug as it must at least contain the exception names Match
 *     and Bind. We put the state associated with the initial basis in
 *     here, too.
 *)

signature INITIAL_DYNAMIC_BASIS =
sig
    (* Import *)

    type Basis = DynamicObjectsModule.Basis
    type State = DynamicObjectsCore.State


    (* Export *)

    val B0 : Basis
    val s0 : State
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML core view of the initial dynamic basis
 *
 * Definition, Appendix D and Section 6.5
 *
 * Note:
 *     The Definition does not specify what the initial state has to contain.
 *     This is a bug as it must at least contain the exception names Match
 *     and Bind. We put the state associated with the initial environment in
 *     here, too.
 *)

signature INITIAL_DYNAMIC_ENV =
sig
    (* Import *)

    type Env    = DynamicObjectsCore.Env
    type ExName = DynamicObjectsCore.ExName
    type State  = DynamicObjectsCore.State


    (* Basic exception names [Section 6.5] *)

    val enMatch : ExName
    val enBind :  ExName

    (* Initial environment [Appendix D] *)

    val E0 : Env

    (* Associated state *)

    val s0 : State
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML core view of the initial dynamic basis
 *
 * Definition, Appendix D and Section 6.5
 *
 * Note:
 *     The Definition does not specify what the initial state has to contain.
 *     This is a bug as it must at least contain the exception names Match
 *     and Bind. We put the state associated with the initial environment in
 *     here, too.
 *)

structure InitialDynamicEnv : INITIAL_DYNAMIC_ENV =
struct
    (* Import *)

    open DynamicObjectsCore
    open IdStatus


    (* VIds [Appendix D] *)

    val vidEq     = VId.fromString "="
    val vidAssign = VId.fromString ":="

    val vidFalse  = VId.fromString "false"
    val vidTrue   = VId.fromString "true"
    val vidNil    = VId.fromString "nil"
    val vidCons   = VId.fromString "::"
    val vidRef    = VId.fromString "ref"

    val vidMatch  = VId.fromString "Match"
    val vidBind   = VId.fromString "Bind"


    (* Basic exception names [Section 6.5] *)

    val enMatch = ExName.exname vidMatch
    val enBind  = ExName.exname vidBind


    (* Value entries [Appendix D] *)

    val valstrEq     = (BasVal "=", v)
    val valstrAssign = (Assign,     v)

    val valstrFalse  = (VId vidFalse, c)
    val valstrTrue   = (VId vidTrue,  c)
    val valstrNil    = (VId vidNil,   c)
    val valstrCons   = (VId vidCons,  c)
    val valstrRef    = (VId vidRef,   c)

    val valstrMatch  = (ExVal(ExName enMatch), e)
    val valstrBind   = (ExVal(ExName enBind),  e)


    (* TyCons [Figure 26] *)

    val tyconUnit   = TyCon.fromString "unit"
    val tyconBool   = TyCon.fromString "bool"
    val tyconInt    = TyCon.fromString "int"
    val tyconWord   = TyCon.fromString "word"
    val tyconReal   = TyCon.fromString "real"
    val tyconString = TyCon.fromString "string"
    val tyconChar   = TyCon.fromString "char"
    val tyconList   = TyCon.fromString "list"
    val tyconRef    = TyCon.fromString "ref"
    val tyconExn    = TyCon.fromString "exn"


    (* Type ValEnvs [Figure 26] *)

    val VEUnit   = VIdMap.empty
    val VEBool   = VIdMap.fromList[(vidFalse, valstrFalse),
				   (vidTrue,  valstrTrue)] : ValEnv
    val VEInt    = VIdMap.empty
    val VEWord   = VIdMap.empty
    val VEReal   = VIdMap.empty
    val VEString = VIdMap.empty
    val VEChar   = VIdMap.empty
    val VEList   = VIdMap.fromList[(vidNil,   valstrNil),
				   (vidCons,  valstrCons)] : ValEnv
    val VERef    = VIdMap.fromList[(vidRef,   valstrRef)] : ValEnv
    val VEExn    = VIdMap.empty


    (* Environments [Appendix D] *)

    val SE0 = StrIdMap.empty

    val TE0 = TyConMap.fromList[(tyconUnit,   VEUnit),
 				(tyconBool,   VEBool),
 				(tyconInt,    VEInt),
 				(tyconWord,   VEWord),
 				(tyconReal,   VEReal),
 				(tyconString, VEString),
 				(tyconChar,   VEChar),
 				(tyconList,   VEList),
 				(tyconRef,    VERef),
 				(tyconExn,    VEExn)]

    val VE0 = VIdMap.fromList  [(vidEq,     valstrEq),
				(vidAssign, valstrAssign),
				(vidRef,    valstrRef),
				(vidNil,    valstrNil),
				(vidCons,   valstrCons),
				(vidFalse,  valstrFalse),
				(vidTrue,   valstrTrue),
				(vidMatch,  valstrMatch),
				(vidBind,   valstrBind)] : ValEnv

    val E0 = Env(SE0,TE0,VE0)


    (* Associated state *)

    val mem0 = AddrMap.empty
    val ens0 = ExNameSet.fromList[enMatch, enBind]

    val s0 = (mem0, ens0)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML initial dynamic basis
 *
 * Definition, Appendix D
 *
 * Note:
 *     The Definition does not specify what the initial state has to contain.
 *     This is a bug as it must at least contain the exception names Match
 *     and Bind. We put the state associated with the initial basis in
 *     here, too.
 *)

structure InitialDynamicBasis : INITIAL_DYNAMIC_BASIS =
struct
    (* Import *)

    type Basis = DynamicObjectsModule.Basis
    type State = DynamicObjectsCore.State


    (* Enviornments *)

    val F0 = FunIdMap.empty
    val G0 = SigIdMap.empty
    val E0 = InitialDynamicEnv.E0

    val B0 = (F0,G0,E0)


    (* Associated state *)

    val s0 = InitialDynamicEnv.s0
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML objects of the static semantics
 *
 * Definition, Sections 4.1, 4.2, and Appendix E
 *
 * Notes:
 *   - Types are references so that unification can work via side effects.
 *     We need links (forwards) to unify two type variables.
 *   - Undetermined types are represented separately from type variables.
 *     They carry an additional time stamp to detect attempts of referring
 *     types not in scope. The time stamps are also used to prevent invalid
 *     unification with skolem types (i.e. newer type names) during signature
 *     matching. Time stamps are propagated during unification.
 *   - To represent overloaded type (variables), we also add a special type.
 *   - Record types may contain a row variable to represent open record types
 *     (which appear during type inference). Flexible rows have to carry an
 *     equality flag and time stamp to properly propagate information enforced
 *     by unification when extending a row.
 *   - Types built bottom-up have to be `normalised' to induce the required
 *     sharing on type variables. Care has to be taken to clone types at the
 *     proper places.
 *   - Env is modelled by a datatype to deal with type recursion.
 *   - We call the domain type of value environments ValStr.
 *)

structure StaticObjectsCore =
struct
    (* Import *)

    type 'a LabMap	= 'a IdsCore.LabMap
    type 'a VIdMap	= 'a IdsCore.VIdMap
    type 'a TyConMap	= 'a IdsCore.TyConMap
    type 'a StrIdMap	= 'a IdsCore.StrIdMap


    (* Simple objects [Section 4.1 and Appendix E] *)

    type TyVar		= TyVar.TyVar				(* [alpha] *)
    type TyName		= TyName.TyName				(* [t] *)
    type IdStatus	= IdStatus.IdStatus			(* [is] *)

    type OverloadingClass = OverloadingClass.OverloadingClass	(* [O] *)


    (* Compound objects [Section 4.2] *)

    type RowVar		= {eq : bool, time : Stamp.stamp}	(* [r] *)

    datatype Type'	=					(* [tau] *)
	  TyVar		of TyVar
	| RowType	of RowType
	| FunType	of FunType
	| ConsType	of ConsType
	| Undetermined	of {stamp : Stamp.stamp, eq : bool, time : Stamp.stamp}
	| Overloaded	of OverloadingClass
	| Determined	of Type

    withtype Type	= Type' ref
    and      RowType	= Type' ref LabMap * RowVar option	(* [rho] *)
    and      FunType	= Type' ref * Type' ref
    and      ConsType	= Type' ref list * TyName

    type     TypeFcn	= TyVar list * Type			(* [theta] *)
    type     TypeScheme	= TyVar list * Type			(* [sigma] *)

    datatype Env	= Env of StrEnv * TyEnv * ValEnv	(* [E] *)
    withtype StrEnv	= Env StrIdMap				(* [SE] *)
    and      TyEnv	= (TypeFcn * (TypeScheme * IdStatus) VIdMap) TyConMap
								(* [TE] *)
    and      ValEnv	= (TypeScheme * IdStatus) VIdMap	(* [VE] *)
    type     ValStr	= TypeScheme * IdStatus
    type     TyStr	= TypeFcn * ValEnv

    type     TyNameSet	= TyNameSet.set				(* [T] *)
    type     TyVarSet	= TyVarSet.set				(* [U] *)
    type     Context	= TyNameSet * TyVarSet * Env		(* [C] *)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML objects of the static semantics of modules
 *
 * Definition, Section 5.1
 *)

structure StaticObjectsModule =
struct
    (* Import *)

    type 'a SigIdMap	= 'a IdsModule.SigIdMap
    type 'a FunIdMap	= 'a IdsModule.FunIdMap

    type Env		= StaticObjectsCore.Env
    type TyNameSet	= StaticObjectsCore.TyNameSet


    (* Compound objects [Section 5.1] *)

    type Sig		= TyNameSet * Env			(* [Sigma] *)
    type FunSig		= TyNameSet * (Env * Sig)		(* [Phi] *)

    type SigEnv		= Sig SigIdMap				(* [G] *)
    type FunEnv		= FunSig FunIdMap			(* [F] *)
    type Basis		= TyNameSet * FunEnv * SigEnv * Env	(* [B] *)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML initial static basis
 *
 * Definition, Appendices C and E
 *)

signature INITIAL_STATIC_BASIS =
sig
    (* Import *)

    type Basis = StaticObjectsModule.Basis

    (* Export *)

    val B0 : Basis
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML core view of the initial static basis
 *
 * Definition, Appendix C
 *)

signature INITIAL_STATIC_ENV =
sig
    (* Import *)

    type Type             = StaticObjectsCore.Type
    type TyName           = StaticObjectsCore.TyName
    type TyNameSet        = StaticObjectsCore.TyNameSet
    type OverloadingClass = StaticObjectsCore.OverloadingClass
    type Env              = StaticObjectsCore.Env


    (* Predefined monomorphic types [Figure 24] *)

    val tBool :		TyName
    val tInt :		TyName
    val tWord :		TyName
    val tReal :		TyName
    val tString :	TyName
    val tChar :		TyName
    val tExn :		TyName

    val tauBool :	Type
    val tauInt :	Type
    val tauWord :	Type
    val tauReal :	Type
    val tauString :	Type
    val tauChar :	Type
    val tauExn :	Type

    (* Initial environment [Appendix C] *)

    val T0 :		TyNameSet
    val E0 :		Env
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML types
 *
 * Definition, Section 4.2 and 4.4
 *
 * Notes:
 *   - Types are references so that unification can work via side effects.
 *     We need links (forwards) to unify two type variables.
 *   - Types built bottom-up have to be `normalised' to induce the required
 *     sharing on type variables.
 *   - Care has to be taken to clone types at the proper places.
 *   - Undetermined types are represented separately from type variables.
 *     They carry an additional time stamp to detect attempts of referring
 *     types not in scope. The time stamps are also used to prevent invalid
 *     unification with skolem types (i.e. newer type names) during signature
 *     matching. Time stamps are propagated during unification.
 *   - Substitution creates a clone, but shares undetermined types.
 *   - To represent overloaded type (variables), we add a special type.
 *   - Record types may contain a row variable to represent open record types
 *     (which appear during type inference). Flexible rows have to carry an
 *     equality flag and time stamp to properly propagate information enforced
 *     by unification when extending a row.
 *)

signature TYPE =
sig
    (* Import types *)

    type Lab              = Lab.Lab
    type TyVar            = StaticObjectsCore.TyVar
    type TyVarSet         = StaticObjectsCore.TyVarSet
    type TyName           = StaticObjectsCore.TyName
    type TyNameSet        = StaticObjectsCore.TyNameSet
    type OverloadingClass = StaticObjectsCore.OverloadingClass
    type Type             = StaticObjectsCore.Type
    type RowType          = StaticObjectsCore.RowType
    type FunType          = StaticObjectsCore.FunType
    type ConsType         = StaticObjectsCore.ConsType
    type TypeFcn          = StaticObjectsCore.TypeFcn

    type 'a TyVarMap      = 'a TyVarMap.map
    type 'a TyNameMap     = 'a TyNameMap.map
 

    (* Types [Section 4.2 and 5.2] *)

    type Substitution = Type TyVarMap				(* [mu] *)
    type Realisation  = TypeFcn TyNameMap			(* [phi] *)


    (* Operations *)

    val guess :			bool -> Type
    val invent :		bool -> Type
    val fromTyVar :		TyVar -> Type
    val fromRowType :		RowType -> Type
    val fromFunType :		FunType -> Type
    val fromConsType :		ConsType -> Type
    val fromOverloadingClass :	OverloadingClass -> Type

    val range :			Type -> Type
    val tyname :		Type -> TyName
    val equals :		Type * Type -> bool

    val substitute :		Substitution -> Type -> Type
    val realise :		Realisation  -> Type -> Type
    val determine :		Type StampMap.map -> Type -> Type

    val tyvars :		Type -> TyVarSet
    val tynames :		Type -> TyNameSet
    val undetermined :		Type -> bool StampMap.map
    val admitsEquality :	Type -> bool
    val isOverloaded :		Type -> bool

    exception Unify
    exception Flexible
    val unify :			Type * Type -> unit	(* Unify *)
    val resolve :		Type -> unit		(* Flexible *)


    (* Operations on rows *)

    val emptyRow :		RowType
    val singletonRow :		Lab * Type -> RowType
    val insertRow :		RowType * Lab * Type -> RowType
    val guessRow :		unit -> RowType
    val findLab :		RowType * Lab -> Type option
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML types
 *
 * Definition, Section 4.2 and 4.4
 *
 * Notes:
 *   - Types are references so that unification can work via side effects.
 *     We need links (forwards) to unify two type variables.
 *   - Types built bottom-up have to be `normalised' to induce the required
 *     sharing on type variables.
 *   - Care has to be taken to clone types at the proper places.
 *   - Undetermined types are represented separately from type variables.
 *     They carry an additional time stamp to detect attempts of referring
 *     types not in scope. The time stamps are also used to prevent invalid
 *     unification with skolem types (i.e. newer type names) during signature
 *     matching. Time stamps are propagated during unification.
 *   - Substitution creates a clone, but shares undetermined types.
 *   - To represent overloaded type (variables), we add a special type.
 *   - Record types may contain a row variable to represent open record types
 *     (which appear during type inference). Flexible rows have to carry an
 *     equality flag and time stamp to properly propagate information enforced
 *     by unification when extending a row.
 *)

structure Type :> TYPE =
struct
    (* Import *)

    open StaticObjectsCore

    type Lab = Lab.Lab

    type 'a TyVarMap      = 'a TyVarMap.map
    type 'a TyNameMap     = 'a TyNameMap.map


    (* Types [Section 4.2 and 5.2] *)

    type Substitution = Type TyVarMap			(* [mu] *)
    type Realisation  = TypeFcn TyNameMap		(* [phi] *)


    (* Creation *)

    fun fromTyVar alpha        = ref(TyVar alpha)
    fun fromRowType rho        = ref(RowType rho)
    fun fromFunType x          = ref(FunType x)
    fun fromConsType x         = ref(ConsType x)
    fun fromOverloadingClass O = ref(Overloaded O)

    fun guess eq               = let val stamp = Stamp.stamp()
				 in ref(Undetermined{stamp = stamp, eq = eq,
						     time = stamp}) end
    fun invent eq              = ref(ConsType([], TyName.invent(0, eq)))

    fun isOverloaded(ref(Determined(tau))) = isOverloaded tau
      | isOverloaded(ref(Overloaded _))    = true
      | isOverloaded _                     = false


    (* Projections *)

    fun range(ref(FunType(tau1,tau2))) = tau2
      | range(ref(Determined(tau)))    = range tau
      | range tau                      = tau

    fun tyname(ref(ConsType(taus,t)))  = t
      | tyname(ref(Determined(tau)))   = tyname tau
      | tyname  _                      =
	    raise Fail "Type.tyname: non-constructed type"


    (* Cloning under a substitution and a type realisation *)

    fun clone (mu,phi,det) tau =
	let
	    (* Cloning must respect sharing (at least for not fully
	     * determined types). An association list is used to remember
	     * nodes already visited together with their copy.
	     *)

	    val mu'    = ref mu
	    val cloned = ref []

	    fun clone tau =
		case List.find (fn(tau1,_) => tau1 = tau) (!cloned)
		  of SOME(_,tau2) => tau2
		   | NONE         => let val tau2 = clone' tau in
					 cloned := (tau,tau2) :: !cloned ;
					 tau2
				     end

	    and clone' tau =
		case !tau
		  of TyVar(alpha) =>
		     (case TyVarMap.find(!mu', alpha)
			of NONE     => tau
			 | SOME tau => tau
		     )
		   | RowType(rho, NONE) =>
			(* If row is closed, we can safely copy. *)
			ref(RowType(LabMap.map clone rho, NONE))

		   | RowType(rho, SOME _) =>
			(* If the row is not closed, than we must keep sharing!
			 * The row may not contain any tynames or tyvars
			 * of the domains of mu and phi in this case.
			 * We conjecture that this does not happen since
			 * the only possibility for this case is instantiation
			 * of a local type scheme, where the type (and thus
			 * all contained tyvars) will be bound in the context.
			 *)
			tau

		   | FunType(tau1,tau2) =>
			ref(FunType(clone tau1, clone tau2))

		   | ConsType(taus,t) =>
		     let
			val taus2 = List.map clone taus
		     in
			case TyNameMap.find(phi, t)
			  of NONE       => ref(ConsType(taus2,t))
			   | SOME theta =>
			     let
				val (alphas,tau1) = renameTypeFcn theta
				val cloned'       = !cloned
			     in
				mu' := ListPair.foldlEq
					(fn(alpha,tau2,mu) =>
					    TyVarMap.insert(mu,alpha,tau2))
					(!mu') (alphas,taus2) ;
				clone' tau1
				before cloned := cloned'
			     end
		     end

		   | Undetermined{stamp,...} =>
		     (case StampMap.find(det, stamp)
		        of SOME tau1 => tau1
			 | NONE      => tau
		     )
		   | Overloaded(O) =>
			tau

		   | Determined(tau) =>
			clone tau
	in
	    clone tau
	end

    and renameTypeFcn (alphas,tau) =
	let
	    val alphas' = List.map (TyVar.invent o TyVar.admitsEquality) alphas
	    val taus    = List.map fromTyVar alphas'
	    val mu      = TyVarMap.fromList(ListPair.zipEq(alphas, taus))
	in
	    ( alphas', substitute mu tau )
	end


    (* Substitution, realisation [Section 5.2] and determination *)

    and substitute mu = clone(mu, TyNameMap.empty, StampMap.empty)
    fun realise phi   = clone(TyVarMap.empty, phi, StampMap.empty)
    fun determine det = clone(TyVarMap.empty, TyNameMap.empty, det)


    (* Type variable and type name extraction [Section 4.2] *)

    fun tyvars(ref tau') = tyvars' tau'

    and tyvars'(TyVar(alpha)) = TyVarSet.singleton alpha

      | tyvars'(RowType(rho,r)) =
	    LabMap.foldl (fn(tau,U) => TyVarSet.union(U, tyvars tau))
			 TyVarSet.empty rho

      | tyvars'(FunType(tau1,tau2)) =
	    TyVarSet.union(tyvars tau1, tyvars tau2)
 
      | tyvars'(ConsType(taus,t)) =
	    List.foldl (fn(tau,U) => TyVarSet.union(U, tyvars tau))
		       TyVarSet.empty taus

      | tyvars'(Undetermined{...}) =
	    (* Not quite right, but we never fill in types containing tyvars. *)
	    TyVarSet.empty

      | tyvars'(Overloaded(O)) =
	    TyVarSet.empty

      | tyvars'(Determined(tau)) =
	    tyvars tau


    fun tynames(ref tau') = tynames' tau'

    and tynames'(TyVar(alpha)) = TyNameSet.empty

      | tynames'(RowType(rho,r)) =
	    LabMap.foldl (fn(tau,T) =>
			  TyNameSet.union(T, tynames tau)) TyNameSet.empty rho

      | tynames'(FunType(tau1,tau2)) =
	    TyNameSet.union(tynames tau1, tynames tau2)
 
      | tynames'(ConsType(taus,t)) =
	let
	    val T = List.foldl (fn(tau,T) => TyNameSet.union(T, tynames tau))
			       TyNameSet.empty taus
	in
	    TyNameSet.add(T, t)
	end

      | tynames'(Undetermined{...}) =
	    (* Not quite right, but currently it is OK for all uses of
	     * of this function in HaMLet. :-P *)
	    TyNameSet.empty

      | tynames'(Overloaded(O)) =
	    (* Approximation *)
	    OverloadingClass.set O

      | tynames'(Determined(tau)) =
	    tynames tau


    fun undetermined(ref tau') = undetermined' tau'

    and undetermined'(TyVar(alpha)) = StampMap.empty

      | undetermined'(RowType(rho,r)) =
	    LabMap.foldl (fn(tau,Z) =>
		StampMap.unionWith #2 (Z, undetermined tau)) StampMap.empty rho

      | undetermined'(FunType(tau1,tau2)) =
	    StampMap.unionWith #2 (undetermined tau1, undetermined tau2)

      | undetermined'(ConsType(taus,t)) =
	    List.foldl (fn(tau,Z) =>
		StampMap.unionWith #2 (Z, undetermined tau)) StampMap.empty taus

      | undetermined'(Undetermined{stamp,eq,...}) =
	    StampMap.singleton(stamp, eq)

      | undetermined'(Overloaded(O)) =
	    StampMap.empty

      | undetermined'(Determined(tau)) =
	    undetermined tau


    (* Check for equality type [Section 4.4] *)

    fun admitsEquality(ref tau') = admitsEquality' tau'

    and admitsEquality'(TyVar alpha) =
	    TyVar.admitsEquality alpha

      | admitsEquality'(RowType(rho,NONE)) =
	    LabMap.all admitsEquality rho

      | admitsEquality'(RowType(rho,SOME{eq,...})) =
	    eq andalso LabMap.all admitsEquality rho
	    orelse raise Fail "Type.admitsEquality: undetermined row type"

      | admitsEquality'(FunType _) = false

      | admitsEquality'(ConsType(taus,t)) =
	TyName.admitsEquality t andalso List.all admitsEquality taus
	orelse TyName.toString t = "ref"

      | admitsEquality'(Undetermined{eq,...}) =
	    eq orelse raise Fail "Type.admitsEquality: undetermined type"

      | admitsEquality'(Overloaded(O)) =
	    raise Fail "Type.admitsEquality: overloaded type"

      | admitsEquality'(Determined(tau)) =
	    admitsEquality tau


    (* Equality *)

    fun equals(ref(Determined(tau1)), tau2) = equals(tau1, tau2)
      | equals(tau1, ref(Determined(tau2))) = equals(tau1, tau2)

      | equals(tau1 as ref tau1', tau2 as ref tau2') =
	    tau1 = tau2 orelse equals'(tau1',tau2')

    and equals'(TyVar(alpha1), TyVar(alpha2)) =
	   alpha1 = alpha2

      | equals'(FunType(tau11,tau12), FunType(tau21,tau22)) =
	   equals(tau11,tau21) andalso equals(tau12,tau22)

      | equals'(RowType(rho1,r1), RowType(rho2,r2)) =
	let
	    fun equalsField(lab, tau1) =
		case LabMap.find(rho2, lab)
		  of SOME tau2 => equals(tau1,tau2)
		   | NONE      => false
	in
	    r1 = r2 andalso LabMap.numItems rho1 = LabMap.numItems rho2 andalso
	    LabMap.alli equalsField rho1
	end

      | equals'(tau' as ConsType(taus1,t1), ConsType(taus2,t2)) =
	    t1 = t2 andalso ListPair.allEq equals (taus1,taus2)

      | equals'(Undetermined{stamp=z1,...}, Undetermined{stamp=z2,...}) =
	    z1 = z2 orelse raise Fail "Type.equals: undetermined"

      | equals'(Overloaded(O1), Overloaded(O2)) =
	   raise Fail "Type.equals: overloaded"

      | equals' _ = false


    (* Unification *)

    exception Unify


    fun occurs(z, ref tau') = occurs'(z, tau')

    and occurs'(z, TyVar(alpha)) =
	    false
      | occurs'(z, RowType(rho,r)) =
	    LabMap.exists (fn tau => occurs(z, tau)) rho
      | occurs'(z, FunType(tau1,tau2)) =
	    occurs(z, tau1) orelse occurs(z, tau2)
      | occurs'(z, ConsType(taus,t)) =
	    List.exists (fn tau => occurs(z, tau)) taus
      | occurs'(z, Undetermined{stamp,...}) =
	    stamp = z
      | occurs'(z, Overloaded(O)) =
	    false
      | occurs'(z, Determined(tau)) =
	    occurs(z, tau)


    fun unify(ref(Determined(tau1)), tau2) = unify(tau1, tau2)
      | unify(tau1, ref(Determined(tau2))) = unify(tau1, tau2)

      | unify(tau1 as ref tau1', tau2 as ref tau2') =
	    if tau1 = tau2 then () else
	    ( tau1 := unify'(tau1',tau2')
	    ; tau2 := Determined(tau1)
	    )

    and unify'(Undetermined{stamp,eq,time}, tau') =
	    unifyUndetermined(stamp, eq, time, tau')
      | unify'(tau', Undetermined{stamp,eq,time}) =
	    unifyUndetermined(stamp, eq, time, tau')
      | unify'(Overloaded(O), tau')     = unifyOverloaded(O, tau')
      | unify'(tau', Overloaded(O))     = unifyOverloaded(O, tau')

      | unify'(tau' as TyVar(alpha1), TyVar(alpha2)) =
	if alpha1 = alpha2 then
	    tau'
	else
	    raise Unify

      | unify'(tau' as FunType(tau11,tau12), FunType(tau21,tau22)) =
	   ( unify(tau11,tau21)
	   ; unify(tau12,tau22)
	   ; tau'
	   )

      | unify'(RowType(rho1,r1), RowType(rho2,r2)) =
	let
	    fun unifyField r (lab, tau1, rho) =
		case LabMap.find(rho, lab)
		  of SOME tau2  => ( unify(tau1,tau2)
				   ; #1(LabMap.remove(rho,lab))
				   )
		   | NONE =>
		case r
		  of NONE          => raise Unify
		   | SOME{eq,time} => ( propagate (time, eq) tau1 ; rho )

	    val rho1'  = LabMap.foldli (unifyField r1) rho1 rho2
	    val _      = LabMap.foldli (unifyField r2) rho2 rho1'
	    val r      = case (r1,r2)
			   of (NONE, _) => NONE
			    | (_, NONE) => NONE
			    | (SOME{eq=eq1, time=time1},
			       SOME{eq=eq2, time=time2}) =>
				  SOME{eq = eq1 orelse eq2,
				       time = Stamp.min(time1, time2)}
	in
	    RowType(LabMap.unionWith #2 (rho2,rho1'), r)
	end

      | unify'(tau' as ConsType(taus1,t1), ConsType(taus2,t2)) =
	if t1 = t2 then
	    ( ListPair.appEq unify (taus1,taus2)
	    ; tau'
	    )
	else
	    raise Unify

      | unify' _ = raise Unify

    and unifyUndetermined(z, eq, time, tau') =
	if occurs'(z, tau') then
	    raise Unify
	else
	    propagate'(time, eq) tau'

    and unifyOverloaded(O, Undetermined{stamp,eq,time}) =
	    unifyUndetermined(stamp, eq, time, Overloaded(O))

      | unifyOverloaded(O, tau' as ConsType([],t)) =
	if OverloadingClass.member(O, t) then
	    tau'
	else
	    raise Unify

      | unifyOverloaded(O1, Overloaded(O2)) =
	(case OverloadingClass.intersection(O1,O2)
	   of NONE   => raise Unify
	    | SOME O => Overloaded(O)
	)
      | unifyOverloaded(O, _) =
	    raise Unify


    and propagate (time, eq) (tau as ref tau') =
	    tau := propagate'(time, eq) tau'

    and propagate'(time, eq) (tau' as TyVar(alpha)) =
	if not eq orelse TyVar.admitsEquality alpha then
	    tau'
	else
	    raise Unify

      | propagate'(time, eq) (RowType(rho,r)) =
	    ( LabMap.app (propagate(time, eq)) rho
	    ; RowType(rho, Option.map (propagateRowVar(time, eq)) r)
	    )
      | propagate'(time, eq) (tau' as FunType(tau1,tau2)) =
	if eq then
	    raise Unify
	else
	    ( propagate (time, eq) tau1
	    ; propagate (time, eq) tau2
	    ; tau'
	    )
      | propagate'(time, eq) (tau' as ConsType(taus,t)) =
	(case Stamp.compare(TyName.time t, time)
	   of GREATER => raise Unify
	    | _ =>
	      if not eq orelse TyName.toString t = "ref" then
		  ( List.app (propagate(time, false)) taus ; tau' )
	      else if TyName.admitsEquality t then
		  ( List.app (propagate(time, eq)) taus ; tau' )
	      else
		  raise Unify
	)
      | propagate'(time, eq) (Undetermined{stamp=z, eq=eq', time=time'}) =
	    Undetermined{stamp = z, eq = eq orelse eq',
			 time = Stamp.min(time, time')}

      | propagate'(time, eq) (tau' as Overloaded(O)) =
	if not eq then tau' else
	(case OverloadingClass.makeEquality O
	   of NONE    => raise Unify
	    | SOME O' => Overloaded(O')
	)
      | propagate'(time, eq) (tau' as Determined(tau)) =
	    ( propagate (time, eq) tau ; tau' )

    and propagateRowVar (time, eq) {eq=eq', time=time'} =
	    {eq = eq orelse eq', time = Stamp.min(time, time')}


    (* Assign default to overloaded type and check for remaining
     * flexible types [Appendix E and Section 4.11] *)

    exception Flexible

    fun resolve(ref(Determined tau)) =
	    resolve tau

      | resolve(tau as ref(Overloaded(O))) =
	    tau := ConsType([], OverloadingClass.default O)

      | resolve(ref(RowType(rho, SOME _))) =
	    raise Flexible

      | resolve _ = ()


    (* Operations on rows *)

    val emptyRow                     = ( LabMap.empty, NONE )
    fun singletonRow(lab,tau)        = ( LabMap.singleton(lab,tau), NONE )
    fun guessRow()                   = ( LabMap.empty,
					 SOME{eq=false, time=Stamp.stamp()} )
    fun insertRow((rho,r), lab, tau) = ( LabMap.insert(rho, lab, tau), r )
    fun findLab((rho,r), lab)        = LabMap.find(rho, lab)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML core view of the initial static basis
 *
 * Definition, Appendix C
 *)

structure InitialStaticEnv : INITIAL_STATIC_ENV =
struct
    (* Import *)

    open StaticObjectsCore
    open IdStatus


    (* VIds [Figure 25] *)

    val vidEq     = VId.fromString "="
    val vidAssign = VId.fromString ":="

    val vidFalse  = VId.fromString "false"
    val vidTrue   = VId.fromString "true"
    val vidNil    = VId.fromString "nil"
    val vidCons   = VId.fromString "::"
    val vidRef    = VId.fromString "ref"

    val vidMatch  = VId.fromString "Match"
    val vidBind   = VId.fromString "Bind"


    (* TyCons [Figure 24] *)

    val tyconUnit   = TyCon.fromString "unit"
    val tyconBool   = TyCon.fromString "bool"
    val tyconInt    = TyCon.fromString "int"
    val tyconWord   = TyCon.fromString "word"
    val tyconReal   = TyCon.fromString "real"
    val tyconString = TyCon.fromString "string"
    val tyconChar   = TyCon.fromString "char"
    val tyconList   = TyCon.fromString "list"
    val tyconRef    = TyCon.fromString "ref"
    val tyconExn    = TyCon.fromString "exn"


    (* TyNames [Appendix C] *)

    val tBool   = TyName.tyname(TyCon.toString tyconBool,   0, true,  2)
    val tInt    = TyName.tyname(TyCon.toString tyconInt,    0, true,  0)
    val tWord   = TyName.tyname(TyCon.toString tyconWord,   0, true,  0)
    val tReal   = TyName.tyname(TyCon.toString tyconReal,   0, false, 0)
    val tString = TyName.tyname(TyCon.toString tyconString, 0, true,  0)
    val tChar   = TyName.tyname(TyCon.toString tyconChar,   0, true,  0)
    val tList   = TyName.tyname(TyCon.toString tyconList,   1, true,  2)
    val tRef    = TyName.tyname(TyCon.toString tyconRef,    1, true,  1)
    val tExn    = TyName.tyname(TyCon.toString tyconExn,    0, false, 0)

    val T0      = TyNameSet.fromList[tBool, tInt, tWord, tReal, tString, tChar,
				     tList, tRef, tExn]


    (* Types *)

    val alpha      = TyVar.fromString "'a"
    val alphaEq    = TyVar.fromString "''a"
    val tauAlpha   = Type.fromTyVar alpha
    val tauAlphaEq = Type.fromTyVar alphaEq

    val tauUnit      = Type.fromRowType Type.emptyRow
    val tauBool      = Type.fromConsType([], tBool)
    val tauInt       = Type.fromConsType([], tInt)
    val tauWord      = Type.fromConsType([], tWord)
    val tauReal      = Type.fromConsType([], tReal)
    val tauString    = Type.fromConsType([], tString)
    val tauChar      = Type.fromConsType([], tChar)
    val tauExn       = Type.fromConsType([], tExn)
    val tauAlphaList = Type.fromConsType([tauAlpha], tList)
    val tauAlphaRef  = Type.fromConsType([tauAlpha], tRef)


    (* TypeSchemes [Figure 25] *)

    fun pairType(tau1,tau2) =
	Type.fromRowType(
	    Type.insertRow(Type.insertRow(Type.emptyRow, Lab.fromInt 1, tau1),
							 Lab.fromInt 2, tau2))
    val funType = Type.fromFunType

    val sigmaEq     = ([alphaEq],
		       funType(pairType(tauAlphaEq,tauAlphaEq), tauBool))
    val sigmaAssign = ([alpha],
		       funType(pairType(tauAlphaRef,tauAlpha), tauUnit))
    val sigmaFalse  = ([], tauBool)
    val sigmaTrue   = ([], tauBool)
    val sigmaNil    = ([alpha], tauAlphaList)
    val sigmaCons   = ([alpha],
		       funType(pairType(tauAlpha, tauAlphaList), tauAlphaList))
    val sigmaRef    = ([alpha], funType(tauAlpha, tauAlphaRef))

    val sigmaMatch  = ([], tauExn)
    val sigmaBind   = ([], tauExn)


    (* Value entries [Figure 25] *)

    val valstrEq     = (sigmaEq,     v)
    val valstrAssign = (sigmaAssign, v)

    val valstrFalse  = (sigmaFalse,  c)
    val valstrTrue   = (sigmaTrue,   c)
    val valstrNil    = (sigmaNil,    c)
    val valstrCons   = (sigmaCons,   c)
    val valstrRef    = (sigmaRef,    c)

    val valstrMatch  = (sigmaMatch,  e)
    val valstrBind   = (sigmaBind,   e)


    (* TypeFcns [Figure 24] *)

    val thetaUnit   = ([], tauUnit)
    val thetaBool   = ([], tauBool)
    val thetaInt    = ([], tauInt)
    val thetaWord   = ([], tauWord)
    val thetaReal   = ([], tauReal)
    val thetaString = ([], tauString)
    val thetaChar   = ([], tauChar)
    val thetaExn    = ([], tauExn)
    val thetaList   = ([alpha], tauAlphaList)
    val thetaRef    = ([alpha], tauAlphaRef)


    (* TyStrs [Figure 25] *)

    val VEEmpty = VIdMap.empty
    val VEBool  = VIdMap.fromList[(vidFalse, valstrFalse),
				  (vidTrue,  valstrTrue)] : ValEnv
    val VEList  = VIdMap.fromList[(vidNil,   valstrNil),
				  (vidCons,  valstrCons)]
    val VERef   = VIdMap.fromList[(vidRef,   valstrRef)]

    val tystrUnit   = (thetaUnit,   VEEmpty)
    val tystrBool   = (thetaBool,   VEBool )
    val tystrInt    = (thetaInt,    VEEmpty)
    val tystrWord   = (thetaWord,   VEEmpty)
    val tystrReal   = (thetaReal,   VEEmpty)
    val tystrString = (thetaString, VEEmpty)
    val tystrChar   = (thetaChar,   VEEmpty)
    val tystrList   = (thetaList,   VEList )
    val tystrRef    = (thetaRef,    VERef  )
    val tystrExn    = (thetaExn,    VEEmpty)


    (* Environments [Appendix C] *)

    val SE0 = StrIdMap.empty

    val TE0 = TyConMap.fromList[(tyconUnit,   tystrUnit),
 				(tyconBool,   tystrBool),
 				(tyconInt,    tystrInt),
 				(tyconWord,   tystrWord),
 				(tyconReal,   tystrReal),
 				(tyconString, tystrString),
 				(tyconChar,   tystrChar),
 				(tyconList,   tystrList),
 				(tyconRef,    tystrRef),
 				(tyconExn,    tystrExn)]

    val VE0 = VIdMap.fromList  [(vidEq,     valstrEq),
				(vidAssign, valstrAssign),
				(vidRef,    valstrRef),
				(vidNil,    valstrNil),
				(vidCons,   valstrCons),
				(vidFalse,  valstrFalse),
				(vidTrue,   valstrTrue),
				(vidMatch,  valstrMatch),
				(vidBind,   valstrBind)]

    val E0 = Env(SE0,TE0,VE0)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML initial static basis
 *
 * Definition, Appendices C and E
 *)

structure InitialStaticBasis : INITIAL_STATIC_BASIS =
struct
    (* Import *)

    type Basis = StaticObjectsModule.Basis

    (* Environments *)

    val T0 = InitialStaticEnv.T0
    val F0 = FunIdMap.empty
    val G0 = SigIdMap.empty
    val E0 = InitialStaticEnv.E0

    val B0 = (T0,F0,G0,E0)
end;
(*
 * (c) Andreas Rossberg 2001-2007
 *
 * Standard ML Basis Library primitives
 *
 * Definition, Sections 6.2, 6.4 and Appendices C, D and E
 *)

signature LIBRARY =
sig
    (* Import *)

    type TyName			= StaticObjectsCore.TyName
    type OverloadingClass	= StaticObjectsCore.OverloadingClass

    type Val			= DynamicObjectsCore.Val
    type BasVal			= DynamicObjectsCore.BasVal
    type State			= DynamicObjectsCore.State

    type StaticBasis		= StaticObjectsModule.Basis
    type DynamicBasis		= DynamicObjectsModule.Basis


    (* Hook file *)

    val file :			string

    (* Overloading classes [Section E.1] *)

    val Int :			OverloadingClass
    val Word :			OverloadingClass
    val Real :			OverloadingClass
    val String :		OverloadingClass
    val Char :			OverloadingClass

    val span :			TyName -> int

    (* Value class representation [Section 6.2] *)

    include LIBRARY_SVAL

    val intFromString :		SCon.base * string * TyName option -> IntSVal
    val wordFromString :	SCon.base * string * TyName option -> WordSVal
    val realFromString :	string * TyName option -> RealSVal
    val stringFromString :	string * TyName option -> StringSVal
    val charFromString :	string * TyName option -> CharSVal
				(* all xFromString can raise Overflow *)

    (* Initial basis [Appendices C and D] *)

    val B0_STAT : 		StaticBasis
    val B0_DYN :  		DynamicBasis
    val s0 :			State

    (* APPLY function [Section 6.4] *)

    exception TypeError		of string
    val APPLY : 		BasVal * Val -> Val
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML type functions
 *
 * Definition, Section 4.2, 4.4, and 4.8
 *
 * Note:
 *    Application copies the type (except free type variables).
 *)

signature TYPEFCN =
sig
    (* Import *)

    type Type			= StaticObjectsCore.Type
    type TypeFcn		= StaticObjectsCore.TypeFcn
    type TyVar			= StaticObjectsCore.TyVar
    type TyName			= StaticObjectsCore.TyName
    type TyVarSet		= StaticObjectsCore.TyVarSet
    type TyNameSet		= StaticObjectsCore.TyNameSet
    type Realisation		= Type.Realisation


    (* Operations *)

    val fromTyName :		TyName  -> TypeFcn
    val toTyName :		TypeFcn -> TyName option

    val arity :			TypeFcn -> int
    val admitsEquality :	TypeFcn -> bool

    val tyvars :		TypeFcn -> TyVarSet
    val tynames :		TypeFcn -> TyNameSet
    val undetermined :		TypeFcn -> bool StampMap.map
    val normalise :		TypeFcn -> TypeFcn
    val rename :		TypeFcn -> TypeFcn

    val equals :		TypeFcn * TypeFcn -> bool

    exception Apply
    val apply :			Type list * TypeFcn -> Type (* raises Apply *)

    val realise :		Realisation -> TypeFcn -> TypeFcn
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML type functions
 *
 * Definition, Section 4.2, 4.4, and 4.8
 *
 * Note:
 *    Application copies the type (except free type variables).
 *)

structure TypeFcn :> TYPEFCN =
struct
    (* Import *)

    open StaticObjectsCore

    type Realisation  = Type.Realisation


    (* Renaming *)

    fun rename (alphas,tau) =
	let
	    val alphas' = List.map (TyVar.invent o TyVar.admitsEquality) alphas
	    val taus    = List.map Type.fromTyVar alphas'
	    val mu      = TyVarMap.fromList(ListPair.zipEq(alphas, taus))
	in
	    ( alphas', Type.substitute mu tau )
	end


    (* Type variable and type name extraction [Section 4.2] *)

    fun tyvars (alphas,tau) =
	let
	    val U = Type.tyvars tau
	in
	    List.foldl
		(fn(alpha,U) => TyVarSet.delete(U,alpha)
				handle TyVarSet.NotFound => U)
		U alphas
	end

    fun tynames (alphas,tau)      = Type.tynames tau
    fun undetermined (alphas,tau) = Type.undetermined tau


    (* Arity [Section 4.4] *)

    fun arity (alphas,tau) = List.length alphas


    (* Comparison [Section 4.5] *)

    fun equals((alphas1,tau1), (alphas2,tau2)) =
	List.length alphas1 = List.length alphas2 andalso
	let
	    val taus2 = List.map Type.fromTyVar alphas2
	    val mu    = TyVarMap.fromList(ListPair.zipEq(alphas1, taus2))
	in
	    Type.equals(Type.substitute mu tau1, tau2)
	end


    (* Equality [Section 4.4] *)

    fun admitsEquality (alphas,tau) =
	let
	    val taus = List.map (fn _ => Type.guess true) alphas
	    val mu   = TyVarMap.fromList(ListPair.zipEq(alphas, taus))
	in
	    Type.admitsEquality(Type.substitute mu tau)
	end
    

    (* Eta-conversion [Section 4.4] *)

    fun fromTyName t =
	let
	    val alphas = List.tabulate(TyName.arity t, TyVar.fromInt false)
	in
	    ( alphas, Type.fromConsType(List.map Type.fromTyVar alphas, t) )
	end

    fun toTyName(alphas, ref(ConsType(taus,t))) =
	let
	    fun isSame(alpha, ref(TyVar alpha')) = alpha = alpha'
	      | isSame(alpha,          _       ) = false
	in
	    if List.length alphas = List.length taus
	    andalso ListPair.allEq isSame (alphas, taus) then
		SOME t
	    else
		NONE
	end

      | toTyName _ = NONE


    (* Application [Section 4.4] *)

    exception Apply

    fun apply(taus, (alphas,tau)) =
	let
	    val mu = TyVarMap.fromList(ListPair.zipEq(alphas, taus))
		     handle ListPair.UnequalLengths => raise Apply
	in
	    Type.substitute mu tau
	end


    (* Normalisation (for output) *)

    fun normalise (alphas,tau) =
	let
	    val ns      = List.tabulate(List.length alphas, fn n => n) 
	    val alphas' = ListPair.mapEq (fn(alpha, n) =>
					  TyVar.fromInt
					      (TyVar.admitsEquality alpha) n)
					 (alphas, ns)
	    val taus    = List.map Type.fromTyVar alphas'
	    val mu      = TyVarMap.fromList(ListPair.zipEq(alphas, taus))
	in
	    ( alphas', Type.substitute mu tau )
	end


    (* Realisation [Section 5.2] *)

    fun realise phi (alphas,tau) = (alphas, Type.realise phi tau)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML generic core environment
 *
 * Definition, Sections 4.2, 4.3, 6.3 and 7.2
 *)

signature GENERIC_ENV =
sig
    (* Import *)

    type VId		= IdsCore.VId
    type TyCon		= IdsCore.TyCon
    type StrId		= IdsCore.StrId
    type longVId	= IdsCore.longVId
    type longTyCon	= IdsCore.longTyCon
    type longStrId	= IdsCore.longStrId

    type 'a VIdMap	= 'a IdsCore.VIdMap
    type 'a TyConMap	= 'a IdsCore.TyConMap
    type 'a StrIdMap	= 'a IdsCore.StrIdMap


    (* Types [Section 4.2 and 6.3] *)

    type ValStr
    type TyStr
    type Env

    type StrEnv		= Env StrIdMap
    type TyEnv		= TyStr TyConMap
    type ValEnv		= ValStr VIdMap


    (* Operations *)

    val empty :		Env

    val fromSE :	StrEnv -> Env
    val fromTE :	TyEnv  -> Env
    val fromVE :	ValEnv -> Env
    val fromVEandTE :	ValEnv * TyEnv -> Env

    val SEof :		Env -> StrEnv
    val TEof :		Env -> TyEnv
    val VEof :		Env -> ValEnv

    val plus :		Env * Env    -> Env
    val plusVE :	Env * ValEnv -> Env
    val plusTE :	Env * TyEnv  -> Env
    val plusSE :	Env * StrEnv -> Env
    val plusVEandTE :	Env * (ValEnv * TyEnv) -> Env

    val findVId :	Env * VId       -> ValStr option
    val findTyCon :	Env * TyCon     -> TyStr option
    val findStrId :	Env * StrId     -> Env option
    val findLongVId :	Env * longVId   -> ValStr option
    val findLongTyCon :	Env * longTyCon -> TyStr option
    val findLongStrId :	Env * longStrId -> Env option

    val disjoint :	Env * Env -> bool
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML environments of the static semantics of the core
 *
 * Definition, Sections 4.2, 4.3, 4.8, 4.9, and 5.5
 *
 * Note:
 *     We call the domain type of value environments ValStr.
 *)

signature STATIC_ENV =
sig
    (* Inheritance *)

    include GENERIC_ENV
    where type Env		= StaticObjectsCore.Env
    and   type ValStr		= StaticObjectsCore.ValStr
    and   type TyStr		= StaticObjectsCore.TyStr


    (* Import *)

    type TyNameSet		= StaticObjectsCore.TyNameSet
    type TyVarSet		= StaticObjectsCore.TyVarSet
    type Realisation		= Type.Realisation


    (* Operations *)

    val tyvarsVE :		ValEnv -> TyVarSet
    val tyvars :		Env    -> TyVarSet
    val tynamesVE :		ValEnv -> TyNameSet
    val tynamesTE :		TyEnv  -> TyNameSet
    val tynamesSE :		StrEnv -> TyNameSet
    val tynames :		Env    -> TyNameSet
    val undetermined :		Env    -> bool StampMap.map

    val isWellFormed :		Env -> bool

    val Clos :			ValEnv -> ValEnv
    val maximiseEquality :	TyEnv * ValEnv -> TyEnv * ValEnv
    val Abs :			TyEnv * Env -> Env
    val realise :		Realisation -> Env -> Env

    val enriches :		Env * Env -> bool
    val equalsVE :		ValEnv * ValEnv -> bool
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML generic core environment
 *
 * Definition, Sections 4.2, 4.3, 6.3 and 7.2
 *)

functor GenericEnvFn(
    type Env
    type ValStr
    type TyStr
    val Env :   Env StrIdMap.map * TyStr TyConMap.map * ValStr VIdMap.map -> Env
    val unEnv : Env -> Env StrIdMap.map * TyStr TyConMap.map * ValStr VIdMap.map
) :> GENERIC_ENV
    where type Env	= Env
    and   type ValStr	= ValStr
    and   type TyStr	= TyStr
    =
struct
    (* Import *)

    open IdsCore

    type ValStr	= ValStr
    type TyStr	= TyStr
    type ValEnv	= ValStr VIdMap
    type TyEnv	= TyStr TyConMap
    type StrEnv	= Env StrIdMap
    type Env	= Env


    (* Injections [Section 4.3] *)

    val emptySE            = StrIdMap.empty
    val emptyTE            = TyConMap.empty
    val emptyVE            = VIdMap.empty
    val empty              = Env(emptySE, emptyTE, emptyVE)

    fun fromSE SE          = Env(SE,      emptyTE, emptyVE)
    fun fromTE TE          = Env(emptySE, TE,      emptyVE)
    fun fromVE VE          = Env(emptySE, emptyTE, VE     )
    fun fromVEandTE(VE,TE) = Env(emptySE, TE,      VE     )


    (* Projections [Section 4.3] *)

    fun SEof E = #1(unEnv E)
    fun TEof E = #2(unEnv E)
    fun VEof E = #3(unEnv E)


    (* Modifications [Section 4.3] *)

    infix plus plusVE plusTE plusSE plusVEandTE

    fun E plus E' =
	Env( StrIdMap.unionWith #2 (SEof E, SEof E')
	   , TyConMap.unionWith #2 (TEof E, TEof E')
	   , VIdMap.unionWith   #2 (VEof E, VEof E')
	   )

    fun E plusVE VE = Env(SEof E, TEof E, VIdMap.unionWith #2 (VEof E,VE))
    fun E plusTE TE = Env(SEof E, TyConMap.unionWith #2 (TEof E,TE), VEof E)
    fun E plusSE SE = Env(StrIdMap.unionWith #2 (SEof E,SE), TEof E, VEof E)
    fun E plusVEandTE (VE,TE) =
	Env( SEof E
	   , TyConMap.unionWith #2 (TEof E,TE)
	   , VIdMap.unionWith   #2 (VEof E,VE)
	   )


    (* Application (lookup) [Section 4.3] *)

    fun findVId  (E, vid)   = VIdMap.find(VEof E, vid)
    fun findTyCon(E, tycon) = TyConMap.find(TEof E, tycon)
    fun findStrId(E, strid) = StrIdMap.find(SEof E, strid)

    fun findLongX'(E, findX,      [],       x) = findX(E, x)
      | findLongX'(E, findX, strid::strids, x) =
	    Option.mapPartial (fn E => findLongX'(E, findX, strids, x))
			      (findStrId(E, strid))

    fun findLongX (explodeLongX, findX) (E, longX) =
	let
	    val (strids,x) = explodeLongX longX
	in
	    findLongX'(E, findX, strids, x)
	end

    fun findLongVId   x = findLongX (LongVId.explode,   findVId) x
    fun findLongTyCon x = findLongX (LongTyCon.explode, findTyCon) x
    fun findLongStrId x = findLongX (LongStrId.explode, findStrId) x


    (* Disjointness *)

    fun disjoint(E1, E2) =
	    StrIdMap.disjoint(SEof E1, SEof E2) andalso
	    TyConMap.disjoint(TEof E1, TEof E2) andalso
	    VIdMap.disjoint(VEof E1, VEof E2)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML type schemes
 *
 * Definition, Section 4.2, 4.5, and 4.8
 *
 * Note:
 *    Instantiation copies a type (except free type variables).
 *    Closure does not!
 *)

signature TYPESCHEME =
sig
    (* Import *)

    type Type			= StaticObjectsCore.Type
    type TypeScheme		= StaticObjectsCore.TypeScheme
    type TyVar			= StaticObjectsCore.TyVar
    type TyName			= StaticObjectsCore.TyName
    type TyVarSet		= StaticObjectsCore.TyVarSet
    type TyNameSet		= StaticObjectsCore.TyNameSet

    type Substitution		= Type.Substitution
    type Realisation		= Type.Realisation
    type 'a TyNameMap		= 'a TyNameMap.map


    (* Operations *)

    val instance :		TypeScheme -> Type list * Type
    val Clos :			Type -> TypeScheme
    val ClosRestricted :	TyVarSet -> Type -> TypeScheme
    val isClosed :		TypeScheme -> bool

    val tyvars :		TypeScheme -> TyVarSet
    val tynames :		TypeScheme -> TyNameSet
    val undetermined :		TypeScheme -> bool StampMap.map
    val normalise :		TypeScheme -> TypeScheme

    val generalises :		TypeScheme * TypeScheme -> bool
    val equals :		TypeScheme * TypeScheme -> bool

    val substitute :		Substitution -> TypeScheme -> TypeScheme
    val realise :		Realisation  -> TypeScheme -> TypeScheme
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML type schemes
 *
 * Definition, Section 4.2, 4.5, and 4.8
 *
 * Note:
 *    Instantiation copies a type (except free type variables).
 *    Closure does not!
 *)

structure TypeScheme :> TYPESCHEME =
struct
    (* Import *)

    open StaticObjectsCore

    type Substitution = Type.Substitution
    type Realisation  = Type.Realisation
    type 'a TyNameMap = 'a TyNameMap.map


    (* Type variable and type name extraction [Section 4.2] *)

    fun tyvars (alphas,tau) =
	let
	    val U = Type.tyvars tau
	in
	    List.foldl (fn(alpha,U) => TyVarSet.delete(U,alpha)
				       handle TyVarSet.NotFound => U) U alphas
	end

    fun tynames (alphas,tau)      = Type.tynames tau
    fun undetermined (alphas,tau) = Type.undetermined tau



    (* Instantiation *)

    fun instanceTyVar alpha =
	case TyVar.overloadingClass alpha
	  of SOME O => Type.fromOverloadingClass O
	   | NONE   => Type.guess(TyVar.admitsEquality alpha)

    fun instance (alphas,tau) =
	let
	    val taus = List.map instanceTyVar alphas
	    val mu   = TyVarMap.fromList(ListPair.zipEq(alphas, taus))
	in
	    ( taus, Type.substitute mu tau )
	end


    (* Skolemisation *)

    fun skolem (alphas,tau) =
	let
	    val taus = List.map (Type.invent o TyVar.admitsEquality) alphas
	    val mu   = TyVarMap.fromList(ListPair.zipEq(alphas, taus))
	in
	    ( taus, Type.substitute mu tau )
	end


    (* Generalisation [Section 4.5] *)

    fun generalisesType(sigma, tau) =
	    ( Type.unify(#2(instance sigma), tau) ; true )
	    handle Type.Unify => false

    fun generalises(sigma1, sigma2) =
	    generalisesType(sigma1, #2(skolem sigma2))


    (* Comparison [Section 4.5] *)

    fun equals(sigma1, sigma2) =
	generalises(sigma1, sigma2) andalso generalises(sigma2, sigma1)


    (* Closure [Section 4.8] *)

    fun Clos tau =
	    (* Does not copy! *)
	    ( TyVarSet.listItems(Type.tyvars tau), tau )

    fun ClosRestricted U tau =
	    ( TyVarSet.listItems(TyVarSet.difference(Type.tyvars tau, U)), tau )

    fun isClosed (alphas,tau) =
	TyVarSet.isSubset(Type.tyvars tau, TyVarSet.fromList alphas)


    (* Normalisation (for output) *)

    fun normalise (alphas,tau) =
	let
	    val ns      = List.tabulate(List.length alphas, fn n => n) 
	    val alphas' = ListPair.mapEq (fn(alpha, n) =>
					  TyVar.fromInt
					      (TyVar.admitsEquality alpha) n)
					 (alphas, ns)
	    val taus    = List.map Type.fromTyVar alphas'
	    val mu      = TyVarMap.fromList(ListPair.zipEq(alphas, taus))
	in
	    ( alphas', Type.substitute mu tau )
	end


    (* Realisation [Section 5.2] and substitution *)

    fun realise phi (alphas,tau) = (alphas, Type.realise phi tau)

    fun substitute mu (alphas,tau) =
	let
	    val mu' = List.foldl (fn(alpha, mu) => TyVarMap.delete(mu, alpha))
				 mu alphas
	in
	    (alphas, Type.substitute mu' tau)
	end
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML environments of the static semantics of the core
 *
 * Definition, Sections 4.2, 4.3, 4.8, 4.9, and 5.5
 *
 * Note:
 *     We call the domain type of value environments ValStr.
 *)

structure StaticEnv :> STATIC_ENV =
struct
    (* Inheritance *)

    structure GenericEnv = GenericEnvFn(open StaticObjectsCore
					fun unEnv(Env E) = E)
    open GenericEnv


    (* Import *)

    open StaticObjectsCore

    type Realisation = Type.Realisation


    (* Further modifications [Section 4.3] *)

    infix TEplus

    fun TE' TEplus (Env(SE,TE,VE)) = Env(SE, TyConMap.unionWith #2 (TE',TE), VE)


    (* Type variable and type name set [Section 4.2] *)

    fun collect (empty, union, collectTypeScheme, collectTypeFcn) =
	let
	    fun collectVE(VE : ValEnv) =
		VIdMap.foldl
		    (fn((sigma,is), S) => union(S, collectTypeScheme sigma))
		    empty VE

	    fun collectTE(TE : TyEnv) =
		TyConMap.foldl
		    (fn((theta,VE), S) => union(union(S, collectTypeFcn theta),
						collectVE VE)) empty TE
	    fun collectSE(SE : StrEnv) =
		StrIdMap.foldl (fn(E, S) => union(S, collect E)) empty SE

	    and collect(Env(SE,TE,VE)) =
		union(union(collectSE SE, collectTE TE), collectVE VE)
	in
	    (collect, collectSE, collectTE, collectVE)
	end

    val (tyvars, tyvarsSE, tyvarsTE, tyvarsVE) =
	collect(TyVarSet.empty, TyVarSet.union,
		TypeScheme.tyvars, TypeFcn.tyvars)
    val (tynames, tynamesSE, tynamesTE, tynamesVE) =
	collect(TyNameSet.empty, TyNameSet.union,
		TypeScheme.tynames, TypeFcn.tynames)
    val (undetermined, _, _, _) =
	collect(StampMap.empty, StampMap.unionWith #2,
		TypeScheme.undetermined, TypeFcn.undetermined)


    (* Well-formedness [Section 4.9] *)

    fun isWellFormedTyStr (theta,VE) =
	VIdMap.isEmpty VE orelse isSome(TypeFcn.toTyName theta)

    fun isWellFormedTE TE =
	TyConMap.all isWellFormedTyStr TE

    fun isWellFormedSE SE =
	StrIdMap.all isWellFormed SE

    and isWellFormed (Env(SE,TE,VE)) =
	isWellFormedTE TE andalso isWellFormedSE SE



    (* Closure [Section 4.8] *)

    fun Clos VE =
	VIdMap.map (fn((_,tau), is) => (TypeScheme.Clos tau, is)) VE


    (* Realisation [Section 5.2] *)

    fun realiseVE phi VE =
	VIdMap.map (fn(sigma,is) => ( TypeScheme.realise phi sigma, is )) VE

    and realiseTE phi TE =
	TyConMap.map (fn(theta,VE) => ( TypeFcn.realise phi theta
				      , realiseVE phi VE
				      )) TE
    and realiseSE phi SE =
	StrIdMap.map (realise phi) SE

    and realise phi (Env(SE,TE,VE)) =
	Env( realiseSE phi SE
	   , realiseTE phi TE
	   , realiseVE phi VE
	   )


    (* Maximise equality of a type environment [Section 4.9],
     * together with its companion value environment
     *)

    fun respectsEqualityValStr ((alphas, ref(FunType(tau, _))), is) =
	    TypeFcn.admitsEquality (alphas, tau)
      | respectsEqualityValStr _ = true

    fun respectsEquality ((alphas,tau), VE) =
	let
	    val t = Type.tyname tau
	in
	    if TyName.admitsEquality t then
		TyName.toString t = "ref" orelse
		VIdMap.all respectsEqualityValStr VE
	    else
		true
	end

    fun maximiseEquality(TE,VE) =
	let
	    fun checkTyStr((theta, VE), (phi, change)) =
		if respectsEquality (theta,VE) then ( phi, change ) else
		let
		    val t      = Option.valOf(TypeFcn.toTyName theta)
		    val theta' = TypeFcn.fromTyName(TyName.removeEquality t)
		in
		    ( TyNameMap.insert(phi, t, theta'), true )
		end

	    fun checkTE(TE, phi) =
		let
		    val (phi',change) = TyConMap.foldl checkTyStr (phi,false) TE
		    val TE'           = realiseTE phi' TE
		in
		    if change then checkTE(TE', phi')
			      else (TE', phi')
		end

	    val (TE',phi) = checkTE(TE, TyNameMap.empty)
	in
	    ( TE', realiseVE phi VE )
	end


    (* Abstraction of a type environment [Section 4.9] *)

    fun AbsTE(TE) = TyConMap.map (fn(theta,VE) => (theta,VIdMap.empty)) TE

    fun Abs(TE,E) =
	let
	    val ts  = TyConMap.foldl (fn((theta,VE), ts) =>
				      valOf (TypeFcn.toTyName theta)::ts) [] TE
	    val phi = List.foldl
			(fn(t,phi) => TyNameMap.insert(phi, t,
					TypeFcn.fromTyName(TyName.Abs t)))
			TyNameMap.empty ts
	in
	    realise phi (AbsTE(TE) TEplus E)
	end


    (* Disjointness *)

    fun disjoint(Env(SE1,TE1,VE1), Env(SE2,TE2,VE2)) =
	    StrIdMap.disjoint(SE1,SE2) andalso
	    TyConMap.disjoint(TE1,TE2) andalso
	    VIdMap.disjoint(VE1,VE2)


    (* Enrichment [Section 5.5] *)

    fun equalsVE(VE1,VE2) =
	VIdMap.numItems VE1 = VIdMap.numItems VE2 andalso
	VIdMap.alli
	    (fn(vid, (sigma1,is1)) =>
		case VIdMap.find(VE2, vid)
		  of NONE             => false
		   | SOME(sigma2,is2) =>
			TypeScheme.equals(sigma1,sigma2) andalso is1 = is2
	    )
	    VE1


    fun enriches(Env(SE1,TE1,VE1), Env(SE2,TE2,VE2)) =
	    enrichesSE(SE1,SE2) andalso
	    enrichesTE(TE1,TE2) andalso
	    enrichesVE(VE1,VE2)

    and enrichesSE(SE1,SE2) =
	StrIdMap.alli
	    (fn(strid, E2) =>
		case StrIdMap.find(SE1, strid)
		  of NONE    => false
		   | SOME E1 => enriches(E1,E2)
	    )
	    SE2

    and enrichesTE(TE1,TE2) =
	TyConMap.alli
	    (fn(tycon, tystr2) =>
		case TyConMap.find(TE1, tycon)
		  of NONE        => false
		   | SOME tystr1 => enrichesTyStr(tystr1,tystr2)
	    )
	    TE2

    and enrichesVE(VE1,VE2) =
	VIdMap.alli
	    (fn(vid, valstr2) =>
		case VIdMap.find(VE1, vid)
		  of NONE         => false
		   | SOME valstr1 => enrichesValStr(valstr1,valstr2)
	    )
	    VE2

    and enrichesTyStr((theta1,VE1), (theta2,VE2)) =
	    TypeFcn.equals(theta1,theta2) andalso
	    ( VIdMap.isEmpty VE2 orelse equalsVE(VE1,VE2) )

    and enrichesValStr((sigma1,is1), (sigma2,is2)) =
	    TypeScheme.generalises(sigma1,sigma2) andalso
	    IdStatus.generalises(is1,is2)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML contexts
 *
 * Definition, Sections 4.2, 4.3, 4.7, and 4.9
 *)

signature CONTEXT =
sig
    (* Import *)

    type VId		= IdsCore.VId
    type TyCon		= IdsCore.TyCon
    type StrId		= IdsCore.StrId
    type longVId	= IdsCore.longVId
    type longTyCon	= IdsCore.longTyCon
    type longStrId	= IdsCore.longStrId

    type TyNameSet	= StaticObjectsCore.TyNameSet
    type TyVarSet	= StaticObjectsCore.TyVarSet
    type TyStr		= StaticObjectsCore.TyStr
    type TyEnv		= StaticObjectsCore.TyEnv
    type ValStr		= StaticObjectsCore.ValStr
    type ValEnv		= StaticObjectsCore.ValEnv
    type Env		= StaticObjectsCore.Env
    type Context	= StaticObjectsCore.Context


    (* Operations *)

    val Tof :		Context -> TyNameSet
    val Uof :		Context -> TyVarSet
    val Eof :		Context -> Env

    val plusVE :	Context * ValEnv   -> Context
    val plusU :		Context * TyVarSet -> Context
    val oplusE :	Context * Env      -> Context
    val oplusTE :	Context * TyEnv    -> Context
    val oplusVEandTE :	Context * (ValEnv * TyEnv) -> Context

    val findVId :	Context * VId       -> ValStr option
    val findTyCon :	Context * TyCon     -> TyStr option
    val findStrId :	Context * StrId     -> Env option
    val findLongVId :	Context * longVId   -> ValStr option
    val findLongTyCon :	Context * longTyCon -> TyStr option
    val findLongStrId :	Context * longStrId -> Env option

    val tyvars :	Context -> TyVarSet
    val undetermined :	Context -> bool StampMap.map
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML contexts
 *
 * Definition, Sections 4.2, 4.3, 4.7, and 4.9
 *)

structure Context :> CONTEXT =
struct
    (* Import *)

    open IdsCore
    open StaticObjectsCore


    (* Projections [Section 4.3] *)

    fun Tof (T,U,E) = T
    fun Uof (T,U,E) = U
    fun Eof (T,U,E) = E


    (* Modification [Section 4.3] *)

    infix plusVE plusU oplusE oplusTE oplusVEandTE

    fun (T,U,E) plusVE VE = ( T, U, StaticEnv.plusVE(E,VE) )
    fun (T,U,E) plusU  U' = ( T, TyVarSet.union(U,U'), E )

    fun (T,U,E) oplusE E' =
	( TyNameSet.union(T, StaticEnv.tynames E')
	, U
	, StaticEnv.plus(E,E')
	)

    fun (T,U,E) oplusTE TE =
	( TyNameSet.union(T, StaticEnv.tynamesTE TE)
	, U
	, StaticEnv.plusTE(E,TE)
	)

    fun (T,U,E) oplusVEandTE (VE,TE) =
	( TyNameSet.union(T, StaticEnv.tynamesTE TE)
	, U
	, StaticEnv.plusVEandTE(E, (VE,TE))
	)


    (* Application (lookup) [Section 4.3] *)

    fun findVId  ((T,U,E), vid)   = StaticEnv.findVId(E, vid)
    fun findTyCon((T,U,E), tycon) = StaticEnv.findTyCon(E, tycon)
    fun findStrId((T,U,E), strid) = StaticEnv.findStrId(E, strid)

    fun findLongVId  ((T,U,E), longvid)   = StaticEnv.findLongVId(E,longvid)
    fun findLongTyCon((T,U,E), longtycon) = StaticEnv.findLongTyCon(E,longtycon)
    fun findLongStrId((T,U,E), longstrid) = StaticEnv.findLongStrId(E,longstrid)


    (* Calculation of tyvars [Section 4.2] *)

    fun tyvars (T,U,E)       = TyVarSet.union(U, StaticEnv.tyvars E)
    fun undetermined (T,U,E) = StaticEnv.undetermined E
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML signatures
 *
 * Definition, Sections 5.1, 5.3, and 5.6
 *)

signature SIG =
sig
    (* Import types *)

    type TyVarSet    = StaticObjectsCore.TyVarSet
    type TyNameSet   = StaticObjectsCore.TyNameSet
    type Env         = StaticObjectsCore.Env
    type Sig         = StaticObjectsModule.Sig
    type Realisation = Type.Realisation


    (* Operations *)

    val tyvars :	Sig -> TyVarSet
    val tynames :	Sig -> TyNameSet
    val undetermined :	Sig -> bool StampMap.map

    val rename :	Sig -> Sig

    exception Match
    val match :		Env * Sig -> Env * Realisation (* Matching *)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML signatures
 *
 * Definition, Sections 5.1, 5.3, and 5.6
 *)

structure Sig :> SIG =
struct
    (* Import *)

    open StaticObjectsCore
    open StaticObjectsModule

    type Realisation = Type.Realisation


    (* Type variable and type name extraction [Section 4.2] *)

    fun tyvars (T,E)       = StaticEnv.tyvars E
    fun tynames (T,E)      = TyNameSet.difference(StaticEnv.tynames E, T)
    fun undetermined (T,E) = StaticEnv.undetermined E


    (* Alpha Renaming *)

    fun rename (T,E) =
	let
	    val phi' = TyNameSet.foldl
			 (fn(t,phi')=> TyNameMap.insert(phi',t,TyName.rename t))
			 TyNameMap.empty T
	    val phi = TyNameMap.map (TypeFcn.rename o TypeFcn.fromTyName) phi'
	    val T'  = TyNameSet.map (fn t => valOf(TyNameMap.find(phi',t))) T
	    val E'  = StaticEnv.realise phi E
	in
	    (T',E')
	end


    (* Matching [Section 5.6] *)

    exception Match

    fun matchTheta(theta', theta, phi, T) =
	if TypeFcn.arity theta <> TypeFcn.arity theta' then
	    raise Match
	else
	case TypeFcn.toTyName theta
	  of NONE   => phi
	   | SOME t =>
		if not(TyNameSet.member(T, t))
		orelse TyNameMap.inDomain(phi, t) then
		    phi
		else if TyName.admitsEquality t
		andalso not(TypeFcn.admitsEquality theta') then
		    raise Match
		else
		let
		    val phi' = TyNameMap.insert(phi, t, TypeFcn.rename theta')
		in
		    TyNameMap.map (TypeFcn.realise phi') phi'
		end

    fun matchTE(TE', TE, phi, T) =
	let
	    fun matchTyStr(tycon, (theta,VE), phi) =
		case TyConMap.find(TE', tycon)
		  of NONE             => raise Match
		   | SOME(theta',VE') => matchTheta(theta', theta, phi, T)
	in
	    TyConMap.foldli matchTyStr phi TE
	end

    fun matchSE(SE', SE, phi, T) =
	let
	    fun matchStr(strid, E, phi) =
		case StrIdMap.find(SE', strid)
		  of NONE    => raise Match
		   | SOME E' => matchE(E', E, phi, T)
	in
	    StrIdMap.foldli matchStr phi SE
	end

    and matchE(Env(SE',TE',VE'), Env(SE,TE,VE), phi, T) =
	let
	    val phi1 = matchTE(TE', TE, phi, T)
	    val phi2 = matchSE(SE', SE, phi1, T)
	in
	    phi2
	end

    fun match(E', (T,E)) =
	let
	    val phi    = matchE(E', E, TyNameMap.empty, T)
	    val Eminus = StaticEnv.realise phi E
	in
	    if StaticEnv.enriches(E',Eminus) then
		(Eminus, phi)
	    else
		raise Match
	end
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML functor signatures
 *
 * Definition, Sections 5.1 and 5.4
 *)

signature FUNSIG =
sig
    (* Import *)

    type TyVarSet	= StaticObjectsCore.TyVarSet
    type TyNameSet	= StaticObjectsCore.TyNameSet
    type FunSig		= StaticObjectsModule.FunSig


    (* Operations *)

    val tyvars :	FunSig -> TyVarSet
    val tynames :	FunSig -> TyNameSet
    val undetermined :	FunSig -> bool StampMap.map
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML functor signatures
 *
 * Definition, Sections 5.1 and 5.4
 *)

structure FunSig :> FUNSIG =
struct
    (* Import types *)

    open StaticObjectsCore
    open StaticObjectsModule


    (* Type variable and type name extraction [Section 4.2] *)

    fun tyvars (T,(E,Sigma)) =
	TyVarSet.union(StaticEnv.tyvars E, Sig.tyvars Sigma)

    fun tynames (T,(E,Sigma)) =
	TyNameSet.difference(TyNameSet.union(StaticEnv.tynames E,
					     Sig.tynames Sigma), T)
    fun undetermined (T,(E,Sigma)) =
	StampMap.unionWith #2 (StaticEnv.undetermined E, Sig.undetermined Sigma)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML objects for binding analysis of the core
 *
 * Definition, Section 2.9
 *
 * Notes:
 *   - The "syntactic restrictions" defined in the Definition are not purely
 *     syntactic. E.g. the restriction that valbinds may not bind the same vid
 *     twice [2nd bullet] cannot be checked without proper binding analysis,
 *     to compute identifier status.
 *   - Also, checking of type variable shadowing [last bullet] is a global
 *     check dependent on context. Moreover, it requires the transformation from
 *     Section 4.6 to be done first.
 *)

structure BindingObjectsCore =
struct
    (* Import *)

    type 'a VIdMap	= 'a IdsCore.VIdMap
    type 'a TyConMap	= 'a IdsCore.TyConMap
    type 'a StrIdMap	= 'a IdsCore.StrIdMap

    type IdStatus	= IdStatus.IdStatus
    type TyVarSet	= TyVarSet.set


    (* Types *)

    type ValStr		= IdStatus
    type ValEnv		= IdStatus VIdMap
    type TyStr		= ValEnv
    type TyEnv		= ValEnv TyConMap
    datatype Env	= Env of StrEnv * TyEnv * ValEnv
    withtype StrEnv	= Env StrIdMap

    type Context	= TyVarSet * Env
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML objects for binding analysis of modules
 *
 * Definition, Sections 2.9 and 3.5
 *
 * Notes:
 *   - The "syntactic restrictions" defined in the Definition are not purely
 *     syntactic. E.g. the restriction that valbinds may not bind the same vid
 *     twice [2nd bullet] cannot be checked without proper binding analysis,
 *     to compute identifier status.
 *)

structure BindingObjectsModule =
struct
    (* Import *)

    type 'a SigIdMap		= 'a IdsModule.SigIdMap
    type 'a FunIdMap		= 'a IdsModule.FunIdMap

    type Env			= BindingObjectsCore.Env


    (* Types *)

    type SigEnv			= Env SigIdMap
    type FunEnv			= Env FunIdMap
    type Basis			= FunEnv * SigEnv * Env
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML environment for binding analysis
 *)

signature BINDING_BASIS =
sig
    (* Import *)

    type StrId		= IdsCore.StrId
    type SigId		= IdsModule.SigId
    type FunId		= IdsModule.FunId
    type longStrId	= IdsCore.longStrId
    type longTyCon	= IdsCore.longTyCon

    type Env		= BindingObjectsCore.Env
    type ValEnv		= BindingObjectsCore.ValEnv
    type StrEnv		= BindingObjectsCore.StrEnv
    type Context	= BindingObjectsCore.Context

    type SigEnv		= BindingObjectsModule.SigEnv
    type FunEnv		= BindingObjectsModule.FunEnv
    type Basis		= BindingObjectsModule.Basis


    (* Operations *)

    val empty :		Basis
    val fromE :		Env    -> Basis
    val fromF :		FunEnv -> Basis
    val fromG :		SigEnv -> Basis

    val Cof :		Basis -> Context

    val plus :		Basis * Basis     -> Basis
    val plusSE :	Basis * StrEnv    -> Basis
    val plusG :		Basis * SigEnv    -> Basis
    val plusF :		Basis * FunEnv    -> Basis
    val plusE :		Basis * Env       -> Basis

    val findStrId :	Basis * StrId     -> Env option
    val findSigId :	Basis * SigId     -> Env option
    val findFunId :	Basis * FunId     -> Env option
    val findLongStrId :	Basis * longStrId -> Env option
    val findLongTyCon :	Basis * longTyCon -> ValEnv option
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML environment for binding analysis
 *)

signature BINDING_ENV =
sig
    (* Inheritance *)

    include GENERIC_ENV
    where type Env		= BindingObjectsCore.Env
    and   type ValStr		= BindingObjectsCore.IdStatus
    and   type TyStr		= BindingObjectsCore.ValEnv
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML environment for binding analysis
 *)

structure BindingEnv : BINDING_ENV =
struct
    (* Inheritance *)

    structure GenericEnv = GenericEnvFn(open BindingObjectsCore
					type ValStr = IdStatus
					type TyStr  = ValEnv
					fun unEnv(Env E) = E)
    open GenericEnv
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML environment for binding analysis
 *)

structure BindingBasis : BINDING_BASIS =
struct
    (* Import *)

    open IdsCore
    open IdsModule
    open BindingObjectsCore
    open BindingObjectsModule


    (* Injection [Sections 4.3 and 5.1] *)

    val empty   = ( FunIdMap.empty, SigIdMap.empty, BindingEnv.empty )

    fun fromE E = ( FunIdMap.empty, SigIdMap.empty, E )
    fun fromF F = ( F, SigIdMap.empty, BindingEnv.empty )
    fun fromG G = ( FunIdMap.empty, G, BindingEnv.empty )


    (* Projections [Sections 4.3 and 5.1] *)

    fun Cof (F,G,E) = (TyVarSet.empty, E)


    (* Modifications [Sections 4.3 and 5.1] *)

    infix plus plusG plusF plusE plusSE

    fun (F,G,E) plus (F',G',E') =
	( FunIdMap.unionWith #2 (F,F')
	, SigIdMap.unionWith #2 (G,G')
	, BindingEnv.plus(E,E')
	)

    fun (F,G,E) plusG  G' = ( F, SigIdMap.unionWith #2 (G,G'), E )
    fun (F,G,E) plusF  F' = ( FunIdMap.unionWith #2 (F,F'), G, E )
    fun (F,G,E) plusE  E' = ( F, G, BindingEnv.plus(E,E') )
    fun (F,G,E) plusSE SE = ( F, G, BindingEnv.plusSE(E,SE) )


    (* Application (lookup) [Sections 5.1 and 4.3] *)

    fun findStrId((F,G,E), strid) = BindingEnv.findStrId(E, strid)
    fun findSigId((F,G,E), sigid) = SigIdMap.find(G, sigid)
    fun findFunId((F,G,E), funid) = FunIdMap.find(F, funid)
    fun findLongStrId((F,G,E), longstrid) =
	BindingEnv.findLongStrId(E, longstrid)
    fun findLongTyCon((F,G,E), longtycon) =
	BindingEnv.findLongTyCon(E, longtycon)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML static basis and environments of modules
 *
 * Definition, Section 5.1
 *)

signature STATIC_BASIS =
sig
    (* Import *)

    type StrId		= IdsCore.StrId
    type SigId		= IdsModule.SigId
    type FunId		= IdsModule.FunId
    type longStrId	= IdsCore.longStrId
    type longTyCon	= IdsCore.longTyCon

    type Env		= StaticObjectsCore.Env
    type StrEnv		= StaticObjectsCore.StrEnv
    type TyStr		= StaticObjectsCore.TyStr
    type Context	= StaticObjectsCore.Context
    type TyVarSet	= StaticObjectsCore.TyVarSet
    type TyNameSet	= StaticObjectsCore.TyNameSet

    type Sig		= StaticObjectsModule.Sig
    type FunSig		= StaticObjectsModule.FunSig
    type SigEnv		= StaticObjectsModule.SigEnv
    type FunEnv		= StaticObjectsModule.FunEnv
    type Basis		= StaticObjectsModule.Basis


    (* Operations *)

    val empty :		Basis
    val fromTandE :	TyNameSet * Env    -> Basis
    val fromTandF :	TyNameSet * FunEnv -> Basis
    val fromTandG :	TyNameSet * SigEnv -> Basis

    val Tof :		Basis -> TyNameSet
    val Cof :		Basis -> Context

    val plus :		Basis * Basis     -> Basis
    val plusT :		Basis * TyNameSet -> Basis
    val oplusSE :	Basis * StrEnv    -> Basis
    val oplusG :	Basis * SigEnv    -> Basis
    val oplusF :	Basis * FunEnv    -> Basis
    val oplusE :	Basis * Env       -> Basis

    val findStrId :	Basis * StrId     -> Env option
    val findSigId :	Basis * SigId     -> Sig option
    val findFunId :	Basis * FunId     -> FunSig option
    val findLongStrId :	Basis * longStrId -> Env option
    val findLongTyCon :	Basis * longTyCon -> TyStr option

    val tyvars :	Basis  -> TyVarSet
    val tynames :	Basis  -> TyNameSet
    val tynamesF :	FunEnv -> TyNameSet
    val tynamesG :	SigEnv -> TyNameSet
    val undetermined :	Basis  -> bool StampMap.map

    val toBindingBasis : Basis -> BindingBasis.Basis
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML static basis and environments of modules
 *
 * Definition, Section 5.1
 *)

structure StaticBasis :> STATIC_BASIS =
struct
    (* Import *)

    open IdsCore
    open IdsModule
    open StaticObjectsCore
    open StaticObjectsModule


    (* Calculation of type variable and type name sets [Section 4.2] *)

    fun collect (empty, union, collectSig, collectFunSig, collectEnv) =
	let
	    fun collectG(G : SigEnv) =
		SigIdMap.foldl
		    (fn(Sigma, S) => union(S, collectSig Sigma)) empty G

	    fun collectF(F : FunEnv) =
		FunIdMap.foldl
		    (fn(Phi, S) => union(S, collectFunSig Phi)) empty F

	    fun collect((T,F,G,E) : Basis) =
		union(union(collectF F, collectG G), collectEnv E)
	in
	    (collect, collectF, collectG)
	end

    val (tyvars, tyvarsF, tyvarsG) =
	collect(TyVarSet.empty, TyVarSet.union,
		Sig.tyvars, FunSig.tyvars, StaticEnv.tyvars)
    val (tynames', tynamesF, tynamesG) =
	collect(TyNameSet.empty, TyNameSet.union,
		Sig.tynames, FunSig.tynames, StaticEnv.tynames)

    fun tynames(B as (T,F,G,E)) = TyNameSet.union(T, tynames' B)

    val (undetermined, undeterminedF, _) =
	collect(StampMap.empty, StampMap.unionWith #2,
		Sig.undetermined, FunSig.undetermined, StaticEnv.undetermined)


    (* Injection [Sections 4.3 and 5.1] *)

    val empty =
	( TyNameSet.empty, FunIdMap.empty, SigIdMap.empty, StaticEnv.empty )

    fun fromTandE(T,E) = ( T, FunIdMap.empty, SigIdMap.empty, E )
    fun fromTandF(T,F) = ( T, F, SigIdMap.empty, StaticEnv.empty )
    fun fromTandG(T,G) = ( T, FunIdMap.empty, G, StaticEnv.empty )


    (* Projections [Sections 4.3 and 5.1] *)

    fun Tof (T,F,G,E) = T
    fun Cof (T,F,G,E) = (T, TyVarSet.empty, E)


    (* Modifications [Sections 4.3 and 5.1] *)

    infix plus plusT oplusG oplusF oplusE oplusSE

    fun (T,F,G,E) plus (T',F',G',E') =
	( TyNameSet.union(T,T')
	, FunIdMap.unionWith #2 (F,F')
	, SigIdMap.unionWith #2 (G,G')
	, StaticEnv.plus(E,E')
	)

    fun (T,F,G,E) plusT T' = ( TyNameSet.union(T,T'), F, G, E )

    fun (T,F,G,E) oplusG G' =
	( TyNameSet.union(T, tynamesG G')
	, F
	, SigIdMap.unionWith #2 (G,G')
	, E
	)

    fun (T,F,G,E) oplusF F' =
	( TyNameSet.union(T, tynamesF F')
	, FunIdMap.unionWith #2 (F,F')
	, G
	, E
	)

    fun (T,F,G,E) oplusE E' =
	( TyNameSet.union(T, StaticEnv.tynames E')
	, F
	, G
	, StaticEnv.plus(E,E')
	)

    fun (T,F,G,E) oplusSE SE =
	( TyNameSet.union(T, StaticEnv.tynamesSE SE)
	, F
	, G
	, StaticEnv.plusSE(E,SE)
	)

    (* Application (lookup) [Sections 5.1 and 4.3] *)

    fun findStrId((T,F,G,E), strid) = StaticEnv.findStrId(E, strid)
    fun findSigId((T,F,G,E), sigid) = SigIdMap.find(G, sigid)
    fun findFunId((T,F,G,E), funid) = FunIdMap.find(F, funid)
    fun findLongStrId((T,F,G,E), longstrid) =
	StaticEnv.findLongStrId(E, longstrid)
    fun findLongTyCon((T,F,G,E), longtycon) =
	StaticEnv.findLongTyCon(E, longtycon)


    (* Conversion to binding basis *)

    fun toBindingValEnv VE = VIdMap.map (fn(sigma,is) => is) VE
    fun toBindingTyEnv TE = TyConMap.map (fn(theta,VE) => toBindingValEnv VE) TE
    fun toBindingStrEnv SE = StrIdMap.map toBindingEnv SE
    and toBindingEnv(Env(SE,TE,VE)) =
	BindingObjectsCore.Env
	    (toBindingStrEnv SE, toBindingTyEnv TE, toBindingValEnv VE)

    fun toBindingSigEnv G = SigIdMap.map (fn(T,E) => toBindingEnv E) G
    fun toBindingFunEnv F = FunIdMap.map (fn(T,(E,(T',E')))=> toBindingEnv E') F

    fun toBindingBasis (T,F,G,E) =
	( toBindingFunEnv F, toBindingSigEnv G, toBindingEnv E )
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML state
 *
 * Definition, Section 6.3
 *
 * Notes:
 *   - Memory gets represented by references. This avoids expanding out all
 *     occurances of the state convention in the inference rules.
 *   - Since exception names are generated by stamps we do not really need the
 *     exception name set. We maintain it anyway.
 *)

signature STATE =
sig
    (* Import *)

    type Addr		= DynamicObjectsCore.Addr
    type ExName		= DynamicObjectsCore.ExName
    type Val		= DynamicObjectsCore.Val
    type State		= DynamicObjectsCore.State


    (* Operations *)

    val insertAddr :	State * Addr * Val -> State
    val insertExName :	State * ExName     -> State

    val findAddr :	State * Addr -> Val option
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML state
 *
 * Definition, Section 6.3
 *
 * Notes:
 *   - Memory gets represented by references. This avoids expanding out all
 *     occurances of the state convention in the inference rules.
 *   - Since exception names are generated by stamps we do not really need the
 *     exception name set. We maintain it anyway.
 *)

structure State :> STATE =
struct
    (* Import *)

    type Addr		= DynamicObjectsCore.Addr
    type ExName		= DynamicObjectsCore.ExName
    type Val		= DynamicObjectsCore.Val
    type State		= DynamicObjectsCore.State


    (* Operations *)

    fun insertAddr  ((mem,ens), a, v) = ( AddrMap.insert(mem, a, v), ens )
    fun insertExName((mem,ens), en)   = ( mem, ExNameSet.add(ens, en) )

    fun findAddr((mem,ens), a) = AddrMap.find(mem, a)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML dynamic basis and environments of modules
 *
 * Definition, Section 7.2
 *)

signature DYNAMIC_BASIS =
sig
    (* Import *)

    type StrId		= IdsCore.StrId
    type SigId		= IdsModule.SigId
    type FunId		= IdsModule.FunId
    type longStrId	= IdsCore.longStrId
    type longTyCon	= IdsCore.longTyCon

    type Env		= DynamicObjectsCore.Env
    type ValEnv		= DynamicObjectsCore.ValEnv
    type StrEnv		= DynamicObjectsCore.StrEnv
    type SigEnv		= DynamicObjectsModule.SigEnv
    type FunEnv		= DynamicObjectsModule.FunEnv
    type Int		= DynamicObjectsModule.Int
    type FunctorClosure	= DynamicObjectsModule.FunctorClosure
    type Basis		= DynamicObjectsModule.Basis


    (* Operations *)

    val empty :		Basis
    val fromE :		Env    -> Basis
    val fromF :		FunEnv -> Basis
    val fromG :		SigEnv -> Basis

    val Eof :		Basis -> Env

    val plus :		Basis * Basis     -> Basis
    val plusSE :	Basis * StrEnv    -> Basis
    val plusG :		Basis * SigEnv    -> Basis
    val plusF :		Basis * FunEnv    -> Basis
    val plusE :		Basis * Env       -> Basis

    val findStrId :	Basis * StrId     -> Env option
    val findSigId :	Basis * SigId     -> Int option
    val findFunId :	Basis * FunId     -> FunctorClosure option
    val findLongStrId :	Basis * longStrId -> Env option
    val findLongTyCon :	Basis * longTyCon -> ValEnv option
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML interfaces
 *
 * Definition, Section 7.2
 *)

signature INTER =
sig
    (* Import *)

    type longTyCon	= IdsCore.longTyCon

    type Env		= DynamicObjectsCore.Env
    type Int		= DynamicObjectsModule.Int
    type ValInt		= DynamicObjectsModule.ValInt
    type TyInt		= DynamicObjectsModule.TyInt
    type StrInt		= DynamicObjectsModule.StrInt


    (* Operations *)

    val empty :		Int

    val fromSI :	StrInt -> Int
    val fromTI :	TyInt  -> Int
    val fromVI :	ValInt -> Int
    val fromVIandTI :	ValInt * TyInt -> Int

    val SIof :		Int -> StrInt
    val TIof :		Int -> TyInt
    val VIof :		Int -> ValInt

    val plus :		Int * Int -> Int

    val findLongTyCon :	Int * longTyCon -> ValInt option

    val Inter :		Env -> Int
    val cutdown :	Env * Int -> Env
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML environments of the dynamic semantics of the core
 *
 * Definition, Sections 6.3 and 6.6
 *)

signature DYNAMIC_ENV =
sig
    (* Inheritance *)

    include GENERIC_ENV
    where type Env	= DynamicObjectsCore.Env
    and   type ValStr	= DynamicObjectsCore.ValStr
    and   type TyStr	= DynamicObjectsCore.ValEnv


    (* Operations *)

    val Rec :		ValEnv -> ValEnv
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML environments of the dynamic semantics of the core
 *
 * Definition, Sections 6.3 and 6.6
 *)

structure DynamicEnv :> DYNAMIC_ENV =
struct
    (* Inheritance *)

    structure GenericEnv = GenericEnvFn(open DynamicObjectsCore
					type TyStr = ValEnv
					fun unEnv(Env E) = E)
    open GenericEnv
    open DynamicObjectsCore


    (* Unrolling [Section 6.6] *)

    fun Rec VE =
	    VIdMap.map
		(fn (FcnClosure(match',E',VE'), IdStatus.v) =>
		    (FcnClosure(match',E',VE), IdStatus.v)
		  | valstr => valstr
		) VE
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML interfaces
 *
 * Definition, Section 7.2
 *)

structure Inter :> INTER =
struct
    (* Import *)

    open IdsCore
    open DynamicObjectsModule


    (* Inheritance *)

    structure GenericEnv = GenericEnvFn(type Env    = Int
					type ValStr = IdStatus
					type TyStr  = ValInt
					val Env     = Int
					fun unEnv(Int I) = I)

    open DynamicObjectsCore


    (* Injections [Section 4.3] *)

    val empty		= GenericEnv.empty

    val fromSI		= GenericEnv.fromSE
    val fromTI		= GenericEnv.fromTE
    val fromVI		= GenericEnv.fromVE
    val fromVIandTI	= GenericEnv.fromVEandTE


    (* Projections [Section 4.3] *)

    val SIof		= GenericEnv.SEof
    val TIof		= GenericEnv.TEof
    val VIof		= GenericEnv.VEof


    (* Modification [Section 4.3] *)

    val plus		= GenericEnv.plus


    (* Extracting interfaces from environments [Section 7.2] *)

    fun InterVE VE = VIdMap.map (fn(v,is) => is) VE
    fun InterTE TE = TyConMap.map (fn VE => InterVE VE) TE
    fun InterSE SE = StrIdMap.map (fn E => Inter E) SE

    and Inter(Env(SE,TE,VE)) = Int(InterSE SE, InterTE TE, InterVE VE)


    (* Modification [Lookup 4.3] *)

    val findLongTyCon = GenericEnv.findLongTyCon


    (* Cutting down environments [Section 7.2] *)

    fun cutdownVE(VE, VI) =
	VIdMap.foldli
	    (fn(vid, is, VE') =>
		case VIdMap.find(VE, vid)
		  of SOME(v,is') => VIdMap.insert(VE', vid, (v,is))
		   | NONE        => VE'
	    ) VIdMap.empty VI

    fun cutdownTE(TE, TI) =
	TyConMap.foldli
	    (fn(tycon, VI', TE') =>
		case TyConMap.find(TE, tycon)
		  of SOME VE' => TyConMap.insert(TE', tycon, cutdownVE(VE',VI'))
		   | NONE     => TE'
	    ) TyConMap.empty TI

    fun cutdownSE(SE, SI) =
	StrIdMap.foldli
	    (fn(strid, I, SE') =>
		case StrIdMap.find(SE, strid)
		  of SOME E =>
		       StrIdMap.insert(SE', strid, cutdown(E,I))
		   | NONE => SE'
	    ) StrIdMap.empty SI

    and cutdown(Env(SE,TE,VE), Int(SI,TI,VI)) =
	    Env(cutdownSE(SE, SI), cutdownTE(TE, TI), cutdownVE(VE, VI))
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML dynamic basis and environments of modules
 *
 * Definition, Section 7.2
 *)

structure DynamicBasis :> DYNAMIC_BASIS =
struct
    (* Import *)

    open IdsCore
    open IdsModule
    open DynamicObjectsCore
    open DynamicObjectsModule


    (* Injections [Sections 4.3 and 7.2] *)

    val empty = ( FunIdMap.empty, SigIdMap.empty, DynamicEnv.empty )

    fun fromE E = ( FunIdMap.empty, SigIdMap.empty, E )
    fun fromF F = ( F, SigIdMap.empty, DynamicEnv.empty )
    fun fromG G = ( FunIdMap.empty, G, DynamicEnv.empty )


    (* Projections [Sections 4.3 and 7.2] *)

    fun Eof (F,G,E) = E


    (* Modifications [Sections 4.3 and 7.2] *)

    infix plus plusG plusF plusE plusSE IBplusI

    fun (F,G,E) plus (F',G',E') =
	( FunIdMap.unionWith #2 (F,F')
	, SigIdMap.unionWith #2 (G,G')
	, DynamicEnv.plus(E,E')
	)

    fun (F,G,E) plusG  G' = ( F, SigIdMap.unionWith #2 (G,G'), E )
    fun (F,G,E) plusF  F' = ( FunIdMap.unionWith #2 (F,F'), G, E )
    fun (F,G,E) plusE  E' = ( F, G, DynamicEnv.plus(E,E') )
    fun (F,G,E) plusSE SE = ( F, G, DynamicEnv.plusSE(E,SE) )


    (* Application (lookup) [Sections 7.2 and 4.3] *)

    fun findStrId((F,G,E), strid) = DynamicEnv.findStrId(E, strid)
    fun findSigId((F,G,E), sigid) = SigIdMap.find(G, sigid)
    fun findFunId((F,G,E), funid) = FunIdMap.find(F, funid)
    fun findLongStrId((F,G,E), longstrid) =
	DynamicEnv.findLongStrId(E, longstrid)
    fun findLongTyCon((F,G,E), longtycon) =
	DynamicEnv.findLongTyCon(E, longtycon)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML values
 *
 * Definition, Sections 6.2, 6.3, and 6.4
 *
 * Note:
 *   - All value types are parameterised over the representation of function
 *     closures to break up the recursion between values and environments.
 *   - The basic values are just strings.
 *)

signature VAL =
sig
    (* Import *)

    type Val		= DynamicObjectsCore.Val
    type ExVal		= DynamicObjectsCore.ExVal
    type ExName		= DynamicObjectsCore.ExName


    (* Operations *)

    val equal :		Val * Val -> bool

    val exname :	ExVal -> ExName

    val toPair :	Val -> (Val * Val) option
    val toList :	Val -> Val list option
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML values
 *
 * Definition, Sections 6.2, 6.3, and 6.4
 *
 * Note:
 *   - All value types are parameterised over the representation of function
 *     closures to break up the recursion between values and environments.
 *   - The basic values are just represented by strings.
 *)

structure Val :> VAL =
struct
    (* Import *)

    open DynamicObjectsCore


    (* Operations *)

    fun exname(ExName en)	= en
      | exname(ExNameVal(en,v)) = en

    fun toPair(Record r) =
	if LabMap.numItems r <> 2 then NONE else
	(case (LabMap.find(r, Lab.fromInt 1), LabMap.find(r, Lab.fromInt 2))
	   of (SOME v1, SOME v2) => SOME(v1, v2)
	    | _ => NONE
	)
      | toPair _ = NONE

    fun toList v = toList'(v, nil)
    and toList'(VId vid, vs) =
	if vid = VId.fromString "nil" then
	    SOME(List.rev vs)
	else
	    NONE
      | toList'(VIdVal(vid, v), vs) =
	if vid = VId.fromString "::" then
	    case toPair v
	      of SOME(v1, v2) => toList'(v2, v1::vs)
	       | NONE         => NONE
	else
	    NONE
      | toList'(_, vs) = NONE


    (* Implementation of polymorphic equality *)

    fun equal(SVal sv1,          SVal sv2         ) = SVal.equal(sv1, sv2)
      | equal(VId vid1,          VId vid2         ) = vid1 = vid2
      | equal(ExVal(ExName en1), ExVal(ExName en2)) = en1 = en2
      | equal(Addr a1,           Addr a2          ) = a1 = a2

      | equal(VIdVal(vid1, v1), VIdVal(vid2, v2)) =
	    vid1 = vid2 andalso equal(v1, v2)

      | equal(ExVal(ExNameVal(en1,v1)), ExVal(ExNameVal(en2,v2))) =
	    en1 = en2 andalso equal(v1, v2)

      | equal(Record r1, Record r2) =
	    LabMap.numItems r1 = LabMap.numItems r2 andalso
	    LabMap.alli (fn(lab, v1) =>
			   case LabMap.find(r2, lab)
			     of SOME v2 => equal(v1, v2)
			      | NONE    => false
			) r1

      | equal _ = false
end;
(*
 * (c) Andreas Rossberg 2001-2007
 *
 * Standard ML Basis Library `use' function
 *)

signature USE =
sig
    val enqueue : string -> unit		(* may raise Path *)
    val extract : unit -> string option
end;
(*
 * (c) Andreas Rossberg 2001-2007
 *
 * Standard ML Basis Library `use' function
 *)

structure Use : USE =
struct
    val inn = ref [] : string list ref
    val out = ref [] : string list ref

    fun enqueue name =
	if OS.Path.isAbsolute name then
	    inn := name :: !inn
	else
	    let
		val dir  = OS.FileSys.getDir()
		val path = OS.Path.mkAbsolute{path=name, relativeTo=dir}
	    in
		inn := path :: !inn
	    end

    fun extract() =
	case (!inn, !out)
	  of ([], [])   => NONE
	   | (_, s::ss) => (out := ss ; SOME s)
	   | (ss, [])   => (inn := [] ; out := List.rev ss ; extract())
end;
(*
 * (c) Andreas Rossberg 2001-2007
 *
 * Standard ML Basis Library primitives
 *
 * Definition, Sections 6.2, 6.4 and Appendices C, D, and E
 * Standard Basis Specification
 *
 * Notes:
 *   - These defines all entitities of the Standard Basis that are primitive,
 *     because they cannot be implemented in the source language itself:
 *     - either because their behaviour cannot be implemented out of nowhere,
 *     - or because they have special types (ie. overloaded types).
 *   - The initial basis only contains:
 *     - the vector type,
 *     - the overloaded functions,
 *     - the exceptions used by primitives,
 *     - the function `use'.
 *     Everything else can be implemented in the source language from these.
 *     We piggyback the `use' function to allow the actual library
 *     implementation to gain access to the primitives. Use is given the unsafe
 *     type 'a -> 'b in the initial basis. Applying it to a record of type
 *     {b:string} will return the basic value denoted by the string b. The
 *     library implementation should annotate the result type properly to be
 *     type-safe. Of course, it should restrict the type of `use' to
 *     string -> unit in its export environment so that user code cannot use
 *     the unsafe behaviour.
 *   - Primitive constants are implemented as functions from unit.
 *   - Currently Real.toString is also primitive for simplicity.
 *   - The actual behaviour of `use' is implemented by putting all strings into
 *     a queue processed by the interpreter loop after evaluation.
 *   - The dynamic semantics of the Definition does not really allow the direct
 *     addition of arbitrary library types - in general this would require
 *     extending the set Val. Moreover, the APPLY function would need access
 *     to the state (eg. to implement arrays).
 *   - But we can at least encode vectors by abusing the record representation.
 *     Arrays can then be implemented via vectors and references in the source
 *     language (making their implementation type transparent, however).
 *   - I/O types could be implemented magically as indices into a stateful
 *     table.
 *   - In order to approximate the correct semantics as close as possible in
 *     evaluation mode, we allow values of the default type of any overloading
 *     class to be implicitly coerced to other types of the same class.
 *     This way, we can deal with expressions like (Word8.toInt 0wff) in
 *     evaluation mode. Same for type dispatch in overloaded functions.
 *   - Figure 27 of the Definition contains typos in the types of the comparison
 *     operators: their types should be numtxt * numtxt -> bool.
 *   - We do not yet cover all required functionality if the Standard Basis.
 *   - OS.SysErr currently is not in the environment (because its type contains
 *     the non-primitive option type).
 *)

structure Library : LIBRARY =
struct
    (* Import *)

    type StaticBasis		= StaticObjectsModule.Basis
    type DynamicBasis		= DynamicObjectsModule.Basis


    (* Hook file *)

    val file = "all.sml"

    (* Structure, type, exception and value identifiers *)

    val e = IdStatus.e
    val v = IdStatus.v

    val stridIO		= StrId.fromString "IO"
    val stridWord8	= StrId.fromString "Word8"
    val tyconWord8	= TyCon.fromString "word"
    val tyconVector	= TyCon.fromString "vector"

    val vid_Chr		= VId.fromString "Chr"
    val vid_Div		= VId.fromString "Div"
    val vid_Domain	= VId.fromString "Domain"
    val vid_Overflow	= VId.fromString "Overflow"
    val vid_Size	= VId.fromString "Size"
    val vid_Subscript	= VId.fromString "Subscript"
    val vid_Io		= VId.fromString "Io"
    val vid_SysErr	= VId.fromString "SysErr"

    val vidAbs		= VId.fromString "abs"
    val vidNeg		= VId.fromString "~"
    val vidPlus		= VId.fromString "+"
    val vidMinus	= VId.fromString "-"
    val vidTimes	= VId.fromString "*"
    val vidDiv		= VId.fromString "div"
    val vidMod		= VId.fromString "mod"
    val vidBy		= VId.fromString "/"
    val vidLess		= VId.fromString "<"
    val vidGreater	= VId.fromString ">"
    val vidLessEq	= VId.fromString "<="
    val vidGreaterEq	= VId.fromString ">="

    val vidUse		= VId.fromString "use"


    (* Static objects for the Core *)

    open StaticObjectsCore
    open InitialStaticEnv

    (* Types *)

    val tWord8		= TyName.tyname(TyCon.toString tyconWord8, 0, true, 0)
    val tVector		= TyName.tyname(TyCon.toString tyconVector, 1, true, 0)

    val thetaWord8	= TypeFcn.fromTyName tWord8
    val thetaVector	= TypeFcn.fromTyName tVector

    (* Overloading classes [Section E.1] *)

    val Int	= OverloadingClass.make(TyNameSet.singleton tInt,    tInt)
    val Real	= OverloadingClass.make(TyNameSet.singleton tReal,   tReal)
    val Word	= OverloadingClass.make(TyNameSet.fromList[tWord, tWord8],
					tWord)
    val String	= OverloadingClass.make(TyNameSet.singleton tString, tString)
    val Char	= OverloadingClass.make(TyNameSet.singleton tChar,   tChar)
    val WordInt	= OverloadingClass.union(Word, Int)	(* default is 2nd *)
    val RealInt	= OverloadingClass.union(Real, Int)
    val Num	= OverloadingClass.union(Word, RealInt)
    val Txt	= OverloadingClass.union(String, Char)
    val NumTxt	= OverloadingClass.union(Txt, Num)

    (* Type Schemes *)

    fun pairType tau =
	Type.fromRowType(
	    Type.insertRow(Type.insertRow(Type.emptyRow, Lab.fromInt 1, tau),
							 Lab.fromInt 2, tau))

    val sigmaExn	= ([], tauExn)

    val rhoIo		= Type.insertRow(Type.insertRow(Type.insertRow(
				Type.emptyRow,
				Lab.fromString "name", tauString),
				Lab.fromString "function", tauString),
				Lab.fromString "cause", tauExn)
    val tauIo		= Type.fromFunType(Type.fromRowType rhoIo, tauExn)
    val sigmaIo		= ([], tauIo)

    val alphaReal	= TyVar.fromOverloadingClass("Real",    Real)
    val alphaRealInt	= TyVar.fromOverloadingClass("realint", RealInt)
    val alphaWordInt	= TyVar.fromOverloadingClass("wordint", WordInt)
    val alphaNum	= TyVar.fromOverloadingClass("num",     Num)
    val alphaNumTxt	= TyVar.fromOverloadingClass("numtxt",  NumTxt)

    val tauReal		= Type.fromTyVar alphaReal
    val tauRealInt	= Type.fromTyVar alphaRealInt
    val tauWordInt	= Type.fromTyVar alphaWordInt
    val tauNum		= Type.fromTyVar alphaNum
    val tauNumTxt	= Type.fromTyVar alphaNumTxt

    val tauRealInt1	= Type.fromFunType(tauRealInt, tauRealInt)
    val tauNum1		= Type.fromFunType(tauNum, tauNum)
    val tauReal2	= Type.fromFunType(pairType tauReal, tauReal)
    val tauWordInt2	= Type.fromFunType(pairType tauWordInt, tauWordInt)
    val tauNum2		= Type.fromFunType(pairType tauNum, tauNum)
    val tauNumTxt2	= Type.fromFunType(pairType tauNumTxt,
					   InitialStaticEnv.tauBool)
    val sigmaRealInt1	= ([alphaRealInt], tauRealInt1)
    val sigmaNum1	= ([alphaNum], tauNum1)
    val sigmaReal2	= ([alphaReal], tauReal2)
    val sigmaWordInt2	= ([alphaWordInt], tauWordInt2)
    val sigmaNum2	= ([alphaNum], tauNum2)
    val sigmaNumTxt2	= ([alphaNumTxt], tauNumTxt2)

    val alpha1		= TyVar.fromInt false 1
    val alpha2		= TyVar.fromInt false 2
    val sigmaUse	= ([alpha1, alpha2],
			   Type.fromFunType(Type.fromTyVar alpha1,
					    Type.fromTyVar alpha2)
			  )

    (* Static objects for Modules *)

    open StaticObjectsModule

    (* Static basis *)

    val emptySE = StrIdMap.empty
    val emptyTE = TyConMap.empty
    val emptyVE = VIdMap.empty

    val TE_Word8 : TyEnv =
	TyConMap.singleton(tyconWord8, (thetaWord8, emptyVE))

    val VE_IO : ValEnv =
	VIdMap.singleton(vid_Io, (sigmaIo, e))

    val SE : StrEnv =
	StrIdMap.fromList[(stridWord8, Env(emptySE, TE_Word8, emptyVE)),
			  (stridIO,    Env(emptySE, emptyTE,  VE_IO))]

    val TE : TyEnv =
	TyConMap.singleton(tyconVector, (thetaVector, VIdMap.empty))
								

    val VE : ValEnv =
	VIdMap.fromList[(vid_Chr,       (sigmaExn, e)),
			(vid_Div,       (sigmaExn, e)),
			(vid_Domain,    (sigmaExn, e)),
			(vid_Overflow,  (sigmaExn, e)),
			(vid_Size,      (sigmaExn, e)),
			(vid_Subscript, (sigmaExn, e)),
			(vidAbs,        (sigmaRealInt1, v)),
			(vidNeg,        (sigmaNum1,     v)),
			(vidPlus,       (sigmaNum2,     v)),
			(vidMinus,      (sigmaNum2,     v)),
			(vidTimes,      (sigmaNum2,     v)),
			(vidDiv,        (sigmaWordInt2, v)),
			(vidMod,        (sigmaWordInt2, v)),
			(vidBy,         (sigmaReal2,    v)),
			(vidLess,       (sigmaNumTxt2,  v)),
			(vidGreater,    (sigmaNumTxt2,  v)),
			(vidLessEq,     (sigmaNumTxt2,  v)),
			(vidGreaterEq,  (sigmaNumTxt2,  v)),
			(vidUse,        (sigmaUse, v))]

    val E = Env(SE,TE,VE)
    val F = FunIdMap.empty
    val G = SigIdMap.empty
    val T = TyNameSet.fromList[tWord8, tVector]

    val B0_STAT = StaticBasis.plus(InitialStaticBasis.B0, (T,F,G,E))


    (* Dynamic objects for the Core *)

    open DynamicObjectsCore

    (* Exception names *)

    val enChr       = ExName.exname vid_Chr
    val enDiv       = ExName.exname vid_Div
    val enDomain    = ExName.exname vid_Domain
    val enOverflow  = ExName.exname vid_Overflow
    val enSize      = ExName.exname vid_Size
    val enSubscript = ExName.exname vid_Subscript
    val enIo        = ExName.exname vid_Io
    val enSysErr    = ExName.exname vid_SysErr

    val ens = [enChr, enDiv, enDomain, enOverflow, enSize, enSubscript,
	       enIo, enSysErr]
    val s0  = List.foldl (fn(en, s) => State.insertExName(s, en))
			 InitialDynamicBasis.s0 ens


    (* Dynamic objects for Modules *)

    (* Dynamic basis *)

    val TE_Word8 : TyEnv =
	TyConMap.singleton(tyconWord8, emptyVE)

    val VE_IO : ValEnv =
	VIdMap.singleton(vid_Io, (ExVal(ExName enIo), e))

    val SE : StrEnv =
	StrIdMap.fromList[(stridWord8, Env(emptySE, TE_Word8, emptyVE)),
			  (stridIO,    Env(emptySE, emptyTE,  VE_IO))]

    val TE : TyEnv = TyConMap.fromList [(tyconVector,  VIdMap.empty)]

    val VE : ValEnv =
	VIdMap.fromList[(vid_Chr,       (ExVal(ExName enChr), e)),
			(vid_Div,       (ExVal(ExName enDiv), e)),
			(vid_Domain,    (ExVal(ExName enDomain), e)),
			(vid_Overflow,  (ExVal(ExName enOverflow), e)),
			(vid_Size,      (ExVal(ExName enSize), e)),
			(vid_Subscript, (ExVal(ExName enSubscript), e)),
			(vidAbs,        (BasVal "abs", v)),
			(vidNeg,        (BasVal "~", v)),
			(vidPlus,       (BasVal "+", v)),
			(vidMinus,      (BasVal "-", v)),
			(vidTimes,      (BasVal "*", v)),
			(vidDiv,        (BasVal "div", v)),
			(vidMod,        (BasVal "mod", v)),
			(vidBy,         (BasVal "/", v)),
			(vidLess,       (BasVal "<", v)),
			(vidGreater,    (BasVal ">", v)),
			(vidLessEq,     (BasVal "<=", v)),
			(vidGreaterEq,  (BasVal ">=", v)),
			(vidUse,        (BasVal "use", v))]

    val E = Env(SE,TE,VE)
    val F = FunIdMap.empty
    val G = SigIdMap.empty

    val B0_DYN = DynamicBasis.plus(InitialDynamicBasis.B0, (F,G,E))


    (* Representation types for special values [Section 6.2] *)

    open LibrarySVal

    fun baseToBase SCon.DEC = StringCvt.DEC
      | baseToBase SCon.HEX = StringCvt.HEX

    fun intFromString(base, s, t_opt) =
	INT(valOf(StringCvt.scanString (Int.scan(baseToBase base)) s))

    fun wordFromString' (WORDn,scan) (base, s) =
	WORDn(valOf(StringCvt.scanString (scan(baseToBase base)) s))
    fun wordFromString(base, s, NONE) =
	wordFromString' (WORD, Word.scan) (base, s)
      | wordFromString(base, s, SOME t) =
	if t = tWord8 then wordFromString' (WORD8, Word8.scan) (base, s)
		      else wordFromString' (WORD, Word.scan) (base, s)

    fun realFromString(s, t_opt) =
	REAL(Real.checkFloat(valOf(Real.fromString s)))
	handle Option => raise Overflow

    fun stringFromString(s, t_opt) =
	STRING(valOf(String.fromString s))
	handle Option => raise Overflow

    fun charFromString(s, t_opt) =
	CHAR(valOf(Char.fromString s))
	handle Option => raise Overflow

    fun span t =
	if      t = tInt then 0
	else if t = tWord then 0
	else if t = tWord8 then 256
	else if t = tReal then 0
	else if t = tChar then Char.maxOrd + 1
	else if t = tString then 0
	else raise Fail "Library.span: unknown tyname"


    (* Value representation packing and unpacking *)

    exception TypeError of string

    fun toInt(SVal(SVal.INT(INT n))) = n
      | toInt _ = raise TypeError "int value expected"
    fun toWord(SVal(SVal.WORD(WORD w))) = w
      | toWord _ = raise TypeError "word value expected"
    fun toWord8(SVal(SVal.WORD(WORD8 w))) = w
      | toWord8(SVal(SVal.WORD(WORD w))) = wordToWord8 w (* Implicit coercion *)
      | toWord8 _ = raise TypeError "Word8.word value expected"
    fun toString(SVal(SVal.STRING(STRING s))) = s
      | toString _ = raise TypeError "string value expected"
    fun toChar(SVal(SVal.CHAR(CHAR c))) = c
      | toChar _ = raise TypeError "char value expected"
    fun toReal(SVal(SVal.REAL(REAL x))) = x
      | toReal _ = raise TypeError "real value expected"

    fun toExn(ExVal en)		= en
      | toExn _			= raise TypeError "exception value expected"

    fun toUnit(Record r)	= if LabMap.isEmpty r then () else
				  raise TypeError "unit expected"
      | toUnit _		= raise TypeError "unit expected"

    fun toPair1 toX v		= case Val.toPair v
				    of SOME(v1, v2) => (toX v1, toX v2)
				     | NONE => raise TypeError "pair expected"
    fun toPair2 (toX, toY) v	= case Val.toPair v
				    of SOME(v1, v2) => (toX v1, toY v2)
				     | NONE => raise TypeError "pair expected"
    fun toList v		= case Val.toList v
				    of SOME vs => vs
				     | NONE => raise TypeError "list expected"

    fun fromInt n    = SVal(SVal.INT(INT n))
    fun fromWord w   = SVal(SVal.WORD(WORD w))
    fun fromWord8 w  = SVal(SVal.WORD(WORD8 w))
    fun fromString s = SVal(SVal.STRING(STRING s))
    fun fromChar c   = SVal(SVal.CHAR(CHAR c))
    fun fromReal x   = SVal(SVal.REAL(REAL x))
    fun fromUnit()   = Record LabMap.empty
    fun fromBool b   = VId(VId.fromString(if b then "true" else "false"))

    fun fromOption fromX  NONE		= VId(VId.fromString "NONE")
      | fromOption fromX (SOME x)	= VIdVal(VId.fromString "SOME", fromX x)


    (* Vectors encoded as records *)

    val fromVector			= Record
    fun toVector(Record r)		= r
      | toVector _			= raise TypeError "vector expected"

    val vectorLength			= LabMap.numItems
    val vectorMaxLen			= Option.getOpt(Int.maxInt,
							Vector.maxLen)
    fun vectorFromList vs =
	let
	    val labs = List.tabulate(List.length vs, Lab.fromInt)
	in
	    LabMap.fromList(ListPair.zipEq(labs, vs))
	end

    fun vectorSub(r, n) =
	case LabMap.find(r, Lab.fromInt n)
	  of SOME v => v
	   | NONE   => raise Subscript


    (* Tables for mapping streams *)

    structure IntMap	= FinMapFn(type ord_key = int val compare = Int.compare)

    val ixCounter	= ref 0
    val instreams	= ref(IntMap.empty : TextIO.instream IntMap.map)
    val outstreams	= ref(IntMap.empty : TextIO.outstream IntMap.map)

    fun openIn is	= let val ix = !ixCounter in
			      ixCounter := ix + 1;
			      instreams := IntMap.insert(!instreams, ix, is);
			      ix
			  end
    fun openOut os	= let val ix = !ixCounter in
			      ixCounter := ix + 1;
			      outstreams := IntMap.insert(!outstreams, ix, os);
			      ix
			  end
    fun closeIn ix	= instreams := IntMap.delete(!instreams, ix)
    fun closeOut ix	= outstreams := IntMap.delete(!outstreams, ix)

    val stdIn		= openIn TextIO.stdIn
    val stdOut		= openOut TextIO.stdOut
    val stdErr		= openOut TextIO.stdErr

    fun toInstream v	= valOf(IntMap.find(!instreams, toInt v))
    fun toOutstream v	= valOf(IntMap.find(!outstreams, toInt v))
    fun fromInstream s	= fromInt(openIn s)
    fun fromOutstream s	= fromInt(openOut s)

    fun fromIoArg{name, function, cause} =
	let
	    (* Dummy exception only *)
	    val en = ExName.exname(VId.fromString(General.exnName cause))
	    val r  = LabMap.insert(LabMap.insert(LabMap.insert(LabMap.empty,
			Lab.fromString "name", fromString name),
			Lab.fromString "function", fromString function),
			Lab.fromString "cause", ExVal(ExName en))
	in
	    Record r
	end

    fun fromSysErrArg(s, eo) =
	let
	    val r  = LabMap.insert(LabMap.insert(LabMap.empty,
			Lab.fromInt 1, fromString s),
			Lab.fromInt 2, fromOption fromInt NONE)
	in
	    Record r
	end


    (* Dynamic type dispatch *)

    fun realint1 (fInt, fReal) v =
	case v
	  of SVal(SVal.INT(INT n))   => SVal(SVal.INT(INT(fInt n)))
	   | SVal(SVal.REAL(REAL x)) => SVal(SVal.REAL(REAL(fReal x)))
	   | _ => raise TypeError "value of class RealInt expected"

    fun num1 (fInt, fWord, fWord8, fReal) v =
	case v
	  of SVal(SVal.WORD(WORD w))  => SVal(SVal.WORD(WORD(fWord w)))
	   | SVal(SVal.WORD(WORD8 w)) => SVal(SVal.WORD(WORD8(fWord8 w)))
	   | _ => realint1 (fInt, fReal) v handle TypeError _ =>
		  raise TypeError "values of class Num expected"

    fun Real2 (fReal) v =
	case Val.toPair v
	  of SOME(SVal(SVal.REAL(REAL x1)), SVal(SVal.REAL(REAL x2))) =>
		  SVal(SVal.REAL(REAL(fReal(x1, x2))))
	   | _ => raise TypeError "value of class Real expected"

    fun wordint2 (fInt, fWord, fWord8) v =
	case Val.toPair v
	  of SOME(SVal(SVal.INT(INT n1)), SVal(SVal.INT(INT n2))) =>
		  SVal(SVal.INT(INT(fInt(n1, n2))))
	   | SOME(SVal(SVal.WORD(WORD w1)),  SVal(SVal.WORD(WORD w2))) =>
		  SVal(SVal.WORD(WORD(fWord(w1, w2))))
	   | SOME(SVal(SVal.WORD(WORD8 w1)), SVal(SVal.WORD(WORD8 w2))) =>
		  SVal(SVal.WORD(WORD8(fWord8(w1, w2))))
	   | SOME(SVal(SVal.WORD(WORD8 w1)), SVal(SVal.WORD(WORD w2))) =>
		  (* Implicit coercion *)
		  SVal(SVal.WORD(WORD8(fWord8(w1, wordToWord8 w2))))
	   | SOME(SVal(SVal.WORD(WORD w1)),  SVal(SVal.WORD(WORD8 w2))) =>
		  (* Implicit coercion *)
		  SVal(SVal.WORD(WORD8(fWord8(wordToWord8 w1, w2))))
	   | _ => raise TypeError "values of class WordInt expected"

    fun num2 (fInt, fWord, fWord8, fReal) v =
	case Val.toPair v
	  of SOME(SVal(SVal.REAL(REAL x1)), SVal(SVal.REAL(REAL x2))) =>
		  SVal(SVal.REAL(REAL(fReal(x1, x2))))
	   | _ => wordint2 (fInt, fWord, fWord8) v handle TypeError _ =>
		  raise TypeError "values of class Num expected"

    fun numtxt2 (fInt, fWord, fWord8, fReal, fChar, fString) v =
	case Val.toPair v
	  of SOME(SVal(SVal.INT(INT n1)), SVal(SVal.INT(INT n2))) =>
		  fromBool(fInt(n1, n2))
	   | SOME(SVal(SVal.WORD(WORD w1)),  SVal(SVal.WORD(WORD w2))) =>
		  fromBool(fWord(w1, w2))
	   | SOME(SVal(SVal.WORD(WORD8 w1)), SVal(SVal.WORD(WORD8 w2))) =>
		  fromBool(fWord8(w1, w2))
	   | SOME(SVal(SVal.WORD(WORD8 w1)), SVal(SVal.WORD(WORD w2))) =>
		  (* Implicit coercion *)
		  fromBool(fWord8(w1, wordToWord8 w2))
	   | SOME(SVal(SVal.WORD(WORD w1)),  SVal(SVal.WORD(WORD8 w2))) =>
		  (* Implicit coercion *)
		  fromBool(fWord8(wordToWord8 w1, w2))
	   | SOME(SVal(SVal.REAL(REAL x1)),  SVal(SVal.REAL(REAL x2))) =>
		  fromBool(fReal(x1, x2))
	   | SOME(SVal(SVal.CHAR(CHAR c1)),  SVal(SVal.CHAR(CHAR c2))) =>
		  fromBool(fChar(c1, c2))
	   | SOME(SVal(SVal.STRING(STRING s1)), SVal(SVal.STRING(STRING s2))) =>
		  fromBool(fString(s1, s2))
	   | _ => raise TypeError "values of class NumTxt expected"


    (* The actual APPLY function [Section 6.4] *)

    fun packEx en	= raise Pack(ExName en)
    fun packIo arg	= raise Pack(ExNameVal(enIo, fromIoArg arg))
    fun packSysErr arg	= raise Pack(ExNameVal(enSysErr,fromSysErrArg arg))

    fun APPLY("abs", v)			= (realint1 (abs, abs) v
					   handle Overflow => packEx enOverflow)
      | APPLY("~", v)			= (num1 (~, Word.~, Word8.~, ~) v
					   handle Overflow => packEx enOverflow)
      | APPLY("+", v)			= (num2 (op+, op+, op+, op+) v
					   handle Overflow => packEx enOverflow)
      | APPLY("-", v)			= (num2 (op-, op-, op-, op-) v
					   handle Overflow => packEx enOverflow)
      | APPLY("*", v)			= (num2 (op*, op*, op*, op* ) v
					   handle Overflow => packEx enOverflow)
      | APPLY("div", v)			= (wordint2 (op div, op div, op div) v
					   handle Overflow => packEx enOverflow
					        | Div      => packEx enDiv)
      | APPLY("mod", v)			= (wordint2 (op mod, op mod, op mod) v
					   handle Div      => packEx enDiv)
      | APPLY("/", v)			= Real2 (op/) v
      | APPLY("<", v)			= numtxt2 (op<,op<,op<,op<,op<,op<) v
      | APPLY(">", v)			= numtxt2 (op>,op>,op>,op>,op>,op>) v
      | APPLY("<=", v)			= numtxt2
					      (op<=,op<=,op<=,op<=,op<=,op<=) v
      | APPLY(">=", v)			= numtxt2
					      (op>=,op>=,op>=,op>=,op>=,op>=) v

      | APPLY("General.exnName", v)	= fromString(ExName.toString
							(Val.exname(toExn v)))

      | APPLY("Char.maxOrd", v)		= fromInt Char.maxOrd
      | APPLY("Char.ord", v)		= fromInt(Char.ord(toChar v))
      | APPLY("Char.chr", v)		= (fromChar(Char.chr(toInt v))
					   handle Chr => packEx enChr)

      | APPLY("String.maxSize", v)	= fromInt String.maxSize
      | APPLY("String.size", v)		= fromInt(String.size(toString v))
      | APPLY("String.sub", v)		= (fromChar(String.sub
					          (toPair2 (toString, toInt) v))
					   handle Subscript =>
						  packEx enSubscript)
      | APPLY("String.str", v)		= fromString(String.str(toChar v))
      | APPLY("String.^", v)		= (fromString(op^(toPair1 toString v))
					   handle Size => packEx enSize)

      | APPLY("Int.precision", v)	= fromOption fromInt Int.precision
      | APPLY("Int.minInt", v)		= fromOption fromInt Int.minInt
      | APPLY("Int.maxInt", v)		= fromOption fromInt Int.maxInt
      | APPLY("Int.quot", v)		= (fromInt(Int.quot(toPair1 toInt v))
					   handle Overflow => packEx enOverflow
						| Div      => packEx enDiv)
      | APPLY("Int.rem", v)		= (fromInt(Int.rem(toPair1 toInt v))
					   handle Div => packEx enDiv)

      | APPLY("Word.wordSize", v)	= fromInt Word.wordSize
      | APPLY("Word.toInt", v)		= (fromInt(Word.toInt(toWord v))
					   handle Overflow =>
						  packEx enOverflow)
      | APPLY("Word.toIntX", v)		= (fromInt(Word.toIntX(toWord v))
					   handle Overflow =>
						  packEx enOverflow)
      | APPLY("Word.fromInt", v)	= fromWord(Word.fromInt(toInt v))
      | APPLY("Word.notb", v)		= fromWord(Word.notb(toWord v))
      | APPLY("Word.orb", v)		= fromWord(Word.orb(toPair1 toWord v))
      | APPLY("Word.xorb", v)		= fromWord(Word.xorb(toPair1 toWord v))
      | APPLY("Word.andb", v)		= fromWord(Word.andb(toPair1 toWord v))
      | APPLY("Word.<<", v)		= fromWord(Word.<<(toPair1 toWord v))
      | APPLY("Word.>>", v)		= fromWord(Word.>>(toPair1 toWord v))
      | APPLY("Word.~>>", v)		= fromWord(Word.~>>(toPair1 toWord v))

      | APPLY("Word8.toLarge", v)	= fromWord(word8ToWord(toWord8 v))
      | APPLY("Word8.toLargeX", v)	= fromWord(word8ToWordX(toWord8 v))
      | APPLY("Word8.fromLarge", v)	= fromWord8(wordToWord8(toWord v))
      | APPLY("Word8.toInt", v)		= (fromInt(Word8.toInt(toWord8 v))
					   handle Overflow => packEx enOverflow)
      | APPLY("Word8.toIntX", v)	= (fromInt(Word8.toIntX(toWord8 v))
					   handle Overflow => packEx enOverflow)
      | APPLY("Word8.fromInt", v)	= fromWord8(Word8.fromInt(toInt v))
      | APPLY("Word8.notb", v)		= fromWord8(Word8.notb(toWord8 v))
      | APPLY("Word8.orb", v)		= fromWord8
					  (Word8.orb(toPair1 toWord8 v))
      | APPLY("Word8.xorb", v)		= fromWord8
					  (Word8.xorb(toPair1 toWord8 v))
      | APPLY("Word8.andb", v)		= fromWord8
					  (Word8.andb(toPair1 toWord8 v))
      | APPLY("Word8.<<", v)		= fromWord8
					  (Word8.<<(toPair2 (toWord8,toWord) v))
      | APPLY("Word8.>>", v)		= fromWord8
					  (Word8.>>(toPair2 (toWord8,toWord) v))
      | APPLY("Word8.~>>", v)		= fromWord8
					  (Word8.~>>(toPair2 (toWord8,toWord)v))

(* Not supported by all implementations:
      | APPLY("Real.radix", v)		= fromInt Real.radix
      | APPLY("Real.precision", v)	= fromInt Real.precision
      | APPLY("Real.maxFinite", v)	= fromReal Real.maxFinite
      | APPLY("Real.minPos", v)		= fromReal Real.minPos
      | APPLY("Real.minNormalPos", v)	= fromReal Real.minNormalPos
*)
      | APPLY("Real.==", v)		= fromBool(Real.==(toPair1 toReal v))
      | APPLY("Real.?=", v)		= fromBool(Real.?=(toPair1 toReal v))
      | APPLY("Real.isFinite", v)	= fromBool(Real.isFinite(toReal v))
      | APPLY("Real.isNan", v)		= fromBool(Real.isNan(toReal v))
      | APPLY("Real.isNormal", v)	= fromBool(Real.isNormal(toReal v))
      | APPLY("Real.signBit", v)	= fromBool(Real.signBit(toReal v))
      | APPLY("Real.copySign", v)	= fromReal
					   (Real.copySign(toPair1 toReal v))
(* Not supported by all implementations:
      | APPLY("Real.nextAfter", v)	= fromReal
					   (Real.nextAfter(toPair1 toReal v))
      | APPLY("Real.rem", v)		= fromReal(Real.rem(toPair1 toReal v))
*)
      | APPLY("Real.checkFloat", v)	= (fromReal(Real.checkFloat(toReal v))
					   handle Overflow => packEx enOverflow
						| Div      => packEx enDiv)
(* Not supported by all implementations:
      | APPLY("Real.realFloor", v)	= fromReal(Real.realFloor(toReal v))
      | APPLY("Real.realCeil", v)	= fromReal(Real.realCeil(toReal v))
      | APPLY("Real.realTrunc", v)	= fromReal(Real.realTrunc(toReal v))
      | APPLY("Real.realRound", v)	= fromReal(Real.realRound(toReal v))
*)
      | APPLY("Real.floor", v)		= (fromInt(Real.floor(toReal v))
					   handle Overflow => packEx enOverflow
						| Domain   => packEx enDomain)
      | APPLY("Real.ceil", v)		= (fromInt(Real.ceil(toReal v))
					   handle Overflow => packEx enOverflow
						| Domain   => packEx enDomain)
      | APPLY("Real.trunc", v)		= (fromInt(Real.trunc(toReal v))
					   handle Overflow => packEx enOverflow
						| Domain   => packEx enDomain)
      | APPLY("Real.round", v)		= (fromInt(Real.round(toReal v))
					   handle Overflow => packEx enOverflow
						| Domain   => packEx enDomain)
      | APPLY("Real.fromInt", v)	= fromReal(Real.fromInt(toInt v))
      | APPLY("Real.toString", v)	= fromString(Real.toString(toReal v))

      | APPLY("Math.e", v)		= fromReal Math.e
      | APPLY("Math.pi", v)		= fromReal Math.pi
      | APPLY("Math.sqrt", v)		= fromReal(Math.sqrt(toReal v))
      | APPLY("Math.sin", v)		= fromReal(Math.sin(toReal v))
      | APPLY("Math.cos", v)		= fromReal(Math.cos(toReal v))
      | APPLY("Math.tan", v)		= fromReal(Math.tan(toReal v))
      | APPLY("Math.asin", v)		= fromReal(Math.asin(toReal v))
      | APPLY("Math.acos", v)		= fromReal(Math.acos(toReal v))
      | APPLY("Math.atan", v)		= fromReal(Math.atan(toReal v))
      | APPLY("Math.atan2", v)		= fromReal(Math.atan2(toPair1 toReal v))
      | APPLY("Math.exp", v)		= fromReal(Math.exp(toReal v))
      | APPLY("Math.pow", v)		= fromReal(Math.pow(toPair1 toReal v))
      | APPLY("Math.ln", v)		= fromReal(Math.ln(toReal v))
      | APPLY("Math.log10", v)		= fromReal(Math.log10(toReal v))
      | APPLY("Math.sinh", v)		= fromReal(Math.sinh(toReal v))
      | APPLY("Math.cosh", v)		= fromReal(Math.cosh(toReal v))
      | APPLY("Math.tanh", v)		= fromReal(Math.tanh(toReal v))

      | APPLY("Vector.maxLen", v)	= fromInt vectorMaxLen
      | APPLY("Vector.fromList", v)	= fromVector(vectorFromList(toList v))
      | APPLY("Vector.length", v)	= fromInt(vectorLength(toVector v))
      | APPLY("Vector.sub", v)		= (vectorSub(toPair2 (toVector,toInt) v)
					   handle Subscript =>
						  packEx enSubscript)
      | APPLY("CharVector.fromList", v)	= fromString
					    (CharVector.fromList
					      (List.map toChar (toList v)))

      | APPLY("TextIO.stdIn", v)	= fromInt stdIn
      | APPLY("TextIO.stdOut", v)	= fromInt stdOut
      | APPLY("TextIO.stdErr", v)	= fromInt stdErr
      | APPLY("TextIO.openIn", v)	= (fromInstream
						   (TextIO.openIn(toString v))
					   handle IO.Io x => packIo x)
      | APPLY("TextIO.openOut", v)	= (fromOutstream
						   (TextIO.openOut(toString v))
					   handle IO.Io x => packIo x)
      | APPLY("TextIO.openAppend", v)	= (fromOutstream
						 (TextIO.openAppend(toString v))
					   handle IO.Io x => packIo x)
      | APPLY("TextIO.closeIn", v)	= (fromUnit
						 (TextIO.closeIn(toInstream v))
					   handle IO.Io x => packIo x)
      | APPLY("TextIO.closeOut", v)	= (fromUnit
						(TextIO.closeOut(toOutstream v))
					   handle IO.Io x => packIo x)
      | APPLY("TextIO.input", v)	= (fromString
						    (TextIO.input(toInstream v))
					   handle IO.Io x => packIo x)
      | APPLY("TextIO.input1", v)	= (fromOption fromChar
						   (TextIO.input1(toInstream v))
					   handle IO.Io x => packIo x)
      | APPLY("TextIO.inputN", v)	= (fromString
					     (TextIO.inputN
					         (toPair2 (toInstream,toInt) v))
					   handle IO.Io x => packIo x)
      | APPLY("TextIO.inputAll", v)	= (fromString
						 (TextIO.inputAll(toInstream v))
					   handle IO.Io x => packIo x)
      | APPLY("TextIO.inputLine", v)	= (fromOption fromString
						(TextIO.inputLine(toInstream v))
					   handle IO.Io x => packIo x)
      | APPLY("TextIO.endOfStream", v)	= (fromBool
					      (TextIO.endOfStream(toInstream v))
					   handle IO.Io x => packIo x)
      | APPLY("TextIO.output", v)	= (fromUnit
					   (TextIO.output
					    (toPair2 (toOutstream,toString) v))
					   handle IO.Io x => packIo x)
      | APPLY("TextIO.output1", v)	= (fromUnit
					   (TextIO.output1
					    (toPair2 (toOutstream,toChar) v))
					   handle IO.Io x => packIo x)
      | APPLY("TextIO.flushOut", v)	= (fromUnit
						(TextIO.flushOut(toOutstream v))
					   handle IO.Io x => packIo x)

      | APPLY("OS.FileSys.getDir", v)	= fromString
						  (OS.FileSys.getDir(toUnit v))
      | APPLY("OS.FileSys.chDir", v)	= (fromUnit
						  (OS.FileSys.chDir(toString v))
					   handle OS.SysErr x => packSysErr x)
      | APPLY("OS.FileSys.mkDir", v)	= (fromUnit
						  (OS.FileSys.mkDir(toString v))
					   handle OS.SysErr x => packSysErr x)
      | APPLY("OS.FileSys.rmDir", v)	= (fromUnit
						  (OS.FileSys.rmDir(toString v))
					   handle OS.SysErr x => packSysErr x)
      | APPLY("OS.FileSys.isDir", v)	= (fromBool
						  (OS.FileSys.isDir(toString v))
					   handle OS.SysErr x => packSysErr x)

      | APPLY("use", v) =
	(case v
	   of SVal(SVal.STRING(STRING s)) => fromUnit(Use.enqueue s)
	    | Record r =>
	      (* We piggybag `use' to enable introduction of primitives. *)
	      (case LabMap.find(r, Lab.fromString "b")
	         of SOME(SVal(SVal.STRING(STRING s))) => BasVal s
		  | _ => raise TypeError "invalid argument to `use'"
	      )
	    | _ => raise TypeError "string value expected"
	)

      | APPLY(b, v) = raise TypeError("unknown basic value `" ^ b ^ "'")
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML core derived forms
 *
 * Definition, Section 2.7 and Appendix A
 *
 * Note:
 *   Two phrases named Fmatch and Fmrule have been added to factorize FvalBind.
 *)

signature DERIVED_FORMS_CORE =
sig
    (* Import *)

    type Info      = GrammarCore.Info

    type Lab       = GrammarCore.Lab
    type VId       = GrammarCore.VId

    type Op        = GrammarCore.Op
    type AtExp     = GrammarCore.AtExp
    type AppExp    = GrammarCore.AtExp list
    type InfExp    = GrammarCore.Exp
    type Exp       = GrammarCore.Exp
    type Match     = GrammarCore.Match
    type Mrule     = GrammarCore.Mrule
    type Dec       = GrammarCore.Dec
    type ValBind   = GrammarCore.ValBind
    type FvalBind  = GrammarCore.ValBind
    type Fmatch    = GrammarCore.Match * VId * int
    type Fmrule    = GrammarCore.Mrule * VId * int
    type TypBind   = GrammarCore.TypBind
    type DatBind   = GrammarCore.DatBind
    type AtPat     = GrammarCore.AtPat
    type PatRow    = GrammarCore.PatRow
    type Pat       = GrammarCore.Pat
    type Ty        = GrammarCore.Ty
    type TyVarseq  = GrammarCore.TyVarseq


    (* Expressions [Figure 15] *)

    val UNITAtExp :	Info					-> AtExp
    val TUPLEAtExp :	Info * Exp list				-> AtExp
    val HASHAtExp :	Info * Lab				-> AtExp
    val CASEExp :	Info * Exp * Match			-> Exp
    val IFExp :		Info * Exp * Exp * Exp			-> Exp
    val ANDALSOExp :	Info * Exp * Exp			-> Exp
    val ORELSEExp :	Info * Exp * Exp			-> Exp
    val SEQAtExp :	Info * Exp list				-> AtExp
    val LETAtExp :	Info * Dec * Exp list			-> AtExp
    val WHILEExp :	Info * Exp * Exp			-> Exp
    val LISTAtExp :	Info * Exp list				-> AtExp

    (* Patterns [Figure 16] *)

    val UNITAtPat :	Info					-> AtPat
    val TUPLEAtPat :	Info * Pat list				-> AtPat
    val LISTAtPat :	Info * Pat list				-> AtPat

    val IDPatRow :	Info * VId * Ty option * Pat option * PatRow option
								-> PatRow
    (* Types [Figure 16] *)

    val TUPLETy :	Info * Ty list				-> Ty

    (* Function-value bindings [Figure 17] *)

    val FvalBind :	Info * Fmatch * FvalBind option		-> FvalBind
    val Fmatch :	Info * Fmrule * Fmatch option		-> Fmatch
    val Fmrule :	Info * Op * VId * AtPat list * Ty option * Exp -> Fmrule

    (* Declarations [Figure 17] *)

    val FUNDec :	Info * TyVarseq * FvalBind		-> Dec
    val DATATYPEDec :	Info * DatBind * TypBind option		-> Dec
    val ABSTYPEDec :	Info * DatBind * TypBind option * Dec	-> Dec
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML core derived forms
 *
 * Definition, Section 2.7 and Appendix A
 *
 * Notes:
 * - Two phrases named Fmatch and Fmrule have been added to factorize FvalBind.
 * - In Fvalbinds we do not enforce that all optional type annotations are
 *   syntactically identical (as the Definition enforces, although this seems
 *   to be a mistake).
 * - The Definition is somewhat inaccurate about the derived forms of Exp
 *   [Definition, Appendix A, Figure 15] in that most forms are actually AtExp
 *   derived forms, as can be seen from the full grammar [Definition,
 *   Appendix B, Figure 20]. To achieve consistency, the equivalent forms must
 *   be put in parentheses in some cases.
 * - The same goes for pattern derived forms [Definition, Appendix A, Figure 16;
 *   Appendix B, Figure 22].
 *)

structure DerivedFormsCore :> DERIVED_FORMS_CORE =
struct
    (* Import *)

    structure C    = GrammarCore

    type Info      = C.Info

    type Lab       = C.Lab
    type VId       = C.VId

    type Op        = C.Op
    type AtExp     = C.AtExp
    type AppExp    = C.AtExp list
    type InfExp    = C.Exp
    type Exp       = C.Exp
    type Match     = C.Match
    type Mrule     = C.Mrule
    type Dec       = C.Dec
    type ValBind   = C.ValBind
    type FvalBind  = C.ValBind
    type Fmatch    = C.Match * C.VId * int
    type Fmrule    = C.Mrule * C.VId * int
    type TypBind   = C.TypBind
    type DatBind   = C.DatBind
    type AtPat     = C.AtPat
    type PatRow    = C.PatRow
    type Pat       = C.Pat
    type Ty        = C.Ty
    type TyVarseq  = C.TyVarseq


    (* Some helpers *)

    val vidFALSE               = VId.fromString "false"
    val vidTRUE                = VId.fromString "true"
    val vidNIL                 = VId.fromString "nil"
    val vidCONS                = VId.fromString "::"
    val longvidCONS            = LongVId.fromId vidCONS

    fun LONGVIDExp(I, longvid) = C.ATExp(I, C.IDAtExp(I, C.SANSOp, longvid))
    fun LONGVIDPat(I, longvid) = C.ATPat(I, C.IDAtPat(I, C.SANSOp, longvid))

    fun VIDAtExp(I, vid)       = C.IDAtExp(I, C.SANSOp, LongVId.fromId vid)
    fun VIDAtPat(I, vid)       = C.IDAtPat(I, C.SANSOp, LongVId.fromId vid)
    fun VIDExp(I, vid)         = LONGVIDExp(I, LongVId.fromId vid)
    fun VIDPat(I, vid)         = LONGVIDPat(I, LongVId.fromId vid)

    fun FALSEExp(I)            = VIDExp(I, vidFALSE)
    fun TRUEExp(I)             = VIDExp(I, vidTRUE)
    fun NILAtExp(I)            = VIDAtExp(I, vidNIL)
    fun CONSExp(I)             = VIDExp(I, vidCONS)

    fun FALSEPat(I)            = VIDPat(I, vidFALSE)
    fun TRUEPat(I)             = VIDPat(I, vidTRUE)
    fun NILAtPat(I)            = VIDAtPat(I, vidNIL)


    (* Rewriting of withtype declarations [Appendix A, 2nd bullet] *)

    fun findTyCon(tycon, C.TypBind(_, tyvarseq, tycon', ty, typbind_opt)) =
	    if tycon' = tycon then
		SOME(tyvarseq, ty)
	    else
		Option.mapPartial (fn typbind => findTyCon(tycon, typbind))
				  typbind_opt


    fun replaceTy (C.TyVarseq(_,tyvars), C.Tyseq(I',tys)) (C.VARTy(I,tyvar)) =
	let
	    fun loop(tyvar'::tyvars', ty'::tys') =
		    if tyvar' = tyvar then
			ty'
		    else
			loop(tyvars', tys')
	      | loop([],_) =
		    Error.error(I, "unbound type variable")
	      | loop(_,[]) =
		    Error.error(I', "type sequence has wrong arity")
	in
	    loop(tyvars, tys)
	end

      | replaceTy tyvarseq_tyseq (C.RECORDTy(I, tyrow_opt)) =
	    C.RECORDTy(I, Option.map (replaceTyRow tyvarseq_tyseq) tyrow_opt)

      | replaceTy tyvarseq_tyseq (C.CONTy(I, tyseq', tycon)) =
	    C.CONTy(I, replaceTyseq tyvarseq_tyseq tyseq', tycon)

      | replaceTy tyvarseq_tyseq (C.ARROWTy(I, ty1, ty2)) =
	    C.ARROWTy(I, replaceTy tyvarseq_tyseq ty1,
			 replaceTy tyvarseq_tyseq ty2)

      | replaceTy tyvarseq_tyseq (C.PARTy(I, ty)) =
	    C.PARTy(I, replaceTy tyvarseq_tyseq ty)

    and replaceTyRow tyvarseq_tyseq (C.TyRow(I, lab, ty, tyrow_opt)) =
	    C.TyRow(I, lab, replaceTy tyvarseq_tyseq ty, 
		       Option.map (replaceTyRow tyvarseq_tyseq) tyrow_opt)

    and replaceTyseq tyvarseq_tyseq (C.Tyseq(I, tys)) =	  
	    C.Tyseq(I, List.map (replaceTy tyvarseq_tyseq) tys)


    fun rewriteTy typbind (ty as C.VARTy _) = ty

      | rewriteTy typbind (C.RECORDTy(I, tyrow_opt)) =
	    C.RECORDTy(I, Option.map (rewriteTyRow typbind) tyrow_opt)

      | rewriteTy typbind (C.CONTy(I, tyseq, longtycon)) =
	let 
	    val tyseq'          = rewriteTyseq typbind tyseq
	    val (strids, tycon) = LongTyCon.explode longtycon
	in
	    if not(List.null strids) then
		C.CONTy(I, tyseq', longtycon)
	    else
		case findTyCon(tycon, typbind)
		  of SOME(tyvarseq', ty') => replaceTy (tyvarseq',tyseq') ty'
		   | NONE                 => C.CONTy(I, tyseq', longtycon)
	end

      | rewriteTy typbind (C.ARROWTy(I, ty1, ty2)) =
	    C.ARROWTy(I, rewriteTy typbind ty1, rewriteTy typbind ty2)

      | rewriteTy typbind (C.PARTy(I, ty)) =
	    C.PARTy(I, rewriteTy typbind ty)

    and rewriteTyRow typbind (C.TyRow(I, lab, ty, tyrow_opt)) =
	    C.TyRow(I, lab, rewriteTy typbind ty,
		       Option.map (rewriteTyRow typbind) tyrow_opt)

    and rewriteTyseq typbind (C.Tyseq(I, tys)) =
	    C.Tyseq(I, List.map (rewriteTy typbind) tys)

    fun rewriteConBind typbind (C.ConBind(I, op_opt, vid, ty_opt, conbind_opt))=
	    C.ConBind(I, op_opt, vid,
			 Option.map (rewriteTy typbind) ty_opt,
			 Option.map (rewriteConBind typbind) conbind_opt)

    fun rewriteDatBind typbind (C.DatBind(I, tyvarseq, tycon, conbind,
							      datbind_opt)) =
	case findTyCon(tycon, typbind)
	  of NONE =>
	     C.DatBind(I, tyvarseq, tycon, rewriteConBind typbind conbind,
			  Option.map (rewriteDatBind typbind) datbind_opt)
	   | SOME _ =>
		Error.error(I, "duplicate type constructor \
			       \in recursive type declaration")


    (* Patterns [Figure 16] *)

    fun UNITAtPat(I) = C.RECORDAtPat(I, NONE)

    fun TUPLEAtPat(I, [pat]) = C.PARAtPat(I, pat)
      | TUPLEAtPat(I,  pats) =
	let
	    fun toPatRow(n,    []     ) = NONE
	      | toPatRow(n, pat::pats') =
		SOME(C.FIELDPatRow(I, Lab.fromInt n, pat, toPatRow(n+1,pats')))
	in
	    C.RECORDAtPat(I, toPatRow(1, pats))
	end

    fun LISTAtPat(I, [])   = NILAtPat(I)
      | LISTAtPat(I, pats) = 
	let
	    fun toPatList    []       = C.ATPat(I, NILAtPat(I))
	      | toPatList(pat::pats') =
		C.CONPat(I, C.SANSOp, longvidCONS,
			    TUPLEAtPat(I, [pat, toPatList pats']))
	in
	    C.PARAtPat(I, toPatList pats)
	end


    (* Pattern Rows [Figure 16] *)

    fun IDPatRow(I, vid, ty_opt, pat_opt, patrow_opt) =
	let
	    val lab    = Lab.fromString(VId.toString vid)
	    val vidPat = VIDPat(I, vid)
	    val pat    =
		case (ty_opt, pat_opt)
		  of (NONE,    NONE) => vidPat
		   | (SOME ty, NONE) => C.COLONPat(I, vidPat, ty)
		   | ( _ , SOME pat) => C.ASPat(I, C.SANSOp,vid,ty_opt,pat)
	in
	    C.FIELDPatRow(I, lab, pat, patrow_opt)
	end


    (* Expressions [Figure 15] *)

    fun UNITAtExp(I) = C.RECORDAtExp(I, NONE)

    fun TUPLEAtExp(I, [exp]) = C.PARAtExp(I, exp)
      | TUPLEAtExp(I,  exps) =
	let
	    fun toExpRow(n,    []     ) = NONE
	      | toExpRow(n, exp::exps') =
		SOME(C.ExpRow(I, Lab.fromInt n, exp, toExpRow(n+1, exps')))
	in
	    C.RECORDAtExp(I, toExpRow(1, exps))
	end

    fun HASHAtExp(I, lab) =
	let
	    val vid    = VId.invent()
	    val dots   = C.DOTSPatRow(I)
	    val patrow = C.FIELDPatRow(I, lab, VIDPat(I, vid), SOME dots)
	    val pat    = C.ATPat(I, C.RECORDAtPat(I, SOME patrow))
	    val mrule  = C.Mrule(I, pat, VIDExp(I, vid))
	    val match  = C.Match(I, mrule, NONE)
	in
	    C.PARAtExp(I, C.FNExp(I, match))
	end

    fun CASEExp(I, exp, match) =
	let
	    val function = C.ATExp(I, C.PARAtExp(I, C.FNExp(I, match)))
	in
	    C.APPExp(I, function, C.PARAtExp(I, exp))
	end

    fun IFExp(I, exp1, exp2, exp3) =
	let
	    val mruleTrue  = C.Mrule(I, TRUEPat(I), exp2)
	    val mruleFalse = C.Mrule(I, FALSEPat(I), exp3)
	    val matchFalse = C.Match(I, mruleFalse, NONE)
	    val matchTrue  = C.Match(I, mruleTrue, SOME matchFalse)
	in
	    CASEExp(I, exp1, matchTrue)
	end

    fun ORELSEExp (I, exp1, exp2) = IFExp(I, exp1, TRUEExp(I), exp2)

    fun ANDALSOExp(I, exp1, exp2) = IFExp(I, exp1, exp2, FALSEExp(I))

    fun SEQAtExp(I, exps) =
	let
	    val wildcard             = C.ATPat(I, C.WILDCARDAtPat(I))

	    fun toExpSeq []          = raise Fail "DerivedFormsCore.SEQAtExp: \
						  \empty exp list"
	      | toExpSeq [exp]       = exp
	      | toExpSeq(exp::exps') =
		  let
		      val mrule = C.Mrule(I, wildcard, toExpSeq exps')
		      val match = C.Match(I, mrule, NONE)
		  in
		      CASEExp(I, exp, match)
		  end
	in
	    C.PARAtExp(I, toExpSeq exps)
	end

    fun LETAtExp(I, dec, [exp]) = C.LETAtExp(I, dec, exp)
      | LETAtExp(I, dec,  exps) =
	    C.LETAtExp(I, dec, C.ATExp(I, SEQAtExp(I, exps)))

    fun WHILEExp(I, exp1, exp2) =
	let
	    val vid       = VId.invent()
	    val vidExp    = VIDExp(I, vid)
	    val unitAtExp = UNITAtExp(I)
	    val unitExp   = C.ATExp(I, unitAtExp)
	    val callVid   = C.APPExp(I, vidExp, unitAtExp)

	    val seqExp    = C.ATExp(I, SEQAtExp(I, [exp2, callVid]))
	    val fnBody    = IFExp(I, exp1, seqExp, unitExp)
	    val mrule     = C.Mrule(I, C.ATPat(I, UNITAtPat(I)), fnBody)
	    val match     = C.Match(I, mrule, NONE)
	    val fnExp     = C.FNExp(I, match)
	    val fnBind    = C.PLAINValBind(I, VIDPat(I, vid), fnExp, NONE)
	    val valbind   = C.RECValBind(I, fnBind)
	    val dec       = C.VALDec(I, C.TyVarseq(I, []), valbind)
	in
	    C.ATExp(I, C.LETAtExp(I, dec, callVid))
	end

    fun LISTAtExp(I, [])   = NILAtExp(I)
      | LISTAtExp(I, exps) =
	let
	    fun toExpList    []       = C.ATExp(I, NILAtExp(I))
	      | toExpList(exp::exps') =
		C.APPExp(I, CONSExp(I), TUPLEAtExp(I, [exp, toExpList exps']))
	in
	    C.PARAtExp(I, toExpList exps)
	end


    (* Type Expressions [Figure 16] *)

    fun TUPLETy(I, [ty]) = ty
      | TUPLETy(I,  tys) =
	let
	    fun toTyRow(n,   []    ) = NONE
	      | toTyRow(n, ty::tys') =
		SOME(C.TyRow(I, Lab.fromInt n, ty, toTyRow(n+1, tys')))
	in
	    C.RECORDTy(I, toTyRow(1, tys))
	end


    (* Function-value Bindings [Figure 17] *)

    fun FvalBind(I, (match, vid, arity), fvalbind_opt) =
	let
	    fun abstract(0, vidExps) =
		let
		    val exp = C.ATExp(I, TUPLEAtExp(I, List.rev vidExps))
		in
		    CASEExp(I, exp, match)
		end

	      | abstract(n, vidExps) =
		let
		    val vid   = VId.invent()
		    val exp   = VIDExp(I, vid)
		    val pat   = VIDPat(I, vid)
		    val mrule = C.Mrule(I, pat, abstract(n-1, exp::vidExps))
		in
		    C.FNExp(I, C.Match(I, mrule, NONE))
		end

	    val exp = abstract(arity, [])
	    val pat = VIDPat(I, vid)
	in
	    C.PLAINValBind(I, pat, exp, fvalbind_opt)
	end


    fun Fmatch(I, (mrule, vid, arity), NONE) =
	    ( C.Match(I, mrule, NONE), vid, arity )

      | Fmatch(I, (mrule, vid, arity), SOME(match, vid', arity')) =
	    if vid <> vid' then
		Error.error(I, "inconsistent function identifier")
	    else if arity <> arity' then
		Error.error(I, "inconsistent function arity")
	    else
		( C.Match(I, mrule, SOME match), vid, arity )


    fun Fmrule(I, _, vid, atpats, ty_opt, exp) =
	let
	    val pats = List.map (fn atpat => C.ATPat(I, atpat)) atpats
	    val pat' = C.ATPat(I, TUPLEAtPat(I, pats))
	    val exp' = case ty_opt
			 of NONE    => exp
			  | SOME ty => C.COLONExp(I, exp, ty)
	    val arity = List.length atpats
	in
	    ( C.Mrule(I, pat', exp'), vid, arity )
	end


    (* Declarations [Figure 17] *)

    fun FUNDec(I, tyvarseq, fvalbind) =
	    C.VALDec(I, tyvarseq, C.RECValBind(I, fvalbind))

    fun DATATYPEDec(I, datbind, NONE)         = C.DATATYPEDec(I, datbind)
      | DATATYPEDec(I, datbind, SOME typbind) =
	let
	    val datbind' = rewriteDatBind typbind datbind
	in
	    C.SEQDec(I, C.DATATYPEDec(C.infoDatBind datbind, datbind'),
			C.TYPEDec(C.infoTypBind typbind, typbind))
	end

    fun ABSTYPEDec(I, datbind, NONE, dec)         = C.ABSTYPEDec(I, datbind,dec)
      | ABSTYPEDec(I, datbind, SOME typbind, dec) =
	let
	    val I'       = C.infoTypBind typbind
	    val datbind' = rewriteDatBind typbind datbind
	in
	    C.ABSTYPEDec(I, datbind', C.SEQDec(I, C.TYPEDec(I', typbind), dec))
	end
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML modules derived forms
 *
 * Definition, Appendix A
 *
 * Notes:
 * - A phrase named SynDesc has been added to factorize type synonym
 *   specifications.
 * - Similarly, a phrase named TyReaDesc has been added to factorize type
 *   realisation signature expressions.
 * - The structure sharing derived form is missing since it cannot be resolved
 *   syntactically. It has been moved to the bare grammar.
 *)

signature DERIVED_FORMS_MODULE =
sig
    (* Import *)

    type Info       = GrammarModule.Info

    type VId        = GrammarCore.VId
    type TyCon      = GrammarCore.TyCon
    type StrId      = GrammarCore.StrId
    type SigId      = GrammarModule.SigId
    type FunId      = GrammarModule.FunId
    type longTyCon  = GrammarCore.longTyCon

    type Ty         = GrammarCore.Ty
    type TyVarseq   = GrammarCore.TyVarseq

    type StrExp     = GrammarModule.StrExp
    type StrDec     = GrammarModule.StrDec
    type StrBind    = GrammarModule.StrBind
    type SigExp     = GrammarModule.SigExp
    type TyReaDesc  = (Info * TyVarseq * longTyCon * Ty) list
    type Spec       = GrammarModule.Spec
    type SynDesc    = (Info * TyVarseq * TyCon * Ty) list
    type FunBind    = GrammarModule.FunBind


    (* Structure Bindings [Figure 18] *)

    val TRANSStrBind :		Info * StrId * SigExp option * StrExp
				     * StrBind option -> StrBind
    val SEALStrBind :		Info * StrId * SigExp * StrExp
				     * StrBind option -> StrBind

    (* Structure Expressions [Figure 18] *)

    val APPDECStrExp :		Info * FunId * StrDec -> StrExp

    (* Functor Bindings [Figure 18] *)

    val TRANSFunBind :		Info * FunId * StrId * SigExp * SigExp option
				     * StrExp * FunBind option -> FunBind
    val SEALFunBind :		Info * FunId * StrId * SigExp * SigExp
				     * StrExp * FunBind option -> FunBind
    val TRANSSPECFunBind :	Info * FunId * Spec * SigExp option
				     * StrExp * FunBind option -> FunBind
    val SEALSPECFunBind :	Info * FunId * Spec * SigExp
				     * StrExp * FunBind option -> FunBind

    (* Specifications [Figure 19] *)

    val SYNSpec :		Info * SynDesc -> Spec
    val INCLUDEMULTISpec :	Info * SigId list -> Spec

    val SynDesc :		Info * TyVarseq * TyCon * Ty
				     * SynDesc option -> SynDesc

    (* Signature Expressions [Figure 19] *)

    val WHERETYPESigExp :	Info * SigExp * TyReaDesc -> SigExp

    val TyReaDesc :		Info * TyVarseq * longTyCon * Ty
				     * TyReaDesc option -> TyReaDesc
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML modules derived forms
 *
 * Definition, Appendix A
 *
 * Notes:
 * - A phrase named SynDesc has been added to factorize type synonym
 *   specifications.
 * - Similarly, a phrase named TyReaDesc has been added to factorize type
 *   realisation signature expressions.
 * - The structure sharing derived form is missing since it cannot be resolved
 *   syntactically. It has been moved to the bare grammar.
 *)

structure DerivedFormsModule :> DERIVED_FORMS_MODULE =
struct
    (* Import *)

    structure C     = GrammarCore
    structure M     = GrammarModule

    type Info       = M.Info

    type VId        = M.VId
    type TyCon      = M.TyCon
    type StrId      = M.StrId
    type SigId      = M.SigId
    type FunId      = M.FunId
    type longTyCon  = M.longTyCon

    type Ty         = M.Ty
    type TyVarseq   = M.TyVarseq

    type StrExp     = M.StrExp
    type StrDec     = M.StrDec
    type StrBind    = M.StrBind
    type SigExp     = M.SigExp
    type TyReaDesc  = (M.Info * M.TyVarseq * M.longTyCon * M.Ty) list
    type Spec       = M.Spec
    type SynDesc    = (M.Info * M.TyVarseq * M.TyCon * M.Ty) list
    type FunBind    = M.FunBind


    (* Structure Bindings [Figure 18] *)

    fun TRANSStrBind(I, strid, NONE, strexp, strbind_opt) =
	    M.StrBind(I, strid, strexp, strbind_opt)

      | TRANSStrBind(I, strid, SOME sigexp, strexp, strbind_opt) =
	    M.StrBind(I, strid, M.COLONStrExp(I, strexp, sigexp), strbind_opt)

    fun SEALStrBind(I, strid, sigexp, strexp, strbind_opt) =
	    M.StrBind(I, strid, M.SEALStrExp(I, strexp, sigexp), strbind_opt)


    (* Structure Expressions [Figure 18] *)

    fun APPDECStrExp(I, funid, strdec) =
	    M.APPStrExp(I, funid, M.STRUCTStrExp(M.infoStrDec strdec, strdec))


    (* Functor Bindings [Figure 18] *)

    fun TRANSFunBind(I, funid, strid, sigexp, NONE, strexp, funbind_opt) =
	    M.FunBind(I, funid, strid, sigexp, strexp, funbind_opt)

      | TRANSFunBind(I, funid, strid,sigexp, SOME sigexp', strexp, funbind_opt)=
	    M.FunBind(I, funid, strid, sigexp, M.COLONStrExp(I, strexp,sigexp'),
			 funbind_opt)

    fun SEALFunBind(I, funid, strid, sigexp, sigexp', strexp, funbind_opt) =
	    M.FunBind(I, funid, strid, sigexp, M.SEALStrExp(I, strexp, sigexp'),
			 funbind_opt)


    fun TRANSSPECFunBind(I, funid, spec, sigexp_opt, strexp, funbind_opt) =
	let
	    val I'     = M.infoStrExp strexp
	    val strid  = StrId.invent()
	    val sigexp = M.SIGSigExp(M.infoSpec spec, spec)

	    val strdec = M.DECStrDec(I', C.OPENDec(I',[LongStrId.fromId strid]))
	    val strexp'= case sigexp_opt
			   of NONE         => strexp
			    | SOME sigexp' => M.COLONStrExp(I', strexp, sigexp')
	    val letexp = M.LETStrExp(I', strdec, strexp')
	in
	    M.FunBind(I, funid, strid, sigexp, letexp, funbind_opt)
	end

    fun SEALSPECFunBind(I, funid, spec, sigexp', strexp, funbind_opt) =
	let
	    val I'     = M.infoStrExp strexp
	    val strid  = StrId.invent()
	    val sigexp = M.SIGSigExp(M.infoSpec spec, spec)

	    val strdec = M.DECStrDec(I', C.OPENDec(I',[LongStrId.fromId strid]))
	    val strexp'= M.COLONStrExp(I', strexp, sigexp')
	    val letexp = M.LETStrExp(I', strdec, strexp')
	in
	    M.FunBind(I, funid, strid, sigexp, letexp, funbind_opt)
	end


    (* Specifications [Figure 19] *)

    fun SYNSpec(I, [])                            = M.EMPTYSpec(I)
      | SYNSpec(I, (I',tyvarseq,tycon,ty)::syns') =
	let
	    val longtycon = LongTyCon.fromId tycon
	    val typdesc = M.TypDesc(I', tyvarseq, tycon, NONE)
	    val sigexp  = M.SIGSigExp(I', M.TYPESpec(I', typdesc))
	    val sigexp' = M.WHERETYPESigExp(I', sigexp, tyvarseq, longtycon, ty)
	    val spec1   = M.INCLUDESpec(I', sigexp')
	in
	    M.SEQSpec(I, spec1, SYNSpec(I, syns'))
	end

    fun INCLUDEMULTISpec(I,      []       ) = M.EMPTYSpec(I)
      | INCLUDEMULTISpec(I, sigid::sigids') =
	let
	    val spec1 = M.INCLUDESpec(I, M.IDSigExp(I, sigid))
	in
	    M.SEQSpec(I, spec1, INCLUDEMULTISpec(I, sigids'))
	end


    fun SynDesc(I, tyvarseq, tycon, ty, NONE) =
	    (I, tyvarseq, tycon, ty) :: []

      | SynDesc(I, tyvarseq, tycon, ty, SOME syndesc) =
	    (I, tyvarseq, tycon, ty) :: syndesc


    (* Signature Expressions [Figure 19] *)

    fun WHERETYPESigExp(I, sigexp,                           []     ) = sigexp
      | WHERETYPESigExp(I, sigexp, (I',tyvarseq,longtycon,ty)::reas') =
	let
	    val sigexp' = M.WHERETYPESigExp(I', sigexp, tyvarseq, longtycon, ty)
	in
	    WHERETYPESigExp(I, sigexp', reas')
	end


    fun TyReaDesc(I, tyvarseq, longtycon, ty, NONE) =
	    (I, tyvarseq, longtycon, ty) :: []

      | TyReaDesc(I, tyvarseq, longtycon, ty, SOME tyreadesc) =
	    (I, tyvarseq, longtycon, ty) :: tyreadesc
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML program derived forms
 *
 * Definition, Appendix A
 *)

signature DERIVED_FORMS_PROGRAM =
sig
    (* Import *)

    type Info    = GrammarProgram.Info

    type Exp     = GrammarCore.Exp
    type TopDec  = GrammarModule.TopDec
    type Program = GrammarProgram.Program


    (* Programs [Figure 18] *)

    val TOPDECProgram :	Info * TopDec * Program option -> Program
    val EXPProgram :	Info *  Exp   * Program option -> Program
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML program derived forms
 *
 * Definition, Appendix A
 *)

structure DerivedFormsProgram :> DERIVED_FORMS_PROGRAM =
struct
    (* Import *)

    structure C  = GrammarCore
    structure M  = GrammarModule
    structure P  = GrammarProgram

    type Info    = GrammarProgram.Info

    type Exp     = GrammarCore.Exp
    type TopDec  = GrammarModule.TopDec
    type Program = GrammarProgram.Program


    (* Programs [Figure 18] *)

    fun TOPDECProgram(I, topdec, program_opt) =
	    P.Program(I, topdec, program_opt)

    fun EXPProgram(I, exp, program_opt) =
	let
	    val longvid = LongVId.fromId(VId.fromString "it")
	    val pat     = C.ATPat(I, C.IDAtPat(I, C.SANSOp, longvid))
	    val valbind = C.PLAINValBind(I, pat, exp, NONE)
	    val dec     = C.VALDec(I, C.TyVarseq(I, []), valbind)
	    val topdec  = M.STRDECTopDec(I, M.DECStrDec(I, dec), NONE)
	in
	    P.Program(I, topdec, program_opt)
	end
end;
(* ML-Yacc Parser Generator (c) 1989 Andrew W. Appel, David R. Tarditi 
 *
 * $Log$
 * Revision 1.2  2001/09/18 15:20:28  rossberg
 * *** empty log message ***
 *
 * Revision 1.1  2000/11/24 13:32:28  rossberg
 * *** empty log message ***
 *
 * Revision 1.1.1.1  1997/01/14 01:38:04  george
 *   Version 109.24
 *
 * Revision 1.1.1.1  1996/01/31  16:01:42  george
 * Version 109
 * 
 *)

(* base.sig: Base signature file for SML-Yacc.  This file contains signatures
   that must be loaded before any of the files produced by ML-Yacc are loaded
*)

(* STREAM: signature for a lazy stream.*)

signature STREAM =
 sig type 'xa stream
     val streamify : (unit -> '_a) -> '_a stream
     val cons : '_a * '_a stream -> '_a stream
     val get : '_a stream -> '_a * '_a stream
 end

(* LR_TABLE: signature for an LR Table.

   The list of actions and gotos passed to mkLrTable must be ordered by state
   number. The values for state 0 are the first in the list, the values for
    state 1 are next, etc.
*)

signature LR_TABLE =
    sig
        datatype ('a,'b) pairlist = EMPTY | PAIR of 'a * 'b * ('a,'b) pairlist
	datatype state = STATE of int
	datatype term = T of int
	datatype nonterm = NT of int
	datatype action = SHIFT of state
			| REDUCE of int
			| ACCEPT
			| ERROR
	type table
	
	val numStates : table -> int
	val numRules : table -> int
	val describeActions : table -> state ->
				(term,action) pairlist * action
	val describeGoto : table -> state -> (nonterm,state) pairlist
	val action : table -> state * term -> action
	val goto : table -> state * nonterm -> state
	val initialState : table -> state
	exception Goto of state * nonterm

	val mkLrTable : {actions : ((term,action) pairlist * action) array,
			 gotos : (nonterm,state) pairlist array,
			 numStates : int, numRules : int,
			 initialState : state} -> table
    end

(* TOKEN: signature revealing the internal structure of a token. This signature
   TOKEN distinct from the signature {parser name}_TOKENS produced by ML-Yacc.
   The {parser name}_TOKENS structures contain some types and functions to
    construct tokens from values and positions.

   The representation of token was very carefully chosen here to allow the
   polymorphic parser to work without knowing the types of semantic values
   or line numbers.

   This has had an impact on the TOKENS structure produced by SML-Yacc, which
   is a structure parameter to lexer functors.  We would like to have some
   type 'a token which functions to construct tokens would create.  A
   constructor function for a integer token might be

	  INT: int * 'a * 'a -> 'a token.
 
   This is not possible because we need to have tokens with the representation
   given below for the polymorphic parser.

   Thus our constructur functions for tokens have the form:

	  INT: int * 'a * 'a -> (svalue,'a) token

   This in turn has had an impact on the signature that lexers for SML-Yacc
   must match and the types that a user must declare in the user declarations
   section of lexers.
*)

signature TOKEN =
    sig
	structure LrTable : LR_TABLE
        datatype ('a,'b) token = TOKEN of LrTable.term * ('a * 'b * 'b)
	val sameToken : ('a,'b) token * ('a,'b) token -> bool
    end

(* LR_PARSER: signature for a polymorphic LR parser *)

signature LR_PARSER =
    sig
	structure Stream: STREAM
	structure LrTable : LR_TABLE
	structure Token : TOKEN

	sharing type LrTable.pairlist = Token.LrTable.pairlist
	sharing type LrTable.state = Token.LrTable.state
	sharing type LrTable.term = Token.LrTable.term
	sharing type LrTable.nonterm = Token.LrTable.nonterm
	sharing type LrTable.action = Token.LrTable.action
	sharing type LrTable.table = Token.LrTable.table

	exception ParseError

	val parse : {table : LrTable.table,
		     lexer : ('_b,'_c) Token.token Stream.stream,
		     arg: 'arg,
		     saction : int *
			       '_c *
				(LrTable.state * ('_b * '_c * '_c)) list * 
				'arg ->
				     LrTable.nonterm *
				     ('_b * '_c * '_c) *
				     ((LrTable.state *('_b * '_c * '_c)) list),
		     void : '_b,
		     ec : { is_keyword : LrTable.term -> bool,
			    noShift : LrTable.term -> bool,
			    preferred_change : (LrTable.term list * LrTable.term list) list,
			    errtermvalue : LrTable.term -> '_b,
			    showTerminal : LrTable.term -> string,
			    terms: LrTable.term list,
			    error : string * '_c * '_c -> unit
			   },
		     lookahead : int  (* max amount of lookahead used in *)
				      (* error correction *)
			} -> '_b *
			     (('_b,'_c) Token.token Stream.stream)
    end

(* LEXER: a signature that most lexers produced for use with SML-Yacc's
   output will match.  The user is responsible for declaring type token,
   type pos, and type svalue in the UserDeclarations section of a lexer.

   Note that type token is abstract in the lexer.  This allows SML-Yacc to
   create a TOKENS signature for use with lexers produced by ML-Lex that
   treats the type token abstractly.  Lexers that are functors parametrized by
   a Tokens structure matching a TOKENS signature cannot examine the structure
   of tokens.
*)

signature LEXER =
   sig
       structure UserDeclarations :
	   sig
	        type ('a,'b) token
		type pos
		type svalue
	   end
	val makeLexer : (int -> string) -> unit -> 
         (UserDeclarations.svalue,UserDeclarations.pos) UserDeclarations.token
   end

(* ARG_LEXER: the %arg option of ML-Lex allows users to produce lexers which
   also take an argument before yielding a function from unit to a token
*)

signature ARG_LEXER =
   sig
       structure UserDeclarations :
	   sig
	        type ('a,'b) token
		type pos
		type svalue
		type arg
	   end
	val makeLexer : (int -> string) -> UserDeclarations.arg -> unit -> 
         (UserDeclarations.svalue,UserDeclarations.pos) UserDeclarations.token
   end

(* PARSER_DATA: the signature of ParserData structures in {parser name}LrValsFun
   produced by  SML-Yacc.  All such structures match this signature.  

   The {parser name}LrValsFun produces a structure which contains all the values
   except for the lexer needed to call the polymorphic parser mentioned
   before.

*)

signature PARSER_DATA =
   sig
        (* the type of line numbers *)

	type pos

	(* the type of semantic values *)

	type svalue

         (* the type of the user-supplied argument to the parser *)
 	type arg
 
	(* the intended type of the result of the parser.  This value is
	   produced by applying extract from the structure Actions to the
	   final semantic value resultiing from a parse.
	 *)

	type result

	structure LrTable : LR_TABLE
	structure Token : TOKEN
	sharing type LrTable.pairlist = Token.LrTable.pairlist
	sharing type LrTable.state = Token.LrTable.state
	sharing type LrTable.term = Token.LrTable.term
	sharing type LrTable.nonterm = Token.LrTable.nonterm
	sharing type LrTable.action = Token.LrTable.action
	sharing type LrTable.table = Token.LrTable.table

	(* structure Actions contains the functions which mantain the
	   semantic values stack in the parser.  Void is used to provide
	   a default value for the semantic stack.
	 *)

	structure Actions : 
	  sig
	      val actions : int * pos *
		   (LrTable.state * (svalue * pos * pos)) list * arg->
		         LrTable.nonterm * (svalue * pos * pos) *
			 ((LrTable.state *(svalue * pos * pos)) list)
	      val void : svalue
	      val extract : svalue -> result
	  end

	(* structure EC contains information used to improve error
	   recovery in an error-correcting parser *)

	structure EC :
	   sig
	     val is_keyword : LrTable.term -> bool
	     val noShift : LrTable.term -> bool
 	     val preferred_change : (LrTable.term list * LrTable.term list) list
	     val errtermvalue : LrTable.term -> svalue
	     val showTerminal : LrTable.term -> string
	     val terms: LrTable.term list
	   end

	(* table is the LR table for the parser *)

	val table : LrTable.table
    end

(* signature PARSER is the signature that most user parsers created by 
   SML-Yacc will match.
*)

signature PARSER =
    sig
        structure Token : TOKEN
	structure Stream : STREAM
	exception ParseError

	(* type pos is the type of line numbers *)

	type pos

	(* type result is the type of the result from the parser *)

	type result

         (* the type of the user-supplied argument to the parser *)
 	type arg
	
	(* type svalue is the type of semantic values for the semantic value
	   stack
	 *)

	type svalue

	(* val makeLexer is used to create a stream of tokens for the parser *)

	val makeLexer : (int -> string) ->
			 (svalue,pos) Token.token Stream.stream

	(* val parse takes a stream of tokens and a function to print
	   errors and returns a value of type result and a stream containing
	   the unused tokens
	 *)

	val parse : int * ((svalue,pos) Token.token Stream.stream) *
		    (string * pos * pos -> unit) * arg ->
				result * (svalue,pos) Token.token Stream.stream

	val sameToken : (svalue,pos) Token.token * (svalue,pos) Token.token ->
				bool
     end

(* signature ARG_PARSER is the signature that will be matched by parsers whose
    lexer takes an additional argument.
*)

signature ARG_PARSER = 
    sig
        structure Token : TOKEN
	structure Stream : STREAM
	exception ParseError

	type arg
	type lexarg
	type pos
	type result
	type svalue

	val makeLexer : (int -> string) -> lexarg ->
			 (svalue,pos) Token.token Stream.stream
	val parse : int * ((svalue,pos) Token.token Stream.stream) *
		    (string * pos * pos -> unit) * arg ->
				result * (svalue,pos) Token.token Stream.stream

	val sameToken : (svalue,pos) Token.token * (svalue,pos) Token.token ->
				bool
     end
;
signature Parser_TOKENS =
sig
type ('a,'b) token
type svalue
val LONGID: (string list*string) *  'a * 'a -> (svalue,'a) token
val ETYVAR: (string) *  'a * 'a -> (svalue,'a) token
val TYVAR: (string) *  'a * 'a -> (svalue,'a) token
val STAR:  'a * 'a -> (svalue,'a) token
val SYMBOL: (string) *  'a * 'a -> (svalue,'a) token
val ALPHA: (string) *  'a * 'a -> (svalue,'a) token
val CHAR: (string) *  'a * 'a -> (svalue,'a) token
val STRING: (string) *  'a * 'a -> (svalue,'a) token
val REAL: (string) *  'a * 'a -> (svalue,'a) token
val HEXWORD: (string) *  'a * 'a -> (svalue,'a) token
val WORD: (string) *  'a * 'a -> (svalue,'a) token
val HEXINT: (string) *  'a * 'a -> (svalue,'a) token
val INT: (string) *  'a * 'a -> (svalue,'a) token
val NUMERIC: (string) *  'a * 'a -> (svalue,'a) token
val DIGIT: (string) *  'a * 'a -> (svalue,'a) token
val ZERO:  'a * 'a -> (svalue,'a) token
val SEAL:  'a * 'a -> (svalue,'a) token
val WHERE:  'a * 'a -> (svalue,'a) token
val STRUCTURE:  'a * 'a -> (svalue,'a) token
val STRUCT:  'a * 'a -> (svalue,'a) token
val SIGNATURE:  'a * 'a -> (svalue,'a) token
val SIG:  'a * 'a -> (svalue,'a) token
val SHARING:  'a * 'a -> (svalue,'a) token
val INCLUDE:  'a * 'a -> (svalue,'a) token
val FUNCTOR:  'a * 'a -> (svalue,'a) token
val EQTYPE:  'a * 'a -> (svalue,'a) token
val HASH:  'a * 'a -> (svalue,'a) token
val ARROW:  'a * 'a -> (svalue,'a) token
val DARROW:  'a * 'a -> (svalue,'a) token
val EQUALS:  'a * 'a -> (svalue,'a) token
val BAR:  'a * 'a -> (svalue,'a) token
val UNDERBAR:  'a * 'a -> (svalue,'a) token
val DOTS:  'a * 'a -> (svalue,'a) token
val SEMICOLON:  'a * 'a -> (svalue,'a) token
val COLON:  'a * 'a -> (svalue,'a) token
val COMMA:  'a * 'a -> (svalue,'a) token
val RBRACE:  'a * 'a -> (svalue,'a) token
val LBRACE:  'a * 'a -> (svalue,'a) token
val RBRACK:  'a * 'a -> (svalue,'a) token
val LBRACK:  'a * 'a -> (svalue,'a) token
val RPAR:  'a * 'a -> (svalue,'a) token
val LPAR:  'a * 'a -> (svalue,'a) token
val WHILE:  'a * 'a -> (svalue,'a) token
val WITHTYPE:  'a * 'a -> (svalue,'a) token
val WITH:  'a * 'a -> (svalue,'a) token
val VAL:  'a * 'a -> (svalue,'a) token
val TYPE:  'a * 'a -> (svalue,'a) token
val THEN:  'a * 'a -> (svalue,'a) token
val REC:  'a * 'a -> (svalue,'a) token
val RAISE:  'a * 'a -> (svalue,'a) token
val ORELSE:  'a * 'a -> (svalue,'a) token
val OPEN:  'a * 'a -> (svalue,'a) token
val OP:  'a * 'a -> (svalue,'a) token
val OF:  'a * 'a -> (svalue,'a) token
val NONFIX:  'a * 'a -> (svalue,'a) token
val LOCAL:  'a * 'a -> (svalue,'a) token
val LET:  'a * 'a -> (svalue,'a) token
val INFIXR:  'a * 'a -> (svalue,'a) token
val INFIX:  'a * 'a -> (svalue,'a) token
val IN:  'a * 'a -> (svalue,'a) token
val IF:  'a * 'a -> (svalue,'a) token
val HANDLE:  'a * 'a -> (svalue,'a) token
val FUN:  'a * 'a -> (svalue,'a) token
val FN:  'a * 'a -> (svalue,'a) token
val EXCEPTION:  'a * 'a -> (svalue,'a) token
val END:  'a * 'a -> (svalue,'a) token
val ELSE:  'a * 'a -> (svalue,'a) token
val DATATYPE:  'a * 'a -> (svalue,'a) token
val DO:  'a * 'a -> (svalue,'a) token
val CASE:  'a * 'a -> (svalue,'a) token
val AS:  'a * 'a -> (svalue,'a) token
val ANDALSO:  'a * 'a -> (svalue,'a) token
val AND:  'a * 'a -> (svalue,'a) token
val ABSTYPE:  'a * 'a -> (svalue,'a) token
val EOF:  'a * 'a -> (svalue,'a) token
end
signature Parser_LRVALS=
sig

structure ParserData:PARSER_DATA
structure Tokens : Parser_TOKENS
sharing type ParserData.Token.token = Tokens.token
sharing type ParserData.svalue = Tokens.svalue
end
;
functor LrValsFn(structure Token : TOKEN) = 
struct
structure ParserData=
struct
structure Header = 
struct
(*										*)
(* (c) Andreas Rossberg 1999-2007						*)
(*										*)
(* Standard ML syntactical analysis						*)
(*										*)
(* Definition, Sections 2, 3, and 8, Appendix A and B				*)
(*										*)
(* Notes:									*)
(*   - Two phrases named Fmatch and Fmrule have been added to factorize		*)
(*     Fvalbind.								*)
(*   - A phrase named SynDesc has been added to factorize type synonym		*)
(*     specifications. Similarly, a phrase named TyReaDesc has been added to	*)
(*     factorize type realisation signature expressions.			*)
(*   - Infix expressions [Definition, Section 2.6] are resolved externally in	*)
(*     structure Infix. The parser just maintains the infix environment J by	*)
(*     side effect. To achieve correct treatment of scoped fixity directives,	*)
(*     a stack of environments is used. To handle `local' we even need a	*)
(*     second environment J' (together with a a second stack).			*)
(*   - Syntactic restrictions [Definition, Sections 2.9 and 3.5] are checked	*)
(*     in a separate pass.							*)
(*   - Although not completely clear from the wording we assume that the	*)
(*     Definition rules out the use of `=' as a tycon. Otherwise we would have	*)
(*     a massive amount of grammar conflicts.					*)
(*   - The Definition is also vague about what consists a non-infixed occurence	*)
(*     of an infix identifier: we assume any occurences in expressions		*)
(*     or patterns. This implies that uses of the keyword `op' in constructor	*)
(*     and exception bindings are completely redundant.				*)
(*   - Datatype replication requires rules for datatype to be duplicated to	*)
(*     avoid conflicts on empty tyvarseqs.					*)
(*   - Layered patterns require some grammar transformation hack, see pat.	*)
(*   - The messy `sigexp where type ... and type ...' syntax requires some	*)
(*     really ugly transformations (in absence of a lookahead of 2), watch out	*)
(*     for non-terminals of the form xxx__AND_yyybind_opt.			*)
(*   - ML-Yacc does not seem to like comments that stretch over several		*)
(*     lines... Similarly, comments in semantic actions make it puke...		*)
(*										*)
(* Bugs:									*)
(*   - We do NOT support declarations like					*)
(*	  fun f p1 = case e1 of p2 => e2					*)
(*	    | f p3 = e3								*)
(*     (without parentheses around the case) because the transformations	*)
(*     required to support this would be even a magnitude uglier than those	*)
(*     above. In fact, no compiler I know of supports this.			*)
(*										*)


    (* Import *)

    open GrammarCore
    open GrammarModule
    open GrammarProgram
    open DerivedFormsCore
    open DerivedFormsModule
    open DerivedFormsProgram


    (* Handling infix environments *)

    val J  = ref Infix.empty	(* context *)
    val J' = ref Infix.empty	(* local environment (+ enclosing one) *)

    val stackJ  = ref [] : Infix.InfEnv list ref
    val stackJ' = ref [] : Infix.InfEnv list ref

    fun initJandJ'(J0) =
	(
	    J       := J0;
	    J'      := J0;
	    stackJ  := [];
	    stackJ' := []
	)

    fun pushJ() =
	(
	    stackJ  := !J :: !stackJ
	)

    fun popJ() =
	(
	    J       := List.hd(!stackJ);
	    stackJ  := List.tl(!stackJ)
	)

    fun pushJ'shiftJ() =
	(
	    stackJ' := !J' :: !stackJ';
	    J'      := List.hd(!stackJ)
	)

    fun popJandJ'() =
	(
	    J       := !J';
	    J'      := List.hd(!stackJ');
	    stackJ  := List.tl(!stackJ);
	    stackJ' := List.tl(!stackJ')
	)


    fun assignInfix(infstatus, vids) =
	(
	    J  := Infix.assign(!J, vids, infstatus);
	    J' := Infix.assign(!J', vids, infstatus)
	)

    fun cancelInfix(vids) =
	(
	    J  := Infix.cancel(!J, vids);
	    J' := Infix.cancel(!J', vids)
	)


    (* Helper for long identifiers *)

    fun toLongId toId (strids, id) =
	    ( List.map StrId.fromString strids, toId id )


    (* Helper to handle typed patterns (needed because of layered patterns) *)

    fun typedPat(pat,   []   ) = pat
      | typedPat(pat, ty::tys) =
	let
	    val I = Source.over(infoPat pat, infoTy ty)
	in
	    typedPat(COLONPat(I, pat, ty), tys)
	end




end
structure LrTable = Token.LrTable
structure Token = Token
local open LrTable in 
val table=let val actionRows =
"\
\\001\000\001\000\000\000\000\000\
\\001\000\001\000\194\003\002\000\051\000\006\000\050\000\008\000\049\000\
\\011\000\048\000\012\000\047\000\013\000\046\000\015\000\045\000\
\\017\000\044\000\018\000\043\000\019\000\042\000\020\000\041\000\
\\021\000\040\000\023\000\039\000\024\000\038\000\026\000\037\000\
\\029\000\036\000\030\000\035\000\033\000\034\000\034\000\033\000\
\\036\000\032\000\038\000\031\000\042\000\184\003\046\000\158\002\
\\049\000\030\000\051\000\029\000\055\000\028\000\057\000\027\000\
\\060\000\026\000\061\000\025\000\062\000\024\000\063\000\023\000\
\\064\000\022\000\065\000\021\000\066\000\020\000\067\000\019\000\
\\068\000\018\000\069\000\017\000\070\000\158\002\071\000\158\002\
\\072\000\158\002\075\000\158\002\000\000\
\\001\000\002\000\146\002\003\000\146\002\008\000\146\002\010\000\146\002\
\\011\000\146\002\013\000\146\002\016\000\146\002\017\000\146\002\
\\018\000\146\002\020\000\146\002\021\000\146\002\024\000\146\002\
\\029\000\146\002\030\000\146\002\034\000\148\002\035\000\146\002\
\\041\000\146\002\042\000\146\002\051\000\146\002\055\000\146\002\
\\057\000\146\002\059\000\146\002\000\000\
\\001\000\002\000\185\002\003\000\185\002\004\000\185\002\007\000\185\002\
\\008\000\185\002\009\000\185\002\010\000\185\002\011\000\185\002\
\\013\000\185\002\014\000\185\002\016\000\185\002\017\000\185\002\
\\018\000\185\002\019\000\042\000\020\000\185\002\021\000\185\002\
\\022\000\185\002\023\000\039\000\024\000\185\002\025\000\185\002\
\\028\000\185\002\029\000\185\002\030\000\185\002\034\000\033\000\
\\035\000\185\002\036\000\032\000\037\000\185\002\038\000\031\000\
\\039\000\185\002\040\000\185\002\041\000\185\002\042\000\185\002\
\\045\000\185\002\046\000\158\002\049\000\030\000\051\000\185\002\
\\055\000\185\002\057\000\185\002\060\000\026\000\061\000\025\000\
\\062\000\024\000\063\000\023\000\064\000\022\000\065\000\021\000\
\\066\000\020\000\067\000\019\000\068\000\018\000\069\000\017\000\
\\070\000\158\002\071\000\158\002\072\000\158\002\075\000\158\002\000\000\
\\001\000\002\000\041\003\003\000\041\003\004\000\041\003\005\000\041\003\
\\007\000\041\003\008\000\041\003\009\000\041\003\010\000\041\003\
\\011\000\041\003\013\000\041\003\014\000\041\003\016\000\041\003\
\\017\000\041\003\018\000\041\003\020\000\041\003\021\000\041\003\
\\022\000\041\003\024\000\041\003\025\000\041\003\028\000\041\003\
\\029\000\041\003\030\000\041\003\031\000\041\003\032\000\041\003\
\\035\000\041\003\037\000\041\003\039\000\041\003\040\000\041\003\
\\041\000\041\003\042\000\041\003\045\000\041\003\046\000\041\003\
\\047\000\041\003\048\000\041\003\050\000\041\003\051\000\041\003\
\\052\000\041\003\053\000\041\003\055\000\041\003\057\000\041\003\
\\058\000\041\003\059\000\041\003\070\000\052\003\071\000\052\003\
\\072\000\250\000\075\000\052\003\000\000\
\\001\000\002\000\082\003\003\000\152\001\008\000\082\003\010\000\082\003\
\\011\000\082\003\013\000\082\003\016\000\082\003\017\000\082\003\
\\018\000\082\003\020\000\082\003\021\000\082\003\024\000\082\003\
\\029\000\082\003\030\000\082\003\035\000\082\003\041\000\063\003\
\\042\000\082\003\051\000\082\003\055\000\082\003\057\000\082\003\
\\059\000\063\003\000\000\
\\001\000\002\000\082\003\003\000\152\001\008\000\082\003\010\000\082\003\
\\011\000\082\003\013\000\082\003\016\000\082\003\017\000\082\003\
\\018\000\082\003\020\000\082\003\021\000\082\003\024\000\082\003\
\\029\000\082\003\030\000\082\003\035\000\082\003\041\000\093\003\
\\042\000\082\003\051\000\082\003\055\000\082\003\057\000\082\003\
\\058\000\093\003\059\000\093\003\000\000\
\\001\000\002\000\082\003\003\000\102\002\008\000\082\003\010\000\082\003\
\\011\000\082\003\013\000\082\003\016\000\082\003\017\000\082\003\
\\018\000\082\003\020\000\082\003\021\000\082\003\024\000\082\003\
\\029\000\082\003\030\000\082\003\035\000\082\003\041\000\108\003\
\\042\000\082\003\051\000\082\003\055\000\082\003\057\000\082\003\
\\058\000\108\003\059\000\108\003\000\000\
\\001\000\002\000\100\003\003\000\088\001\008\000\100\003\011\000\100\003\
\\013\000\100\003\017\000\100\003\018\000\100\003\020\000\100\003\
\\021\000\100\003\024\000\100\003\029\000\100\003\030\000\100\003\
\\042\000\100\003\051\000\100\003\055\000\100\003\057\000\100\003\
\\058\000\093\003\000\000\
\\001\000\002\000\100\003\003\000\082\002\008\000\100\003\011\000\100\003\
\\013\000\100\003\017\000\100\003\018\000\100\003\020\000\100\003\
\\021\000\100\003\024\000\100\003\029\000\100\003\030\000\100\003\
\\042\000\100\003\051\000\100\003\055\000\100\003\057\000\100\003\
\\058\000\108\003\000\000\
\\001\000\002\000\174\003\003\000\037\002\008\000\174\003\011\000\174\003\
\\013\000\174\003\017\000\174\003\018\000\174\003\020\000\174\003\
\\021\000\174\003\024\000\174\003\029\000\174\003\030\000\174\003\
\\041\000\063\003\042\000\174\003\051\000\174\003\055\000\174\003\
\\057\000\174\003\059\000\063\003\000\000\
\\001\000\002\000\174\003\003\000\037\002\008\000\174\003\011\000\174\003\
\\013\000\174\003\017\000\174\003\018\000\174\003\020\000\174\003\
\\021\000\174\003\024\000\174\003\029\000\174\003\030\000\174\003\
\\041\000\093\003\042\000\174\003\051\000\174\003\055\000\174\003\
\\057\000\174\003\058\000\093\003\059\000\093\003\000\000\
\\001\000\002\000\174\003\003\000\113\002\008\000\174\003\011\000\174\003\
\\013\000\174\003\017\000\174\003\018\000\174\003\020\000\174\003\
\\021\000\174\003\024\000\174\003\029\000\174\003\030\000\174\003\
\\041\000\108\003\042\000\174\003\051\000\174\003\055\000\174\003\
\\057\000\174\003\058\000\108\003\059\000\108\003\000\000\
\\001\000\002\000\051\000\006\000\050\000\008\000\049\000\011\000\048\000\
\\012\000\047\000\013\000\046\000\015\000\045\000\017\000\044\000\
\\018\000\043\000\019\000\042\000\020\000\041\000\021\000\040\000\
\\023\000\039\000\024\000\038\000\026\000\037\000\029\000\036\000\
\\030\000\035\000\033\000\034\000\034\000\033\000\036\000\032\000\
\\038\000\031\000\042\000\184\003\046\000\158\002\049\000\030\000\
\\051\000\029\000\055\000\028\000\057\000\027\000\060\000\026\000\
\\061\000\025\000\062\000\024\000\063\000\023\000\064\000\022\000\
\\065\000\021\000\066\000\020\000\067\000\019\000\068\000\018\000\
\\069\000\017\000\070\000\158\002\071\000\158\002\072\000\158\002\
\\075\000\158\002\000\000\
\\001\000\003\000\010\002\008\000\162\003\010\000\162\003\011\000\162\003\
\\029\000\162\003\030\000\162\003\035\000\162\003\042\000\162\003\
\\050\000\162\003\052\000\162\003\053\000\162\003\057\000\162\003\
\\058\000\093\003\000\000\
\\001\000\003\000\106\002\008\000\162\003\010\000\162\003\011\000\162\003\
\\029\000\162\003\030\000\162\003\035\000\162\003\042\000\162\003\
\\050\000\162\003\052\000\162\003\053\000\162\003\057\000\162\003\
\\058\000\108\003\000\000\
\\001\000\004\000\061\000\007\000\185\000\014\000\060\000\025\000\059\000\
\\041\000\058\000\000\000\
\\001\000\004\000\061\000\009\000\135\001\014\000\060\000\025\000\059\000\
\\041\000\058\000\000\000\
\\001\000\004\000\061\000\014\000\060\000\022\000\244\000\025\000\059\000\
\\041\000\058\000\000\000\
\\001\000\004\000\061\000\014\000\060\000\025\000\059\000\028\000\226\000\
\\041\000\058\000\000\000\
\\001\000\004\000\061\000\014\000\060\000\025\000\059\000\035\000\182\000\
\\040\000\181\000\041\000\058\000\042\000\180\000\000\000\
\\001\000\004\000\061\000\014\000\060\000\025\000\059\000\041\000\058\000\
\\042\000\057\000\000\000\
\\001\000\005\000\140\002\039\000\140\002\040\000\140\002\041\000\140\002\
\\046\000\133\002\000\000\
\\001\000\005\000\141\002\039\000\141\002\040\000\141\002\041\000\141\002\
\\046\000\134\002\000\000\
\\001\000\005\000\142\002\039\000\142\002\040\000\142\002\041\000\142\002\
\\046\000\135\002\000\000\
\\001\000\005\000\019\003\035\000\036\003\037\000\036\003\039\000\036\003\
\\040\000\036\003\041\000\188\000\046\000\036\003\047\000\036\003\000\000\
\\001\000\005\000\020\003\023\000\151\002\034\000\151\002\035\000\151\002\
\\036\000\151\002\037\000\151\002\038\000\151\002\039\000\151\002\
\\040\000\151\002\041\000\033\001\044\000\151\002\046\000\151\002\
\\047\000\151\002\060\000\151\002\061\000\151\002\062\000\151\002\
\\063\000\151\002\064\000\151\002\065\000\151\002\066\000\151\002\
\\067\000\151\002\068\000\151\002\069\000\151\002\070\000\151\002\
\\071\000\151\002\072\000\151\002\075\000\151\002\000\000\
\\001\000\005\000\122\001\000\000\
\\001\000\006\000\050\000\012\000\047\000\015\000\045\000\019\000\042\000\
\\023\000\039\000\026\000\037\000\033\000\034\000\034\000\033\000\
\\036\000\032\000\037\000\170\002\038\000\031\000\046\000\158\002\
\\049\000\030\000\060\000\026\000\061\000\025\000\062\000\024\000\
\\063\000\023\000\064\000\022\000\065\000\021\000\066\000\020\000\
\\067\000\019\000\068\000\018\000\069\000\017\000\070\000\158\002\
\\071\000\158\002\072\000\158\002\075\000\158\002\000\000\
\\001\000\008\000\238\001\070\000\069\000\071\000\068\000\072\000\067\000\000\000\
\\001\000\010\000\161\001\000\000\
\\001\000\010\000\203\001\000\000\
\\001\000\010\000\243\001\000\000\
\\001\000\010\000\025\002\000\000\
\\001\000\010\000\032\002\000\000\
\\001\000\010\000\075\002\000\000\
\\001\000\010\000\077\002\000\000\
\\001\000\016\000\047\001\000\000\
\\001\000\016\000\049\001\000\000\
\\001\000\016\000\195\001\000\000\
\\001\000\016\000\000\002\000\000\
\\001\000\019\000\083\001\056\000\082\001\070\000\081\001\075\000\120\000\000\000\
\\001\000\023\000\039\000\034\000\144\000\035\000\025\003\036\000\109\000\
\\037\000\025\003\038\000\108\000\039\000\025\003\040\000\025\003\
\\044\000\107\000\046\000\025\003\047\000\025\003\060\000\026\000\
\\061\000\025\000\062\000\024\000\063\000\023\000\064\000\022\000\
\\065\000\021\000\066\000\020\000\067\000\019\000\068\000\018\000\
\\069\000\017\000\070\000\158\002\071\000\158\002\072\000\158\002\
\\075\000\158\002\000\000\
\\001\000\023\000\039\000\034\000\144\000\035\000\033\003\036\000\109\000\
\\037\000\033\003\038\000\108\000\039\000\033\003\040\000\033\003\
\\041\000\033\003\044\000\107\000\046\000\033\003\047\000\033\003\
\\060\000\026\000\061\000\025\000\062\000\024\000\063\000\023\000\
\\064\000\022\000\065\000\021\000\066\000\020\000\067\000\019\000\
\\068\000\018\000\069\000\017\000\070\000\158\002\071\000\158\002\
\\072\000\158\002\075\000\158\002\000\000\
\\001\000\023\000\039\000\034\000\144\000\036\000\109\000\037\000\010\003\
\\038\000\108\000\044\000\107\000\060\000\026\000\061\000\025\000\
\\062\000\024\000\063\000\023\000\064\000\022\000\065\000\021\000\
\\066\000\020\000\067\000\019\000\068\000\018\000\069\000\017\000\
\\070\000\158\002\071\000\158\002\072\000\158\002\075\000\158\002\000\000\
\\001\000\029\000\100\001\070\000\073\000\075\000\120\000\000\000\
\\001\000\029\000\159\001\000\000\
\\001\000\029\000\164\001\000\000\
\\001\000\029\000\164\001\070\000\076\000\000\000\
\\001\000\029\000\167\001\070\000\073\000\075\000\120\000\000\000\
\\001\000\029\000\029\002\000\000\
\\001\000\029\000\029\002\070\000\073\000\000\000\
\\001\000\029\000\045\002\000\000\
\\001\000\029\000\045\002\070\000\073\000\000\000\
\\001\000\029\000\093\002\000\000\
\\001\000\029\000\093\002\070\000\079\000\000\000\
\\001\000\031\000\068\001\000\000\
\\001\000\034\000\115\000\070\000\152\000\071\000\151\000\073\000\106\000\000\000\
\\001\000\034\000\175\000\000\000\
\\001\000\034\000\155\001\000\000\
\\001\000\035\000\183\000\000\000\
\\001\000\035\000\184\000\000\000\
\\001\000\035\000\040\001\000\000\
\\001\000\035\000\042\001\040\000\041\001\000\000\
\\001\000\035\000\043\001\000\000\
\\001\000\035\000\073\001\000\000\
\\001\000\035\000\075\001\040\000\074\001\000\000\
\\001\000\035\000\094\001\000\000\
\\001\000\035\000\222\001\058\000\084\001\000\000\
\\001\000\035\000\251\001\000\000\
\\001\000\035\000\254\001\041\000\253\001\059\000\252\001\000\000\
\\001\000\037\000\179\000\000\000\
\\001\000\037\000\039\001\000\000\
\\001\000\039\000\176\000\000\000\
\\001\000\039\000\035\001\000\000\
\\001\000\039\000\071\001\000\000\
\\001\000\041\000\072\001\000\000\
\\001\000\041\000\095\001\000\000\
\\001\000\041\000\154\001\059\000\153\001\000\000\
\\001\000\041\000\171\001\000\000\
\\001\000\041\000\178\001\000\000\
\\001\000\041\000\039\002\059\000\038\002\000\000\
\\001\000\042\000\052\000\000\000\
\\001\000\043\000\202\000\061\000\085\000\062\000\084\000\070\000\201\000\
\\071\000\200\000\072\000\199\000\000\000\
\\001\000\046\000\070\000\070\000\069\000\071\000\068\000\072\000\067\000\
\\075\000\066\000\000\000\
\\001\000\046\000\124\000\070\000\069\000\071\000\068\000\072\000\067\000\000\000\
\\001\000\046\000\174\000\000\000\
\\001\000\046\000\177\000\000\000\
\\001\000\046\000\189\000\000\000\
\\001\000\046\000\243\000\000\000\
\\001\000\046\000\001\001\000\000\
\\001\000\046\000\037\001\000\000\
\\001\000\046\000\045\001\000\000\
\\001\000\046\000\052\001\000\000\
\\001\000\046\000\061\001\000\000\
\\001\000\046\000\067\001\000\000\
\\001\000\046\000\085\001\058\000\084\001\000\000\
\\001\000\046\000\172\001\000\000\
\\001\000\046\000\182\001\000\000\
\\001\000\046\000\220\001\000\000\
\\001\000\046\000\228\001\000\000\
\\001\000\046\000\232\001\000\000\
\\001\000\046\000\235\001\000\000\
\\001\000\046\000\006\002\058\000\084\001\000\000\
\\001\000\046\000\034\002\000\000\
\\001\000\046\000\035\002\000\000\
\\001\000\046\000\041\002\000\000\
\\001\000\046\000\068\002\058\000\084\001\000\000\
\\001\000\046\000\088\002\000\000\
\\001\000\046\000\089\002\000\000\
\\001\000\046\000\094\002\000\000\
\\001\000\046\000\108\002\000\000\
\\001\000\047\000\235\000\000\000\
\\001\000\054\000\005\001\070\000\076\000\000\000\
\\001\000\058\000\089\001\000\000\
\\001\000\058\000\011\002\000\000\
\\001\000\061\000\085\000\062\000\084\000\070\000\083\000\071\000\082\000\
\\072\000\081\000\000\000\
\\001\000\070\000\069\000\071\000\068\000\072\000\067\000\000\000\
\\001\000\070\000\069\000\071\000\068\000\072\000\067\000\075\000\066\000\000\000\
\\001\000\070\000\069\000\071\000\068\000\072\000\067\000\075\000\194\000\000\000\
\\001\000\070\000\073\000\000\000\
\\001\000\070\000\073\000\075\000\120\000\000\000\
\\001\000\070\000\076\000\000\000\
\\001\000\070\000\079\000\000\000\
\\001\000\070\000\152\000\071\000\151\000\000\000\
\\001\000\070\000\152\000\071\000\151\000\075\000\249\000\000\000\
\\001\000\073\000\106\000\000\000\
\\116\002\000\000\
\\117\002\000\000\
\\118\002\000\000\
\\118\002\041\000\253\001\059\000\252\001\000\000\
\\119\002\000\000\
\\120\002\000\000\
\\121\002\000\000\
\\122\002\000\000\
\\123\002\000\000\
\\124\002\000\000\
\\125\002\000\000\
\\126\002\000\000\
\\127\002\000\000\
\\128\002\000\000\
\\129\002\000\000\
\\130\002\000\000\
\\131\002\000\000\
\\132\002\000\000\
\\133\002\000\000\
\\134\002\000\000\
\\135\002\000\000\
\\136\002\000\000\
\\137\002\000\000\
\\138\002\000\000\
\\139\002\000\000\
\\140\002\000\000\
\\141\002\000\000\
\\142\002\000\000\
\\143\002\000\000\
\\144\002\000\000\
\\145\002\000\000\
\\146\002\000\000\
\\147\002\000\000\
\\148\002\000\000\
\\149\002\000\000\
\\150\002\000\000\
\\151\002\000\000\
\\152\002\000\000\
\\152\002\041\000\188\000\000\000\
\\153\002\000\000\
\\154\002\000\000\
\\155\002\000\000\
\\156\002\000\000\
\\157\002\000\000\
\\158\002\006\000\050\000\012\000\047\000\015\000\045\000\019\000\042\000\
\\023\000\039\000\026\000\037\000\033\000\034\000\034\000\033\000\
\\035\000\095\000\036\000\032\000\038\000\031\000\049\000\030\000\
\\060\000\026\000\061\000\025\000\062\000\024\000\063\000\023\000\
\\064\000\022\000\065\000\021\000\066\000\020\000\067\000\019\000\
\\068\000\018\000\069\000\017\000\000\000\
\\158\002\006\000\050\000\012\000\047\000\015\000\045\000\019\000\042\000\
\\023\000\039\000\026\000\037\000\033\000\034\000\034\000\033\000\
\\036\000\032\000\038\000\031\000\049\000\030\000\060\000\026\000\
\\061\000\025\000\062\000\024\000\063\000\023\000\064\000\022\000\
\\065\000\021\000\066\000\020\000\067\000\019\000\068\000\018\000\
\\069\000\017\000\000\000\
\\158\002\008\000\065\001\023\000\039\000\000\000\
\\158\002\023\000\039\000\000\000\
\\158\002\023\000\039\000\027\000\111\000\034\000\110\000\036\000\109\000\
\\038\000\108\000\044\000\107\000\060\000\026\000\061\000\025\000\
\\062\000\024\000\063\000\023\000\064\000\022\000\065\000\021\000\
\\066\000\020\000\067\000\019\000\068\000\018\000\069\000\017\000\
\\073\000\106\000\000\000\
\\158\002\023\000\039\000\027\000\111\000\034\000\144\000\036\000\109\000\
\\038\000\108\000\044\000\107\000\060\000\026\000\061\000\025\000\
\\062\000\024\000\063\000\023\000\064\000\022\000\065\000\021\000\
\\066\000\020\000\067\000\019\000\068\000\018\000\069\000\017\000\000\000\
\\158\002\023\000\039\000\034\000\110\000\036\000\109\000\038\000\108\000\
\\044\000\107\000\060\000\026\000\061\000\025\000\062\000\024\000\
\\063\000\023\000\064\000\022\000\065\000\021\000\066\000\020\000\
\\067\000\019\000\068\000\018\000\069\000\017\000\073\000\106\000\000\000\
\\158\002\023\000\039\000\034\000\144\000\035\000\210\000\036\000\109\000\
\\038\000\108\000\044\000\107\000\060\000\026\000\061\000\025\000\
\\062\000\024\000\063\000\023\000\064\000\022\000\065\000\021\000\
\\066\000\020\000\067\000\019\000\068\000\018\000\069\000\017\000\000\000\
\\158\002\023\000\039\000\034\000\144\000\035\000\210\000\036\000\109\000\
\\038\000\108\000\044\000\107\000\060\000\026\000\061\000\025\000\
\\062\000\024\000\063\000\023\000\064\000\022\000\065\000\021\000\
\\066\000\020\000\067\000\019\000\068\000\018\000\069\000\017\000\
\\073\000\106\000\000\000\
\\158\002\023\000\039\000\034\000\144\000\036\000\109\000\038\000\108\000\
\\044\000\107\000\060\000\026\000\061\000\025\000\062\000\024\000\
\\063\000\023\000\064\000\022\000\065\000\021\000\066\000\020\000\
\\067\000\019\000\068\000\018\000\069\000\017\000\000\000\
\\159\002\000\000\
\\160\002\000\000\
\\161\002\000\000\
\\162\002\000\000\
\\163\002\000\000\
\\164\002\000\000\
\\165\002\000\000\
\\166\002\000\000\
\\167\002\000\000\
\\168\002\000\000\
\\169\002\000\000\
\\171\002\000\000\
\\172\002\004\000\061\000\014\000\060\000\025\000\059\000\040\000\178\000\
\\041\000\058\000\000\000\
\\173\002\000\000\
\\174\002\000\000\
\\175\002\004\000\061\000\014\000\060\000\025\000\059\000\041\000\058\000\
\\042\000\193\001\000\000\
\\176\002\000\000\
\\177\002\004\000\061\000\014\000\060\000\025\000\059\000\041\000\058\000\
\\042\000\180\000\000\000\
\\178\002\000\000\
\\179\002\000\000\
\\180\002\004\000\061\000\014\000\060\000\025\000\059\000\040\000\118\001\
\\041\000\058\000\000\000\
\\181\002\000\000\
\\182\002\061\000\085\000\062\000\084\000\070\000\083\000\071\000\082\000\
\\072\000\081\000\000\000\
\\183\002\000\000\
\\184\002\000\000\
\\186\002\000\000\
\\187\002\000\000\
\\188\002\004\000\061\000\041\000\058\000\000\000\
\\189\002\004\000\061\000\025\000\059\000\041\000\058\000\000\000\
\\190\002\000\000\
\\191\002\004\000\061\000\014\000\060\000\025\000\059\000\041\000\058\000\000\000\
\\192\002\004\000\061\000\014\000\060\000\025\000\059\000\041\000\058\000\000\000\
\\193\002\004\000\061\000\014\000\060\000\025\000\059\000\041\000\058\000\000\000\
\\194\002\000\000\
\\195\002\000\000\
\\196\002\000\000\
\\197\002\000\000\
\\198\002\045\000\237\000\000\000\
\\199\002\004\000\061\000\014\000\060\000\025\000\059\000\041\000\058\000\000\000\
\\200\002\002\000\051\000\008\000\049\000\011\000\048\000\013\000\046\000\
\\017\000\044\000\018\000\043\000\020\000\223\000\021\000\040\000\
\\024\000\038\000\029\000\036\000\030\000\035\000\042\000\222\000\000\000\
\\201\002\002\000\051\000\008\000\049\000\011\000\048\000\013\000\046\000\
\\017\000\044\000\018\000\043\000\020\000\223\000\021\000\040\000\
\\024\000\038\000\029\000\036\000\030\000\035\000\042\000\222\000\000\000\
\\202\002\000\000\
\\203\002\000\000\
\\204\002\002\000\051\000\008\000\049\000\011\000\048\000\013\000\046\000\
\\017\000\044\000\018\000\043\000\020\000\223\000\021\000\040\000\
\\024\000\038\000\029\000\036\000\030\000\035\000\000\000\
\\205\002\000\000\
\\206\002\000\000\
\\207\002\000\000\
\\208\002\000\000\
\\209\002\000\000\
\\210\002\000\000\
\\211\002\000\000\
\\212\002\000\000\
\\213\002\000\000\
\\214\002\000\000\
\\215\002\000\000\
\\216\002\000\000\
\\217\002\000\000\
\\218\002\000\000\
\\219\002\000\000\
\\220\002\000\000\
\\221\002\032\000\241\000\000\000\
\\222\002\000\000\
\\223\002\046\000\124\000\070\000\069\000\071\000\068\000\072\000\067\000\000\000\
\\224\002\000\000\
\\225\002\070\000\073\000\075\000\120\000\000\000\
\\226\002\000\000\
\\227\002\060\000\130\000\061\000\129\000\000\000\
\\228\002\000\000\
\\229\002\000\000\
\\230\002\000\000\
\\231\002\003\000\121\001\004\000\061\000\014\000\060\000\025\000\059\000\
\\041\000\058\000\000\000\
\\232\002\000\000\
\\233\002\000\000\
\\234\002\003\000\234\000\000\000\
\\235\002\000\000\
\\236\002\000\000\
\\237\002\045\000\232\000\000\000\
\\238\002\004\000\061\000\014\000\060\000\025\000\059\000\041\000\058\000\000\000\
\\239\002\000\000\
\\240\002\000\000\
\\241\002\003\000\191\001\000\000\
\\242\002\000\000\
\\243\002\000\000\
\\244\002\000\000\
\\245\002\000\000\
\\246\002\003\000\143\001\000\000\
\\247\002\000\000\
\\248\002\000\000\
\\249\002\045\000\247\001\000\000\
\\250\002\000\000\
\\251\002\022\000\060\001\000\000\
\\251\002\022\000\060\001\046\000\059\001\000\000\
\\252\002\000\000\
\\253\002\000\000\
\\254\002\000\000\
\\255\002\003\000\138\001\000\000\
\\000\003\000\000\
\\000\003\041\000\188\000\000\000\
\\001\003\000\000\
\\002\003\000\000\
\\003\003\000\000\
\\004\003\000\000\
\\005\003\000\000\
\\006\003\000\000\
\\007\003\000\000\
\\008\003\000\000\
\\009\003\000\000\
\\011\003\000\000\
\\012\003\040\000\038\001\000\000\
\\013\003\000\000\
\\014\003\000\000\
\\015\003\000\000\
\\016\003\000\000\
\\017\003\000\000\
\\018\003\040\000\187\001\000\000\
\\019\003\000\000\
\\020\003\041\000\229\000\000\000\
\\021\003\000\000\
\\022\003\005\000\125\001\000\000\
\\023\003\000\000\
\\024\003\043\000\202\000\061\000\085\000\062\000\084\000\070\000\201\000\
\\071\000\200\000\072\000\199\000\000\000\
\\026\003\041\000\188\000\000\000\
\\027\003\000\000\
\\028\003\000\000\
\\029\003\000\000\
\\030\003\000\000\
\\031\003\000\000\
\\032\003\000\000\
\\034\003\000\000\
\\035\003\000\000\
\\036\003\041\000\188\000\000\000\
\\037\003\048\000\251\000\000\000\
\\038\003\000\000\
\\039\003\000\000\
\\040\003\000\000\
\\042\003\000\000\
\\043\003\000\000\
\\044\003\000\000\
\\045\003\000\000\
\\046\003\000\000\
\\047\003\000\000\
\\048\003\000\000\
\\049\003\040\000\205\001\000\000\
\\050\003\000\000\
\\051\003\061\000\085\000\062\000\084\000\070\000\083\000\071\000\082\000\
\\072\000\081\000\000\000\
\\053\003\034\000\167\000\038\000\166\000\073\000\106\000\000\000\
\\054\003\000\000\
\\055\003\000\000\
\\056\003\040\000\074\001\000\000\
\\057\003\000\000\
\\058\003\034\000\115\000\073\000\106\000\000\000\
\\059\003\000\000\
\\060\003\000\000\
\\061\003\000\000\
\\062\003\040\000\044\001\000\000\
\\063\003\000\000\
\\064\003\058\000\084\001\000\000\
\\064\003\058\000\250\001\000\000\
\\064\003\058\000\084\002\000\000\
\\065\003\058\000\084\001\000\000\
\\065\003\058\000\250\001\000\000\
\\065\003\058\000\084\002\000\000\
\\066\003\000\000\
\\067\003\000\000\
\\068\003\000\000\
\\069\003\000\000\
\\070\003\000\000\
\\071\003\002\000\051\000\008\000\049\000\011\000\048\000\013\000\046\000\
\\017\000\044\000\018\000\043\000\020\000\041\000\021\000\040\000\
\\024\000\038\000\029\000\036\000\030\000\035\000\042\000\218\000\
\\057\000\027\000\000\000\
\\072\003\002\000\051\000\008\000\049\000\011\000\048\000\013\000\046\000\
\\017\000\044\000\018\000\043\000\019\000\083\001\020\000\041\000\
\\021\000\040\000\024\000\038\000\029\000\036\000\030\000\035\000\
\\042\000\218\000\056\000\082\001\057\000\027\000\070\000\081\001\
\\075\000\120\000\000\000\
\\072\003\002\000\051\000\008\000\049\000\011\000\048\000\013\000\046\000\
\\017\000\044\000\018\000\043\000\020\000\041\000\021\000\040\000\
\\024\000\038\000\029\000\036\000\030\000\035\000\042\000\218\000\
\\057\000\027\000\000\000\
\\073\003\000\000\
\\074\003\002\000\051\000\008\000\049\000\011\000\048\000\013\000\046\000\
\\017\000\044\000\018\000\043\000\020\000\041\000\021\000\040\000\
\\024\000\038\000\029\000\036\000\030\000\035\000\057\000\027\000\000\000\
\\075\003\000\000\
\\076\003\000\000\
\\077\003\000\000\
\\078\003\000\000\
\\079\003\000\000\
\\080\003\000\000\
\\081\003\000\000\
\\083\003\000\000\
\\084\003\000\000\
\\085\003\000\000\
\\086\003\000\000\
\\087\003\000\000\
\\088\003\000\000\
\\089\003\000\000\
\\090\003\000\000\
\\091\003\058\000\084\001\000\000\
\\092\003\041\000\173\000\059\000\172\000\000\000\
\\092\003\041\000\173\000\059\000\169\001\000\000\
\\092\003\041\000\173\000\059\000\008\002\000\000\
\\093\003\000\000\
\\094\003\000\000\
\\095\003\000\000\
\\096\003\000\000\
\\096\003\070\000\076\000\000\000\
\\097\003\000\000\
\\098\003\000\000\
\\099\003\000\000\
\\101\003\000\000\
\\102\003\000\000\
\\103\003\000\000\
\\104\003\000\000\
\\105\003\000\000\
\\106\003\000\000\
\\107\003\000\000\
\\108\003\003\000\079\002\000\000\
\\109\003\008\000\022\001\011\000\021\001\029\000\020\001\030\000\019\001\
\\042\000\093\001\050\000\017\001\052\000\016\001\053\000\092\001\
\\057\000\014\001\000\000\
\\110\003\008\000\022\001\011\000\021\001\029\000\020\001\030\000\019\001\
\\042\000\018\001\050\000\017\001\052\000\016\001\053\000\015\001\
\\057\000\014\001\000\000\
\\110\003\008\000\022\001\011\000\021\001\029\000\020\001\030\000\019\001\
\\042\000\018\001\050\000\017\001\052\000\016\001\053\000\015\001\
\\057\000\014\001\070\000\073\000\000\000\
\\111\003\000\000\
\\112\003\008\000\022\001\011\000\021\001\029\000\020\001\030\000\019\001\
\\050\000\017\001\052\000\016\001\057\000\014\001\000\000\
\\113\003\000\000\
\\114\003\000\000\
\\115\003\000\000\
\\116\003\000\000\
\\117\003\000\000\
\\118\003\000\000\
\\119\003\008\000\022\001\011\000\021\001\029\000\020\001\030\000\019\001\
\\050\000\017\001\052\000\016\001\057\000\014\001\000\000\
\\120\003\000\000\
\\121\003\000\000\
\\122\003\000\000\
\\123\003\000\000\
\\124\003\000\000\
\\125\003\000\000\
\\126\003\000\000\
\\127\003\000\000\
\\128\003\000\000\
\\129\003\000\000\
\\130\003\058\000\084\001\000\000\
\\131\003\000\000\
\\132\003\000\000\
\\133\003\070\000\076\000\000\000\
\\134\003\000\000\
\\135\003\046\000\047\002\000\000\
\\136\003\000\000\
\\137\003\000\000\
\\138\003\046\000\012\002\000\000\
\\139\003\000\000\
\\140\003\000\000\
\\141\003\000\000\
\\142\003\003\000\017\002\000\000\
\\143\003\000\000\
\\144\003\000\000\
\\145\003\003\000\230\001\000\000\
\\145\003\003\000\230\001\046\000\232\001\000\000\
\\146\003\000\000\
\\147\003\000\000\
\\148\003\003\000\050\002\000\000\
\\149\003\000\000\
\\150\003\000\000\
\\151\003\000\000\
\\152\003\000\000\
\\153\003\003\000\022\002\000\000\
\\154\003\000\000\
\\155\003\000\000\
\\156\003\045\000\055\002\000\000\
\\157\003\000\000\
\\158\003\000\000\
\\159\003\003\000\234\001\000\000\
\\160\003\000\000\
\\161\003\000\000\
\\163\003\000\000\
\\164\003\000\000\
\\165\003\000\000\
\\166\003\000\000\
\\167\003\000\000\
\\168\003\000\000\
\\169\003\000\000\
\\170\003\000\000\
\\171\003\000\000\
\\172\003\000\000\
\\173\003\000\000\
\\175\003\000\000\
\\176\003\000\000\
\\177\003\000\000\
\\178\003\000\000\
\\179\003\000\000\
\\180\003\000\000\
\\181\003\000\000\
\\182\003\000\000\
\\183\003\000\000\
\\185\003\000\000\
\\186\003\000\000\
\\187\003\000\000\
\\188\003\000\000\
\\189\003\002\000\051\000\008\000\049\000\011\000\048\000\013\000\046\000\
\\017\000\044\000\018\000\043\000\020\000\041\000\021\000\040\000\
\\024\000\038\000\029\000\036\000\030\000\035\000\051\000\029\000\
\\055\000\028\000\057\000\027\000\000\000\
\\190\003\000\000\
\\191\003\000\000\
\\192\003\000\000\
\\193\003\000\000\
\"
val actionRowNumbers =
"\127\000\013\000\212\001\206\001\
\\082\000\211\001\211\001\211\001\
\\098\001\021\000\206\000\003\000\
\\204\000\084\000\181\000\142\000\
\\141\000\140\000\139\000\138\000\
\\137\000\136\000\135\000\134\000\
\\133\000\120\000\122\000\123\000\
\\116\000\203\000\028\000\171\000\
\\172\000\175\000\075\001\172\000\
\\121\000\170\000\085\000\128\000\
\\128\000\247\000\247\000\172\000\
\\177\000\180\000\174\000\057\000\
\\172\000\075\001\001\000\209\001\
\\210\001\208\001\207\001\001\000\
\\070\001\172\000\180\000\172\000\
\\205\000\161\000\182\000\163\000\
\\164\000\154\000\153\000\152\000\
\\162\000\099\001\113\001\158\000\
\\121\001\086\000\159\000\192\001\
\\058\000\160\000\184\000\147\000\
\\146\000\145\000\149\000\148\000\
\\073\000\202\000\087\000\193\000\
\\191\000\071\000\020\000\060\000\
\\061\000\185\000\016\000\176\000\
\\046\001\088\000\022\001\042\000\
\\226\000\119\000\076\001\025\001\
\\157\000\024\001\045\001\044\000\
\\179\000\176\000\074\001\124\000\
\\230\000\126\000\211\000\236\000\
\\245\000\168\000\169\000\239\000\
\\150\000\243\000\151\000\094\001\
\\221\000\085\000\246\000\144\000\
\\143\000\085\000\019\000\180\000\
\\041\001\021\001\043\000\001\001\
\\254\000\228\000\118\000\112\000\
\\218\000\215\000\178\000\235\000\
\\117\000\124\000\241\000\241\000\
\\089\000\156\000\155\000\018\000\
\\124\000\241\000\213\001\215\001\
\\214\001\125\000\060\001\004\000\
\\058\001\056\001\207\000\062\001\
\\069\001\070\001\209\000\210\000\
\\208\000\090\000\113\000\113\000\
\\113\000\134\001\183\000\172\000\
\\172\000\187\000\172\000\172\000\
\\190\000\188\000\186\000\172\000\
\\227\000\048\001\070\001\172\000\
\\047\001\053\001\023\001\026\000\
\\165\000\074\000\044\001\041\001\
\\091\000\024\000\023\000\022\000\
\\035\001\033\001\031\001\072\000\
\\062\000\063\000\064\000\079\001\
\\027\001\249\000\092\000\244\000\
\\242\000\095\001\092\001\037\000\
\\097\001\222\000\220\000\038\000\
\\225\000\128\000\238\000\237\000\
\\172\000\229\000\093\000\070\001\
\\052\001\255\000\180\000\252\000\
\\180\000\172\000\216\000\180\000\
\\016\001\094\000\232\000\075\001\
\\231\000\173\000\180\000\095\000\
\\056\000\061\001\166\000\167\000\
\\070\001\070\001\075\000\068\001\
\\076\000\065\000\066\000\041\000\
\\116\001\096\000\119\001\133\001\
\\112\001\122\001\008\000\114\000\
\\135\001\132\001\067\000\077\000\
\\120\000\045\000\113\000\075\001\
\\137\001\117\000\075\001\117\000\
\\057\000\201\000\192\000\198\000\
\\197\000\194\000\213\000\055\001\
\\251\000\049\001\027\000\070\001\
\\050\001\026\001\043\001\180\000\
\\180\000\029\001\077\001\180\000\
\\030\001\028\001\126\000\070\001\
\\096\001\131\000\224\000\172\000\
\\221\000\017\000\172\000\040\001\
\\000\001\253\000\219\000\217\000\
\\020\001\174\000\070\001\174\000\
\\240\000\010\001\117\000\125\000\
\\214\000\174\000\221\000\059\001\
\\057\001\063\001\070\001\071\001\
\\070\001\064\001\101\001\005\000\
\\078\000\088\001\059\000\002\000\
\\128\000\128\000\046\000\041\000\
\\030\000\124\001\122\000\047\000\
\\142\001\136\001\049\000\144\001\
\\114\001\113\000\153\001\079\000\
\\140\001\097\000\125\000\155\001\
\\154\001\120\001\147\001\124\000\
\\145\001\080\000\148\001\146\001\
\\124\000\152\001\015\001\150\001\
\\149\001\124\000\098\000\199\000\
\\116\000\054\001\248\000\176\000\
\\180\000\025\000\039\001\180\000\
\\039\001\032\001\034\001\078\001\
\\005\001\094\001\196\000\129\000\
\\039\000\172\000\002\001\017\001\
\\174\000\084\000\014\001\010\001\
\\007\001\075\001\015\001\233\000\
\\010\001\031\000\067\001\072\001\
\\073\001\104\001\120\000\113\000\
\\113\000\093\001\094\001\094\001\
\\117\001\075\001\102\001\118\001\
\\123\001\125\001\075\001\143\001\
\\141\001\125\000\099\000\113\000\
\\068\000\113\000\121\000\138\001\
\\100\000\156\001\157\001\169\001\
\\070\001\170\001\184\001\102\000\
\\029\000\200\000\250\000\051\001\
\\037\001\083\000\042\001\036\001\
\\003\001\075\001\132\000\172\000\
\\032\000\131\000\212\000\019\001\
\\020\001\008\001\009\001\013\001\
\\006\001\234\000\065\001\116\000\
\\103\001\006\000\085\001\106\001\
\\082\001\105\001\069\000\080\001\
\\070\000\129\000\040\000\125\000\
\\125\000\139\001\041\000\103\000\
\\115\001\185\001\014\000\115\000\
\\163\001\162\001\125\000\167\001\
\\075\001\166\001\070\001\182\001\
\\117\000\117\000\178\001\015\001\
\\125\000\038\001\004\001\033\000\
\\195\000\189\000\221\000\018\001\
\\011\001\174\000\066\001\107\001\
\\050\000\090\001\113\000\113\000\
\\089\001\034\000\041\000\104\000\
\\105\000\195\001\010\000\081\000\
\\041\000\106\000\113\000\187\001\
\\120\000\052\000\121\000\160\001\
\\159\001\168\001\164\001\117\000\
\\173\001\183\001\178\001\175\001\
\\075\001\181\001\151\001\100\001\
\\132\000\012\001\108\001\075\001\
\\084\001\081\001\087\001\130\000\
\\070\001\070\001\198\001\123\000\
\\113\000\113\000\196\001\041\000\
\\107\000\186\001\188\001\075\001\
\\161\001\125\000\165\001\171\001\
\\075\001\176\001\177\001\124\000\
\\179\001\117\000\035\000\125\000\
\\036\000\131\001\009\000\197\001\
\\200\001\011\000\086\001\199\001\
\\083\001\193\001\041\000\125\000\
\\158\001\172\001\124\000\108\000\
\\180\001\223\000\109\000\091\001\
\\129\001\046\000\126\001\127\001\
\\048\000\201\001\054\000\194\001\
\\110\000\101\000\117\000\070\001\
\\130\001\128\001\202\001\075\001\
\\070\001\178\001\007\000\125\000\
\\015\000\174\001\109\001\110\001\
\\051\000\111\000\189\001\190\001\
\\053\000\111\001\070\001\191\001\
\\012\000\203\001\204\001\055\000\
\\205\001\000\000"
val gotoT =
"\
\\143\000\113\002\146\000\001\000\000\000\
\\001\000\014\000\015\000\013\000\016\000\012\000\025\000\011\000\
\\026\000\010\000\027\000\009\000\033\000\008\000\087\000\007\000\
\\097\000\006\000\133\000\005\000\140\000\004\000\141\000\003\000\
\\144\000\002\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\033\000\008\000\087\000\007\000\097\000\006\000\133\000\005\000\
\\141\000\052\000\142\000\051\000\000\000\
\\033\000\008\000\087\000\007\000\097\000\006\000\133\000\005\000\
\\141\000\052\000\142\000\053\000\000\000\
\\033\000\008\000\087\000\007\000\097\000\006\000\133\000\005\000\
\\141\000\052\000\142\000\054\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\001\000\014\000\015\000\013\000\016\000\060\000\000\000\
\\000\000\
\\005\000\063\000\011\000\062\000\012\000\061\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\008\000\070\000\088\000\069\000\000\000\
\\009\000\073\000\098\000\072\000\000\000\
\\010\000\076\000\134\000\075\000\000\000\
\\003\000\078\000\000\000\
\\003\000\086\000\022\000\085\000\023\000\084\000\000\000\
\\001\000\014\000\015\000\013\000\016\000\012\000\017\000\089\000\
\\018\000\088\000\025\000\011\000\026\000\010\000\027\000\087\000\000\000\
\\001\000\014\000\015\000\013\000\016\000\012\000\019\000\092\000\
\\021\000\091\000\025\000\011\000\026\000\010\000\027\000\090\000\000\000\
\\001\000\014\000\015\000\013\000\016\000\012\000\025\000\011\000\
\\026\000\010\000\027\000\094\000\000\000\
\\001\000\103\000\007\000\102\000\015\000\101\000\038\000\100\000\
\\056\000\099\000\057\000\098\000\066\000\097\000\068\000\096\000\
\\081\000\095\000\000\000\
\\007\000\102\000\045\000\112\000\080\000\111\000\081\000\110\000\000\000\
\\001\000\014\000\015\000\013\000\016\000\012\000\025\000\011\000\
\\026\000\010\000\027\000\114\000\000\000\
\\008\000\117\000\014\000\116\000\036\000\115\000\000\000\
\\000\000\
\\004\000\121\000\005\000\120\000\035\000\119\000\000\000\
\\147\000\123\000\000\000\
\\147\000\124\000\000\000\
\\002\000\126\000\037\000\125\000\000\000\
\\002\000\126\000\037\000\129\000\000\000\
\\001\000\014\000\015\000\013\000\016\000\012\000\025\000\011\000\
\\026\000\010\000\027\000\130\000\000\000\
\\001\000\103\000\007\000\102\000\015\000\138\000\040\000\137\000\
\\042\000\136\000\044\000\135\000\056\000\134\000\057\000\133\000\
\\067\000\132\000\081\000\131\000\000\000\
\\001\000\103\000\015\000\101\000\028\000\141\000\030\000\140\000\
\\056\000\099\000\057\000\098\000\066\000\139\000\068\000\096\000\000\000\
\\015\000\144\000\054\000\143\000\000\000\
\\006\000\148\000\007\000\102\000\048\000\147\000\049\000\146\000\
\\081\000\145\000\000\000\
\\001\000\014\000\015\000\013\000\016\000\012\000\025\000\011\000\
\\026\000\010\000\027\000\151\000\000\000\
\\007\000\102\000\047\000\153\000\080\000\152\000\081\000\110\000\000\000\
\\001\000\014\000\015\000\013\000\016\000\012\000\025\000\011\000\
\\026\000\010\000\027\000\009\000\033\000\008\000\087\000\007\000\
\\097\000\006\000\133\000\005\000\140\000\004\000\141\000\003\000\
\\144\000\155\000\145\000\154\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\001\000\014\000\015\000\013\000\016\000\012\000\025\000\011\000\
\\026\000\010\000\027\000\009\000\033\000\008\000\087\000\007\000\
\\097\000\006\000\133\000\005\000\140\000\004\000\141\000\003\000\
\\144\000\155\000\145\000\156\000\000\000\
\\007\000\163\000\070\000\162\000\071\000\161\000\072\000\160\000\
\\073\000\159\000\074\000\158\000\078\000\157\000\000\000\
\\001\000\014\000\015\000\013\000\016\000\012\000\025\000\011\000\
\\026\000\010\000\027\000\166\000\000\000\
\\001\000\103\000\015\000\101\000\028\000\167\000\030\000\140\000\
\\056\000\099\000\057\000\098\000\066\000\139\000\068\000\096\000\000\000\
\\001\000\014\000\015\000\013\000\016\000\012\000\025\000\011\000\
\\026\000\010\000\027\000\168\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\094\000\169\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\001\000\103\000\015\000\101\000\038\000\184\000\056\000\099\000\
\\057\000\098\000\066\000\097\000\068\000\096\000\000\000\
\\069\000\185\000\000\000\
\\000\000\
\\069\000\188\000\000\000\
\\001\000\103\000\015\000\138\000\056\000\134\000\057\000\133\000\
\\067\000\189\000\000\000\
\\000\000\
\\005\000\191\000\012\000\190\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\003\000\196\000\005\000\195\000\061\000\194\000\062\000\193\000\000\000\
\\001\000\103\000\015\000\101\000\056\000\099\000\057\000\098\000\
\\058\000\203\000\059\000\202\000\066\000\201\000\068\000\096\000\000\000\
\\001\000\103\000\007\000\207\000\015\000\101\000\056\000\099\000\
\\057\000\098\000\060\000\206\000\066\000\205\000\068\000\096\000\
\\082\000\204\000\000\000\
\\001\000\103\000\015\000\101\000\038\000\209\000\056\000\099\000\
\\057\000\098\000\066\000\097\000\068\000\096\000\000\000\
\\000\000\
\\006\000\210\000\000\000\
\\000\000\
\\007\000\207\000\082\000\204\000\000\000\
\\000\000\
\\000\000\
\\008\000\117\000\014\000\116\000\036\000\211\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\004\000\121\000\005\000\120\000\035\000\212\000\000\000\
\\000\000\
\\033\000\008\000\085\000\215\000\086\000\214\000\087\000\213\000\000\000\
\\031\000\219\000\032\000\218\000\033\000\217\000\000\000\
\\004\000\121\000\005\000\120\000\035\000\222\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\004\000\121\000\005\000\120\000\035\000\223\000\000\000\
\\000\000\
\\001\000\103\000\015\000\138\000\040\000\225\000\042\000\136\000\
\\044\000\135\000\056\000\134\000\057\000\133\000\067\000\132\000\000\000\
\\064\000\226\000\000\000\
\\000\000\
\\001\000\103\000\015\000\138\000\056\000\134\000\057\000\133\000\
\\067\000\228\000\000\000\
\\043\000\229\000\000\000\
\\041\000\231\000\000\000\
\\000\000\
\\005\000\063\000\012\000\190\000\000\000\
\\000\000\
\\029\000\234\000\000\000\
\\000\000\
\\001\000\103\000\015\000\101\000\056\000\099\000\057\000\098\000\
\\060\000\206\000\066\000\205\000\068\000\096\000\000\000\
\\000\000\
\\005\000\236\000\000\000\
\\006\000\237\000\000\000\
\\034\000\238\000\000\000\
\\034\000\240\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\006\000\243\000\000\000\
\\034\000\244\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\006\000\246\000\013\000\245\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\003\000\252\000\075\000\251\000\076\000\250\000\000\000\
\\007\000\163\000\070\000\254\000\071\000\161\000\072\000\160\000\
\\073\000\159\000\074\000\158\000\078\000\157\000\079\000\253\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\009\000\002\001\095\000\001\001\096\000\000\001\000\000\
\\009\000\002\001\095\000\004\001\096\000\000\001\000\000\
\\009\000\002\001\095\000\007\001\096\000\006\001\100\000\005\001\000\000\
\\008\000\011\001\105\000\010\001\106\000\009\001\108\000\008\001\000\000\
\\000\000\
\\001\000\014\000\015\000\013\000\016\000\012\000\025\000\011\000\
\\026\000\010\000\027\000\021\001\000\000\
\\001\000\014\000\015\000\013\000\016\000\012\000\018\000\022\001\
\\025\000\011\000\026\000\010\000\027\000\087\000\000\000\
\\000\000\
\\001\000\014\000\015\000\013\000\016\000\012\000\021\000\024\001\
\\025\000\011\000\026\000\010\000\027\000\023\001\000\000\
\\001\000\014\000\015\000\013\000\016\000\012\000\018\000\025\001\
\\025\000\011\000\026\000\010\000\027\000\087\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\001\000\014\000\015\000\013\000\016\000\012\000\025\000\011\000\
\\026\000\010\000\027\000\026\001\000\000\
\\000\000\
\\000\000\
\\007\000\163\000\070\000\027\001\071\000\161\000\072\000\160\000\
\\073\000\159\000\074\000\158\000\078\000\157\000\000\000\
\\001\000\014\000\015\000\013\000\016\000\012\000\025\000\011\000\
\\026\000\010\000\027\000\028\001\000\000\
\\000\000\
\\000\000\
\\000\000\
\\064\000\030\001\069\000\029\001\000\000\
\\069\000\032\001\000\000\
\\000\000\
\\000\000\
\\064\000\034\001\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\033\000\008\000\086\000\044\001\087\000\213\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\032\000\046\001\033\000\217\000\000\000\
\\000\000\
\\000\000\
\\147\000\048\001\000\000\
\\000\000\
\\000\000\
\\001\000\014\000\015\000\013\000\016\000\012\000\025\000\011\000\
\\026\000\010\000\027\000\049\001\000\000\
\\000\000\
\\000\000\
\\007\000\163\000\070\000\051\001\071\000\161\000\072\000\160\000\
\\073\000\159\000\074\000\158\000\078\000\157\000\000\000\
\\000\000\
\\000\000\
\\001\000\103\000\015\000\138\000\042\000\052\001\044\000\135\000\
\\056\000\134\000\057\000\133\000\067\000\132\000\000\000\
\\000\000\
\\001\000\103\000\015\000\138\000\040\000\053\001\042\000\136\000\
\\044\000\135\000\056\000\134\000\057\000\133\000\067\000\132\000\000\000\
\\001\000\014\000\015\000\013\000\016\000\012\000\025\000\011\000\
\\026\000\010\000\027\000\054\001\000\000\
\\000\000\
\\001\000\103\000\015\000\101\000\028\000\055\001\030\000\140\000\
\\056\000\099\000\057\000\098\000\066\000\139\000\068\000\096\000\000\000\
\\053\000\056\001\000\000\
\\000\000\
\\000\000\
\\007\000\102\000\045\000\060\001\080\000\111\000\081\000\110\000\000\000\
\\000\000\
\\015\000\062\001\051\000\061\001\000\000\
\\001\000\103\000\015\000\101\000\028\000\064\001\030\000\140\000\
\\056\000\099\000\057\000\098\000\066\000\139\000\068\000\096\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\007\000\163\000\072\000\067\001\073\000\159\000\074\000\158\000\
\\078\000\157\000\000\000\
\\007\000\163\000\070\000\068\001\071\000\161\000\072\000\160\000\
\\073\000\159\000\074\000\158\000\078\000\157\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\008\000\117\000\010\000\078\001\014\000\077\001\083\000\076\001\
\\084\000\075\001\090\000\074\001\000\000\
\\000\000\
\\000\000\
\\000\000\
\\105\000\084\001\106\000\009\001\108\000\008\001\000\000\
\\000\000\
\\000\000\
\\099\000\085\001\000\000\
\\000\000\
\\000\000\
\\107\000\089\001\108\000\088\001\000\000\
\\000\000\
\\000\000\
\\008\000\095\001\128\000\094\001\000\000\
\\008\000\117\000\014\000\097\001\113\000\096\001\000\000\
\\009\000\101\001\095\000\100\001\096\000\000\001\109\000\099\001\000\000\
\\007\000\102\000\080\000\103\001\081\000\110\000\116\000\102\001\000\000\
\\000\000\
\\005\000\105\001\114\000\104\001\000\000\
\\007\000\102\000\080\000\108\001\081\000\110\000\116\000\107\001\
\\118\000\106\001\000\000\
\\005\000\110\001\126\000\109\001\000\000\
\\006\000\114\001\007\000\102\000\081\000\113\001\121\000\112\001\
\\122\000\111\001\000\000\
\\024\000\115\001\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\069\000\117\001\000\000\
\\039\000\118\001\000\000\
\\000\000\
\\000\000\
\\007\000\163\000\070\000\121\001\071\000\161\000\072\000\160\000\
\\073\000\159\000\074\000\158\000\078\000\157\000\000\000\
\\000\000\
\\000\000\
\\065\000\122\001\000\000\
\\001\000\103\000\015\000\101\000\056\000\099\000\057\000\098\000\
\\066\000\124\001\068\000\096\000\000\000\
\\001\000\103\000\015\000\101\000\056\000\099\000\057\000\098\000\
\\059\000\125\001\066\000\201\000\068\000\096\000\000\000\
\\000\000\
\\000\000\
\\001\000\103\000\015\000\101\000\056\000\099\000\057\000\098\000\
\\059\000\126\001\066\000\201\000\068\000\096\000\000\000\
\\000\000\
\\000\000\
\\007\000\207\000\082\000\127\001\000\000\
\\007\000\163\000\070\000\128\001\071\000\161\000\072\000\160\000\
\\073\000\159\000\074\000\158\000\078\000\157\000\000\000\
\\033\000\008\000\086\000\044\001\087\000\213\000\000\000\
\\149\000\129\001\000\000\
\\032\000\046\001\033\000\217\000\000\000\
\\001\000\014\000\015\000\013\000\016\000\012\000\020\000\131\001\
\\025\000\011\000\026\000\010\000\027\000\130\001\000\000\
\\031\000\132\001\032\000\218\000\033\000\217\000\000\000\
\\000\000\
\\001\000\014\000\015\000\013\000\016\000\012\000\025\000\011\000\
\\026\000\010\000\027\000\134\001\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\055\000\135\001\000\000\
\\015\000\137\001\000\000\
\\007\000\163\000\070\000\138\001\071\000\161\000\072\000\160\000\
\\073\000\159\000\074\000\158\000\078\000\157\000\000\000\
\\015\000\062\001\051\000\139\001\000\000\
\\000\000\
\\050\000\140\001\000\000\
\\005\000\142\001\000\000\
\\006\000\246\000\013\000\143\001\000\000\
\\000\000\
\\015\000\062\001\051\000\144\001\000\000\
\\031\000\145\001\032\000\218\000\033\000\217\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\007\000\163\000\070\000\146\001\071\000\161\000\072\000\160\000\
\\073\000\159\000\074\000\158\000\078\000\157\000\000\000\
\\000\000\
\\007\000\163\000\070\000\148\001\071\000\161\000\072\000\160\000\
\\073\000\159\000\074\000\158\000\078\000\157\000\079\000\147\001\000\000\
\\000\000\
\\000\000\
\\089\000\149\001\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\147\000\154\001\000\000\
\\147\000\155\001\000\000\
\\103\000\156\001\000\000\
\\008\000\117\000\010\000\078\001\014\000\077\001\083\000\076\001\
\\084\000\075\001\090\000\158\001\000\000\
\\000\000\
\\000\000\
\\009\000\073\000\098\000\160\001\000\000\
\\101\000\161\001\103\000\156\001\000\000\
\\000\000\
\\107\000\163\001\108\000\088\001\000\000\
\\008\000\117\000\014\000\097\001\113\000\164\001\000\000\
\\000\000\
\\094\000\166\001\000\000\
\\009\000\002\001\095\000\168\001\096\000\000\001\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\006\000\246\000\013\000\172\001\111\000\171\001\000\000\
\\000\000\
\\000\000\
\\009\000\174\001\109\000\173\001\000\000\
\\000\000\
\\006\000\175\001\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\006\000\177\001\000\000\
\\000\000\
\\053\000\178\001\000\000\
\\000\000\
\\000\000\
\\006\000\179\001\000\000\
\\000\000\
\\000\000\
\\003\000\086\000\022\000\181\001\000\000\
\\000\000\
\\000\000\
\\001\000\103\000\015\000\101\000\038\000\182\001\056\000\099\000\
\\057\000\098\000\066\000\097\000\068\000\096\000\000\000\
\\001\000\103\000\015\000\101\000\056\000\099\000\057\000\098\000\
\\066\000\183\001\068\000\096\000\000\000\
\\069\000\117\001\000\000\
\\063\000\184\001\000\000\
\\001\000\103\000\015\000\101\000\056\000\099\000\057\000\098\000\
\\066\000\186\001\068\000\096\000\000\000\
\\063\000\187\001\000\000\
\\000\000\
\\000\000\
\\000\000\
\\046\000\188\001\000\000\
\\033\000\008\000\085\000\190\001\086\000\214\000\087\000\213\000\000\000\
\\000\000\
\\148\000\192\001\000\000\
\\000\000\
\\001\000\014\000\015\000\013\000\016\000\012\000\025\000\011\000\
\\026\000\010\000\027\000\194\001\000\000\
\\000\000\
\\000\000\
\\015\000\144\000\054\000\195\001\000\000\
\\005\000\063\000\011\000\196\001\012\000\061\000\000\000\
\\000\000\
\\050\000\197\001\000\000\
\\000\000\
\\007\000\102\000\047\000\198\001\080\000\152\000\081\000\110\000\000\000\
\\053\000\199\001\000\000\
\\000\000\
\\050\000\200\001\000\000\
\\000\000\
\\077\000\202\001\000\000\
\\000\000\
\\000\000\
\\000\000\
\\008\000\070\000\088\000\204\001\000\000\
\\009\000\002\001\091\000\207\001\095\000\206\001\096\000\205\001\000\000\
\\009\000\002\001\091\000\209\001\095\000\208\001\096\000\205\001\000\000\
\\008\000\117\000\010\000\078\001\014\000\077\001\033\000\008\000\
\\083\000\212\001\084\000\211\001\085\000\210\001\086\000\214\000\
\\087\000\213\000\000\000\
\\033\000\008\000\085\000\213\001\086\000\214\000\087\000\213\000\000\000\
\\033\000\008\000\085\000\214\001\086\000\214\000\087\000\213\000\000\000\
\\000\000\
\\007\000\102\000\080\000\215\001\081\000\110\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\007\000\102\000\080\000\216\001\081\000\110\000\000\000\
\\107\000\163\001\108\000\088\001\000\000\
\\000\000\
\\006\000\246\000\013\000\172\001\111\000\217\001\000\000\
\\000\000\
\\009\000\002\001\095\000\219\001\096\000\000\001\000\000\
\\000\000\
\\009\000\002\001\095\000\223\001\096\000\222\001\130\000\221\001\000\000\
\\008\000\117\000\014\000\225\001\112\000\224\001\000\000\
\\000\000\
\\000\000\
\\000\000\
\\009\000\174\001\109\000\173\001\000\000\
\\117\000\227\001\000\000\
\\007\000\163\000\070\000\229\001\071\000\161\000\072\000\160\000\
\\073\000\159\000\074\000\158\000\078\000\157\000\000\000\
\\117\000\227\001\000\000\
\\127\000\231\001\000\000\
\\000\000\
\\005\000\235\001\124\000\234\001\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\003\000\196\000\005\000\195\000\061\000\237\001\000\000\
\\000\000\
\\000\000\
\\000\000\
\\007\000\102\000\045\000\238\001\080\000\111\000\081\000\110\000\000\000\
\\150\000\239\001\000\000\
\\001\000\014\000\015\000\013\000\016\000\012\000\020\000\240\001\
\\025\000\011\000\026\000\010\000\027\000\130\001\000\000\
\\000\000\
\\149\000\242\001\000\000\
\\000\000\
\\000\000\
\\055\000\243\001\000\000\
\\000\000\
\\000\000\
\\052\000\244\001\000\000\
\\000\000\
\\000\000\
\\000\000\
\\003\000\252\000\075\000\246\001\000\000\
\\000\000\
\\089\000\247\001\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\148\000\253\001\000\000\
\\000\000\
\\006\000\246\000\013\000\255\001\000\000\
\\006\000\246\000\013\000\000\002\000\000\
\\000\000\
\\008\000\117\000\010\000\078\001\014\000\077\001\083\000\003\002\
\\084\000\002\002\136\000\001\002\000\000\
\\000\000\
\\094\000\005\002\000\000\
\\000\000\
\\129\000\007\002\000\000\
\\000\000\
\\000\000\
\\000\000\
\\006\000\246\000\013\000\012\002\110\000\011\002\000\000\
\\000\000\
\\007\000\102\000\080\000\103\001\081\000\110\000\116\000\013\002\000\000\
\\115\000\014\002\000\000\
\\007\000\163\000\070\000\016\002\071\000\161\000\072\000\160\000\
\\073\000\159\000\074\000\158\000\078\000\157\000\000\000\
\\000\000\
\\005\000\110\001\126\000\017\002\000\000\
\\005\000\235\001\124\000\018\002\000\000\
\\123\000\019\002\000\000\
\\053\000\021\002\000\000\
\\006\000\246\000\013\000\022\002\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\031\000\024\002\032\000\218\000\033\000\217\000\000\000\
\\000\000\
\\000\000\
\\015\000\062\001\051\000\025\002\000\000\
\\000\000\
\\000\000\
\\092\000\026\002\103\000\156\001\000\000\
\\000\000\
\\009\000\002\001\095\000\028\002\096\000\000\001\000\000\
\\009\000\002\001\095\000\029\002\096\000\000\001\000\000\
\\000\000\
\\000\000\
\\008\000\117\000\010\000\078\001\014\000\077\001\083\000\031\002\
\\084\000\211\001\000\000\
\\000\000\
\\000\000\
\\000\000\
\\135\000\034\002\000\000\
\\000\000\
\\008\000\117\000\010\000\078\001\014\000\077\001\083\000\003\002\
\\084\000\002\002\136\000\038\002\000\000\
\\000\000\
\\009\000\002\001\095\000\040\002\096\000\000\001\000\000\
\\000\000\
\\008\000\095\001\128\000\041\002\000\000\
\\103\000\156\001\131\000\042\002\000\000\
\\008\000\117\000\014\000\225\001\112\000\044\002\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\005\000\105\001\114\000\046\002\000\000\
\\119\000\047\002\000\000\
\\000\000\
\\123\000\049\002\000\000\
\\000\000\
\\007\000\102\000\080\000\051\002\081\000\110\000\120\000\050\002\000\000\
\\125\000\052\002\000\000\
\\000\000\
\\000\000\
\\150\000\054\002\000\000\
\\000\000\
\\000\000\
\\007\000\102\000\080\000\055\002\081\000\110\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\148\000\056\002\000\000\
\\007\000\163\000\070\000\057\002\071\000\161\000\072\000\160\000\
\\073\000\159\000\074\000\158\000\078\000\157\000\000\000\
\\007\000\163\000\070\000\058\002\071\000\161\000\072\000\160\000\
\\073\000\159\000\074\000\158\000\078\000\157\000\000\000\
\\000\000\
\\010\000\076\000\134\000\059\002\000\000\
\\009\000\002\001\095\000\062\002\096\000\061\002\137\000\060\002\000\000\
\\009\000\002\001\095\000\064\002\096\000\061\002\137\000\063\002\000\000\
\\000\000\
\\008\000\117\000\010\000\078\001\014\000\077\001\083\000\003\002\
\\084\000\002\002\136\000\065\002\000\000\
\\000\000\
\\000\000\
\\000\000\
\\007\000\102\000\080\000\067\002\081\000\110\000\000\000\
\\000\000\
\\006\000\246\000\013\000\012\002\110\000\068\002\000\000\
\\000\000\
\\000\000\
\\007\000\102\000\080\000\070\002\081\000\110\000\118\000\069\002\000\000\
\\000\000\
\\000\000\
\\006\000\071\002\000\000\
\\000\000\
\\005\000\235\001\124\000\072\002\000\000\
\\000\000\
\\006\000\246\000\013\000\074\002\000\000\
\\000\000\
\\104\000\076\002\000\000\
\\099\000\079\002\102\000\078\002\104\000\076\002\000\000\
\\000\000\
\\000\000\
\\135\000\081\002\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\008\000\117\000\010\000\078\001\014\000\077\001\083\000\003\002\
\\084\000\002\002\136\000\083\002\000\000\
\\006\000\246\000\013\000\084\002\000\000\
\\000\000\
\\000\000\
\\006\000\085\002\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\103\000\088\002\000\000\
\\000\000\
\\000\000\
\\009\000\073\000\098\000\160\001\101\000\089\002\103\000\088\002\000\000\
\\000\000\
\\103\000\156\001\138\000\090\002\000\000\
\\000\000\
\\000\000\
\\000\000\
\\005\000\235\001\124\000\093\002\000\000\
\\007\000\163\000\070\000\094\002\071\000\161\000\072\000\160\000\
\\073\000\159\000\074\000\158\000\078\000\157\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\007\000\102\000\080\000\095\002\081\000\110\000\000\000\
\\007\000\163\000\070\000\096\002\071\000\161\000\072\000\160\000\
\\073\000\159\000\074\000\158\000\078\000\157\000\000\000\
\\123\000\097\002\000\000\
\\089\000\099\002\093\000\098\002\104\000\076\002\000\000\
\\006\000\246\000\013\000\101\002\000\000\
\\104\000\076\002\129\000\103\002\132\000\102\002\000\000\
\\000\000\
\\000\000\
\\000\000\
\\008\000\070\000\088\000\204\001\092\000\105\002\103\000\088\002\000\000\
\\000\000\
\\000\000\
\\000\000\
\\008\000\095\001\103\000\088\002\128\000\041\002\131\000\107\002\000\000\
\\000\000\
\\007\000\163\000\070\000\108\002\071\000\161\000\072\000\160\000\
\\073\000\159\000\074\000\158\000\078\000\157\000\000\000\
\\000\000\
\\104\000\076\002\135\000\110\002\139\000\109\002\000\000\
\\000\000\
\\000\000\
\\010\000\076\000\103\000\088\002\134\000\059\002\138\000\112\002\000\000\
\\000\000\
\\000\000\
\"
val numstates = 626
val numrules = 335
val s = ref "" and index = ref 0
val string_to_int = fn () => 
let val i = !index
in index := i+2; Char.ord(String.sub(!s,i)) + Char.ord(String.sub(!s,i+1)) * 256
end
val string_to_list = fn s' =>
    let val len = String.size s'
        fun f () =
           if !index < len then string_to_int() :: f()
           else nil
   in index := 0; s := s'; f ()
   end
val string_to_pairlist = fn (conv_key,conv_entry) =>
     let fun f () =
         case string_to_int()
         of 0 => EMPTY
          | n => PAIR(conv_key (n-1),conv_entry (string_to_int()),f())
     in f
     end
val string_to_pairlist_default = fn (conv_key,conv_entry) =>
    let val conv_row = string_to_pairlist(conv_key,conv_entry)
    in fn () =>
       let val default = conv_entry(string_to_int())
           val row = conv_row()
       in (row,default)
       end
   end
val string_to_table = fn (convert_row,s') =>
    let val len = String.size s'
        fun f ()=
           if !index < len then convert_row() :: f()
           else nil
     in (s := s'; index := 0; f ())
     end
local
  val memo = Array.array(numstates+numrules,ERROR)
  val _ =let fun g i=(Array.update(memo,i,REDUCE(i-numstates)); g(i+1))
       fun f i =
            if i=numstates then g i
            else (Array.update(memo,i,SHIFT (STATE i)); f (i+1))
          in f 0 handle Subscript => ()
          end
in
val entry_to_action = fn 0 => ACCEPT | 1 => ERROR | j => Array.sub(memo,(j-2))
end
val gotoT=Array.fromList(string_to_table(string_to_pairlist(NT,STATE),gotoT))
val actionRows=string_to_table(string_to_pairlist_default(T,entry_to_action),actionRows)
val actionRowNumbers = string_to_list actionRowNumbers
val actionT = let val actionRowLookUp=
let val a=Array.fromList(actionRows) in fn i=>Array.sub(a,i) end
in Array.fromList(map actionRowLookUp actionRowNumbers)
end
in LrTable.mkLrTable {actions=actionT,gotos=gotoT,numRules=numrules,
numStates=numstates,initialState=STATE 0}
end
end
local open Header in
type pos = Source.pos
type arg =  ( Source.pos * Source.pos -> Source.info ) *Infix.InfEnv
structure MlyValue = 
struct
datatype svalue = VOID | ntVOID of unit ->  unit
 | LONGID of unit ->  (string list*string)
 | ETYVAR of unit ->  (string) | TYVAR of unit ->  (string)
 | SYMBOL of unit ->  (string) | ALPHA of unit ->  (string)
 | CHAR of unit ->  (string) | STRING of unit ->  (string)
 | REAL of unit ->  (string) | HEXWORD of unit ->  (string)
 | WORD of unit ->  (string) | HEXINT of unit ->  (string)
 | INT of unit ->  (string) | NUMERIC of unit ->  (string)
 | DIGIT of unit ->  (string) | popLocalInfix of unit ->  (unit)
 | pushLocalInfix of unit ->  (unit) | popInfix of unit ->  (unit)
 | pushInfix of unit ->  (unit) | initInfix of unit ->  (unit)
 | program_opt of unit ->  (Program option)
 | program' of unit ->  (Program)
 | program of unit ->  (Program*Infix.InfEnv)
 | topdec_opt of unit ->  (TopDec option)
 | topdec1 of unit ->  (TopDec) | topdec of unit ->  (TopDec)
 | AND_tyreadesc_opt__AND_funbind_opt of unit ->  (TyReaDesc option*FunBind option)
 | tyreadesc__AND_funbind_opt of unit ->  (TyReaDesc*FunBind option)
 | sigexp__AND_funbind_opt of unit ->  (SigExp*FunBind option)
 | strexp__AND_funbind_opt of unit ->  (StrExp*FunBind option)
 | AND_funbind_opt of unit ->  (FunBind option)
 | funbind of unit ->  (FunBind) | fundec of unit ->  (FunDec)
 | AND_tyreadesc_opt__AND_strdesc_opt of unit ->  (TyReaDesc option*StrDesc option)
 | tyreadesc__AND_strdesc_opt of unit ->  (TyReaDesc*StrDesc option)
 | sigexp__AND_strdesc_opt of unit ->  (SigExp*StrDesc option)
 | AND_strdesc_opt of unit ->  (StrDesc option)
 | strdesc of unit ->  (StrDesc)
 | AND_exdesc_opt of unit ->  (ExDesc option)
 | exdesc of unit ->  (ExDesc)
 | BAR_condesc_opt of unit ->  (ConDesc option)
 | condesc of unit ->  (ConDesc)
 | AND_datdesc_opt of unit ->  (DatDesc option)
 | datdesc1 of unit ->  (DatDesc) | datdesc0 of unit ->  (DatDesc)
 | datdesc of unit ->  (DatDesc)
 | AND_syndesc_opt of unit ->  (SynDesc option)
 | syndesc of unit ->  (SynDesc)
 | AND_typdesc_opt of unit ->  (TypDesc option)
 | typdesc of unit ->  (TypDesc)
 | AND_valdesc_opt of unit ->  (ValDesc option)
 | valdesc of unit ->  (ValDesc)
 | longstrid_EQUALS_list2 of unit ->  (longStrId list)
 | longstrid_EQUALS_list1 of unit ->  (longStrId list)
 | longtycon_EQUALS_list2 of unit ->  (longTyCon list)
 | longtycon_EQUALS_list1 of unit ->  (longTyCon list)
 | sigid_list2 of unit ->  (SigId list) | spec1'' of unit ->  (Spec)
 | spec1' of unit ->  (Spec) | spec1 of unit ->  (Spec)
 | spec of unit ->  (Spec)
 | AND_tyreadesc_opt of unit ->  (TyReaDesc option)
 | tyreadesc of unit ->  (TyReaDesc)
 | AND_tyreadesc_opt__AND_sigbind_opt of unit ->  (TyReaDesc option*SigBind option)
 | tyreadesc__AND_sigbind_opt of unit ->  (TyReaDesc*SigBind option)
 | sigexp__AND_sigbind_opt of unit ->  (SigExp*SigBind option)
 | AND_sigbind_opt of unit ->  (SigBind option)
 | sigbind of unit ->  (SigBind) | sigdec of unit ->  (SigDec)
 | sigexp' of unit ->  (SigExp) | sigexp of unit ->  (SigExp)
 | COLON_sigexp_opt of unit ->  (SigExp option)
 | AND_tyreadesc_opt__AND_strbind_opt of unit ->  (TyReaDesc option*StrBind option)
 | tyreadesc__AND_strbind_opt of unit ->  (TyReaDesc*StrBind option)
 | sigexp__AND_strbind_opt of unit ->  (SigExp*StrBind option)
 | strexp__AND_strbind_opt of unit ->  (StrExp*StrBind option)
 | AND_strbind_opt of unit ->  (StrBind option)
 | strbind of unit ->  (StrBind) | strdec1' of unit ->  (StrDec)
 | strdec1 of unit ->  (StrDec) | strdec of unit ->  (StrDec)
 | strexp' of unit ->  (StrExp) | strexp of unit ->  (StrExp)
 | tyvar_COMMA_list1 of unit ->  (TyVar list)
 | tyvarseq1 of unit ->  (TyVarseq) | tyvarseq of unit ->  (TyVarseq)
 | ty_COMMA_list2 of unit ->  (Ty list) | tyseq of unit ->  (Tyseq)
 | COMMA_tyrow_opt of unit ->  (TyRow option)
 | tyrow_opt of unit ->  (TyRow option) | tyrow of unit ->  (TyRow)
 | atty of unit ->  (Ty) | consty of unit ->  (Ty)
 | ty_STAR_list of unit ->  (Ty list) | tupty of unit ->  (Ty)
 | ty of unit ->  (Ty) | COLON_ty_list1 of unit ->  (Ty list)
 | atpat_list2 of unit ->  (AtPat list)
 | atpat_list1 of unit ->  (AtPat list) | pat of unit ->  (Pat)
 | AS_pat_opt of unit ->  (Pat option)
 | COLON_ty_opt of unit ->  (Ty option)
 | COMMA_patrow_opt of unit ->  (PatRow option)
 | patrow_opt of unit ->  (PatRow option)
 | patrow of unit ->  (PatRow)
 | pat_COMMA_list2 of unit ->  (Pat list)
 | pat_COMMA_list1 of unit ->  (Pat list)
 | pat_COMMA_list0 of unit ->  (Pat list) | atpat' of unit ->  (AtPat)
 | atpat of unit ->  (AtPat)
 | AND_exbind_opt of unit ->  (ExBind option)
 | exbind of unit ->  (ExBind) | OF_ty_opt of unit ->  (Ty option)
 | BAR_conbind_opt of unit ->  (ConBind option)
 | conbind of unit ->  (ConBind)
 | AND_datbind_opt of unit ->  (DatBind option)
 | datbind1 of unit ->  (DatBind) | datbind0 of unit ->  (DatBind)
 | datbind of unit ->  (DatBind)
 | AND_typbind_opt of unit ->  (TypBind option)
 | typbind of unit ->  (TypBind) | fmrule of unit ->  (Fmrule)
 | BAR_fmatch_opt of unit ->  (Fmatch option)
 | fmatch of unit ->  (Fmatch)
 | AND_fvalbind_opt of unit ->  (FvalBind option)
 | fvalbind of unit ->  (FvalBind)
 | AND_valbind_opt of unit ->  (ValBind option)
 | valbind of unit ->  (ValBind) | d_opt of unit ->  (int)
 | longstrid_list1 of unit ->  (longStrId list)
 | vid_list1 of unit ->  (VId list)
 | WITHTYPE_typbind_opt of unit ->  (TypBind option)
 | dec1' of unit ->  (Dec) | dec1 of unit ->  (Dec)
 | dec of unit ->  (Dec) | mrule of unit ->  (Mrule)
 | BAR_match_opt of unit ->  (Match option)
 | match of unit ->  (Match) | exp of unit ->  (Exp)
 | infexp of unit ->  (InfExp) | appexp of unit ->  (AppExp)
 | COMMA_exprow_opt of unit ->  (ExpRow option)
 | exprow_opt of unit ->  (ExpRow option)
 | exprow of unit ->  (ExpRow)
 | exp_SEMICOLON_list2 of unit ->  (Exp list)
 | exp_SEMICOLON_list1 of unit ->  (Exp list)
 | exp_COMMA_list2 of unit ->  (Exp list)
 | exp_COMMA_list1 of unit ->  (Exp list)
 | exp_COMMA_list0 of unit ->  (Exp list) | atexp of unit ->  (AtExp)
 | OP_opt of unit ->  (Op) | longstrid of unit ->  (longStrId)
 | longtycon of unit ->  (longTyCon) | longvid' of unit ->  (longVId)
 | longvid of unit ->  (longVId) | funid of unit ->  (FunId)
 | sigid of unit ->  (SigId) | strid of unit ->  (StrId)
 | tyvar of unit ->  (TyVar) | tycon of unit ->  (TyCon)
 | vid' of unit ->  (VId) | vid of unit ->  (VId)
 | lab of unit ->  (Lab) | d of unit ->  (int)
 | scon of unit ->  (SCon)
end
type svalue = MlyValue.svalue
type result = Program*Infix.InfEnv
end
structure EC=
struct
open LrTable
val is_keyword =
fn (T 1) => true | (T 2) => true | (T 3) => true | (T 4) => true | (T 
5) => true | (T 6) => true | (T 7) => true | (T 8) => true | (T 9)
 => true | (T 10) => true | (T 11) => true | (T 12) => true | (T 13)
 => true | (T 14) => true | (T 15) => true | (T 16) => true | (T 17)
 => true | (T 18) => true | (T 19) => true | (T 20) => true | (T 21)
 => true | (T 22) => true | (T 23) => true | (T 24) => true | (T 25)
 => true | (T 26) => true | (T 27) => true | (T 28) => true | (T 29)
 => true | (T 30) => true | (T 31) => true | (T 32) => true | (T 49)
 => true | (T 50) => true | (T 51) => true | (T 52) => true | (T 53)
 => true | (T 54) => true | (T 55) => true | (T 56) => true | (T 57)
 => true | _ => false
val preferred_change = 
nil
val noShift = 
fn (T 0) => true | _ => false
val showTerminal =
fn (T 0) => "EOF"
  | (T 1) => "ABSTYPE"
  | (T 2) => "AND"
  | (T 3) => "ANDALSO"
  | (T 4) => "AS"
  | (T 5) => "CASE"
  | (T 6) => "DO"
  | (T 7) => "DATATYPE"
  | (T 8) => "ELSE"
  | (T 9) => "END"
  | (T 10) => "EXCEPTION"
  | (T 11) => "FN"
  | (T 12) => "FUN"
  | (T 13) => "HANDLE"
  | (T 14) => "IF"
  | (T 15) => "IN"
  | (T 16) => "INFIX"
  | (T 17) => "INFIXR"
  | (T 18) => "LET"
  | (T 19) => "LOCAL"
  | (T 20) => "NONFIX"
  | (T 21) => "OF"
  | (T 22) => "OP"
  | (T 23) => "OPEN"
  | (T 24) => "ORELSE"
  | (T 25) => "RAISE"
  | (T 26) => "REC"
  | (T 27) => "THEN"
  | (T 28) => "TYPE"
  | (T 29) => "VAL"
  | (T 30) => "WITH"
  | (T 31) => "WITHTYPE"
  | (T 32) => "WHILE"
  | (T 33) => "LPAR"
  | (T 34) => "RPAR"
  | (T 35) => "LBRACK"
  | (T 36) => "RBRACK"
  | (T 37) => "LBRACE"
  | (T 38) => "RBRACE"
  | (T 39) => "COMMA"
  | (T 40) => "COLON"
  | (T 41) => "SEMICOLON"
  | (T 42) => "DOTS"
  | (T 43) => "UNDERBAR"
  | (T 44) => "BAR"
  | (T 45) => "EQUALS"
  | (T 46) => "DARROW"
  | (T 47) => "ARROW"
  | (T 48) => "HASH"
  | (T 49) => "EQTYPE"
  | (T 50) => "FUNCTOR"
  | (T 51) => "INCLUDE"
  | (T 52) => "SHARING"
  | (T 53) => "SIG"
  | (T 54) => "SIGNATURE"
  | (T 55) => "STRUCT"
  | (T 56) => "STRUCTURE"
  | (T 57) => "WHERE"
  | (T 58) => "SEAL"
  | (T 59) => "ZERO"
  | (T 60) => "DIGIT"
  | (T 61) => "NUMERIC"
  | (T 62) => "INT"
  | (T 63) => "HEXINT"
  | (T 64) => "WORD"
  | (T 65) => "HEXWORD"
  | (T 66) => "REAL"
  | (T 67) => "STRING"
  | (T 68) => "CHAR"
  | (T 69) => "ALPHA"
  | (T 70) => "SYMBOL"
  | (T 71) => "STAR"
  | (T 72) => "TYVAR"
  | (T 73) => "ETYVAR"
  | (T 74) => "LONGID"
  | _ => "bogus-term"
local open Header in
val errtermvalue=
fn _ => MlyValue.VOID
end
val terms = (T 0) :: (T 1) :: (T 2) :: (T 3) :: (T 4) :: (T 5) :: (T 6
) :: (T 7) :: (T 8) :: (T 9) :: (T 10) :: (T 11) :: (T 12) :: (T 13)
 :: (T 14) :: (T 15) :: (T 16) :: (T 17) :: (T 18) :: (T 19) :: (T 20)
 :: (T 21) :: (T 22) :: (T 23) :: (T 24) :: (T 25) :: (T 26) :: (T 27)
 :: (T 28) :: (T 29) :: (T 30) :: (T 31) :: (T 32) :: (T 33) :: (T 34)
 :: (T 35) :: (T 36) :: (T 37) :: (T 38) :: (T 39) :: (T 40) :: (T 41)
 :: (T 42) :: (T 43) :: (T 44) :: (T 45) :: (T 46) :: (T 47) :: (T 48)
 :: (T 49) :: (T 50) :: (T 51) :: (T 52) :: (T 53) :: (T 54) :: (T 55)
 :: (T 56) :: (T 57) :: (T 58) :: (T 59) :: (T 71) :: nil
end
structure Actions =
struct 
exception mlyAction of int
local open Header in
val actions = 
fn (i392,defaultPos,stack,
    (I, J0):arg) =>
case (i392,stack)
of (0,rest671) => let val result=MlyValue.initInfix(fn _ => (
 initJandJ'(J0) ))
 in (LrTable.NT 145,(result,defaultPos,defaultPos),rest671) end
| (1,rest671) => let val result=MlyValue.pushInfix(fn _ => ( pushJ() )
)
 in (LrTable.NT 146,(result,defaultPos,defaultPos),rest671) end
| (2,rest671) => let val result=MlyValue.popInfix(fn _ => ( popJ() ))
 in (LrTable.NT 147,(result,defaultPos,defaultPos),rest671) end
| (3,rest671) => let val result=MlyValue.pushLocalInfix(fn _ => (
 pushJ'shiftJ() ))
 in (LrTable.NT 148,(result,defaultPos,defaultPos),rest671) end
| (4,rest671) => let val result=MlyValue.popLocalInfix(fn _ => (
 popJandJ'() ))
 in (LrTable.NT 149,(result,defaultPos,defaultPos),rest671) end
| (5,(_,(_,ZERO1left,ZERO1right))::rest671) => let val result=
MlyValue.scon(fn _ => ( SCon.INT(SCon.DEC, "0", ref NONE) ))
 in (LrTable.NT 0,(result,ZERO1left,ZERO1right),rest671) end
| (6,(_,(MlyValue.DIGIT DIGIT1,DIGIT1left,DIGIT1right))::rest671) => 
let val result=MlyValue.scon(fn _ => let val DIGIT as DIGIT1=DIGIT1 ()
 in ( SCon.INT(SCon.DEC, DIGIT, ref NONE) ) end
)
 in (LrTable.NT 0,(result,DIGIT1left,DIGIT1right),rest671) end
| (7,(_,(MlyValue.NUMERIC NUMERIC1,NUMERIC1left,NUMERIC1right))::
rest671) => let val result=MlyValue.scon(fn _ => let val NUMERIC as 
NUMERIC1=NUMERIC1 ()
 in ( SCon.INT(SCon.DEC, NUMERIC, ref NONE) ) end
)
 in (LrTable.NT 0,(result,NUMERIC1left,NUMERIC1right),rest671) end
| (8,(_,(MlyValue.INT INT1,INT1left,INT1right))::rest671) => let val 
result=MlyValue.scon(fn _ => let val INT as INT1=INT1 ()
 in ( SCon.INT(SCon.DEC, INT, ref NONE) ) end
)
 in (LrTable.NT 0,(result,INT1left,INT1right),rest671) end
| (9,(_,(MlyValue.HEXINT HEXINT1,HEXINT1left,HEXINT1right))::rest671)
 => let val result=MlyValue.scon(fn _ => let val HEXINT as HEXINT1=
HEXINT1 ()
 in ( SCon.INT(SCon.HEX, HEXINT, ref NONE) ) end
)
 in (LrTable.NT 0,(result,HEXINT1left,HEXINT1right),rest671) end
| (10,(_,(MlyValue.WORD WORD1,WORD1left,WORD1right))::rest671) => let 
val result=MlyValue.scon(fn _ => let val WORD as WORD1=WORD1 ()
 in ( SCon.WORD(SCon.DEC, WORD, ref NONE) ) end
)
 in (LrTable.NT 0,(result,WORD1left,WORD1right),rest671) end
| (11,(_,(MlyValue.HEXWORD HEXWORD1,HEXWORD1left,HEXWORD1right))::
rest671) => let val result=MlyValue.scon(fn _ => let val HEXWORD as 
HEXWORD1=HEXWORD1 ()
 in ( SCon.WORD(SCon.HEX, HEXWORD, ref NONE) ) end
)
 in (LrTable.NT 0,(result,HEXWORD1left,HEXWORD1right),rest671) end
| (12,(_,(MlyValue.REAL REAL1,REAL1left,REAL1right))::rest671) => let 
val result=MlyValue.scon(fn _ => let val REAL as REAL1=REAL1 ()
 in ( SCon.REAL(REAL, ref NONE) ) end
)
 in (LrTable.NT 0,(result,REAL1left,REAL1right),rest671) end
| (13,(_,(MlyValue.STRING STRING1,STRING1left,STRING1right))::rest671)
 => let val result=MlyValue.scon(fn _ => let val STRING as STRING1=
STRING1 ()
 in ( SCon.STRING(STRING, ref NONE) ) end
)
 in (LrTable.NT 0,(result,STRING1left,STRING1right),rest671) end
| (14,(_,(MlyValue.CHAR CHAR1,CHAR1left,CHAR1right))::rest671) => let 
val result=MlyValue.scon(fn _ => let val CHAR as CHAR1=CHAR1 ()
 in ( SCon.CHAR(CHAR, ref NONE) ) end
)
 in (LrTable.NT 0,(result,CHAR1left,CHAR1right),rest671) end
| (15,(_,(_,ZERO1left,ZERO1right))::rest671) => let val result=
MlyValue.d(fn _ => ( 0 ))
 in (LrTable.NT 1,(result,ZERO1left,ZERO1right),rest671) end
| (16,(_,(MlyValue.DIGIT DIGIT1,DIGIT1left,DIGIT1right))::rest671) => 
let val result=MlyValue.d(fn _ => let val DIGIT as DIGIT1=DIGIT1 ()
 in ( Option.valOf(Int.fromString DIGIT) ) end
)
 in (LrTable.NT 1,(result,DIGIT1left,DIGIT1right),rest671) end
| (17,(_,(MlyValue.ALPHA ALPHA1,ALPHA1left,ALPHA1right))::rest671) => 
let val result=MlyValue.lab(fn _ => let val ALPHA as ALPHA1=ALPHA1 ()
 in ( Lab.fromString ALPHA ) end
)
 in (LrTable.NT 2,(result,ALPHA1left,ALPHA1right),rest671) end
| (18,(_,(MlyValue.SYMBOL SYMBOL1,SYMBOL1left,SYMBOL1right))::rest671)
 => let val result=MlyValue.lab(fn _ => let val SYMBOL as SYMBOL1=
SYMBOL1 ()
 in ( Lab.fromString SYMBOL ) end
)
 in (LrTable.NT 2,(result,SYMBOL1left,SYMBOL1right),rest671) end
| (19,(_,(_,STAR1left,STAR1right))::rest671) => let val result=
MlyValue.lab(fn _ => ( Lab.fromString "*" ))
 in (LrTable.NT 2,(result,STAR1left,STAR1right),rest671) end
| (20,(_,(MlyValue.DIGIT DIGIT1,DIGIT1left,DIGIT1right))::rest671) => 
let val result=MlyValue.lab(fn _ => let val DIGIT as DIGIT1=DIGIT1 ()
 in ( Lab.fromString DIGIT ) end
)
 in (LrTable.NT 2,(result,DIGIT1left,DIGIT1right),rest671) end
| (21,(_,(MlyValue.NUMERIC NUMERIC1,NUMERIC1left,NUMERIC1right))::
rest671) => let val result=MlyValue.lab(fn _ => let val NUMERIC as 
NUMERIC1=NUMERIC1 ()
 in ( Lab.fromString NUMERIC ) end
)
 in (LrTable.NT 2,(result,NUMERIC1left,NUMERIC1right),rest671) end
| (22,(_,(MlyValue.vid' vid'1,vid'1left,vid'1right))::rest671) => let 
val result=MlyValue.vid(fn _ => let val vid' as vid'1=vid'1 ()
 in ( vid' ) end
)
 in (LrTable.NT 3,(result,vid'1left,vid'1right),rest671) end
| (23,(_,(_,EQUALS1left,EQUALS1right))::rest671) => let val result=
MlyValue.vid(fn _ => ( VId.fromString "=" ))
 in (LrTable.NT 3,(result,EQUALS1left,EQUALS1right),rest671) end
| (24,(_,(MlyValue.ALPHA ALPHA1,ALPHA1left,ALPHA1right))::rest671) => 
let val result=MlyValue.vid'(fn _ => let val ALPHA as ALPHA1=ALPHA1 ()
 in ( VId.fromString ALPHA ) end
)
 in (LrTable.NT 4,(result,ALPHA1left,ALPHA1right),rest671) end
| (25,(_,(MlyValue.SYMBOL SYMBOL1,SYMBOL1left,SYMBOL1right))::rest671)
 => let val result=MlyValue.vid'(fn _ => let val SYMBOL as SYMBOL1=
SYMBOL1 ()
 in ( VId.fromString SYMBOL ) end
)
 in (LrTable.NT 4,(result,SYMBOL1left,SYMBOL1right),rest671) end
| (26,(_,(_,STAR1left,STAR1right))::rest671) => let val result=
MlyValue.vid'(fn _ => ( VId.fromString "*" ))
 in (LrTable.NT 4,(result,STAR1left,STAR1right),rest671) end
| (27,(_,(MlyValue.ALPHA ALPHA1,ALPHA1left,ALPHA1right))::rest671) => 
let val result=MlyValue.tycon(fn _ => let val ALPHA as ALPHA1=ALPHA1 
()
 in ( TyCon.fromString ALPHA ) end
)
 in (LrTable.NT 5,(result,ALPHA1left,ALPHA1right),rest671) end
| (28,(_,(MlyValue.SYMBOL SYMBOL1,SYMBOL1left,SYMBOL1right))::rest671)
 => let val result=MlyValue.tycon(fn _ => let val SYMBOL as SYMBOL1=
SYMBOL1 ()
 in ( TyCon.fromString SYMBOL ) end
)
 in (LrTable.NT 5,(result,SYMBOL1left,SYMBOL1right),rest671) end
| (29,(_,(MlyValue.TYVAR TYVAR1,TYVAR1left,TYVAR1right))::rest671) => 
let val result=MlyValue.tyvar(fn _ => let val TYVAR as TYVAR1=TYVAR1 
()
 in ( TyVar.fromString TYVAR ) end
)
 in (LrTable.NT 6,(result,TYVAR1left,TYVAR1right),rest671) end
| (30,(_,(MlyValue.ALPHA ALPHA1,ALPHA1left,ALPHA1right))::rest671) => 
let val result=MlyValue.strid(fn _ => let val ALPHA as ALPHA1=ALPHA1 
()
 in ( StrId.fromString ALPHA ) end
)
 in (LrTable.NT 7,(result,ALPHA1left,ALPHA1right),rest671) end
| (31,(_,(MlyValue.ALPHA ALPHA1,ALPHA1left,ALPHA1right))::rest671) => 
let val result=MlyValue.sigid(fn _ => let val ALPHA as ALPHA1=ALPHA1 
()
 in ( SigId.fromString ALPHA ) end
)
 in (LrTable.NT 8,(result,ALPHA1left,ALPHA1right),rest671) end
| (32,(_,(MlyValue.ALPHA ALPHA1,ALPHA1left,ALPHA1right))::rest671) => 
let val result=MlyValue.funid(fn _ => let val ALPHA as ALPHA1=ALPHA1 
()
 in ( FunId.fromString ALPHA ) end
)
 in (LrTable.NT 9,(result,ALPHA1left,ALPHA1right),rest671) end
| (33,(_,(MlyValue.longvid' longvid'1,longvid'1left,longvid'1right))::
rest671) => let val result=MlyValue.longvid(fn _ => let val longvid'
 as longvid'1=longvid'1 ()
 in ( longvid' ) end
)
 in (LrTable.NT 10,(result,longvid'1left,longvid'1right),rest671) end
| (34,(_,(_,EQUALS1left,EQUALS1right))::rest671) => let val result=
MlyValue.longvid(fn _ => ( LongVId.fromId(VId.fromString "=") ))
 in (LrTable.NT 10,(result,EQUALS1left,EQUALS1right),rest671) end
| (35,(_,(MlyValue.vid' vid'1,vid'1left,vid'1right))::rest671) => let 
val result=MlyValue.longvid'(fn _ => let val vid' as vid'1=vid'1 ()
 in ( LongVId.fromId vid' ) end
)
 in (LrTable.NT 11,(result,vid'1left,vid'1right),rest671) end
| (36,(_,(MlyValue.LONGID LONGID1,LONGID1left,LONGID1right))::rest671)
 => let val result=MlyValue.longvid'(fn _ => let val LONGID as LONGID1
=LONGID1 ()
 in ( LongVId.implode(toLongId VId.fromString LONGID) ) end
)
 in (LrTable.NT 11,(result,LONGID1left,LONGID1right),rest671) end
| (37,(_,(MlyValue.tycon tycon1,tycon1left,tycon1right))::rest671) => 
let val result=MlyValue.longtycon(fn _ => let val tycon as tycon1=
tycon1 ()
 in ( LongTyCon.fromId tycon ) end
)
 in (LrTable.NT 12,(result,tycon1left,tycon1right),rest671) end
| (38,(_,(MlyValue.LONGID LONGID1,LONGID1left,LONGID1right))::rest671)
 => let val result=MlyValue.longtycon(fn _ => let val LONGID as 
LONGID1=LONGID1 ()
 in ( LongTyCon.implode(toLongId TyCon.fromString LONGID) ) end
)
 in (LrTable.NT 12,(result,LONGID1left,LONGID1right),rest671) end
| (39,(_,(MlyValue.strid strid1,strid1left,strid1right))::rest671) => 
let val result=MlyValue.longstrid(fn _ => let val strid as strid1=
strid1 ()
 in ( LongStrId.fromId strid ) end
)
 in (LrTable.NT 13,(result,strid1left,strid1right),rest671) end
| (40,(_,(MlyValue.LONGID LONGID1,LONGID1left,LONGID1right))::rest671)
 => let val result=MlyValue.longstrid(fn _ => let val LONGID as 
LONGID1=LONGID1 ()
 in ( LongStrId.implode(toLongId StrId.fromString LONGID) ) end
)
 in (LrTable.NT 13,(result,LONGID1left,LONGID1right),rest671) end
| (41,(_,(_,OP1left,OP1right))::rest671) => let val result=
MlyValue.OP_opt(fn _ => ( WITHOp ))
 in (LrTable.NT 14,(result,OP1left,OP1right),rest671) end
| (42,rest671) => let val result=MlyValue.OP_opt(fn _ => ( SANSOp ))
 in (LrTable.NT 14,(result,defaultPos,defaultPos),rest671) end
| (43,(_,(MlyValue.scon scon1,sconleft as scon1left,sconright as 
scon1right))::rest671) => let val result=MlyValue.atexp(fn _ => let 
val scon as scon1=scon1 ()
 in ( SCONAtExp(I(sconleft,sconright), scon) ) end
)
 in (LrTable.NT 15,(result,scon1left,scon1right),rest671) end
| (44,(_,(MlyValue.longvid longvid1,_,longvidright as longvid1right))
::(_,(MlyValue.OP_opt OP_opt1,OP_optleft as OP_opt1left,_))::rest671)
 => let val result=MlyValue.atexp(fn _ => let val OP_opt as OP_opt1=
OP_opt1 ()
val longvid as longvid1=longvid1 ()
 in ( IDAtExp(I(OP_optleft,longvidright),
				       OP_opt, longvid) 
) end
)
 in (LrTable.NT 15,(result,OP_opt1left,longvid1right),rest671) end
| (45,(_,(_,_,RBRACEright as RBRACE1right))::(_,(MlyValue.exprow_opt 
exprow_opt1,_,_))::(_,(_,LBRACEleft as LBRACE1left,_))::rest671) => 
let val result=MlyValue.atexp(fn _ => let val exprow_opt as 
exprow_opt1=exprow_opt1 ()
 in ( RECORDAtExp(I(LBRACEleft,RBRACEright), exprow_opt) ) end
)
 in (LrTable.NT 15,(result,LBRACE1left,RBRACE1right),rest671) end
| (46,(_,(MlyValue.lab lab1,_,labright as lab1right))::(_,(_,HASHleft
 as HASH1left,_))::rest671) => let val result=MlyValue.atexp(fn _ => 
let val lab as lab1=lab1 ()
 in ( HASHAtExp(I(HASHleft,labright), lab) ) end
)
 in (LrTable.NT 15,(result,HASH1left,lab1right),rest671) end
| (47,(_,(_,_,RPARright as RPAR1right))::(_,(_,LPARleft as LPAR1left,_
))::rest671) => let val result=MlyValue.atexp(fn _ => (
 UNITAtExp(I(LPARleft,RPARright)) ))
 in (LrTable.NT 15,(result,LPAR1left,RPAR1right),rest671) end
| (48,(_,(_,_,RPARright as RPAR1right))::(_,(MlyValue.exp_COMMA_list2 
exp_COMMA_list21,_,_))::(_,(_,LPARleft as LPAR1left,_))::rest671) => 
let val result=MlyValue.atexp(fn _ => let val exp_COMMA_list2 as 
exp_COMMA_list21=exp_COMMA_list21 ()
 in ( TUPLEAtExp(I(LPARleft,RPARright), exp_COMMA_list2) ) end
)
 in (LrTable.NT 15,(result,LPAR1left,RPAR1right),rest671) end
| (49,(_,(_,_,RBRACKright as RBRACK1right))::(_,(
MlyValue.exp_COMMA_list0 exp_COMMA_list01,_,_))::(_,(_,LBRACKleft as 
LBRACK1left,_))::rest671) => let val result=MlyValue.atexp(fn _ => 
let val exp_COMMA_list0 as exp_COMMA_list01=exp_COMMA_list01 ()
 in ( LISTAtExp(I(LBRACKleft,RBRACKright),
				    exp_COMMA_list0 ))
 end
)
 in (LrTable.NT 15,(result,LBRACK1left,RBRACK1right),rest671) end
| (50,(_,(_,_,RPARright as RPAR1right))::(_,(
MlyValue.exp_SEMICOLON_list2 exp_SEMICOLON_list21,_,_))::(_,(_,
LPARleft as LPAR1left,_))::rest671) => let val result=MlyValue.atexp(
fn _ => let val exp_SEMICOLON_list2 as exp_SEMICOLON_list21=
exp_SEMICOLON_list21 ()
 in ( SEQAtExp(I(LPARleft,RPARright), exp_SEMICOLON_list2) ) end
)
 in (LrTable.NT 15,(result,LPAR1left,RPAR1right),rest671) end
| (51,(_,(_,_,ENDright as END1right))::(_,(MlyValue.popInfix popInfix1
,_,_))::(_,(MlyValue.exp_SEMICOLON_list1 exp_SEMICOLON_list11,_,_))::_
::(_,(MlyValue.dec dec1,_,_))::(_,(MlyValue.pushInfix pushInfix1,_,_))
::(_,(_,LETleft as LET1left,_))::rest671) => let val result=
MlyValue.atexp(fn _ => let val pushInfix1=pushInfix1 ()
val dec as dec1=dec1 ()
val exp_SEMICOLON_list1 as exp_SEMICOLON_list11=exp_SEMICOLON_list11 
()
val popInfix1=popInfix1 ()
 in ( LETAtExp(I(LETleft,ENDright),
				   dec, exp_SEMICOLON_list1) )
 end
)
 in (LrTable.NT 15,(result,LET1left,END1right),rest671) end
| (52,(_,(_,_,RPARright as RPAR1right))::(_,(MlyValue.exp exp1,_,_))::
(_,(_,LPARleft as LPAR1left,_))::rest671) => let val result=
MlyValue.atexp(fn _ => let val exp as exp1=exp1 ()
 in ( PARAtExp(I(LPARleft,RPARright), exp) ) end
)
 in (LrTable.NT 15,(result,LPAR1left,RPAR1right),rest671) end
| (53,(_,(MlyValue.exp_COMMA_list1 exp_COMMA_list11,
exp_COMMA_list11left,exp_COMMA_list11right))::rest671) => let val 
result=MlyValue.exp_COMMA_list0(fn _ => let val exp_COMMA_list1 as 
exp_COMMA_list11=exp_COMMA_list11 ()
 in ( exp_COMMA_list1 ) end
)
 in (LrTable.NT 16,(result,exp_COMMA_list11left,exp_COMMA_list11right)
,rest671) end
| (54,rest671) => let val result=MlyValue.exp_COMMA_list0(fn _ => (
 [] ))
 in (LrTable.NT 16,(result,defaultPos,defaultPos),rest671) end
| (55,(_,(MlyValue.exp_COMMA_list1 exp_COMMA_list11,_,
exp_COMMA_list11right))::_::(_,(MlyValue.exp exp1,exp1left,_))::
rest671) => let val result=MlyValue.exp_COMMA_list1(fn _ => let val 
exp as exp1=exp1 ()
val exp_COMMA_list1 as exp_COMMA_list11=exp_COMMA_list11 ()
 in ( exp::exp_COMMA_list1 ) end
)
 in (LrTable.NT 17,(result,exp1left,exp_COMMA_list11right),rest671)
 end
| (56,(_,(MlyValue.exp exp1,exp1left,exp1right))::rest671) => let val 
result=MlyValue.exp_COMMA_list1(fn _ => let val exp as exp1=exp1 ()
 in ( exp::[] ) end
)
 in (LrTable.NT 17,(result,exp1left,exp1right),rest671) end
| (57,(_,(MlyValue.exp_COMMA_list1 exp_COMMA_list11,_,
exp_COMMA_list11right))::_::(_,(MlyValue.exp exp1,exp1left,_))::
rest671) => let val result=MlyValue.exp_COMMA_list2(fn _ => let val 
exp as exp1=exp1 ()
val exp_COMMA_list1 as exp_COMMA_list11=exp_COMMA_list11 ()
 in ( exp::exp_COMMA_list1 ) end
)
 in (LrTable.NT 18,(result,exp1left,exp_COMMA_list11right),rest671)
 end
| (58,(_,(MlyValue.exp_SEMICOLON_list1 exp_SEMICOLON_list11,_,
exp_SEMICOLON_list11right))::_::(_,(MlyValue.exp exp1,exp1left,_))::
rest671) => let val result=MlyValue.exp_SEMICOLON_list1(fn _ => let 
val exp as exp1=exp1 ()
val exp_SEMICOLON_list1 as exp_SEMICOLON_list11=exp_SEMICOLON_list11 
()
 in ( exp::exp_SEMICOLON_list1 ) end
)
 in (LrTable.NT 19,(result,exp1left,exp_SEMICOLON_list11right),rest671
) end
| (59,(_,(MlyValue.exp exp1,exp1left,exp1right))::rest671) => let val 
result=MlyValue.exp_SEMICOLON_list1(fn _ => let val exp as exp1=exp1 
()
 in ( exp::[] ) end
)
 in (LrTable.NT 19,(result,exp1left,exp1right),rest671) end
| (60,(_,(MlyValue.exp_SEMICOLON_list2 exp_SEMICOLON_list21,_,
exp_SEMICOLON_list21right))::_::(_,(MlyValue.exp exp1,exp1left,_))::
rest671) => let val result=MlyValue.exp_SEMICOLON_list2(fn _ => let 
val exp as exp1=exp1 ()
val exp_SEMICOLON_list2 as exp_SEMICOLON_list21=exp_SEMICOLON_list21 
()
 in ( exp::exp_SEMICOLON_list2 ) end
)
 in (LrTable.NT 20,(result,exp1left,exp_SEMICOLON_list21right),rest671
) end
| (61,(_,(MlyValue.exp exp2,_,exp2right))::_::(_,(MlyValue.exp exp1,
exp1left,_))::rest671) => let val result=MlyValue.exp_SEMICOLON_list2(
fn _ => let val exp1=exp1 ()
val exp2=exp2 ()
 in ( [exp1, exp2] ) end
)
 in (LrTable.NT 20,(result,exp1left,exp2right),rest671) end
| (62,(_,(MlyValue.COMMA_exprow_opt COMMA_exprow_opt1,_,
COMMA_exprow_optright as COMMA_exprow_opt1right))::(_,(MlyValue.exp 
exp1,_,_))::_::(_,(MlyValue.lab lab1,lableft as lab1left,_))::rest671)
 => let val result=MlyValue.exprow(fn _ => let val lab as lab1=lab1 ()
val exp as exp1=exp1 ()
val COMMA_exprow_opt as COMMA_exprow_opt1=COMMA_exprow_opt1 ()
 in (
 ExpRow(I(lableft,COMMA_exprow_optright),
	  				 lab, exp, COMMA_exprow_opt) 
) end
)
 in (LrTable.NT 21,(result,lab1left,COMMA_exprow_opt1right),rest671)
 end
| (63,(_,(MlyValue.exprow exprow1,_,exprow1right))::(_,(_,COMMA1left,_
))::rest671) => let val result=MlyValue.COMMA_exprow_opt(fn _ => let 
val exprow as exprow1=exprow1 ()
 in ( SOME exprow ) end
)
 in (LrTable.NT 23,(result,COMMA1left,exprow1right),rest671) end
| (64,rest671) => let val result=MlyValue.COMMA_exprow_opt(fn _ => (
 NONE ))
 in (LrTable.NT 23,(result,defaultPos,defaultPos),rest671) end
| (65,(_,(MlyValue.exprow exprow1,exprow1left,exprow1right))::rest671)
 => let val result=MlyValue.exprow_opt(fn _ => let val exprow as 
exprow1=exprow1 ()
 in ( SOME exprow ) end
)
 in (LrTable.NT 22,(result,exprow1left,exprow1right),rest671) end
| (66,rest671) => let val result=MlyValue.exprow_opt(fn _ => ( NONE ))
 in (LrTable.NT 22,(result,defaultPos,defaultPos),rest671) end
| (67,(_,(MlyValue.atexp atexp1,atexp1left,atexp1right))::rest671) => 
let val result=MlyValue.appexp(fn _ => let val atexp as atexp1=atexp1 
()
 in ( atexp::[] ) end
)
 in (LrTable.NT 24,(result,atexp1left,atexp1right),rest671) end
| (68,(_,(MlyValue.atexp atexp1,_,atexp1right))::(_,(MlyValue.appexp 
appexp1,appexp1left,_))::rest671) => let val result=MlyValue.appexp(
fn _ => let val appexp as appexp1=appexp1 ()
val atexp as atexp1=atexp1 ()
 in ( atexp::appexp ) end
)
 in (LrTable.NT 24,(result,appexp1left,atexp1right),rest671) end
| (69,(_,(MlyValue.appexp appexp1,appexp1left,appexp1right))::rest671)
 => let val result=MlyValue.infexp(fn _ => let val appexp as appexp1=
appexp1 ()
 in ( Infix.parseExp(!J, List.rev appexp) ) end
)
 in (LrTable.NT 25,(result,appexp1left,appexp1right),rest671) end
| (70,(_,(MlyValue.infexp infexp1,infexp1left,infexp1right))::rest671)
 => let val result=MlyValue.exp(fn _ => let val infexp as infexp1=
infexp1 ()
 in ( infexp ) end
)
 in (LrTable.NT 26,(result,infexp1left,infexp1right),rest671) end
| (71,(_,(MlyValue.ty ty1,_,tyright as ty1right))::_::(_,(MlyValue.exp
 exp1,expleft as exp1left,_))::rest671) => let val result=MlyValue.exp
(fn _ => let val exp as exp1=exp1 ()
val ty as ty1=ty1 ()
 in ( COLONExp(I(expleft,tyright), exp, ty) ) end
)
 in (LrTable.NT 26,(result,exp1left,ty1right),rest671) end
| (72,(_,(MlyValue.exp exp2,_,exp2right))::_::(_,(MlyValue.exp exp1,
exp1left,_))::rest671) => let val result=MlyValue.exp(fn _ => let val 
exp1=exp1 ()
val exp2=exp2 ()
 in ( ANDALSOExp(I(exp1left,exp2right), exp1, exp2)) end
)
 in (LrTable.NT 26,(result,exp1left,exp2right),rest671) end
| (73,(_,(MlyValue.exp exp2,_,exp2right))::_::(_,(MlyValue.exp exp1,
exp1left,_))::rest671) => let val result=MlyValue.exp(fn _ => let val 
exp1=exp1 ()
val exp2=exp2 ()
 in ( ORELSEExp(I(exp1left,exp2right), exp1, exp2) ) end
)
 in (LrTable.NT 26,(result,exp1left,exp2right),rest671) end
| (74,(_,(MlyValue.match match1,_,matchright as match1right))::_::(_,(
MlyValue.exp exp1,expleft as exp1left,_))::rest671) => let val result=
MlyValue.exp(fn _ => let val exp as exp1=exp1 ()
val match as match1=match1 ()
 in ( HANDLEExp(I(expleft,matchright), exp, match) ) end
)
 in (LrTable.NT 26,(result,exp1left,match1right),rest671) end
| (75,(_,(MlyValue.exp exp1,_,expright as exp1right))::(_,(_,RAISEleft
 as RAISE1left,_))::rest671) => let val result=MlyValue.exp(fn _ => 
let val exp as exp1=exp1 ()
 in ( RAISEExp(I(RAISEleft,expright), exp) ) end
)
 in (LrTable.NT 26,(result,RAISE1left,exp1right),rest671) end
| (76,(_,(MlyValue.exp exp3,_,exp3right))::_::(_,(MlyValue.exp exp2,_,
_))::_::(_,(MlyValue.exp exp1,_,_))::(_,(_,IFleft as IF1left,_))::
rest671) => let val result=MlyValue.exp(fn _ => let val exp1=exp1 ()
val exp2=exp2 ()
val exp3=exp3 ()
 in ( IFExp(I(IFleft,exp3right), exp1, exp2, exp3) ) end
)
 in (LrTable.NT 26,(result,IF1left,exp3right),rest671) end
| (77,(_,(MlyValue.exp exp2,_,exp2right))::_::(_,(MlyValue.exp exp1,_,
_))::(_,(_,WHILEleft as WHILE1left,_))::rest671) => let val result=
MlyValue.exp(fn _ => let val exp1=exp1 ()
val exp2=exp2 ()
 in ( WHILEExp(I(WHILEleft,exp2right), exp1, exp2) ) end
)
 in (LrTable.NT 26,(result,WHILE1left,exp2right),rest671) end
| (78,(_,(MlyValue.match match1,_,matchright as match1right))::_::(_,(
MlyValue.exp exp1,_,_))::(_,(_,CASEleft as CASE1left,_))::rest671) => 
let val result=MlyValue.exp(fn _ => let val exp as exp1=exp1 ()
val match as match1=match1 ()
 in ( CASEExp(I(CASEleft,matchright), exp, match) ) end
)
 in (LrTable.NT 26,(result,CASE1left,match1right),rest671) end
| (79,(_,(MlyValue.match match1,_,matchright as match1right))::(_,(_,
FNleft as FN1left,_))::rest671) => let val result=MlyValue.exp(fn _
 => let val match as match1=match1 ()
 in ( FNExp(I(FNleft,matchright), match) ) end
)
 in (LrTable.NT 26,(result,FN1left,match1right),rest671) end
| (80,(_,(MlyValue.BAR_match_opt BAR_match_opt1,_,BAR_match_optright
 as BAR_match_opt1right))::(_,(MlyValue.mrule mrule1,mruleleft as 
mrule1left,_))::rest671) => let val result=MlyValue.match(fn _ => let 
val mrule as mrule1=mrule1 ()
val BAR_match_opt as BAR_match_opt1=BAR_match_opt1 ()
 in (
 Match(I(mruleleft,BAR_match_optright),
					mrule, BAR_match_opt) )
 end
)
 in (LrTable.NT 27,(result,mrule1left,BAR_match_opt1right),rest671)
 end
| (81,(_,(MlyValue.match match1,_,match1right))::(_,(_,BAR1left,_))::
rest671) => let val result=MlyValue.BAR_match_opt(fn _ => let val 
match as match1=match1 ()
 in ( SOME match ) end
)
 in (LrTable.NT 28,(result,BAR1left,match1right),rest671) end
| (82,rest671) => let val result=MlyValue.BAR_match_opt(fn _ => (
 NONE ))
 in (LrTable.NT 28,(result,defaultPos,defaultPos),rest671) end
| (83,(_,(MlyValue.exp exp1,_,expright as exp1right))::_::(_,(
MlyValue.pat pat1,patleft as pat1left,_))::rest671) => let val result=
MlyValue.mrule(fn _ => let val pat as pat1=pat1 ()
val exp as exp1=exp1 ()
 in ( Mrule(I(patleft,expright), pat, exp) ) end
)
 in (LrTable.NT 29,(result,pat1left,exp1right),rest671) end
| (84,(_,(MlyValue.dec1 dec11,dec11left,dec11right))::rest671) => let 
val result=MlyValue.dec(fn _ => let val dec1 as dec11=dec11 ()
 in ( dec1 ) end
)
 in (LrTable.NT 30,(result,dec11left,dec11right),rest671) end
| (85,rest671) => let val result=MlyValue.dec(fn _ => (
 EMPTYDec(I(defaultPos,defaultPos)) ))
 in (LrTable.NT 30,(result,defaultPos,defaultPos),rest671) end
| (86,(_,(MlyValue.dec1' dec1'1,dec1'1left,dec1'1right))::rest671) => 
let val result=MlyValue.dec1(fn _ => let val dec1' as dec1'1=dec1'1 ()
 in ( dec1' ) end
)
 in (LrTable.NT 31,(result,dec1'1left,dec1'1right),rest671) end
| (87,(_,(_,_,ENDright as END1right))::(_,(MlyValue.popLocalInfix 
popLocalInfix1,_,_))::(_,(MlyValue.dec dec2,_,_))::(_,(
MlyValue.pushLocalInfix pushLocalInfix1,_,_))::_::(_,(MlyValue.dec 
dec1,_,_))::(_,(MlyValue.pushInfix pushInfix1,_,_))::(_,(_,LOCALleft
 as LOCAL1left,_))::rest671) => let val result=MlyValue.dec1(fn _ => 
let val pushInfix1=pushInfix1 ()
val dec1=dec1 ()
val pushLocalInfix1=pushLocalInfix1 ()
val dec2=dec2 ()
val popLocalInfix1=popLocalInfix1 ()
 in ( LOCALDec(I(LOCALleft,ENDright), dec1, dec2) ) end
)
 in (LrTable.NT 31,(result,LOCAL1left,END1right),rest671) end
| (88,(_,(MlyValue.dec1 dec12,_,dec12right))::(_,(MlyValue.dec1 dec11,
dec11left,_))::rest671) => let val result=MlyValue.dec1(fn _ => let 
val dec11=dec11 ()
val dec12=dec12 ()
 in ( SEQDec(I(dec11left,dec12right), dec11, dec12) ) end
)
 in (LrTable.NT 31,(result,dec11left,dec12right),rest671) end
| (89,(_,(_,SEMICOLONleft as SEMICOLON1left,SEMICOLONright as 
SEMICOLON1right))::rest671) => let val result=MlyValue.dec1(fn _ => (
 SEQDec(I(SEMICOLONleft,SEMICOLONright),
				 EMPTYDec(I(SEMICOLONleft,SEMICOLONleft)),
				 EMPTYDec(I(SEMICOLONright,SEMICOLONright))) 
))
 in (LrTable.NT 31,(result,SEMICOLON1left,SEMICOLON1right),rest671)
 end
| (90,(_,(MlyValue.valbind valbind1,_,valbindright as valbind1right))
::(_,(_,VALleft as VAL1left,_))::rest671) => let val result=
MlyValue.dec1'(fn _ => let val valbind as valbind1=valbind1 ()
 in (
 VALDec(I(VALleft,valbindright),
			     TyVarseq(I(defaultPos,defaultPos), []), valbind) 
) end
)
 in (LrTable.NT 32,(result,VAL1left,valbind1right),rest671) end
| (91,(_,(MlyValue.valbind valbind1,_,valbindright as valbind1right))
::(_,(MlyValue.tyvarseq1 tyvarseq11,_,_))::(_,(_,VALleft as VAL1left,_
))::rest671) => let val result=MlyValue.dec1'(fn _ => let val 
tyvarseq1 as tyvarseq11=tyvarseq11 ()
val valbind as valbind1=valbind1 ()
 in ( VALDec(I(VALleft,valbindright), tyvarseq1, valbind) ) end
)
 in (LrTable.NT 32,(result,VAL1left,valbind1right),rest671) end
| (92,(_,(MlyValue.fvalbind fvalbind1,_,fvalbindright as 
fvalbind1right))::(_,(_,FUNleft as FUN1left,_))::rest671) => let val 
result=MlyValue.dec1'(fn _ => let val fvalbind as fvalbind1=fvalbind1 
()
 in (
 FUNDec(I(FUNleft,fvalbindright),
			    TyVarseq(I(defaultPos,defaultPos), []), fvalbind) 
) end
)
 in (LrTable.NT 32,(result,FUN1left,fvalbind1right),rest671) end
| (93,(_,(MlyValue.fvalbind fvalbind1,_,fvalbindright as 
fvalbind1right))::(_,(MlyValue.tyvarseq1 tyvarseq11,_,_))::(_,(_,
FUNleft as FUN1left,_))::rest671) => let val result=MlyValue.dec1'(fn 
_ => let val tyvarseq1 as tyvarseq11=tyvarseq11 ()
val fvalbind as fvalbind1=fvalbind1 ()
 in ( FUNDec(I(FUNleft,fvalbindright), tyvarseq1, fvalbind)) end
)
 in (LrTable.NT 32,(result,FUN1left,fvalbind1right),rest671) end
| (94,(_,(MlyValue.typbind typbind1,_,typbindright as typbind1right))
::(_,(_,TYPEleft as TYPE1left,_))::rest671) => let val result=
MlyValue.dec1'(fn _ => let val typbind as typbind1=typbind1 ()
 in ( TYPEDec(I(TYPEleft,typbindright), typbind) ) end
)
 in (LrTable.NT 32,(result,TYPE1left,typbind1right),rest671) end
| (95,(_,(MlyValue.WITHTYPE_typbind_opt WITHTYPE_typbind_opt1,_,
WITHTYPE_typbind_optright as WITHTYPE_typbind_opt1right))::(_,(
MlyValue.datbind0 datbind01,_,_))::(_,(_,DATATYPEleft as DATATYPE1left
,_))::rest671) => let val result=MlyValue.dec1'(fn _ => let val 
datbind0 as datbind01=datbind01 ()
val WITHTYPE_typbind_opt as WITHTYPE_typbind_opt1=
WITHTYPE_typbind_opt1 ()
 in (
 DATATYPEDec(I(DATATYPEleft,WITHTYPE_typbind_optright),
				     datbind0, WITHTYPE_typbind_opt) 
) end
)
 in (LrTable.NT 32,(result,DATATYPE1left,WITHTYPE_typbind_opt1right),
rest671) end
| (96,(_,(MlyValue.WITHTYPE_typbind_opt WITHTYPE_typbind_opt1,_,
WITHTYPE_typbind_optright as WITHTYPE_typbind_opt1right))::(_,(
MlyValue.datbind1 datbind11,_,_))::(_,(_,DATATYPEleft as DATATYPE1left
,_))::rest671) => let val result=MlyValue.dec1'(fn _ => let val 
datbind1 as datbind11=datbind11 ()
val WITHTYPE_typbind_opt as WITHTYPE_typbind_opt1=
WITHTYPE_typbind_opt1 ()
 in (
 DATATYPEDec(I(DATATYPEleft,WITHTYPE_typbind_optright),
				      datbind1, WITHTYPE_typbind_opt) 
) end
)
 in (LrTable.NT 32,(result,DATATYPE1left,WITHTYPE_typbind_opt1right),
rest671) end
| (97,(_,(MlyValue.longtycon longtycon1,_,longtyconright as 
longtycon1right))::_::_::(_,(MlyValue.tycon tycon1,_,_))::(_,(_,
DATATYPEleft as DATATYPE1left,_))::rest671) => let val result=
MlyValue.dec1'(fn _ => let val tycon as tycon1=tycon1 ()
val longtycon as longtycon1=longtycon1 ()
 in (
 DATATYPE2Dec(I(DATATYPEleft,longtyconright),
				       tycon, longtycon) 
) end
)
 in (LrTable.NT 32,(result,DATATYPE1left,longtycon1right),rest671) end
| (98,(_,(_,_,ENDright as END1right))::(_,(MlyValue.dec dec1,_,_))::_
::(_,(MlyValue.WITHTYPE_typbind_opt WITHTYPE_typbind_opt1,_,_))::(_,(
MlyValue.datbind datbind1,_,_))::(_,(_,ABSTYPEleft as ABSTYPE1left,_))
::rest671) => let val result=MlyValue.dec1'(fn _ => let val datbind
 as datbind1=datbind1 ()
val WITHTYPE_typbind_opt as WITHTYPE_typbind_opt1=
WITHTYPE_typbind_opt1 ()
val dec as dec1=dec1 ()
 in (
 ABSTYPEDec(I(ABSTYPEleft,ENDright), datbind,
				     WITHTYPE_typbind_opt, dec) 
) end
)
 in (LrTable.NT 32,(result,ABSTYPE1left,END1right),rest671) end
| (99,(_,(MlyValue.exbind exbind1,_,exbindright as exbind1right))::(_,
(_,EXCEPTIONleft as EXCEPTION1left,_))::rest671) => let val result=
MlyValue.dec1'(fn _ => let val exbind as exbind1=exbind1 ()
 in ( EXCEPTIONDec(I(EXCEPTIONleft,exbindright), exbind) ) end
)
 in (LrTable.NT 32,(result,EXCEPTION1left,exbind1right),rest671) end
| (100,(_,(MlyValue.longstrid_list1 longstrid_list11,_,
longstrid_list1right as longstrid_list11right))::(_,(_,OPENleft as 
OPEN1left,_))::rest671) => let val result=MlyValue.dec1'(fn _ => let 
val longstrid_list1 as longstrid_list11=longstrid_list11 ()
 in (
 OPENDec(I(OPENleft,longstrid_list1right),
				  longstrid_list1) )
 end
)
 in (LrTable.NT 32,(result,OPEN1left,longstrid_list11right),rest671)
 end
| (101,(_,(MlyValue.vid_list1 vid_list11,_,vid_list1right as 
vid_list11right))::(_,(MlyValue.d_opt d_opt1,_,_))::(_,(_,INFIXleft
 as INFIX1left,_))::rest671) => let val result=MlyValue.dec1'(fn _ => 
let val d_opt as d_opt1=d_opt1 ()
val vid_list1 as vid_list11=vid_list11 ()
 in (
 assignInfix((Infix.LEFT, d_opt), vid_list1);
			  EMPTYDec(I(INFIXleft,vid_list1right)) 
) end
)
 in (LrTable.NT 32,(result,INFIX1left,vid_list11right),rest671) end
| (102,(_,(MlyValue.vid_list1 vid_list11,_,vid_list1right as 
vid_list11right))::(_,(MlyValue.d_opt d_opt1,_,_))::(_,(_,INFIXRleft
 as INFIXR1left,_))::rest671) => let val result=MlyValue.dec1'(fn _
 => let val d_opt as d_opt1=d_opt1 ()
val vid_list1 as vid_list11=vid_list11 ()
 in (
 assignInfix((Infix.RIGHT, d_opt), vid_list1);
			  EMPTYDec(I(INFIXRleft,vid_list1right)) 
) end
)
 in (LrTable.NT 32,(result,INFIXR1left,vid_list11right),rest671) end
| (103,(_,(MlyValue.vid_list1 vid_list11,_,vid_list1right as 
vid_list11right))::(_,(_,NONFIXleft as NONFIX1left,_))::rest671) => 
let val result=MlyValue.dec1'(fn _ => let val vid_list1 as vid_list11=
vid_list11 ()
 in (
 cancelInfix(vid_list1);
			  EMPTYDec(I(NONFIXleft,vid_list1right)) )
 end
)
 in (LrTable.NT 32,(result,NONFIX1left,vid_list11right),rest671) end
| (104,(_,(MlyValue.typbind typbind1,_,typbind1right))::(_,(_,
WITHTYPE1left,_))::rest671) => let val result=
MlyValue.WITHTYPE_typbind_opt(fn _ => let val typbind as typbind1=
typbind1 ()
 in ( SOME typbind ) end
)
 in (LrTable.NT 33,(result,WITHTYPE1left,typbind1right),rest671) end
| (105,rest671) => let val result=MlyValue.WITHTYPE_typbind_opt(fn _
 => ( NONE ))
 in (LrTable.NT 33,(result,defaultPos,defaultPos),rest671) end
| (106,(_,(MlyValue.vid_list1 vid_list11,_,vid_list11right))::(_,(
MlyValue.vid vid1,vid1left,_))::rest671) => let val result=
MlyValue.vid_list1(fn _ => let val vid as vid1=vid1 ()
val vid_list1 as vid_list11=vid_list11 ()
 in ( vid::vid_list1 ) end
)
 in (LrTable.NT 34,(result,vid1left,vid_list11right),rest671) end
| (107,(_,(MlyValue.vid vid1,vid1left,vid1right))::rest671) => let 
val result=MlyValue.vid_list1(fn _ => let val vid as vid1=vid1 ()
 in ( vid::[] ) end
)
 in (LrTable.NT 34,(result,vid1left,vid1right),rest671) end
| (108,(_,(MlyValue.longstrid_list1 longstrid_list11,_,
longstrid_list11right))::(_,(MlyValue.longstrid longstrid1,
longstrid1left,_))::rest671) => let val result=
MlyValue.longstrid_list1(fn _ => let val longstrid as longstrid1=
longstrid1 ()
val longstrid_list1 as longstrid_list11=longstrid_list11 ()
 in ( longstrid::longstrid_list1 ) end
)
 in (LrTable.NT 35,(result,longstrid1left,longstrid_list11right),
rest671) end
| (109,(_,(MlyValue.longstrid longstrid1,longstrid1left,
longstrid1right))::rest671) => let val result=MlyValue.longstrid_list1
(fn _ => let val longstrid as longstrid1=longstrid1 ()
 in ( longstrid::[] ) end
)
 in (LrTable.NT 35,(result,longstrid1left,longstrid1right),rest671)
 end
| (110,(_,(MlyValue.d d1,d1left,d1right))::rest671) => let val result=
MlyValue.d_opt(fn _ => let val d as d1=d1 ()
 in ( d ) end
)
 in (LrTable.NT 36,(result,d1left,d1right),rest671) end
| (111,rest671) => let val result=MlyValue.d_opt(fn _ => ( 0 ))
 in (LrTable.NT 36,(result,defaultPos,defaultPos),rest671) end
| (112,(_,(MlyValue.AND_valbind_opt AND_valbind_opt1,_,
AND_valbind_optright as AND_valbind_opt1right))::(_,(MlyValue.exp exp1
,_,_))::_::(_,(MlyValue.pat pat1,patleft as pat1left,_))::rest671) => 
let val result=MlyValue.valbind(fn _ => let val pat as pat1=pat1 ()
val exp as exp1=exp1 ()
val AND_valbind_opt as AND_valbind_opt1=AND_valbind_opt1 ()
 in (
 PLAINValBind(I(patleft,AND_valbind_optright),
				       pat, exp, AND_valbind_opt) 
) end
)
 in (LrTable.NT 37,(result,pat1left,AND_valbind_opt1right),rest671)
 end
| (113,(_,(MlyValue.valbind valbind1,_,valbindright as valbind1right))
::(_,(_,RECleft as REC1left,_))::rest671) => let val result=
MlyValue.valbind(fn _ => let val valbind as valbind1=valbind1 ()
 in ( RECValBind(I(RECleft,valbindright), valbind) ) end
)
 in (LrTable.NT 37,(result,REC1left,valbind1right),rest671) end
| (114,(_,(MlyValue.valbind valbind1,_,valbind1right))::(_,(_,AND1left
,_))::rest671) => let val result=MlyValue.AND_valbind_opt(fn _ => let 
val valbind as valbind1=valbind1 ()
 in ( SOME valbind ) end
)
 in (LrTable.NT 38,(result,AND1left,valbind1right),rest671) end
| (115,rest671) => let val result=MlyValue.AND_valbind_opt(fn _ => (
 NONE ))
 in (LrTable.NT 38,(result,defaultPos,defaultPos),rest671) end
| (116,(_,(MlyValue.AND_fvalbind_opt AND_fvalbind_opt1,_,
AND_fvalbind_optright as AND_fvalbind_opt1right))::(_,(MlyValue.fmatch
 fmatch1,fmatchleft as fmatch1left,_))::rest671) => let val result=
MlyValue.fvalbind(fn _ => let val fmatch as fmatch1=fmatch1 ()
val AND_fvalbind_opt as AND_fvalbind_opt1=AND_fvalbind_opt1 ()
 in (
 FvalBind(I(fmatchleft,AND_fvalbind_optright),
				     fmatch, AND_fvalbind_opt) 
) end
)
 in (LrTable.NT 39,(result,fmatch1left,AND_fvalbind_opt1right),rest671
) end
| (117,(_,(MlyValue.fvalbind fvalbind1,_,fvalbind1right))::(_,(_,
AND1left,_))::rest671) => let val result=MlyValue.AND_fvalbind_opt(fn 
_ => let val fvalbind as fvalbind1=fvalbind1 ()
 in ( SOME fvalbind ) end
)
 in (LrTable.NT 40,(result,AND1left,fvalbind1right),rest671) end
| (118,rest671) => let val result=MlyValue.AND_fvalbind_opt(fn _ => (
 NONE ))
 in (LrTable.NT 40,(result,defaultPos,defaultPos),rest671) end
| (119,(_,(MlyValue.BAR_fmatch_opt BAR_fmatch_opt1,_,
BAR_fmatch_optright as BAR_fmatch_opt1right))::(_,(MlyValue.fmrule 
fmrule1,fmruleleft as fmrule1left,_))::rest671) => let val result=
MlyValue.fmatch(fn _ => let val fmrule as fmrule1=fmrule1 ()
val BAR_fmatch_opt as BAR_fmatch_opt1=BAR_fmatch_opt1 ()
 in (
 Fmatch(I(fmruleleft,BAR_fmatch_optright),
				 fmrule, BAR_fmatch_opt) 
) end
)
 in (LrTable.NT 41,(result,fmrule1left,BAR_fmatch_opt1right),rest671)
 end
| (120,(_,(MlyValue.fmatch fmatch1,_,fmatch1right))::(_,(_,BAR1left,_)
)::rest671) => let val result=MlyValue.BAR_fmatch_opt(fn _ => let val 
fmatch as fmatch1=fmatch1 ()
 in ( SOME fmatch ) end
)
 in (LrTable.NT 42,(result,BAR1left,fmatch1right),rest671) end
| (121,rest671) => let val result=MlyValue.BAR_fmatch_opt(fn _ => (
 NONE ))
 in (LrTable.NT 42,(result,defaultPos,defaultPos),rest671) end
| (122,(_,(MlyValue.exp exp1,_,expright as exp1right))::_::(_,(
MlyValue.COLON_ty_opt COLON_ty_opt1,_,_))::(_,(MlyValue.atpat_list1 
atpat_list11,atpat_list1left as atpat_list11left,_))::rest671) => let 
val result=MlyValue.fmrule(fn _ => let val atpat_list1 as atpat_list11
=atpat_list11 ()
val COLON_ty_opt as COLON_ty_opt1=COLON_ty_opt1 ()
val exp as exp1=exp1 ()
 in (
 let
			    val (op_opt, vid, atpats) =
				Infix.parseFmrule(!J, atpat_list1)
			  in
			    Fmrule(I(atpat_list1left,expright),
				   op_opt, vid, atpats, COLON_ty_opt, exp)
			  end 
) end
)
 in (LrTable.NT 43,(result,atpat_list11left,exp1right),rest671) end
| (123,(_,(MlyValue.AND_typbind_opt AND_typbind_opt1,_,
AND_typbind_optright as AND_typbind_opt1right))::(_,(MlyValue.ty ty1,_
,_))::_::(_,(MlyValue.tycon tycon1,_,_))::(_,(MlyValue.tyvarseq 
tyvarseq1,tyvarseqleft as tyvarseq1left,_))::rest671) => let val 
result=MlyValue.typbind(fn _ => let val tyvarseq as tyvarseq1=
tyvarseq1 ()
val tycon as tycon1=tycon1 ()
val ty as ty1=ty1 ()
val AND_typbind_opt as AND_typbind_opt1=AND_typbind_opt1 ()
 in (
 TypBind(I(tyvarseqleft,AND_typbind_optright),
				  tyvarseq, tycon, ty, AND_typbind_opt) 
) end
)
 in (LrTable.NT 44,(result,tyvarseq1left,AND_typbind_opt1right),
rest671) end
| (124,(_,(MlyValue.typbind typbind1,_,typbind1right))::(_,(_,AND1left
,_))::rest671) => let val result=MlyValue.AND_typbind_opt(fn _ => let 
val typbind as typbind1=typbind1 ()
 in ( SOME typbind ) end
)
 in (LrTable.NT 45,(result,AND1left,typbind1right),rest671) end
| (125,rest671) => let val result=MlyValue.AND_typbind_opt(fn _ => (
 NONE ))
 in (LrTable.NT 45,(result,defaultPos,defaultPos),rest671) end
| (126,(_,(MlyValue.AND_datbind_opt AND_datbind_opt1,_,
AND_datbind_optright as AND_datbind_opt1right))::(_,(MlyValue.conbind 
conbind1,_,_))::_::(_,(MlyValue.tycon tycon1,_,_))::(_,(
MlyValue.tyvarseq tyvarseq1,tyvarseqleft as tyvarseq1left,_))::rest671
) => let val result=MlyValue.datbind(fn _ => let val tyvarseq as 
tyvarseq1=tyvarseq1 ()
val tycon as tycon1=tycon1 ()
val conbind as conbind1=conbind1 ()
val AND_datbind_opt as AND_datbind_opt1=AND_datbind_opt1 ()
 in (
 DatBind(I(tyvarseqleft,AND_datbind_optright),
				  tyvarseq, tycon, conbind, AND_datbind_opt) 
) end
)
 in (LrTable.NT 46,(result,tyvarseq1left,AND_datbind_opt1right),
rest671) end
| (127,(_,(MlyValue.AND_datbind_opt AND_datbind_opt1,_,
AND_datbind_optright as AND_datbind_opt1right))::(_,(MlyValue.conbind 
conbind1,_,_))::_::(_,(MlyValue.tycon tycon1,tyconleft as tycon1left,_
))::rest671) => let val result=MlyValue.datbind0(fn _ => let val tycon
 as tycon1=tycon1 ()
val conbind as conbind1=conbind1 ()
val AND_datbind_opt as AND_datbind_opt1=AND_datbind_opt1 ()
 in (
 DatBind(I(tyconleft,AND_datbind_optright),
				  TyVarseq(I(defaultPos,defaultPos), []),
				  tycon, conbind, AND_datbind_opt) 
) end
)
 in (LrTable.NT 47,(result,tycon1left,AND_datbind_opt1right),rest671)
 end
| (128,(_,(MlyValue.AND_datbind_opt AND_datbind_opt1,_,
AND_datbind_optright as AND_datbind_opt1right))::(_,(MlyValue.conbind 
conbind1,_,_))::_::(_,(MlyValue.tycon tycon1,_,_))::(_,(
MlyValue.tyvarseq1 tyvarseq11,tyvarseq1left as tyvarseq11left,_))::
rest671) => let val result=MlyValue.datbind1(fn _ => let val tyvarseq1
 as tyvarseq11=tyvarseq11 ()
val tycon as tycon1=tycon1 ()
val conbind as conbind1=conbind1 ()
val AND_datbind_opt as AND_datbind_opt1=AND_datbind_opt1 ()
 in (
 DatBind(I(tyvarseq1left,AND_datbind_optright),
				  tyvarseq1, tycon, conbind, AND_datbind_opt) 
) end
)
 in (LrTable.NT 48,(result,tyvarseq11left,AND_datbind_opt1right),
rest671) end
| (129,(_,(MlyValue.datbind datbind1,_,datbind1right))::(_,(_,AND1left
,_))::rest671) => let val result=MlyValue.AND_datbind_opt(fn _ => let 
val datbind as datbind1=datbind1 ()
 in ( SOME datbind ) end
)
 in (LrTable.NT 49,(result,AND1left,datbind1right),rest671) end
| (130,rest671) => let val result=MlyValue.AND_datbind_opt(fn _ => (
 NONE ))
 in (LrTable.NT 49,(result,defaultPos,defaultPos),rest671) end
| (131,(_,(MlyValue.BAR_conbind_opt BAR_conbind_opt1,_,
BAR_conbind_optright as BAR_conbind_opt1right))::(_,(
MlyValue.OF_ty_opt OF_ty_opt1,_,_))::(_,(MlyValue.vid' vid'1,_,_))::(_
,(MlyValue.OP_opt OP_opt1,OP_optleft as OP_opt1left,_))::rest671) => 
let val result=MlyValue.conbind(fn _ => let val OP_opt as OP_opt1=
OP_opt1 ()
val vid' as vid'1=vid'1 ()
val OF_ty_opt as OF_ty_opt1=OF_ty_opt1 ()
val BAR_conbind_opt as BAR_conbind_opt1=BAR_conbind_opt1 ()
 in (
 ConBind(I(OP_optleft,BAR_conbind_optright),
				  OP_opt, vid', OF_ty_opt, BAR_conbind_opt) 
) end
)
 in (LrTable.NT 50,(result,OP_opt1left,BAR_conbind_opt1right),rest671)
 end
| (132,(_,(MlyValue.conbind conbind1,_,conbind1right))::(_,(_,BAR1left
,_))::rest671) => let val result=MlyValue.BAR_conbind_opt(fn _ => let 
val conbind as conbind1=conbind1 ()
 in ( SOME conbind ) end
)
 in (LrTable.NT 51,(result,BAR1left,conbind1right),rest671) end
| (133,rest671) => let val result=MlyValue.BAR_conbind_opt(fn _ => (
 NONE ))
 in (LrTable.NT 51,(result,defaultPos,defaultPos),rest671) end
| (134,(_,(MlyValue.ty ty1,_,ty1right))::(_,(_,OF1left,_))::rest671)
 => let val result=MlyValue.OF_ty_opt(fn _ => let val ty as ty1=ty1 ()
 in ( SOME ty ) end
)
 in (LrTable.NT 52,(result,OF1left,ty1right),rest671) end
| (135,rest671) => let val result=MlyValue.OF_ty_opt(fn _ => ( NONE ))
 in (LrTable.NT 52,(result,defaultPos,defaultPos),rest671) end
| (136,(_,(MlyValue.AND_exbind_opt AND_exbind_opt1,_,
AND_exbind_optright as AND_exbind_opt1right))::(_,(MlyValue.OF_ty_opt 
OF_ty_opt1,_,_))::(_,(MlyValue.vid' vid'1,_,_))::(_,(MlyValue.OP_opt 
OP_opt1,OP_optleft as OP_opt1left,_))::rest671) => let val result=
MlyValue.exbind(fn _ => let val OP_opt as OP_opt1=OP_opt1 ()
val vid' as vid'1=vid'1 ()
val OF_ty_opt as OF_ty_opt1=OF_ty_opt1 ()
val AND_exbind_opt as AND_exbind_opt1=AND_exbind_opt1 ()
 in (
 NEWExBind(I(OP_optleft,AND_exbind_optright),
				    OP_opt, vid', OF_ty_opt, AND_exbind_opt) 
) end
)
 in (LrTable.NT 53,(result,OP_opt1left,AND_exbind_opt1right),rest671)
 end
| (137,(_,(MlyValue.AND_exbind_opt AND_exbind_opt1,_,
AND_exbind_optright as AND_exbind_opt1right))::(_,(MlyValue.longvid 
longvid1,_,_))::(_,(MlyValue.OP_opt OP_opt2,_,_))::_::(_,(
MlyValue.vid' vid'1,_,_))::(_,(MlyValue.OP_opt OP_opt1,OP_opt1left,_))
::rest671) => let val result=MlyValue.exbind(fn _ => let val OP_opt1=
OP_opt1 ()
val vid' as vid'1=vid'1 ()
val OP_opt2=OP_opt2 ()
val longvid as longvid1=longvid1 ()
val AND_exbind_opt as AND_exbind_opt1=AND_exbind_opt1 ()
 in (
 EQUALExBind(I(OP_opt1left,AND_exbind_optright),
				      OP_opt1, vid', OP_opt2, longvid,
				      AND_exbind_opt) 
) end
)
 in (LrTable.NT 53,(result,OP_opt1left,AND_exbind_opt1right),rest671)
 end
| (138,(_,(MlyValue.exbind exbind1,_,exbind1right))::(_,(_,AND1left,_)
)::rest671) => let val result=MlyValue.AND_exbind_opt(fn _ => let val 
exbind as exbind1=exbind1 ()
 in ( SOME exbind ) end
)
 in (LrTable.NT 54,(result,AND1left,exbind1right),rest671) end
| (139,rest671) => let val result=MlyValue.AND_exbind_opt(fn _ => (
 NONE ))
 in (LrTable.NT 54,(result,defaultPos,defaultPos),rest671) end
| (140,(_,(MlyValue.atpat' atpat'1,atpat'1left,atpat'1right))::rest671
) => let val result=MlyValue.atpat(fn _ => let val atpat' as atpat'1=
atpat'1 ()
 in ( atpat' ) end
)
 in (LrTable.NT 55,(result,atpat'1left,atpat'1right),rest671) end
| (141,(_,(MlyValue.longvid' longvid'1,_,longvid'right as 
longvid'1right))::(_,(MlyValue.OP_opt OP_opt1,OP_optleft as 
OP_opt1left,_))::rest671) => let val result=MlyValue.atpat(fn _ => 
let val OP_opt as OP_opt1=OP_opt1 ()
val longvid' as longvid'1=longvid'1 ()
 in ( IDAtPat(I(OP_optleft,longvid'right),
				  OP_opt, longvid') )
 end
)
 in (LrTable.NT 55,(result,OP_opt1left,longvid'1right),rest671) end
| (142,(_,(_,UNDERBARleft as UNDERBAR1left,UNDERBARright as 
UNDERBAR1right))::rest671) => let val result=MlyValue.atpat'(fn _ => (
 WILDCARDAtPat(I(UNDERBARleft,UNDERBARright)) ))
 in (LrTable.NT 56,(result,UNDERBAR1left,UNDERBAR1right),rest671) end
| (143,(_,(MlyValue.scon scon1,sconleft as scon1left,sconright as 
scon1right))::rest671) => let val result=MlyValue.atpat'(fn _ => let 
val scon as scon1=scon1 ()
 in ( SCONAtPat(I(sconleft,sconright), scon) ) end
)
 in (LrTable.NT 56,(result,scon1left,scon1right),rest671) end
| (144,(_,(_,_,RBRACEright as RBRACE1right))::(_,(MlyValue.patrow_opt 
patrow_opt1,_,_))::(_,(_,LBRACEleft as LBRACE1left,_))::rest671) => 
let val result=MlyValue.atpat'(fn _ => let val patrow_opt as 
patrow_opt1=patrow_opt1 ()
 in ( RECORDAtPat(I(LBRACEleft,RBRACEright), patrow_opt) ) end
)
 in (LrTable.NT 56,(result,LBRACE1left,RBRACE1right),rest671) end
| (145,(_,(_,_,RPARright as RPAR1right))::(_,(_,LPARleft as LPAR1left,
_))::rest671) => let val result=MlyValue.atpat'(fn _ => (
 UNITAtPat(I(LPARleft,RPARright)) ))
 in (LrTable.NT 56,(result,LPAR1left,RPAR1right),rest671) end
| (146,(_,(_,_,RPARright as RPAR1right))::(_,(MlyValue.pat_COMMA_list2
 pat_COMMA_list21,_,_))::(_,(_,LPARleft as LPAR1left,_))::rest671) => 
let val result=MlyValue.atpat'(fn _ => let val pat_COMMA_list2 as 
pat_COMMA_list21=pat_COMMA_list21 ()
 in ( TUPLEAtPat(I(LPARleft,RPARright), pat_COMMA_list2) ) end
)
 in (LrTable.NT 56,(result,LPAR1left,RPAR1right),rest671) end
| (147,(_,(_,_,RBRACKright as RBRACK1right))::(_,(
MlyValue.pat_COMMA_list0 pat_COMMA_list01,_,_))::(_,(_,LBRACKleft as 
LBRACK1left,_))::rest671) => let val result=MlyValue.atpat'(fn _ => 
let val pat_COMMA_list0 as pat_COMMA_list01=pat_COMMA_list01 ()
 in ( LISTAtPat(I(LBRACKleft,RBRACKright),
				    pat_COMMA_list0) )
 end
)
 in (LrTable.NT 56,(result,LBRACK1left,RBRACK1right),rest671) end
| (148,(_,(_,_,RPARright as RPAR1right))::(_,(MlyValue.pat pat1,_,_))
::(_,(_,LPARleft as LPAR1left,_))::rest671) => let val result=
MlyValue.atpat'(fn _ => let val pat as pat1=pat1 ()
 in ( PARAtPat(I(LPARleft,RPARright), pat) ) end
)
 in (LrTable.NT 56,(result,LPAR1left,RPAR1right),rest671) end
| (149,(_,(MlyValue.pat_COMMA_list1 pat_COMMA_list11,
pat_COMMA_list11left,pat_COMMA_list11right))::rest671) => let val 
result=MlyValue.pat_COMMA_list0(fn _ => let val pat_COMMA_list1 as 
pat_COMMA_list11=pat_COMMA_list11 ()
 in ( pat_COMMA_list1 ) end
)
 in (LrTable.NT 57,(result,pat_COMMA_list11left,pat_COMMA_list11right)
,rest671) end
| (150,rest671) => let val result=MlyValue.pat_COMMA_list0(fn _ => (
 [] ))
 in (LrTable.NT 57,(result,defaultPos,defaultPos),rest671) end
| (151,(_,(MlyValue.pat_COMMA_list1 pat_COMMA_list11,_,
pat_COMMA_list11right))::_::(_,(MlyValue.pat pat1,pat1left,_))::
rest671) => let val result=MlyValue.pat_COMMA_list1(fn _ => let val 
pat as pat1=pat1 ()
val pat_COMMA_list1 as pat_COMMA_list11=pat_COMMA_list11 ()
 in ( pat::pat_COMMA_list1 ) end
)
 in (LrTable.NT 58,(result,pat1left,pat_COMMA_list11right),rest671)
 end
| (152,(_,(MlyValue.pat pat1,pat1left,pat1right))::rest671) => let 
val result=MlyValue.pat_COMMA_list1(fn _ => let val pat as pat1=pat1 
()
 in ( pat::[] ) end
)
 in (LrTable.NT 58,(result,pat1left,pat1right),rest671) end
| (153,(_,(MlyValue.pat_COMMA_list1 pat_COMMA_list11,_,
pat_COMMA_list11right))::_::(_,(MlyValue.pat pat1,pat1left,_))::
rest671) => let val result=MlyValue.pat_COMMA_list2(fn _ => let val 
pat as pat1=pat1 ()
val pat_COMMA_list1 as pat_COMMA_list11=pat_COMMA_list11 ()
 in ( pat::pat_COMMA_list1 ) end
)
 in (LrTable.NT 59,(result,pat1left,pat_COMMA_list11right),rest671)
 end
| (154,(_,(_,DOTSleft as DOTS1left,DOTSright as DOTS1right))::rest671)
 => let val result=MlyValue.patrow(fn _ => (
 DOTSPatRow(I(DOTSleft,DOTSright)) ))
 in (LrTable.NT 60,(result,DOTS1left,DOTS1right),rest671) end
| (155,(_,(MlyValue.COMMA_patrow_opt COMMA_patrow_opt1,_,
COMMA_patrow_optright as COMMA_patrow_opt1right))::(_,(MlyValue.pat 
pat1,_,_))::_::(_,(MlyValue.lab lab1,lableft as lab1left,_))::rest671)
 => let val result=MlyValue.patrow(fn _ => let val lab as lab1=lab1 ()
val pat as pat1=pat1 ()
val COMMA_patrow_opt as COMMA_patrow_opt1=COMMA_patrow_opt1 ()
 in (
 FIELDPatRow(I(lableft,COMMA_patrow_optright),
				      lab, pat, COMMA_patrow_opt) 
) end
)
 in (LrTable.NT 60,(result,lab1left,COMMA_patrow_opt1right),rest671)
 end
| (156,(_,(MlyValue.COMMA_patrow_opt COMMA_patrow_opt1,_,
COMMA_patrow_optright as COMMA_patrow_opt1right))::(_,(
MlyValue.AS_pat_opt AS_pat_opt1,_,_))::(_,(MlyValue.COLON_ty_opt 
COLON_ty_opt1,_,_))::(_,(MlyValue.vid' vid'1,vid'left as vid'1left,_))
::rest671) => let val result=MlyValue.patrow(fn _ => let val vid' as 
vid'1=vid'1 ()
val COLON_ty_opt as COLON_ty_opt1=COLON_ty_opt1 ()
val AS_pat_opt as AS_pat_opt1=AS_pat_opt1 ()
val COMMA_patrow_opt as COMMA_patrow_opt1=COMMA_patrow_opt1 ()
 in (
 IDPatRow(I(vid'left,COMMA_patrow_optright),
				   vid', COLON_ty_opt, AS_pat_opt,
				   COMMA_patrow_opt) 
) end
)
 in (LrTable.NT 60,(result,vid'1left,COMMA_patrow_opt1right),rest671)
 end
| (157,(_,(MlyValue.patrow patrow1,_,patrow1right))::(_,(_,COMMA1left,
_))::rest671) => let val result=MlyValue.COMMA_patrow_opt(fn _ => let 
val patrow as patrow1=patrow1 ()
 in ( SOME patrow ) end
)
 in (LrTable.NT 62,(result,COMMA1left,patrow1right),rest671) end
| (158,rest671) => let val result=MlyValue.COMMA_patrow_opt(fn _ => (
 NONE ))
 in (LrTable.NT 62,(result,defaultPos,defaultPos),rest671) end
| (159,(_,(MlyValue.ty ty1,_,ty1right))::(_,(_,COLON1left,_))::rest671
) => let val result=MlyValue.COLON_ty_opt(fn _ => let val ty as ty1=
ty1 ()
 in ( SOME ty ) end
)
 in (LrTable.NT 63,(result,COLON1left,ty1right),rest671) end
| (160,rest671) => let val result=MlyValue.COLON_ty_opt(fn _ => (
 NONE ))
 in (LrTable.NT 63,(result,defaultPos,defaultPos),rest671) end
| (161,(_,(MlyValue.pat pat1,_,pat1right))::(_,(_,AS1left,_))::rest671
) => let val result=MlyValue.AS_pat_opt(fn _ => let val pat as pat1=
pat1 ()
 in ( SOME pat ) end
)
 in (LrTable.NT 64,(result,AS1left,pat1right),rest671) end
| (162,rest671) => let val result=MlyValue.AS_pat_opt(fn _ => ( NONE )
)
 in (LrTable.NT 64,(result,defaultPos,defaultPos),rest671) end
| (163,(_,(MlyValue.patrow patrow1,patrow1left,patrow1right))::rest671
) => let val result=MlyValue.patrow_opt(fn _ => let val patrow as 
patrow1=patrow1 ()
 in ( SOME patrow ) end
)
 in (LrTable.NT 61,(result,patrow1left,patrow1right),rest671) end
| (164,rest671) => let val result=MlyValue.patrow_opt(fn _ => ( NONE )
)
 in (LrTable.NT 61,(result,defaultPos,defaultPos),rest671) end
| (165,(_,(MlyValue.atpat atpat1,atpat1left,atpat1right))::rest671)
 => let val result=MlyValue.pat(fn _ => let val atpat as atpat1=atpat1
 ()
 in ( Infix.parsePat(!J, [atpat]) ) end
)
 in (LrTable.NT 65,(result,atpat1left,atpat1right),rest671) end
| (166,(_,(MlyValue.atpat_list2 atpat_list21,atpat_list21left,
atpat_list21right))::rest671) => let val result=MlyValue.pat(fn _ => 
let val atpat_list2 as atpat_list21=atpat_list21 ()
 in ( Infix.parsePat(!J, atpat_list2) ) end
)
 in (LrTable.NT 65,(result,atpat_list21left,atpat_list21right),rest671
) end
| (167,(_,(MlyValue.COLON_ty_list1 COLON_ty_list11,_,
COLON_ty_list11right))::(_,(MlyValue.atpat' atpat'1,atpat'1left,_))::
rest671) => let val result=MlyValue.pat(fn _ => let val atpat' as 
atpat'1=atpat'1 ()
val COLON_ty_list1 as COLON_ty_list11=COLON_ty_list11 ()
 in (
 let val pat = Infix.parsePat(!J, [atpat'])
			  in typedPat(pat, COLON_ty_list1) end 
) end
)
 in (LrTable.NT 65,(result,atpat'1left,COLON_ty_list11right),rest671)
 end
| (168,(_,(MlyValue.COLON_ty_list1 COLON_ty_list11,_,
COLON_ty_list11right))::(_,(MlyValue.atpat_list2 atpat_list21,
atpat_list21left,_))::rest671) => let val result=MlyValue.pat(fn _ => 
let val atpat_list2 as atpat_list21=atpat_list21 ()
val COLON_ty_list1 as COLON_ty_list11=COLON_ty_list11 ()
 in (
 let val pat = Infix.parsePat(!J, atpat_list2)
			  in typedPat(pat, COLON_ty_list1) end 
) end
)
 in (LrTable.NT 65,(result,atpat_list21left,COLON_ty_list11right),
rest671) end
| (169,(_,(MlyValue.COLON_ty_list1 COLON_ty_list11,_,
COLON_ty_list11right))::(_,(MlyValue.vid' vid'1,_,vid'right))::(_,(
MlyValue.OP_opt OP_opt1,OP_optleft as OP_opt1left,_))::rest671) => 
let val result=MlyValue.pat(fn _ => let val OP_opt as OP_opt1=OP_opt1 
()
val vid' as vid'1=vid'1 ()
val COLON_ty_list1 as COLON_ty_list11=COLON_ty_list11 ()
 in (
 let val atpat = IDAtPat(I(OP_optleft,vid'right),
						  OP_opt, LongVId.fromId vid')
			      val pat   = Infix.parsePat(!J, [atpat])
			  in typedPat(pat, COLON_ty_list1) end 
) end
)
 in (LrTable.NT 65,(result,OP_opt1left,COLON_ty_list11right),rest671)
 end
| (170,(_,(MlyValue.COLON_ty_list1 COLON_ty_list11,_,
COLON_ty_list11right))::(_,(MlyValue.LONGID LONGID1,_,LONGIDright))::(
_,(MlyValue.OP_opt OP_opt1,OP_optleft as OP_opt1left,_))::rest671) => 
let val result=MlyValue.pat(fn _ => let val OP_opt as OP_opt1=OP_opt1 
()
val LONGID as LONGID1=LONGID1 ()
val COLON_ty_list1 as COLON_ty_list11=COLON_ty_list11 ()
 in (
 let val longvid = LongVId.implode
						(toLongId VId.fromString LONGID)
			      val atpat = IDAtPat(I(OP_optleft,LONGIDright),
						  OP_opt, longvid)
			      val pat   = Infix.parsePat(!J, [atpat])
			  in typedPat(pat, COLON_ty_list1) end 
) end
)
 in (LrTable.NT 65,(result,OP_opt1left,COLON_ty_list11right),rest671)
 end
| (171,(_,(MlyValue.pat pat1,_,patright as pat1right))::_::(_,(
MlyValue.COLON_ty_opt COLON_ty_opt1,_,_))::(_,(MlyValue.vid' vid'1,_,
vid'right))::(_,(MlyValue.OP_opt OP_opt1,OP_optleft as OP_opt1left,_))
::rest671) => let val result=MlyValue.pat(fn _ => let val OP_opt as 
OP_opt1=OP_opt1 ()
val vid' as vid'1=vid'1 ()
val COLON_ty_opt as COLON_ty_opt1=COLON_ty_opt1 ()
val pat as pat1=pat1 ()
 in (
 Infix.parsePat(!J,
				 [ IDAtPat(I(OP_optleft,vid'right), OP_opt,
					   LongVId.implode([],vid')) ] ) ;
			  ASPat(I(OP_optleft,patright),
				OP_opt, vid', COLON_ty_opt, pat) 
) end
)
 in (LrTable.NT 65,(result,OP_opt1left,pat1right),rest671) end
| (172,(_,(MlyValue.atpat_list1 atpat_list11,_,atpat_list11right))::(_
,(MlyValue.atpat atpat1,atpat1left,_))::rest671) => let val result=
MlyValue.atpat_list1(fn _ => let val atpat as atpat1=atpat1 ()
val atpat_list1 as atpat_list11=atpat_list11 ()
 in ( atpat::atpat_list1 ) end
)
 in (LrTable.NT 66,(result,atpat1left,atpat_list11right),rest671) end
| (173,(_,(MlyValue.atpat atpat1,atpat1left,atpat1right))::rest671)
 => let val result=MlyValue.atpat_list1(fn _ => let val atpat as 
atpat1=atpat1 ()
 in ( atpat::[] ) end
)
 in (LrTable.NT 66,(result,atpat1left,atpat1right),rest671) end
| (174,(_,(MlyValue.atpat_list1 atpat_list11,_,atpat_list11right))::(_
,(MlyValue.atpat atpat1,atpat1left,_))::rest671) => let val result=
MlyValue.atpat_list2(fn _ => let val atpat as atpat1=atpat1 ()
val atpat_list1 as atpat_list11=atpat_list11 ()
 in ( atpat::atpat_list1 ) end
)
 in (LrTable.NT 67,(result,atpat1left,atpat_list11right),rest671) end
| (175,(_,(MlyValue.COLON_ty_list1 COLON_ty_list11,_,
COLON_ty_list11right))::(_,(MlyValue.ty ty1,_,_))::(_,(_,COLON1left,_)
)::rest671) => let val result=MlyValue.COLON_ty_list1(fn _ => let val 
ty as ty1=ty1 ()
val COLON_ty_list1 as COLON_ty_list11=COLON_ty_list11 ()
 in ( ty::COLON_ty_list1 ) end
)
 in (LrTable.NT 68,(result,COLON1left,COLON_ty_list11right),rest671)
 end
| (176,(_,(MlyValue.ty ty1,_,ty1right))::(_,(_,COLON1left,_))::rest671
) => let val result=MlyValue.COLON_ty_list1(fn _ => let val ty as ty1=
ty1 ()
 in ( ty::[] ) end
)
 in (LrTable.NT 68,(result,COLON1left,ty1right),rest671) end
| (177,(_,(MlyValue.tupty tupty1,tupty1left,tupty1right))::rest671)
 => let val result=MlyValue.ty(fn _ => let val tupty as tupty1=tupty1 
()
 in ( tupty ) end
)
 in (LrTable.NT 69,(result,tupty1left,tupty1right),rest671) end
| (178,(_,(MlyValue.ty ty1,_,tyright as ty1right))::_::(_,(
MlyValue.tupty tupty1,tuptyleft as tupty1left,_))::rest671) => let 
val result=MlyValue.ty(fn _ => let val tupty as tupty1=tupty1 ()
val ty as ty1=ty1 ()
 in ( ARROWTy(I(tuptyleft,tyright), tupty, ty) ) end
)
 in (LrTable.NT 69,(result,tupty1left,ty1right),rest671) end
| (179,(_,(MlyValue.ty_STAR_list ty_STAR_list1,ty_STAR_listleft as 
ty_STAR_list1left,ty_STAR_listright as ty_STAR_list1right))::rest671)
 => let val result=MlyValue.tupty(fn _ => let val ty_STAR_list as 
ty_STAR_list1=ty_STAR_list1 ()
 in (
 TUPLETy(I(ty_STAR_listleft,ty_STAR_listright),
				   ty_STAR_list) )
 end
)
 in (LrTable.NT 70,(result,ty_STAR_list1left,ty_STAR_list1right),
rest671) end
| (180,(_,(MlyValue.ty_STAR_list ty_STAR_list1,_,ty_STAR_list1right))
::_::(_,(MlyValue.consty consty1,consty1left,_))::rest671) => let val 
result=MlyValue.ty_STAR_list(fn _ => let val consty as consty1=consty1
 ()
val ty_STAR_list as ty_STAR_list1=ty_STAR_list1 ()
 in ( consty::ty_STAR_list ) end
)
 in (LrTable.NT 71,(result,consty1left,ty_STAR_list1right),rest671)
 end
| (181,(_,(MlyValue.consty consty1,consty1left,consty1right))::rest671
) => let val result=MlyValue.ty_STAR_list(fn _ => let val consty as 
consty1=consty1 ()
 in ( consty::[] ) end
)
 in (LrTable.NT 71,(result,consty1left,consty1right),rest671) end
| (182,(_,(MlyValue.atty atty1,atty1left,atty1right))::rest671) => 
let val result=MlyValue.consty(fn _ => let val atty as atty1=atty1 ()
 in ( atty ) end
)
 in (LrTable.NT 72,(result,atty1left,atty1right),rest671) end
| (183,(_,(MlyValue.longtycon longtycon1,_,longtyconright as 
longtycon1right))::(_,(MlyValue.tyseq tyseq1,tyseqleft as tyseq1left,_
))::rest671) => let val result=MlyValue.consty(fn _ => let val tyseq
 as tyseq1=tyseq1 ()
val longtycon as longtycon1=longtycon1 ()
 in ( CONTy(I(tyseqleft,longtyconright),
					tyseq, longtycon) ) end
)
 in (LrTable.NT 72,(result,tyseq1left,longtycon1right),rest671) end
| (184,(_,(MlyValue.tyvar tyvar1,tyvarleft as tyvar1left,tyvarright
 as tyvar1right))::rest671) => let val result=MlyValue.atty(fn _ => 
let val tyvar as tyvar1=tyvar1 ()
 in ( VARTy(I(tyvarleft,tyvarright), tyvar) ) end
)
 in (LrTable.NT 73,(result,tyvar1left,tyvar1right),rest671) end
| (185,(_,(_,_,RBRACEright as RBRACE1right))::(_,(MlyValue.tyrow_opt 
tyrow_opt1,_,_))::(_,(_,LBRACEleft as LBRACE1left,_))::rest671) => 
let val result=MlyValue.atty(fn _ => let val tyrow_opt as tyrow_opt1=
tyrow_opt1 ()
 in ( RECORDTy(I(LBRACEleft,RBRACEright), tyrow_opt) ) end
)
 in (LrTable.NT 73,(result,LBRACE1left,RBRACE1right),rest671) end
| (186,(_,(_,_,RPARright as RPAR1right))::(_,(MlyValue.ty ty1,_,_))::(
_,(_,LPARleft as LPAR1left,_))::rest671) => let val result=
MlyValue.atty(fn _ => let val ty as ty1=ty1 ()
 in ( PARTy(I(LPARleft,RPARright), ty) ) end
)
 in (LrTable.NT 73,(result,LPAR1left,RPAR1right),rest671) end
| (187,(_,(MlyValue.COMMA_tyrow_opt COMMA_tyrow_opt1,_,
COMMA_tyrow_optright as COMMA_tyrow_opt1right))::(_,(MlyValue.ty ty1,_
,_))::_::(_,(MlyValue.lab lab1,lableft as lab1left,_))::rest671) => 
let val result=MlyValue.tyrow(fn _ => let val lab as lab1=lab1 ()
val ty as ty1=ty1 ()
val COMMA_tyrow_opt as COMMA_tyrow_opt1=COMMA_tyrow_opt1 ()
 in (
 TyRow(I(lableft,COMMA_tyrow_optright),
				lab, ty, COMMA_tyrow_opt) 
) end
)
 in (LrTable.NT 74,(result,lab1left,COMMA_tyrow_opt1right),rest671)
 end
| (188,(_,(MlyValue.tyrow tyrow1,_,tyrow1right))::(_,(_,COMMA1left,_))
::rest671) => let val result=MlyValue.COMMA_tyrow_opt(fn _ => let val 
tyrow as tyrow1=tyrow1 ()
 in ( SOME tyrow ) end
)
 in (LrTable.NT 76,(result,COMMA1left,tyrow1right),rest671) end
| (189,rest671) => let val result=MlyValue.COMMA_tyrow_opt(fn _ => (
 NONE ))
 in (LrTable.NT 76,(result,defaultPos,defaultPos),rest671) end
| (190,(_,(MlyValue.tyrow tyrow1,tyrow1left,tyrow1right))::rest671)
 => let val result=MlyValue.tyrow_opt(fn _ => let val tyrow as tyrow1=
tyrow1 ()
 in ( SOME tyrow ) end
)
 in (LrTable.NT 75,(result,tyrow1left,tyrow1right),rest671) end
| (191,rest671) => let val result=MlyValue.tyrow_opt(fn _ => ( NONE ))
 in (LrTable.NT 75,(result,defaultPos,defaultPos),rest671) end
| (192,(_,(MlyValue.consty consty1,constyleft as consty1left,
constyright as consty1right))::rest671) => let val result=
MlyValue.tyseq(fn _ => let val consty as consty1=consty1 ()
 in ( Tyseq(I(constyleft,constyright),
						[consty]) ) end
)
 in (LrTable.NT 77,(result,consty1left,consty1right),rest671) end
| (193,rest671) => let val result=MlyValue.tyseq(fn _ => (
 Tyseq(I(defaultPos,defaultPos), []) ))
 in (LrTable.NT 77,(result,defaultPos,defaultPos),rest671) end
| (194,(_,(_,_,RPARright as RPAR1right))::(_,(MlyValue.ty_COMMA_list2 
ty_COMMA_list21,_,_))::(_,(_,LPARleft as LPAR1left,_))::rest671) => 
let val result=MlyValue.tyseq(fn _ => let val ty_COMMA_list2 as 
ty_COMMA_list21=ty_COMMA_list21 ()
 in ( Tyseq(I(LPARleft,RPARright),
						ty_COMMA_list2) ) end
)
 in (LrTable.NT 77,(result,LPAR1left,RPAR1right),rest671) end
| (195,(_,(MlyValue.ty_COMMA_list2 ty_COMMA_list21,_,
ty_COMMA_list21right))::_::(_,(MlyValue.ty ty1,ty1left,_))::rest671)
 => let val result=MlyValue.ty_COMMA_list2(fn _ => let val ty as ty1=
ty1 ()
val ty_COMMA_list2 as ty_COMMA_list21=ty_COMMA_list21 ()
 in ( ty::ty_COMMA_list2 ) end
)
 in (LrTable.NT 78,(result,ty1left,ty_COMMA_list21right),rest671) end
| (196,(_,(MlyValue.ty ty2,_,ty2right))::_::(_,(MlyValue.ty ty1,
ty1left,_))::rest671) => let val result=MlyValue.ty_COMMA_list2(fn _
 => let val ty1=ty1 ()
val ty2=ty2 ()
 in ( [ty1, ty2] ) end
)
 in (LrTable.NT 78,(result,ty1left,ty2right),rest671) end
| (197,(_,(MlyValue.tyvarseq1 tyvarseq11,tyvarseq11left,
tyvarseq11right))::rest671) => let val result=MlyValue.tyvarseq(fn _
 => let val tyvarseq1 as tyvarseq11=tyvarseq11 ()
 in ( tyvarseq1 ) end
)
 in (LrTable.NT 79,(result,tyvarseq11left,tyvarseq11right),rest671)
 end
| (198,rest671) => let val result=MlyValue.tyvarseq(fn _ => (
 TyVarseq(I(defaultPos,defaultPos),
						   []) ))
 in (LrTable.NT 79,(result,defaultPos,defaultPos),rest671) end
| (199,(_,(MlyValue.tyvar tyvar1,tyvarleft as tyvar1left,tyvarright
 as tyvar1right))::rest671) => let val result=MlyValue.tyvarseq1(fn _
 => let val tyvar as tyvar1=tyvar1 ()
 in ( TyVarseq(I(tyvarleft,tyvarright),
						   [tyvar]) ) end
)
 in (LrTable.NT 80,(result,tyvar1left,tyvar1right),rest671) end
| (200,(_,(_,_,RPARright as RPAR1right))::(_,(
MlyValue.tyvar_COMMA_list1 tyvar_COMMA_list11,_,_))::(_,(_,LPARleft
 as LPAR1left,_))::rest671) => let val result=MlyValue.tyvarseq1(fn _
 => let val tyvar_COMMA_list1 as tyvar_COMMA_list11=tyvar_COMMA_list11
 ()
 in ( TyVarseq(I(LPARleft,RPARright),
						   tyvar_COMMA_list1) )
 end
)
 in (LrTable.NT 80,(result,LPAR1left,RPAR1right),rest671) end
| (201,(_,(MlyValue.tyvar_COMMA_list1 tyvar_COMMA_list11,_,
tyvar_COMMA_list11right))::_::(_,(MlyValue.tyvar tyvar1,tyvar1left,_))
::rest671) => let val result=MlyValue.tyvar_COMMA_list1(fn _ => let 
val tyvar as tyvar1=tyvar1 ()
val tyvar_COMMA_list1 as tyvar_COMMA_list11=tyvar_COMMA_list11 ()
 in ( tyvar::tyvar_COMMA_list1 ) end
)
 in (LrTable.NT 81,(result,tyvar1left,tyvar_COMMA_list11right),rest671
) end
| (202,(_,(MlyValue.tyvar tyvar1,tyvar1left,tyvar1right))::rest671)
 => let val result=MlyValue.tyvar_COMMA_list1(fn _ => let val tyvar
 as tyvar1=tyvar1 ()
 in ( tyvar::[] ) end
)
 in (LrTable.NT 81,(result,tyvar1left,tyvar1right),rest671) end
| (203,(_,(MlyValue.strexp' strexp'1,strexp'1left,strexp'1right))::
rest671) => let val result=MlyValue.strexp(fn _ => let val strexp' as 
strexp'1=strexp'1 ()
 in ( strexp' ) end
)
 in (LrTable.NT 82,(result,strexp'1left,strexp'1right),rest671) end
| (204,(_,(MlyValue.sigexp sigexp1,_,sigexpright as sigexp1right))::_
::(_,(MlyValue.strexp strexp1,strexpleft as strexp1left,_))::rest671)
 => let val result=MlyValue.strexp(fn _ => let val strexp as strexp1=
strexp1 ()
val sigexp as sigexp1=sigexp1 ()
 in (
 COLONStrExp(I(strexpleft,sigexpright),
				      strexp, sigexp) )
 end
)
 in (LrTable.NT 82,(result,strexp1left,sigexp1right),rest671) end
| (205,(_,(MlyValue.sigexp sigexp1,_,sigexpright as sigexp1right))::_
::(_,(MlyValue.strexp strexp1,strexpleft as strexp1left,_))::rest671)
 => let val result=MlyValue.strexp(fn _ => let val strexp as strexp1=
strexp1 ()
val sigexp as sigexp1=sigexp1 ()
 in ( SEALStrExp(I(strexpleft,sigexpright), strexp, sigexp)) end
)
 in (LrTable.NT 82,(result,strexp1left,sigexp1right),rest671) end
| (206,(_,(_,_,ENDright as END1right))::(_,(MlyValue.popInfix 
popInfix1,_,_))::(_,(MlyValue.strdec strdec1,_,_))::(_,(
MlyValue.pushInfix pushInfix1,_,_))::(_,(_,STRUCTleft as STRUCT1left,_
))::rest671) => let val result=MlyValue.strexp'(fn _ => let val 
pushInfix1=pushInfix1 ()
val strdec as strdec1=strdec1 ()
val popInfix1=popInfix1 ()
 in ( STRUCTStrExp(I(STRUCTleft,ENDright), strdec) ) end
)
 in (LrTable.NT 83,(result,STRUCT1left,END1right),rest671) end
| (207,(_,(MlyValue.longstrid longstrid1,longstridleft as 
longstrid1left,longstridright as longstrid1right))::rest671) => let 
val result=MlyValue.strexp'(fn _ => let val longstrid as longstrid1=
longstrid1 ()
 in ( IDStrExp(I(longstridleft,longstridright), longstrid) ) end
)
 in (LrTable.NT 83,(result,longstrid1left,longstrid1right),rest671)
 end
| (208,(_,(_,_,RPARright as RPAR1right))::(_,(MlyValue.strexp strexp1,
_,_))::_::(_,(MlyValue.funid funid1,funidleft as funid1left,_))::
rest671) => let val result=MlyValue.strexp'(fn _ => let val funid as 
funid1=funid1 ()
val strexp as strexp1=strexp1 ()
 in ( APPStrExp(I(funidleft,RPARright), funid, strexp) ) end
)
 in (LrTable.NT 83,(result,funid1left,RPAR1right),rest671) end
| (209,(_,(_,_,RPARright as RPAR1right))::(_,(MlyValue.strdec strdec1,
_,_))::_::(_,(MlyValue.funid funid1,funidleft as funid1left,_))::
rest671) => let val result=MlyValue.strexp'(fn _ => let val funid as 
funid1=funid1 ()
val strdec as strdec1=strdec1 ()
 in ( APPDECStrExp(I(funidleft,RPARright), funid, strdec) ) end
)
 in (LrTable.NT 83,(result,funid1left,RPAR1right),rest671) end
| (210,(_,(_,_,ENDright as END1right))::(_,(MlyValue.popInfix 
popInfix1,_,_))::(_,(MlyValue.strexp strexp1,_,_))::_::(_,(
MlyValue.strdec strdec1,_,_))::(_,(MlyValue.pushInfix pushInfix1,_,_))
::(_,(_,LETleft as LET1left,_))::rest671) => let val result=
MlyValue.strexp'(fn _ => let val pushInfix1=pushInfix1 ()
val strdec as strdec1=strdec1 ()
val strexp as strexp1=strexp1 ()
val popInfix1=popInfix1 ()
 in ( LETStrExp(I(LETleft,ENDright), strdec, strexp) ) end
)
 in (LrTable.NT 83,(result,LET1left,END1right),rest671) end
| (211,(_,(MlyValue.strdec1 strdec11,strdec11left,strdec11right))::
rest671) => let val result=MlyValue.strdec(fn _ => let val strdec1 as 
strdec11=strdec11 ()
 in ( strdec1 ) end
)
 in (LrTable.NT 84,(result,strdec11left,strdec11right),rest671) end
| (212,rest671) => let val result=MlyValue.strdec(fn _ => (
 EMPTYStrDec(I(defaultPos,defaultPos)) ))
 in (LrTable.NT 84,(result,defaultPos,defaultPos),rest671) end
| (213,(_,(MlyValue.strdec1' strdec1'1,strdec1'1left,strdec1'1right))
::rest671) => let val result=MlyValue.strdec1(fn _ => let val strdec1'
 as strdec1'1=strdec1'1 ()
 in ( strdec1' ) end
)
 in (LrTable.NT 85,(result,strdec1'1left,strdec1'1right),rest671) end
| (214,(_,(MlyValue.strdec1 strdec12,_,strdec12right))::(_,(
MlyValue.strdec1 strdec11,strdec11left,_))::rest671) => let val result
=MlyValue.strdec1(fn _ => let val strdec11=strdec11 ()
val strdec12=strdec12 ()
 in (
 SEQStrDec(I(strdec11left,strdec12right),
				    strdec11, strdec12) 
) end
)
 in (LrTable.NT 85,(result,strdec11left,strdec12right),rest671) end
| (215,(_,(_,SEMICOLONleft as SEMICOLON1left,SEMICOLONright as 
SEMICOLON1right))::rest671) => let val result=MlyValue.strdec1(fn _
 => (
 SEQStrDec(I(SEMICOLONleft,SEMICOLONright),
				 EMPTYStrDec(I(SEMICOLONleft,SEMICOLONleft)),
				 EMPTYStrDec(I(SEMICOLONright,SEMICOLONright))) 
))
 in (LrTable.NT 85,(result,SEMICOLON1left,SEMICOLON1right),rest671)
 end
| (216,(_,(MlyValue.dec1' dec1'1,dec1'left as dec1'1left,dec1'right
 as dec1'1right))::rest671) => let val result=MlyValue.strdec1'(fn _
 => let val dec1' as dec1'1=dec1'1 ()
 in ( DECStrDec(I(dec1'left,dec1'right), dec1') ) end
)
 in (LrTable.NT 86,(result,dec1'1left,dec1'1right),rest671) end
| (217,(_,(MlyValue.strbind strbind1,_,strbindright as strbind1right))
::(_,(_,STRUCTUREleft as STRUCTURE1left,_))::rest671) => let val 
result=MlyValue.strdec1'(fn _ => let val strbind as strbind1=strbind1 
()
 in ( STRUCTUREStrDec(I(STRUCTUREleft,strbindright),
					  strbind) )
 end
)
 in (LrTable.NT 86,(result,STRUCTURE1left,strbind1right),rest671) end
| (218,(_,(_,_,ENDright as END1right))::(_,(MlyValue.popLocalInfix 
popLocalInfix1,_,_))::(_,(MlyValue.strdec strdec2,_,_))::(_,(
MlyValue.pushLocalInfix pushLocalInfix1,_,_))::_::(_,(MlyValue.strdec 
strdec1,_,_))::(_,(MlyValue.pushInfix pushInfix1,_,_))::(_,(_,
LOCALleft as LOCAL1left,_))::rest671) => let val result=
MlyValue.strdec1'(fn _ => let val pushInfix1=pushInfix1 ()
val strdec1=strdec1 ()
val pushLocalInfix1=pushLocalInfix1 ()
val strdec2=strdec2 ()
val popLocalInfix1=popLocalInfix1 ()
 in ( LOCALStrDec(I(LOCALleft,ENDright), strdec1, strdec2) ) end
)
 in (LrTable.NT 86,(result,LOCAL1left,END1right),rest671) end
| (219,(_,(MlyValue.strexp__AND_strbind_opt strexp__AND_strbind_opt1,_
,strexp__AND_strbind_optright as strexp__AND_strbind_opt1right))::_::(
_,(MlyValue.COLON_sigexp_opt COLON_sigexp_opt1,_,_))::(_,(
MlyValue.strid strid1,stridleft as strid1left,_))::rest671) => let 
val result=MlyValue.strbind(fn _ => let val strid as strid1=strid1 ()
val COLON_sigexp_opt as COLON_sigexp_opt1=COLON_sigexp_opt1 ()
val strexp__AND_strbind_opt as strexp__AND_strbind_opt1=
strexp__AND_strbind_opt1 ()
 in (
 TRANSStrBind(I(stridleft,
					 strexp__AND_strbind_optright),
				       strid, COLON_sigexp_opt,
				       #1 strexp__AND_strbind_opt,
				       #2 strexp__AND_strbind_opt) 
) end
)
 in (LrTable.NT 87,(result,strid1left,strexp__AND_strbind_opt1right),
rest671) end
| (220,(_,(MlyValue.strexp__AND_strbind_opt strexp__AND_strbind_opt1,_
,strexp__AND_strbind_optright as strexp__AND_strbind_opt1right))::_::(
_,(MlyValue.sigexp sigexp1,_,_))::_::(_,(MlyValue.strid strid1,
stridleft as strid1left,_))::rest671) => let val result=
MlyValue.strbind(fn _ => let val strid as strid1=strid1 ()
val sigexp as sigexp1=sigexp1 ()
val strexp__AND_strbind_opt as strexp__AND_strbind_opt1=
strexp__AND_strbind_opt1 ()
 in (
 SEALStrBind(I(stridleft,strexp__AND_strbind_optright),
				      strid, sigexp, #1 strexp__AND_strbind_opt,
				      #2 strexp__AND_strbind_opt) 
) end
)
 in (LrTable.NT 87,(result,strid1left,strexp__AND_strbind_opt1right),
rest671) end
| (221,(_,(MlyValue.strbind strbind1,_,strbind1right))::(_,(_,AND1left
,_))::rest671) => let val result=MlyValue.AND_strbind_opt(fn _ => let 
val strbind as strbind1=strbind1 ()
 in ( SOME strbind ) end
)
 in (LrTable.NT 88,(result,AND1left,strbind1right),rest671) end
| (222,rest671) => let val result=MlyValue.AND_strbind_opt(fn _ => (
 NONE ))
 in (LrTable.NT 88,(result,defaultPos,defaultPos),rest671) end
| (223,(_,(MlyValue.AND_strbind_opt AND_strbind_opt1,_,
AND_strbind_opt1right))::(_,(MlyValue.strexp' strexp'1,strexp'1left,_)
)::rest671) => let val result=MlyValue.strexp__AND_strbind_opt(fn _
 => let val strexp' as strexp'1=strexp'1 ()
val AND_strbind_opt as AND_strbind_opt1=AND_strbind_opt1 ()
 in ( ( strexp', AND_strbind_opt ) ) end
)
 in (LrTable.NT 89,(result,strexp'1left,AND_strbind_opt1right),rest671
) end
| (224,(_,(MlyValue.sigexp__AND_strbind_opt sigexp__AND_strbind_opt1,_
,sigexp__AND_strbind_optright as sigexp__AND_strbind_opt1right))::_::(
_,(MlyValue.strexp strexp1,strexpleft as strexp1left,_))::rest671) => 
let val result=MlyValue.strexp__AND_strbind_opt(fn _ => let val strexp
 as strexp1=strexp1 ()
val sigexp__AND_strbind_opt as sigexp__AND_strbind_opt1=
sigexp__AND_strbind_opt1 ()
 in (
 ( COLONStrExp(I(strexpleft,
					  sigexp__AND_strbind_optright),
					strexp, #1 sigexp__AND_strbind_opt),
			    #2 sigexp__AND_strbind_opt ) 
) end
)
 in (LrTable.NT 89,(result,strexp1left,sigexp__AND_strbind_opt1right),
rest671) end
| (225,(_,(MlyValue.sigexp__AND_strbind_opt sigexp__AND_strbind_opt1,_
,sigexp__AND_strbind_optright as sigexp__AND_strbind_opt1right))::_::(
_,(MlyValue.strexp strexp1,strexpleft as strexp1left,_))::rest671) => 
let val result=MlyValue.strexp__AND_strbind_opt(fn _ => let val strexp
 as strexp1=strexp1 ()
val sigexp__AND_strbind_opt as sigexp__AND_strbind_opt1=
sigexp__AND_strbind_opt1 ()
 in (
 ( SEALStrExp(I(strexpleft,
					 sigexp__AND_strbind_optright),
				       strexp, #1 sigexp__AND_strbind_opt),
			    #2 sigexp__AND_strbind_opt ) 
) end
)
 in (LrTable.NT 89,(result,strexp1left,sigexp__AND_strbind_opt1right),
rest671) end
| (226,(_,(MlyValue.AND_strbind_opt AND_strbind_opt1,_,
AND_strbind_opt1right))::(_,(MlyValue.sigexp' sigexp'1,sigexp'1left,_)
)::rest671) => let val result=MlyValue.sigexp__AND_strbind_opt(fn _
 => let val sigexp' as sigexp'1=sigexp'1 ()
val AND_strbind_opt as AND_strbind_opt1=AND_strbind_opt1 ()
 in ( ( sigexp', AND_strbind_opt ) ) end
)
 in (LrTable.NT 90,(result,sigexp'1left,AND_strbind_opt1right),rest671
) end
| (227,(_,(MlyValue.tyreadesc__AND_strbind_opt 
tyreadesc__AND_strbind_opt1,_,tyreadesc__AND_strbind_optright as 
tyreadesc__AND_strbind_opt1right))::_::(_,(MlyValue.sigexp sigexp1,
sigexpleft as sigexp1left,_))::rest671) => let val result=
MlyValue.sigexp__AND_strbind_opt(fn _ => let val sigexp as sigexp1=
sigexp1 ()
val tyreadesc__AND_strbind_opt as tyreadesc__AND_strbind_opt1=
tyreadesc__AND_strbind_opt1 ()
 in (
 ( WHERETYPESigExp(I(sigexpleft,
					      tyreadesc__AND_strbind_optright),
					   sigexp,
					   #1 tyreadesc__AND_strbind_opt),
			    #2 tyreadesc__AND_strbind_opt ) 
) end
)
 in (LrTable.NT 90,(result,sigexp1left,
tyreadesc__AND_strbind_opt1right),rest671) end
| (228,(_,(MlyValue.AND_tyreadesc_opt__AND_strbind_opt 
AND_tyreadesc_opt__AND_strbind_opt1,_,
AND_tyreadesc_opt__AND_strbind_optright as 
AND_tyreadesc_opt__AND_strbind_opt1right))::(_,(MlyValue.ty ty1,_,_))
::_::(_,(MlyValue.longtycon longtycon1,_,_))::(_,(MlyValue.tyvarseq 
tyvarseq1,_,_))::(_,(_,TYPEleft as TYPE1left,_))::rest671) => let val 
result=MlyValue.tyreadesc__AND_strbind_opt(fn _ => let val tyvarseq
 as tyvarseq1=tyvarseq1 ()
val longtycon as longtycon1=longtycon1 ()
val ty as ty1=ty1 ()
val AND_tyreadesc_opt__AND_strbind_opt as 
AND_tyreadesc_opt__AND_strbind_opt1=
AND_tyreadesc_opt__AND_strbind_opt1 ()
 in (
 ( TyReaDesc(I(TYPEleft,
				       AND_tyreadesc_opt__AND_strbind_optright),
				      tyvarseq, longtycon, ty,
				      #1 AND_tyreadesc_opt__AND_strbind_opt),
			    #2 AND_tyreadesc_opt__AND_strbind_opt ) 
) end
)
 in (LrTable.NT 91,(result,TYPE1left,
AND_tyreadesc_opt__AND_strbind_opt1right),rest671) end
| (229,(_,(MlyValue.AND_strbind_opt AND_strbind_opt1,
AND_strbind_opt1left,AND_strbind_opt1right))::rest671) => let val 
result=MlyValue.AND_tyreadesc_opt__AND_strbind_opt(fn _ => let val 
AND_strbind_opt as AND_strbind_opt1=AND_strbind_opt1 ()
 in ( ( NONE, AND_strbind_opt ) ) end
)
 in (LrTable.NT 92,(result,AND_strbind_opt1left,AND_strbind_opt1right)
,rest671) end
| (230,(_,(MlyValue.tyreadesc__AND_strbind_opt 
tyreadesc__AND_strbind_opt1,_,tyreadesc__AND_strbind_opt1right))::(_,(
_,AND1left,_))::rest671) => let val result=
MlyValue.AND_tyreadesc_opt__AND_strbind_opt(fn _ => let val 
tyreadesc__AND_strbind_opt as tyreadesc__AND_strbind_opt1=
tyreadesc__AND_strbind_opt1 ()
 in (
 ( SOME(#1 tyreadesc__AND_strbind_opt),
				    #2 tyreadesc__AND_strbind_opt ) 
) end
)
 in (LrTable.NT 92,(result,AND1left,tyreadesc__AND_strbind_opt1right),
rest671) end
| (231,(_,(MlyValue.sigexp sigexp1,_,sigexp1right))::(_,(_,COLON1left,
_))::rest671) => let val result=MlyValue.COLON_sigexp_opt(fn _ => let 
val sigexp as sigexp1=sigexp1 ()
 in ( SOME sigexp ) end
)
 in (LrTable.NT 93,(result,COLON1left,sigexp1right),rest671) end
| (232,rest671) => let val result=MlyValue.COLON_sigexp_opt(fn _ => (
 NONE ))
 in (LrTable.NT 93,(result,defaultPos,defaultPos),rest671) end
| (233,(_,(MlyValue.sigexp' sigexp'1,sigexp'1left,sigexp'1right))::
rest671) => let val result=MlyValue.sigexp(fn _ => let val sigexp' as 
sigexp'1=sigexp'1 ()
 in ( sigexp' ) end
)
 in (LrTable.NT 94,(result,sigexp'1left,sigexp'1right),rest671) end
| (234,(_,(MlyValue.tyreadesc tyreadesc1,_,tyreadescright as 
tyreadesc1right))::_::(_,(MlyValue.sigexp sigexp1,sigexpleft as 
sigexp1left,_))::rest671) => let val result=MlyValue.sigexp(fn _ => 
let val sigexp as sigexp1=sigexp1 ()
val tyreadesc as tyreadesc1=tyreadesc1 ()
 in (
 WHERETYPESigExp(I(sigexpleft,tyreadescright),
					  sigexp, tyreadesc) 
) end
)
 in (LrTable.NT 94,(result,sigexp1left,tyreadesc1right),rest671) end
| (235,(_,(_,_,ENDright as END1right))::(_,(MlyValue.spec spec1,_,_))
::(_,(_,SIGleft as SIG1left,_))::rest671) => let val result=
MlyValue.sigexp'(fn _ => let val spec as spec1=spec1 ()
 in ( SIGSigExp(I(SIGleft,ENDright), spec) ) end
)
 in (LrTable.NT 95,(result,SIG1left,END1right),rest671) end
| (236,(_,(MlyValue.sigid sigid1,sigidleft as sigid1left,sigidright
 as sigid1right))::rest671) => let val result=MlyValue.sigexp'(fn _
 => let val sigid as sigid1=sigid1 ()
 in ( IDSigExp(I(sigidleft,sigidright), sigid) ) end
)
 in (LrTable.NT 95,(result,sigid1left,sigid1right),rest671) end
| (237,(_,(MlyValue.sigbind sigbind1,_,sigbindright as sigbind1right))
::(_,(_,SIGNATUREleft as SIGNATURE1left,_))::rest671) => let val 
result=MlyValue.sigdec(fn _ => let val sigbind as sigbind1=sigbind1 ()
 in ( SigDec(I(SIGNATUREleft,sigbindright), sigbind) ) end
)
 in (LrTable.NT 96,(result,SIGNATURE1left,sigbind1right),rest671) end
| (238,(_,(MlyValue.sigexp__AND_sigbind_opt sigexp__AND_sigbind_opt1,_
,sigexp__AND_sigbind_optright as sigexp__AND_sigbind_opt1right))::_::(
_,(MlyValue.sigid sigid1,sigidleft as sigid1left,_))::rest671) => let 
val result=MlyValue.sigbind(fn _ => let val sigid as sigid1=sigid1 ()
val sigexp__AND_sigbind_opt as sigexp__AND_sigbind_opt1=
sigexp__AND_sigbind_opt1 ()
 in (
 SigBind(I(sigidleft,sigexp__AND_sigbind_optright),
				  sigid, #1 sigexp__AND_sigbind_opt,
				  #2 sigexp__AND_sigbind_opt) 
) end
)
 in (LrTable.NT 97,(result,sigid1left,sigexp__AND_sigbind_opt1right),
rest671) end
| (239,(_,(MlyValue.sigbind sigbind1,_,sigbind1right))::(_,(_,AND1left
,_))::rest671) => let val result=MlyValue.AND_sigbind_opt(fn _ => let 
val sigbind as sigbind1=sigbind1 ()
 in ( SOME sigbind ) end
)
 in (LrTable.NT 98,(result,AND1left,sigbind1right),rest671) end
| (240,rest671) => let val result=MlyValue.AND_sigbind_opt(fn _ => (
 NONE ))
 in (LrTable.NT 98,(result,defaultPos,defaultPos),rest671) end
| (241,(_,(MlyValue.AND_sigbind_opt AND_sigbind_opt1,_,
AND_sigbind_opt1right))::(_,(MlyValue.sigexp' sigexp'1,sigexp'1left,_)
)::rest671) => let val result=MlyValue.sigexp__AND_sigbind_opt(fn _
 => let val sigexp' as sigexp'1=sigexp'1 ()
val AND_sigbind_opt as AND_sigbind_opt1=AND_sigbind_opt1 ()
 in ( ( sigexp', AND_sigbind_opt ) ) end
)
 in (LrTable.NT 99,(result,sigexp'1left,AND_sigbind_opt1right),rest671
) end
| (242,(_,(MlyValue.tyreadesc__AND_sigbind_opt 
tyreadesc__AND_sigbind_opt1,_,tyreadesc__AND_sigbind_optright as 
tyreadesc__AND_sigbind_opt1right))::_::(_,(MlyValue.sigexp sigexp1,
sigexpleft as sigexp1left,_))::rest671) => let val result=
MlyValue.sigexp__AND_sigbind_opt(fn _ => let val sigexp as sigexp1=
sigexp1 ()
val tyreadesc__AND_sigbind_opt as tyreadesc__AND_sigbind_opt1=
tyreadesc__AND_sigbind_opt1 ()
 in (
 ( WHERETYPESigExp(I(sigexpleft,
					      tyreadesc__AND_sigbind_optright),
					   sigexp,
					   #1 tyreadesc__AND_sigbind_opt),
			    #2 tyreadesc__AND_sigbind_opt ) 
) end
)
 in (LrTable.NT 99,(result,sigexp1left,
tyreadesc__AND_sigbind_opt1right),rest671) end
| (243,(_,(MlyValue.AND_tyreadesc_opt__AND_sigbind_opt 
AND_tyreadesc_opt__AND_sigbind_opt1,_,
AND_tyreadesc_opt__AND_sigbind_optright as 
AND_tyreadesc_opt__AND_sigbind_opt1right))::(_,(MlyValue.ty ty1,_,_))
::_::(_,(MlyValue.longtycon longtycon1,_,_))::(_,(MlyValue.tyvarseq 
tyvarseq1,_,_))::(_,(_,TYPEleft as TYPE1left,_))::rest671) => let val 
result=MlyValue.tyreadesc__AND_sigbind_opt(fn _ => let val tyvarseq
 as tyvarseq1=tyvarseq1 ()
val longtycon as longtycon1=longtycon1 ()
val ty as ty1=ty1 ()
val AND_tyreadesc_opt__AND_sigbind_opt as 
AND_tyreadesc_opt__AND_sigbind_opt1=
AND_tyreadesc_opt__AND_sigbind_opt1 ()
 in (
 ( TyReaDesc(I(TYPEleft,
				       AND_tyreadesc_opt__AND_sigbind_optright),
				      tyvarseq, longtycon, ty,
				      #1 AND_tyreadesc_opt__AND_sigbind_opt),
			    #2 AND_tyreadesc_opt__AND_sigbind_opt ) 
) end
)
 in (LrTable.NT 100,(result,TYPE1left,
AND_tyreadesc_opt__AND_sigbind_opt1right),rest671) end
| (244,(_,(MlyValue.AND_sigbind_opt AND_sigbind_opt1,
AND_sigbind_opt1left,AND_sigbind_opt1right))::rest671) => let val 
result=MlyValue.AND_tyreadesc_opt__AND_sigbind_opt(fn _ => let val 
AND_sigbind_opt as AND_sigbind_opt1=AND_sigbind_opt1 ()
 in ( ( NONE, AND_sigbind_opt) ) end
)
 in (LrTable.NT 101,(result,AND_sigbind_opt1left,AND_sigbind_opt1right
),rest671) end
| (245,(_,(MlyValue.tyreadesc__AND_sigbind_opt 
tyreadesc__AND_sigbind_opt1,_,tyreadesc__AND_sigbind_opt1right))::(_,(
_,AND1left,_))::rest671) => let val result=
MlyValue.AND_tyreadesc_opt__AND_sigbind_opt(fn _ => let val 
tyreadesc__AND_sigbind_opt as tyreadesc__AND_sigbind_opt1=
tyreadesc__AND_sigbind_opt1 ()
 in (
 ( SOME(#1 tyreadesc__AND_sigbind_opt),
				    #2 tyreadesc__AND_sigbind_opt ) 
) end
)
 in (LrTable.NT 101,(result,AND1left,tyreadesc__AND_sigbind_opt1right)
,rest671) end
| (246,(_,(MlyValue.AND_tyreadesc_opt AND_tyreadesc_opt1,_,
AND_tyreadesc_optright as AND_tyreadesc_opt1right))::(_,(MlyValue.ty 
ty1,_,_))::_::(_,(MlyValue.longtycon longtycon1,_,_))::(_,(
MlyValue.tyvarseq tyvarseq1,_,_))::(_,(_,TYPEleft as TYPE1left,_))::
rest671) => let val result=MlyValue.tyreadesc(fn _ => let val tyvarseq
 as tyvarseq1=tyvarseq1 ()
val longtycon as longtycon1=longtycon1 ()
val ty as ty1=ty1 ()
val AND_tyreadesc_opt as AND_tyreadesc_opt1=AND_tyreadesc_opt1 ()
 in (
 TyReaDesc(I(TYPEleft,AND_tyreadesc_optright),
				    tyvarseq, longtycon, ty,
				    AND_tyreadesc_opt) 
) end
)
 in (LrTable.NT 102,(result,TYPE1left,AND_tyreadesc_opt1right),rest671
) end
| (247,(_,(MlyValue.tyreadesc tyreadesc1,_,tyreadesc1right))::(_,(_,
AND1left,_))::rest671) => let val result=MlyValue.AND_tyreadesc_opt(
fn _ => let val tyreadesc as tyreadesc1=tyreadesc1 ()
 in ( SOME tyreadesc ) end
)
 in (LrTable.NT 103,(result,AND1left,tyreadesc1right),rest671) end
| (248,rest671) => let val result=MlyValue.AND_tyreadesc_opt(fn _ => (
 NONE ))
 in (LrTable.NT 103,(result,defaultPos,defaultPos),rest671) end
| (249,(_,(MlyValue.spec1 spec11,spec11left,spec11right))::rest671)
 => let val result=MlyValue.spec(fn _ => let val spec1 as spec11=
spec11 ()
 in ( spec1 ) end
)
 in (LrTable.NT 104,(result,spec11left,spec11right),rest671) end
| (250,rest671) => let val result=MlyValue.spec(fn _ => (
 EMPTYSpec(I(defaultPos,defaultPos)) ))
 in (LrTable.NT 104,(result,defaultPos,defaultPos),rest671) end
| (251,(_,(MlyValue.spec1'' spec1''1,spec1''1left,spec1''1right))::
rest671) => let val result=MlyValue.spec1(fn _ => let val spec1'' as 
spec1''1=spec1''1 ()
 in ( spec1'' ) end
)
 in (LrTable.NT 105,(result,spec1''1left,spec1''1right),rest671) end
| (252,(_,(MlyValue.spec1' spec1'1,_,spec1'right as spec1'1right))::(_
,(MlyValue.spec1 spec11,spec1left as spec11left,_))::rest671) => let 
val result=MlyValue.spec1(fn _ => let val spec1 as spec11=spec11 ()
val spec1' as spec1'1=spec1'1 ()
 in ( SEQSpec(I(spec1left,spec1'right), spec1, spec1') ) end
)
 in (LrTable.NT 105,(result,spec11left,spec1'1right),rest671) end
| (253,(_,(_,SEMICOLONleft as SEMICOLON1left,SEMICOLONright as 
SEMICOLON1right))::rest671) => let val result=MlyValue.spec1(fn _ => (
 SEQSpec(I(SEMICOLONleft,SEMICOLONright),
				 EMPTYSpec(I(SEMICOLONleft,SEMICOLONleft)),
				 EMPTYSpec(I(SEMICOLONright,SEMICOLONright))) 
))
 in (LrTable.NT 105,(result,SEMICOLON1left,SEMICOLON1right),rest671)
 end
| (254,(_,(MlyValue.longtycon_EQUALS_list2 longtycon_EQUALS_list21,_,
longtycon_EQUALS_list2right as longtycon_EQUALS_list21right))::_::(_,(
_,SHARINGleft as SHARING1left,_))::rest671) => let val result=
MlyValue.spec1(fn _ => let val longtycon_EQUALS_list2 as 
longtycon_EQUALS_list21=longtycon_EQUALS_list21 ()
 in (
 SHARINGTYPESpec(I(SHARINGleft,
					    longtycon_EQUALS_list2right),
					  EMPTYSpec(I(SHARINGleft,SHARINGleft)),
					  longtycon_EQUALS_list2) 
) end
)
 in (LrTable.NT 105,(result,SHARING1left,longtycon_EQUALS_list21right)
,rest671) end
| (255,(_,(MlyValue.longtycon_EQUALS_list2 longtycon_EQUALS_list21,_,
longtycon_EQUALS_list2right as longtycon_EQUALS_list21right))::_::_::(
_,(MlyValue.spec1 spec11,spec1left as spec11left,_))::rest671) => let 
val result=MlyValue.spec1(fn _ => let val spec1 as spec11=spec11 ()
val longtycon_EQUALS_list2 as longtycon_EQUALS_list21=
longtycon_EQUALS_list21 ()
 in (
 SHARINGTYPESpec(I(spec1left,
					    longtycon_EQUALS_list2right),
					  spec1, longtycon_EQUALS_list2) 
) end
)
 in (LrTable.NT 105,(result,spec11left,longtycon_EQUALS_list21right),
rest671) end
| (256,(_,(MlyValue.longstrid_EQUALS_list2 longstrid_EQUALS_list21,_,
longstrid_EQUALS_list2right as longstrid_EQUALS_list21right))::(_,(_,
SHARINGleft as SHARING1left,_))::rest671) => let val result=
MlyValue.spec1(fn _ => let val longstrid_EQUALS_list2 as 
longstrid_EQUALS_list21=longstrid_EQUALS_list21 ()
 in (
 SHARINGSpec(I(SHARINGleft,
					longstrid_EQUALS_list2right),
				      EMPTYSpec(I(SHARINGleft,SHARINGleft)),
				      longstrid_EQUALS_list2) 
) end
)
 in (LrTable.NT 105,(result,SHARING1left,longstrid_EQUALS_list21right)
,rest671) end
| (257,(_,(MlyValue.longstrid_EQUALS_list2 longstrid_EQUALS_list21,_,
longstrid_EQUALS_list2right as longstrid_EQUALS_list21right))::_::(_,(
MlyValue.spec1 spec11,spec1left as spec11left,_))::rest671) => let 
val result=MlyValue.spec1(fn _ => let val spec1 as spec11=spec11 ()
val longstrid_EQUALS_list2 as longstrid_EQUALS_list21=
longstrid_EQUALS_list21 ()
 in (
 SHARINGSpec(I(spec1left,longstrid_EQUALS_list2right),
				      spec1, longstrid_EQUALS_list2) 
) end
)
 in (LrTable.NT 105,(result,spec11left,longstrid_EQUALS_list21right),
rest671) end
| (258,(_,(MlyValue.spec1'' spec1''1,spec1''1left,spec1''1right))::
rest671) => let val result=MlyValue.spec1'(fn _ => let val spec1'' as 
spec1''1=spec1''1 ()
 in ( spec1'' ) end
)
 in (LrTable.NT 106,(result,spec1''1left,spec1''1right),rest671) end
| (259,(_,(MlyValue.spec1' spec1'2,_,spec1'2right))::(_,(
MlyValue.spec1' spec1'1,spec1'1left,_))::rest671) => let val result=
MlyValue.spec1'(fn _ => let val spec1'1=spec1'1 ()
val spec1'2=spec1'2 ()
 in ( SEQSpec(I(spec1'1left,spec1'2right),
				  spec1'1, spec1'2) )
 end
)
 in (LrTable.NT 106,(result,spec1'1left,spec1'2right),rest671) end
| (260,(_,(_,SEMICOLONleft as SEMICOLON1left,SEMICOLONright as 
SEMICOLON1right))::rest671) => let val result=MlyValue.spec1'(fn _ => 
(
 SEQSpec(I(SEMICOLONleft,SEMICOLONright),
				 EMPTYSpec(I(SEMICOLONleft,SEMICOLONleft)),
				 EMPTYSpec(I(SEMICOLONright,SEMICOLONright))) 
))
 in (LrTable.NT 106,(result,SEMICOLON1left,SEMICOLON1right),rest671)
 end
| (261,(_,(MlyValue.valdesc valdesc1,_,valdescright as valdesc1right))
::(_,(_,VALleft as VAL1left,_))::rest671) => let val result=
MlyValue.spec1''(fn _ => let val valdesc as valdesc1=valdesc1 ()
 in ( VALSpec(I(VALleft,valdescright), valdesc) ) end
)
 in (LrTable.NT 107,(result,VAL1left,valdesc1right),rest671) end
| (262,(_,(MlyValue.typdesc typdesc1,_,typdescright as typdesc1right))
::(_,(_,TYPEleft as TYPE1left,_))::rest671) => let val result=
MlyValue.spec1''(fn _ => let val typdesc as typdesc1=typdesc1 ()
 in ( TYPESpec(I(TYPEleft,typdescright), typdesc) ) end
)
 in (LrTable.NT 107,(result,TYPE1left,typdesc1right),rest671) end
| (263,(_,(MlyValue.typdesc typdesc1,_,typdescright as typdesc1right))
::(_,(_,EQTYPEleft as EQTYPE1left,_))::rest671) => let val result=
MlyValue.spec1''(fn _ => let val typdesc as typdesc1=typdesc1 ()
 in ( EQTYPESpec(I(EQTYPEleft,typdescright), typdesc) ) end
)
 in (LrTable.NT 107,(result,EQTYPE1left,typdesc1right),rest671) end
| (264,(_,(MlyValue.syndesc syndesc1,_,syndescright as syndesc1right))
::(_,(_,TYPEleft as TYPE1left,_))::rest671) => let val result=
MlyValue.spec1''(fn _ => let val syndesc as syndesc1=syndesc1 ()
 in ( SYNSpec(I(TYPEleft,syndescright), syndesc) ) end
)
 in (LrTable.NT 107,(result,TYPE1left,syndesc1right),rest671) end
| (265,(_,(MlyValue.datdesc0 datdesc01,_,datdesc0right as 
datdesc01right))::(_,(_,DATATYPEleft as DATATYPE1left,_))::rest671)
 => let val result=MlyValue.spec1''(fn _ => let val datdesc0 as 
datdesc01=datdesc01 ()
 in ( DATATYPESpec(I(DATATYPEleft,datdesc0right), datdesc0)) end
)
 in (LrTable.NT 107,(result,DATATYPE1left,datdesc01right),rest671) end
| (266,(_,(MlyValue.datdesc1 datdesc11,_,datdesc1right as 
datdesc11right))::(_,(_,DATATYPEleft as DATATYPE1left,_))::rest671)
 => let val result=MlyValue.spec1''(fn _ => let val datdesc1 as 
datdesc11=datdesc11 ()
 in ( DATATYPESpec(I(DATATYPEleft,datdesc1right), datdesc1)) end
)
 in (LrTable.NT 107,(result,DATATYPE1left,datdesc11right),rest671) end
| (267,(_,(MlyValue.longtycon longtycon1,_,longtyconright as 
longtycon1right))::_::_::(_,(MlyValue.tycon tycon1,_,_))::(_,(_,
DATATYPEleft as DATATYPE1left,_))::rest671) => let val result=
MlyValue.spec1''(fn _ => let val tycon as tycon1=tycon1 ()
val longtycon as longtycon1=longtycon1 ()
 in (
 DATATYPE2Spec(I(DATATYPEleft,longtyconright),
					tycon, longtycon) 
) end
)
 in (LrTable.NT 107,(result,DATATYPE1left,longtycon1right),rest671)
 end
| (268,(_,(MlyValue.exdesc exdesc1,_,exdescright as exdesc1right))::(_
,(_,EXCEPTIONleft as EXCEPTION1left,_))::rest671) => let val result=
MlyValue.spec1''(fn _ => let val exdesc as exdesc1=exdesc1 ()
 in ( EXCEPTIONSpec(I(EXCEPTIONleft,exdescright), exdesc) ) end
)
 in (LrTable.NT 107,(result,EXCEPTION1left,exdesc1right),rest671) end
| (269,(_,(MlyValue.strdesc strdesc1,_,strdescright as strdesc1right))
::(_,(_,STRUCTUREleft as STRUCTURE1left,_))::rest671) => let val 
result=MlyValue.spec1''(fn _ => let val strdesc as strdesc1=strdesc1 
()
 in ( STRUCTURESpec(I(STRUCTUREleft,strdescright), strdesc)) end
)
 in (LrTable.NT 107,(result,STRUCTURE1left,strdesc1right),rest671) end
| (270,(_,(MlyValue.sigexp sigexp1,_,sigexpright as sigexp1right))::(_
,(_,INCLUDEleft as INCLUDE1left,_))::rest671) => let val result=
MlyValue.spec1''(fn _ => let val sigexp as sigexp1=sigexp1 ()
 in ( INCLUDESpec(I(INCLUDEleft,sigexpright), sigexp) ) end
)
 in (LrTable.NT 107,(result,INCLUDE1left,sigexp1right),rest671) end
| (271,(_,(MlyValue.sigid_list2 sigid_list21,_,sigid_list2right as 
sigid_list21right))::(_,(_,INCLUDEleft as INCLUDE1left,_))::rest671)
 => let val result=MlyValue.spec1''(fn _ => let val sigid_list2 as 
sigid_list21=sigid_list21 ()
 in (
 INCLUDEMULTISpec(I(INCLUDEleft,sigid_list2right),
					   sigid_list2) 
) end
)
 in (LrTable.NT 107,(result,INCLUDE1left,sigid_list21right),rest671)
 end
| (272,(_,(MlyValue.sigid_list2 sigid_list21,_,sigid_list21right))::(_
,(MlyValue.sigid sigid1,sigid1left,_))::rest671) => let val result=
MlyValue.sigid_list2(fn _ => let val sigid as sigid1=sigid1 ()
val sigid_list2 as sigid_list21=sigid_list21 ()
 in ( sigid::sigid_list2 ) end
)
 in (LrTable.NT 108,(result,sigid1left,sigid_list21right),rest671) end
| (273,(_,(MlyValue.sigid sigid2,_,sigid2right))::(_,(MlyValue.sigid 
sigid1,sigid1left,_))::rest671) => let val result=MlyValue.sigid_list2
(fn _ => let val sigid1=sigid1 ()
val sigid2=sigid2 ()
 in ( sigid1::sigid2::[] ) end
)
 in (LrTable.NT 108,(result,sigid1left,sigid2right),rest671) end
| (274,(_,(MlyValue.longtycon_EQUALS_list1 longtycon_EQUALS_list11,_,
longtycon_EQUALS_list11right))::_::(_,(MlyValue.longtycon longtycon1,
longtycon1left,_))::rest671) => let val result=
MlyValue.longtycon_EQUALS_list1(fn _ => let val longtycon as 
longtycon1=longtycon1 ()
val longtycon_EQUALS_list1 as longtycon_EQUALS_list11=
longtycon_EQUALS_list11 ()
 in ( longtycon::longtycon_EQUALS_list1 ) end
)
 in (LrTable.NT 109,(result,longtycon1left,
longtycon_EQUALS_list11right),rest671) end
| (275,(_,(MlyValue.longtycon longtycon1,longtycon1left,
longtycon1right))::rest671) => let val result=
MlyValue.longtycon_EQUALS_list1(fn _ => let val longtycon as 
longtycon1=longtycon1 ()
 in ( longtycon::[] ) end
)
 in (LrTable.NT 109,(result,longtycon1left,longtycon1right),rest671)
 end
| (276,(_,(MlyValue.longtycon_EQUALS_list1 longtycon_EQUALS_list11,_,
longtycon_EQUALS_list11right))::_::(_,(MlyValue.longtycon longtycon1,
longtycon1left,_))::rest671) => let val result=
MlyValue.longtycon_EQUALS_list2(fn _ => let val longtycon as 
longtycon1=longtycon1 ()
val longtycon_EQUALS_list1 as longtycon_EQUALS_list11=
longtycon_EQUALS_list11 ()
 in ( longtycon::longtycon_EQUALS_list1 ) end
)
 in (LrTable.NT 110,(result,longtycon1left,
longtycon_EQUALS_list11right),rest671) end
| (277,(_,(MlyValue.longstrid_EQUALS_list1 longstrid_EQUALS_list11,_,
longstrid_EQUALS_list11right))::_::(_,(MlyValue.longstrid longstrid1,
longstrid1left,_))::rest671) => let val result=
MlyValue.longstrid_EQUALS_list1(fn _ => let val longstrid as 
longstrid1=longstrid1 ()
val longstrid_EQUALS_list1 as longstrid_EQUALS_list11=
longstrid_EQUALS_list11 ()
 in ( longstrid::longstrid_EQUALS_list1 ) end
)
 in (LrTable.NT 111,(result,longstrid1left,
longstrid_EQUALS_list11right),rest671) end
| (278,(_,(MlyValue.longstrid longstrid1,longstrid1left,
longstrid1right))::rest671) => let val result=
MlyValue.longstrid_EQUALS_list1(fn _ => let val longstrid as 
longstrid1=longstrid1 ()
 in ( longstrid::[] ) end
)
 in (LrTable.NT 111,(result,longstrid1left,longstrid1right),rest671)
 end
| (279,(_,(MlyValue.longstrid_EQUALS_list1 longstrid_EQUALS_list11,_,
longstrid_EQUALS_list11right))::_::(_,(MlyValue.longstrid longstrid1,
longstrid1left,_))::rest671) => let val result=
MlyValue.longstrid_EQUALS_list2(fn _ => let val longstrid as 
longstrid1=longstrid1 ()
val longstrid_EQUALS_list1 as longstrid_EQUALS_list11=
longstrid_EQUALS_list11 ()
 in ( longstrid::longstrid_EQUALS_list1 ) end
)
 in (LrTable.NT 112,(result,longstrid1left,
longstrid_EQUALS_list11right),rest671) end
| (280,(_,(MlyValue.AND_valdesc_opt AND_valdesc_opt1,_,
AND_valdesc_optright as AND_valdesc_opt1right))::(_,(MlyValue.ty ty1,_
,_))::_::(_,(MlyValue.vid' vid'1,vid'left as vid'1left,_))::rest671)
 => let val result=MlyValue.valdesc(fn _ => let val vid' as vid'1=
vid'1 ()
val ty as ty1=ty1 ()
val AND_valdesc_opt as AND_valdesc_opt1=AND_valdesc_opt1 ()
 in (
 ValDesc(I(vid'left,AND_valdesc_optright),
				  vid', ty, AND_valdesc_opt) 
) end
)
 in (LrTable.NT 113,(result,vid'1left,AND_valdesc_opt1right),rest671)
 end
| (281,(_,(MlyValue.valdesc valdesc1,_,valdesc1right))::(_,(_,AND1left
,_))::rest671) => let val result=MlyValue.AND_valdesc_opt(fn _ => let 
val valdesc as valdesc1=valdesc1 ()
 in ( SOME valdesc ) end
)
 in (LrTable.NT 114,(result,AND1left,valdesc1right),rest671) end
| (282,rest671) => let val result=MlyValue.AND_valdesc_opt(fn _ => (
 NONE ))
 in (LrTable.NT 114,(result,defaultPos,defaultPos),rest671) end
| (283,(_,(MlyValue.AND_typdesc_opt AND_typdesc_opt1,_,
AND_typdesc_optright as AND_typdesc_opt1right))::(_,(MlyValue.tycon 
tycon1,_,_))::(_,(MlyValue.tyvarseq tyvarseq1,tyvarseqleft as 
tyvarseq1left,_))::rest671) => let val result=MlyValue.typdesc(fn _
 => let val tyvarseq as tyvarseq1=tyvarseq1 ()
val tycon as tycon1=tycon1 ()
val AND_typdesc_opt as AND_typdesc_opt1=AND_typdesc_opt1 ()
 in (
 TypDesc(I(tyvarseqleft,AND_typdesc_optright),
				  tyvarseq, tycon, AND_typdesc_opt) 
) end
)
 in (LrTable.NT 115,(result,tyvarseq1left,AND_typdesc_opt1right),
rest671) end
| (284,(_,(MlyValue.typdesc typdesc1,_,typdesc1right))::(_,(_,AND1left
,_))::rest671) => let val result=MlyValue.AND_typdesc_opt(fn _ => let 
val typdesc as typdesc1=typdesc1 ()
 in ( SOME typdesc ) end
)
 in (LrTable.NT 116,(result,AND1left,typdesc1right),rest671) end
| (285,rest671) => let val result=MlyValue.AND_typdesc_opt(fn _ => (
 NONE ))
 in (LrTable.NT 116,(result,defaultPos,defaultPos),rest671) end
| (286,(_,(MlyValue.AND_syndesc_opt AND_syndesc_opt1,_,
AND_syndesc_optright as AND_syndesc_opt1right))::(_,(MlyValue.ty ty1,_
,_))::_::(_,(MlyValue.tycon tycon1,_,_))::(_,(MlyValue.tyvarseq 
tyvarseq1,tyvarseqleft as tyvarseq1left,_))::rest671) => let val 
result=MlyValue.syndesc(fn _ => let val tyvarseq as tyvarseq1=
tyvarseq1 ()
val tycon as tycon1=tycon1 ()
val ty as ty1=ty1 ()
val AND_syndesc_opt as AND_syndesc_opt1=AND_syndesc_opt1 ()
 in (
 SynDesc(I(tyvarseqleft,AND_syndesc_optright),
				  tyvarseq, tycon, ty, AND_syndesc_opt) 
) end
)
 in (LrTable.NT 117,(result,tyvarseq1left,AND_syndesc_opt1right),
rest671) end
| (287,(_,(MlyValue.syndesc syndesc1,_,syndesc1right))::(_,(_,AND1left
,_))::rest671) => let val result=MlyValue.AND_syndesc_opt(fn _ => let 
val syndesc as syndesc1=syndesc1 ()
 in ( SOME syndesc ) end
)
 in (LrTable.NT 118,(result,AND1left,syndesc1right),rest671) end
| (288,rest671) => let val result=MlyValue.AND_syndesc_opt(fn _ => (
 NONE ))
 in (LrTable.NT 118,(result,defaultPos,defaultPos),rest671) end
| (289,(_,(MlyValue.AND_datdesc_opt AND_datdesc_opt1,_,
AND_datdesc_optright as AND_datdesc_opt1right))::(_,(MlyValue.condesc 
condesc1,_,_))::_::(_,(MlyValue.tycon tycon1,_,_))::(_,(
MlyValue.tyvarseq tyvarseq1,tyvarseqleft as tyvarseq1left,_))::rest671
) => let val result=MlyValue.datdesc(fn _ => let val tyvarseq as 
tyvarseq1=tyvarseq1 ()
val tycon as tycon1=tycon1 ()
val condesc as condesc1=condesc1 ()
val AND_datdesc_opt as AND_datdesc_opt1=AND_datdesc_opt1 ()
 in (
 DatDesc(I(tyvarseqleft,AND_datdesc_optright),
	  			  tyvarseq, tycon, condesc, AND_datdesc_opt) 
) end
)
 in (LrTable.NT 119,(result,tyvarseq1left,AND_datdesc_opt1right),
rest671) end
| (290,(_,(MlyValue.AND_datdesc_opt AND_datdesc_opt1,_,
AND_datdesc_optright as AND_datdesc_opt1right))::(_,(MlyValue.condesc 
condesc1,_,_))::_::(_,(MlyValue.tycon tycon1,tyconleft as tycon1left,_
))::rest671) => let val result=MlyValue.datdesc0(fn _ => let val tycon
 as tycon1=tycon1 ()
val condesc as condesc1=condesc1 ()
val AND_datdesc_opt as AND_datdesc_opt1=AND_datdesc_opt1 ()
 in (
 DatDesc(I(tyconleft,AND_datdesc_optright),
	  			  TyVarseq(I(defaultPos,defaultPos), []),
				  tycon, condesc, AND_datdesc_opt) 
) end
)
 in (LrTable.NT 120,(result,tycon1left,AND_datdesc_opt1right),rest671)
 end
| (291,(_,(MlyValue.AND_datdesc_opt AND_datdesc_opt1,_,
AND_datdesc_optright as AND_datdesc_opt1right))::(_,(MlyValue.condesc 
condesc1,_,_))::_::(_,(MlyValue.tycon tycon1,_,_))::(_,(
MlyValue.tyvarseq1 tyvarseq11,tyvarseq1left as tyvarseq11left,_))::
rest671) => let val result=MlyValue.datdesc1(fn _ => let val tyvarseq1
 as tyvarseq11=tyvarseq11 ()
val tycon as tycon1=tycon1 ()
val condesc as condesc1=condesc1 ()
val AND_datdesc_opt as AND_datdesc_opt1=AND_datdesc_opt1 ()
 in (
 DatDesc(I(tyvarseq1left,AND_datdesc_optright),
	  			  tyvarseq1, tycon, condesc, AND_datdesc_opt) 
) end
)
 in (LrTable.NT 121,(result,tyvarseq11left,AND_datdesc_opt1right),
rest671) end
| (292,(_,(MlyValue.datdesc datdesc1,_,datdesc1right))::(_,(_,AND1left
,_))::rest671) => let val result=MlyValue.AND_datdesc_opt(fn _ => let 
val datdesc as datdesc1=datdesc1 ()
 in ( SOME datdesc ) end
)
 in (LrTable.NT 122,(result,AND1left,datdesc1right),rest671) end
| (293,rest671) => let val result=MlyValue.AND_datdesc_opt(fn _ => (
 NONE ))
 in (LrTable.NT 122,(result,defaultPos,defaultPos),rest671) end
| (294,(_,(MlyValue.BAR_condesc_opt BAR_condesc_opt1,_,
BAR_condesc_optright as BAR_condesc_opt1right))::(_,(
MlyValue.OF_ty_opt OF_ty_opt1,_,_))::(_,(MlyValue.vid' vid'1,vid'left
 as vid'1left,_))::rest671) => let val result=MlyValue.condesc(fn _
 => let val vid' as vid'1=vid'1 ()
val OF_ty_opt as OF_ty_opt1=OF_ty_opt1 ()
val BAR_condesc_opt as BAR_condesc_opt1=BAR_condesc_opt1 ()
 in (
 ConDesc(I(vid'left,BAR_condesc_optright),
				  vid', OF_ty_opt, BAR_condesc_opt) 
) end
)
 in (LrTable.NT 123,(result,vid'1left,BAR_condesc_opt1right),rest671)
 end
| (295,(_,(MlyValue.condesc condesc1,_,condesc1right))::(_,(_,BAR1left
,_))::rest671) => let val result=MlyValue.BAR_condesc_opt(fn _ => let 
val condesc as condesc1=condesc1 ()
 in ( SOME condesc ) end
)
 in (LrTable.NT 124,(result,BAR1left,condesc1right),rest671) end
| (296,rest671) => let val result=MlyValue.BAR_condesc_opt(fn _ => (
 NONE ))
 in (LrTable.NT 124,(result,defaultPos,defaultPos),rest671) end
| (297,(_,(MlyValue.AND_exdesc_opt AND_exdesc_opt1,_,
AND_exdesc_optright as AND_exdesc_opt1right))::(_,(MlyValue.OF_ty_opt 
OF_ty_opt1,_,_))::(_,(MlyValue.vid' vid'1,vid'left as vid'1left,_))::
rest671) => let val result=MlyValue.exdesc(fn _ => let val vid' as 
vid'1=vid'1 ()
val OF_ty_opt as OF_ty_opt1=OF_ty_opt1 ()
val AND_exdesc_opt as AND_exdesc_opt1=AND_exdesc_opt1 ()
 in (
 ExDesc(I(vid'left,AND_exdesc_optright),
	  			 vid', OF_ty_opt, AND_exdesc_opt) 
) end
)
 in (LrTable.NT 125,(result,vid'1left,AND_exdesc_opt1right),rest671)
 end
| (298,(_,(MlyValue.exdesc exdesc1,_,exdesc1right))::(_,(_,AND1left,_)
)::rest671) => let val result=MlyValue.AND_exdesc_opt(fn _ => let val 
exdesc as exdesc1=exdesc1 ()
 in ( SOME exdesc ) end
)
 in (LrTable.NT 126,(result,AND1left,exdesc1right),rest671) end
| (299,rest671) => let val result=MlyValue.AND_exdesc_opt(fn _ => (
 NONE ))
 in (LrTable.NT 126,(result,defaultPos,defaultPos),rest671) end
| (300,(_,(MlyValue.sigexp__AND_strdesc_opt sigexp__AND_strdesc_opt1,_
,sigexp__AND_strdesc_optright as sigexp__AND_strdesc_opt1right))::_::(
_,(MlyValue.strid strid1,stridleft as strid1left,_))::rest671) => let 
val result=MlyValue.strdesc(fn _ => let val strid as strid1=strid1 ()
val sigexp__AND_strdesc_opt as sigexp__AND_strdesc_opt1=
sigexp__AND_strdesc_opt1 ()
 in (
 StrDesc(I(stridleft,sigexp__AND_strdesc_optright),
	  			  strid, #1 sigexp__AND_strdesc_opt,
				  #2 sigexp__AND_strdesc_opt) 
) end
)
 in (LrTable.NT 127,(result,strid1left,sigexp__AND_strdesc_opt1right),
rest671) end
| (301,(_,(MlyValue.strdesc strdesc1,_,strdesc1right))::(_,(_,AND1left
,_))::rest671) => let val result=MlyValue.AND_strdesc_opt(fn _ => let 
val strdesc as strdesc1=strdesc1 ()
 in ( SOME strdesc ) end
)
 in (LrTable.NT 128,(result,AND1left,strdesc1right),rest671) end
| (302,rest671) => let val result=MlyValue.AND_strdesc_opt(fn _ => (
 NONE ))
 in (LrTable.NT 128,(result,defaultPos,defaultPos),rest671) end
| (303,(_,(MlyValue.AND_strdesc_opt AND_strdesc_opt1,_,
AND_strdesc_opt1right))::(_,(MlyValue.sigexp' sigexp'1,sigexp'1left,_)
)::rest671) => let val result=MlyValue.sigexp__AND_strdesc_opt(fn _
 => let val sigexp' as sigexp'1=sigexp'1 ()
val AND_strdesc_opt as AND_strdesc_opt1=AND_strdesc_opt1 ()
 in ( ( sigexp', AND_strdesc_opt ) ) end
)
 in (LrTable.NT 129,(result,sigexp'1left,AND_strdesc_opt1right),
rest671) end
| (304,(_,(MlyValue.tyreadesc__AND_strdesc_opt 
tyreadesc__AND_strdesc_opt1,_,tyreadesc__AND_strdesc_optright as 
tyreadesc__AND_strdesc_opt1right))::_::(_,(MlyValue.sigexp sigexp1,
sigexpleft as sigexp1left,_))::rest671) => let val result=
MlyValue.sigexp__AND_strdesc_opt(fn _ => let val sigexp as sigexp1=
sigexp1 ()
val tyreadesc__AND_strdesc_opt as tyreadesc__AND_strdesc_opt1=
tyreadesc__AND_strdesc_opt1 ()
 in (
 ( WHERETYPESigExp(I(sigexpleft,
					      tyreadesc__AND_strdesc_optright),
					   sigexp,
					   #1 tyreadesc__AND_strdesc_opt),
			    #2 tyreadesc__AND_strdesc_opt ) 
) end
)
 in (LrTable.NT 129,(result,sigexp1left,
tyreadesc__AND_strdesc_opt1right),rest671) end
| (305,(_,(MlyValue.AND_tyreadesc_opt__AND_strdesc_opt 
AND_tyreadesc_opt__AND_strdesc_opt1,_,
AND_tyreadesc_opt__AND_strdesc_optright as 
AND_tyreadesc_opt__AND_strdesc_opt1right))::(_,(MlyValue.ty ty1,_,_))
::_::(_,(MlyValue.longtycon longtycon1,_,_))::(_,(MlyValue.tyvarseq 
tyvarseq1,_,_))::(_,(_,TYPEleft as TYPE1left,_))::rest671) => let val 
result=MlyValue.tyreadesc__AND_strdesc_opt(fn _ => let val tyvarseq
 as tyvarseq1=tyvarseq1 ()
val longtycon as longtycon1=longtycon1 ()
val ty as ty1=ty1 ()
val AND_tyreadesc_opt__AND_strdesc_opt as 
AND_tyreadesc_opt__AND_strdesc_opt1=
AND_tyreadesc_opt__AND_strdesc_opt1 ()
 in (
 ( TyReaDesc(I(TYPEleft,
				       AND_tyreadesc_opt__AND_strdesc_optright),
				      tyvarseq, longtycon, ty,
				      #1 AND_tyreadesc_opt__AND_strdesc_opt),
			    #2 AND_tyreadesc_opt__AND_strdesc_opt ) 
) end
)
 in (LrTable.NT 130,(result,TYPE1left,
AND_tyreadesc_opt__AND_strdesc_opt1right),rest671) end
| (306,(_,(MlyValue.AND_strdesc_opt AND_strdesc_opt1,
AND_strdesc_opt1left,AND_strdesc_opt1right))::rest671) => let val 
result=MlyValue.AND_tyreadesc_opt__AND_strdesc_opt(fn _ => let val 
AND_strdesc_opt as AND_strdesc_opt1=AND_strdesc_opt1 ()
 in ( ( NONE, AND_strdesc_opt ) ) end
)
 in (LrTable.NT 131,(result,AND_strdesc_opt1left,AND_strdesc_opt1right
),rest671) end
| (307,(_,(MlyValue.tyreadesc__AND_strdesc_opt 
tyreadesc__AND_strdesc_opt1,_,tyreadesc__AND_strdesc_opt1right))::(_,(
_,AND1left,_))::rest671) => let val result=
MlyValue.AND_tyreadesc_opt__AND_strdesc_opt(fn _ => let val 
tyreadesc__AND_strdesc_opt as tyreadesc__AND_strdesc_opt1=
tyreadesc__AND_strdesc_opt1 ()
 in (
 ( SOME(#1 tyreadesc__AND_strdesc_opt),
				    #2 tyreadesc__AND_strdesc_opt ) 
) end
)
 in (LrTable.NT 131,(result,AND1left,tyreadesc__AND_strdesc_opt1right)
,rest671) end
| (308,(_,(MlyValue.funbind funbind1,_,funbindright as funbind1right))
::(_,(_,FUNCTORleft as FUNCTOR1left,_))::rest671) => let val result=
MlyValue.fundec(fn _ => let val funbind as funbind1=funbind1 ()
 in ( FunDec(I(FUNCTORleft,funbindright), funbind) ) end
)
 in (LrTable.NT 132,(result,FUNCTOR1left,funbind1right),rest671) end
| (309,(_,(MlyValue.strexp__AND_funbind_opt strexp__AND_funbind_opt1,_
,strexp__AND_funbind_optright as strexp__AND_funbind_opt1right))::_::(
_,(MlyValue.COLON_sigexp_opt COLON_sigexp_opt1,_,_))::_::(_,(
MlyValue.sigexp sigexp1,_,_))::_::(_,(MlyValue.strid strid1,_,_))::_::
(_,(MlyValue.funid funid1,funidleft as funid1left,_))::rest671) => 
let val result=MlyValue.funbind(fn _ => let val funid as funid1=funid1
 ()
val strid as strid1=strid1 ()
val sigexp as sigexp1=sigexp1 ()
val COLON_sigexp_opt as COLON_sigexp_opt1=COLON_sigexp_opt1 ()
val strexp__AND_funbind_opt as strexp__AND_funbind_opt1=
strexp__AND_funbind_opt1 ()
 in (
 TRANSFunBind(I(funidleft,
					 strexp__AND_funbind_optright),
				       funid, strid, sigexp, COLON_sigexp_opt,
				       #1 strexp__AND_funbind_opt,
				       #2 strexp__AND_funbind_opt) 
) end
)
 in (LrTable.NT 133,(result,funid1left,strexp__AND_funbind_opt1right),
rest671) end
| (310,(_,(MlyValue.strexp__AND_funbind_opt strexp__AND_funbind_opt1,_
,strexp__AND_funbind_optright as strexp__AND_funbind_opt1right))::_::(
_,(MlyValue.sigexp sigexp2,_,_))::_::_::(_,(MlyValue.sigexp sigexp1,_,
_))::_::(_,(MlyValue.strid strid1,_,_))::_::(_,(MlyValue.funid funid1,
funidleft as funid1left,_))::rest671) => let val result=
MlyValue.funbind(fn _ => let val funid as funid1=funid1 ()
val strid as strid1=strid1 ()
val sigexp1=sigexp1 ()
val sigexp2=sigexp2 ()
val strexp__AND_funbind_opt as strexp__AND_funbind_opt1=
strexp__AND_funbind_opt1 ()
 in (
 SEALFunBind(I(funidleft,strexp__AND_funbind_optright),
				      funid, strid, sigexp1, sigexp2,
				      #1 strexp__AND_funbind_opt,
				      #2 strexp__AND_funbind_opt) 
) end
)
 in (LrTable.NT 133,(result,funid1left,strexp__AND_funbind_opt1right),
rest671) end
| (311,(_,(MlyValue.strexp__AND_funbind_opt strexp__AND_funbind_opt1,_
,strexp__AND_funbind_optright as strexp__AND_funbind_opt1right))::_::(
_,(MlyValue.COLON_sigexp_opt COLON_sigexp_opt1,_,_))::_::(_,(
MlyValue.spec spec1,_,_))::_::(_,(MlyValue.funid funid1,funidleft as 
funid1left,_))::rest671) => let val result=MlyValue.funbind(fn _ => 
let val funid as funid1=funid1 ()
val spec as spec1=spec1 ()
val COLON_sigexp_opt as COLON_sigexp_opt1=COLON_sigexp_opt1 ()
val strexp__AND_funbind_opt as strexp__AND_funbind_opt1=
strexp__AND_funbind_opt1 ()
 in (
 TRANSSPECFunBind(I(funidleft,
					     strexp__AND_funbind_optright),
					   funid, spec, COLON_sigexp_opt,
					   #1 strexp__AND_funbind_opt,
					   #2 strexp__AND_funbind_opt) 
) end
)
 in (LrTable.NT 133,(result,funid1left,strexp__AND_funbind_opt1right),
rest671) end
| (312,(_,(MlyValue.strexp__AND_funbind_opt strexp__AND_funbind_opt1,_
,strexp__AND_funbind_optright as strexp__AND_funbind_opt1right))::_::(
_,(MlyValue.sigexp sigexp1,_,_))::_::_::(_,(MlyValue.spec spec1,_,_))
::_::(_,(MlyValue.funid funid1,funidleft as funid1left,_))::rest671)
 => let val result=MlyValue.funbind(fn _ => let val funid as funid1=
funid1 ()
val spec as spec1=spec1 ()
val sigexp as sigexp1=sigexp1 ()
val strexp__AND_funbind_opt as strexp__AND_funbind_opt1=
strexp__AND_funbind_opt1 ()
 in (
 SEALSPECFunBind(I(funidleft,
					    strexp__AND_funbind_optright),
					  funid, spec, sigexp,
					  #1 strexp__AND_funbind_opt,
					  #2 strexp__AND_funbind_opt) 
) end
)
 in (LrTable.NT 133,(result,funid1left,strexp__AND_funbind_opt1right),
rest671) end
| (313,(_,(MlyValue.funbind funbind1,_,funbind1right))::(_,(_,AND1left
,_))::rest671) => let val result=MlyValue.AND_funbind_opt(fn _ => let 
val funbind as funbind1=funbind1 ()
 in ( SOME funbind ) end
)
 in (LrTable.NT 134,(result,AND1left,funbind1right),rest671) end
| (314,rest671) => let val result=MlyValue.AND_funbind_opt(fn _ => (
 NONE ))
 in (LrTable.NT 134,(result,defaultPos,defaultPos),rest671) end
| (315,(_,(MlyValue.AND_funbind_opt AND_funbind_opt1,_,
AND_funbind_opt1right))::(_,(MlyValue.strexp' strexp'1,strexp'1left,_)
)::rest671) => let val result=MlyValue.strexp__AND_funbind_opt(fn _
 => let val strexp' as strexp'1=strexp'1 ()
val AND_funbind_opt as AND_funbind_opt1=AND_funbind_opt1 ()
 in ( ( strexp', AND_funbind_opt ) ) end
)
 in (LrTable.NT 135,(result,strexp'1left,AND_funbind_opt1right),
rest671) end
| (316,(_,(MlyValue.sigexp__AND_funbind_opt sigexp__AND_funbind_opt1,_
,sigexp__AND_funbind_optright as sigexp__AND_funbind_opt1right))::_::(
_,(MlyValue.strexp strexp1,strexpleft as strexp1left,_))::rest671) => 
let val result=MlyValue.strexp__AND_funbind_opt(fn _ => let val strexp
 as strexp1=strexp1 ()
val sigexp__AND_funbind_opt as sigexp__AND_funbind_opt1=
sigexp__AND_funbind_opt1 ()
 in (
 ( COLONStrExp(I(strexpleft,
					  sigexp__AND_funbind_optright),
					strexp, #1 sigexp__AND_funbind_opt),
			    #2 sigexp__AND_funbind_opt ) 
) end
)
 in (LrTable.NT 135,(result,strexp1left,sigexp__AND_funbind_opt1right)
,rest671) end
| (317,(_,(MlyValue.sigexp__AND_funbind_opt sigexp__AND_funbind_opt1,_
,sigexp__AND_funbind_optright as sigexp__AND_funbind_opt1right))::_::(
_,(MlyValue.strexp strexp1,strexpleft as strexp1left,_))::rest671) => 
let val result=MlyValue.strexp__AND_funbind_opt(fn _ => let val strexp
 as strexp1=strexp1 ()
val sigexp__AND_funbind_opt as sigexp__AND_funbind_opt1=
sigexp__AND_funbind_opt1 ()
 in (
 ( SEALStrExp(I(strexpleft,
					 sigexp__AND_funbind_optright),
				       strexp, #1 sigexp__AND_funbind_opt),
			    #2 sigexp__AND_funbind_opt ) 
) end
)
 in (LrTable.NT 135,(result,strexp1left,sigexp__AND_funbind_opt1right)
,rest671) end
| (318,(_,(MlyValue.AND_funbind_opt AND_funbind_opt1,_,
AND_funbind_opt1right))::(_,(MlyValue.sigexp' sigexp'1,sigexp'1left,_)
)::rest671) => let val result=MlyValue.sigexp__AND_funbind_opt(fn _
 => let val sigexp' as sigexp'1=sigexp'1 ()
val AND_funbind_opt as AND_funbind_opt1=AND_funbind_opt1 ()
 in ( ( sigexp', AND_funbind_opt ) ) end
)
 in (LrTable.NT 136,(result,sigexp'1left,AND_funbind_opt1right),
rest671) end
| (319,(_,(MlyValue.tyreadesc__AND_funbind_opt 
tyreadesc__AND_funbind_opt1,_,tyreadesc__AND_funbind_optright as 
tyreadesc__AND_funbind_opt1right))::_::(_,(MlyValue.sigexp sigexp1,
sigexpleft as sigexp1left,_))::rest671) => let val result=
MlyValue.sigexp__AND_funbind_opt(fn _ => let val sigexp as sigexp1=
sigexp1 ()
val tyreadesc__AND_funbind_opt as tyreadesc__AND_funbind_opt1=
tyreadesc__AND_funbind_opt1 ()
 in (
 ( WHERETYPESigExp(I(sigexpleft,
					      tyreadesc__AND_funbind_optright),
					   sigexp,
					   #1 tyreadesc__AND_funbind_opt),
			    #2 tyreadesc__AND_funbind_opt ) 
) end
)
 in (LrTable.NT 136,(result,sigexp1left,
tyreadesc__AND_funbind_opt1right),rest671) end
| (320,(_,(MlyValue.AND_tyreadesc_opt__AND_funbind_opt 
AND_tyreadesc_opt__AND_funbind_opt1,_,
AND_tyreadesc_opt__AND_funbind_optright as 
AND_tyreadesc_opt__AND_funbind_opt1right))::(_,(MlyValue.ty ty1,_,_))
::_::(_,(MlyValue.longtycon longtycon1,_,_))::(_,(MlyValue.tyvarseq 
tyvarseq1,_,_))::(_,(_,TYPEleft as TYPE1left,_))::rest671) => let val 
result=MlyValue.tyreadesc__AND_funbind_opt(fn _ => let val tyvarseq
 as tyvarseq1=tyvarseq1 ()
val longtycon as longtycon1=longtycon1 ()
val ty as ty1=ty1 ()
val AND_tyreadesc_opt__AND_funbind_opt as 
AND_tyreadesc_opt__AND_funbind_opt1=
AND_tyreadesc_opt__AND_funbind_opt1 ()
 in (
 ( TyReaDesc(I(TYPEleft,
				       AND_tyreadesc_opt__AND_funbind_optright),
				      tyvarseq, longtycon, ty,
				      #1 AND_tyreadesc_opt__AND_funbind_opt),
			    #2 AND_tyreadesc_opt__AND_funbind_opt ) 
) end
)
 in (LrTable.NT 137,(result,TYPE1left,
AND_tyreadesc_opt__AND_funbind_opt1right),rest671) end
| (321,(_,(MlyValue.AND_funbind_opt AND_funbind_opt1,
AND_funbind_opt1left,AND_funbind_opt1right))::rest671) => let val 
result=MlyValue.AND_tyreadesc_opt__AND_funbind_opt(fn _ => let val 
AND_funbind_opt as AND_funbind_opt1=AND_funbind_opt1 ()
 in ( ( NONE, AND_funbind_opt ) ) end
)
 in (LrTable.NT 138,(result,AND_funbind_opt1left,AND_funbind_opt1right
),rest671) end
| (322,(_,(MlyValue.tyreadesc__AND_funbind_opt 
tyreadesc__AND_funbind_opt1,_,tyreadesc__AND_funbind_opt1right))::(_,(
_,AND1left,_))::rest671) => let val result=
MlyValue.AND_tyreadesc_opt__AND_funbind_opt(fn _ => let val 
tyreadesc__AND_funbind_opt as tyreadesc__AND_funbind_opt1=
tyreadesc__AND_funbind_opt1 ()
 in (
 ( SOME(#1 tyreadesc__AND_funbind_opt),
			    #2 tyreadesc__AND_funbind_opt ) 
) end
)
 in (LrTable.NT 138,(result,AND1left,tyreadesc__AND_funbind_opt1right)
,rest671) end
| (323,(_,(MlyValue.topdec1 topdec11,topdec11left,topdec11right))::
rest671) => let val result=MlyValue.topdec(fn _ => let val topdec1 as 
topdec11=topdec11 ()
 in ( topdec1 ) end
)
 in (LrTable.NT 139,(result,topdec11left,topdec11right),rest671) end
| (324,rest671) => let val result=MlyValue.topdec(fn _ => (
 STRDECTopDec(I(defaultPos,defaultPos),
				       EMPTYStrDec(I(defaultPos,defaultPos)),
				       NONE) 
))
 in (LrTable.NT 139,(result,defaultPos,defaultPos),rest671) end
| (325,(_,(MlyValue.topdec_opt topdec_opt1,_,topdec_optright as 
topdec_opt1right))::(_,(MlyValue.strdec1' strdec1'1,strdec1'left as 
strdec1'1left,_))::rest671) => let val result=MlyValue.topdec1(fn _
 => let val strdec1' as strdec1'1=strdec1'1 ()
val topdec_opt as topdec_opt1=topdec_opt1 ()
 in (
 STRDECTopDec(I(strdec1'left,topdec_optright),
				       strdec1', topdec_opt) 
) end
)
 in (LrTable.NT 140,(result,strdec1'1left,topdec_opt1right),rest671)
 end
| (326,(_,(MlyValue.topdec_opt topdec_opt1,_,topdec_optright as 
topdec_opt1right))::(_,(MlyValue.sigdec sigdec1,sigdecleft as 
sigdec1left,_))::rest671) => let val result=MlyValue.topdec1(fn _ => 
let val sigdec as sigdec1=sigdec1 ()
val topdec_opt as topdec_opt1=topdec_opt1 ()
 in (
 SIGDECTopDec(I(sigdecleft,topdec_optright),
				       sigdec, topdec_opt) 
) end
)
 in (LrTable.NT 140,(result,sigdec1left,topdec_opt1right),rest671) end
| (327,(_,(MlyValue.topdec_opt topdec_opt1,_,topdec_optright as 
topdec_opt1right))::(_,(MlyValue.fundec fundec1,fundecleft as 
fundec1left,_))::rest671) => let val result=MlyValue.topdec1(fn _ => 
let val fundec as fundec1=fundec1 ()
val topdec_opt as topdec_opt1=topdec_opt1 ()
 in (
 FUNDECTopDec(I(fundecleft,topdec_optright),
				       fundec, topdec_opt) 
) end
)
 in (LrTable.NT 140,(result,fundec1left,topdec_opt1right),rest671) end
| (328,(_,(MlyValue.topdec1 topdec11,topdec11left,topdec11right))::
rest671) => let val result=MlyValue.topdec_opt(fn _ => let val topdec1
 as topdec11=topdec11 ()
 in ( SOME topdec1 ) end
)
 in (LrTable.NT 141,(result,topdec11left,topdec11right),rest671) end
| (329,rest671) => let val result=MlyValue.topdec_opt(fn _ => ( NONE )
)
 in (LrTable.NT 141,(result,defaultPos,defaultPos),rest671) end
| (330,(_,(MlyValue.program' program'1,_,program'1right))::(_,(
MlyValue.initInfix initInfix1,initInfix1left,_))::rest671) => let val 
result=MlyValue.program(fn _ => let val initInfix1=initInfix1 ()
val program' as program'1=program'1 ()
 in ( (program', !J) ) end
)
 in (LrTable.NT 142,(result,initInfix1left,program'1right),rest671)
 end
| (331,(_,(MlyValue.program_opt program_opt1,_,program_opt1right))::(_
,(_,_,SEMICOLONright))::(_,(MlyValue.topdec topdec1,topdecleft as 
topdec1left,_))::rest671) => let val result=MlyValue.program'(fn _ => 
let val topdec as topdec1=topdec1 ()
val program_opt as program_opt1=program_opt1 ()
 in (
 TOPDECProgram(I(topdecleft,SEMICOLONright),
					topdec, program_opt) 
) end
)
 in (LrTable.NT 143,(result,topdec1left,program_opt1right),rest671)
 end
| (332,(_,(MlyValue.program_opt program_opt1,_,program_opt1right))::(_
,(_,_,SEMICOLONright))::(_,(MlyValue.exp exp1,expleft as exp1left,_))
::rest671) => let val result=MlyValue.program'(fn _ => let val exp as 
exp1=exp1 ()
val program_opt as program_opt1=program_opt1 ()
 in (
 EXPProgram(I(expleft,SEMICOLONright),
				     exp, program_opt) )
 end
)
 in (LrTable.NT 143,(result,exp1left,program_opt1right),rest671) end
| (333,(_,(MlyValue.program' program'1,program'1left,program'1right))
::rest671) => let val result=MlyValue.program_opt(fn _ => let val 
program' as program'1=program'1 ()
 in ( SOME program' ) end
)
 in (LrTable.NT 144,(result,program'1left,program'1right),rest671) end
| (334,rest671) => let val result=MlyValue.program_opt(fn _ => ( NONE 
))
 in (LrTable.NT 144,(result,defaultPos,defaultPos),rest671) end
| _ => raise (mlyAction i392)
end
val void = MlyValue.VOID
val extract = fn a => (fn MlyValue.program x => x
| _ => let exception ParseInternal
	in raise ParseInternal end) a ()
end
end
structure Tokens : Parser_TOKENS =
struct
type svalue = ParserData.svalue
type ('a,'b) token = ('a,'b) Token.token
fun EOF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 0,(
ParserData.MlyValue.VOID,p1,p2))
fun ABSTYPE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 1,(
ParserData.MlyValue.VOID,p1,p2))
fun AND (p1,p2) = Token.TOKEN (ParserData.LrTable.T 2,(
ParserData.MlyValue.VOID,p1,p2))
fun ANDALSO (p1,p2) = Token.TOKEN (ParserData.LrTable.T 3,(
ParserData.MlyValue.VOID,p1,p2))
fun AS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 4,(
ParserData.MlyValue.VOID,p1,p2))
fun CASE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 5,(
ParserData.MlyValue.VOID,p1,p2))
fun DO (p1,p2) = Token.TOKEN (ParserData.LrTable.T 6,(
ParserData.MlyValue.VOID,p1,p2))
fun DATATYPE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 7,(
ParserData.MlyValue.VOID,p1,p2))
fun ELSE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 8,(
ParserData.MlyValue.VOID,p1,p2))
fun END (p1,p2) = Token.TOKEN (ParserData.LrTable.T 9,(
ParserData.MlyValue.VOID,p1,p2))
fun EXCEPTION (p1,p2) = Token.TOKEN (ParserData.LrTable.T 10,(
ParserData.MlyValue.VOID,p1,p2))
fun FN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 11,(
ParserData.MlyValue.VOID,p1,p2))
fun FUN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 12,(
ParserData.MlyValue.VOID,p1,p2))
fun HANDLE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 13,(
ParserData.MlyValue.VOID,p1,p2))
fun IF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 14,(
ParserData.MlyValue.VOID,p1,p2))
fun IN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 15,(
ParserData.MlyValue.VOID,p1,p2))
fun INFIX (p1,p2) = Token.TOKEN (ParserData.LrTable.T 16,(
ParserData.MlyValue.VOID,p1,p2))
fun INFIXR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 17,(
ParserData.MlyValue.VOID,p1,p2))
fun LET (p1,p2) = Token.TOKEN (ParserData.LrTable.T 18,(
ParserData.MlyValue.VOID,p1,p2))
fun LOCAL (p1,p2) = Token.TOKEN (ParserData.LrTable.T 19,(
ParserData.MlyValue.VOID,p1,p2))
fun NONFIX (p1,p2) = Token.TOKEN (ParserData.LrTable.T 20,(
ParserData.MlyValue.VOID,p1,p2))
fun OF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 21,(
ParserData.MlyValue.VOID,p1,p2))
fun OP (p1,p2) = Token.TOKEN (ParserData.LrTable.T 22,(
ParserData.MlyValue.VOID,p1,p2))
fun OPEN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 23,(
ParserData.MlyValue.VOID,p1,p2))
fun ORELSE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 24,(
ParserData.MlyValue.VOID,p1,p2))
fun RAISE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 25,(
ParserData.MlyValue.VOID,p1,p2))
fun REC (p1,p2) = Token.TOKEN (ParserData.LrTable.T 26,(
ParserData.MlyValue.VOID,p1,p2))
fun THEN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 27,(
ParserData.MlyValue.VOID,p1,p2))
fun TYPE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 28,(
ParserData.MlyValue.VOID,p1,p2))
fun VAL (p1,p2) = Token.TOKEN (ParserData.LrTable.T 29,(
ParserData.MlyValue.VOID,p1,p2))
fun WITH (p1,p2) = Token.TOKEN (ParserData.LrTable.T 30,(
ParserData.MlyValue.VOID,p1,p2))
fun WITHTYPE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 31,(
ParserData.MlyValue.VOID,p1,p2))
fun WHILE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 32,(
ParserData.MlyValue.VOID,p1,p2))
fun LPAR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 33,(
ParserData.MlyValue.VOID,p1,p2))
fun RPAR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 34,(
ParserData.MlyValue.VOID,p1,p2))
fun LBRACK (p1,p2) = Token.TOKEN (ParserData.LrTable.T 35,(
ParserData.MlyValue.VOID,p1,p2))
fun RBRACK (p1,p2) = Token.TOKEN (ParserData.LrTable.T 36,(
ParserData.MlyValue.VOID,p1,p2))
fun LBRACE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 37,(
ParserData.MlyValue.VOID,p1,p2))
fun RBRACE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 38,(
ParserData.MlyValue.VOID,p1,p2))
fun COMMA (p1,p2) = Token.TOKEN (ParserData.LrTable.T 39,(
ParserData.MlyValue.VOID,p1,p2))
fun COLON (p1,p2) = Token.TOKEN (ParserData.LrTable.T 40,(
ParserData.MlyValue.VOID,p1,p2))
fun SEMICOLON (p1,p2) = Token.TOKEN (ParserData.LrTable.T 41,(
ParserData.MlyValue.VOID,p1,p2))
fun DOTS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 42,(
ParserData.MlyValue.VOID,p1,p2))
fun UNDERBAR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 43,(
ParserData.MlyValue.VOID,p1,p2))
fun BAR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 44,(
ParserData.MlyValue.VOID,p1,p2))
fun EQUALS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 45,(
ParserData.MlyValue.VOID,p1,p2))
fun DARROW (p1,p2) = Token.TOKEN (ParserData.LrTable.T 46,(
ParserData.MlyValue.VOID,p1,p2))
fun ARROW (p1,p2) = Token.TOKEN (ParserData.LrTable.T 47,(
ParserData.MlyValue.VOID,p1,p2))
fun HASH (p1,p2) = Token.TOKEN (ParserData.LrTable.T 48,(
ParserData.MlyValue.VOID,p1,p2))
fun EQTYPE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 49,(
ParserData.MlyValue.VOID,p1,p2))
fun FUNCTOR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 50,(
ParserData.MlyValue.VOID,p1,p2))
fun INCLUDE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 51,(
ParserData.MlyValue.VOID,p1,p2))
fun SHARING (p1,p2) = Token.TOKEN (ParserData.LrTable.T 52,(
ParserData.MlyValue.VOID,p1,p2))
fun SIG (p1,p2) = Token.TOKEN (ParserData.LrTable.T 53,(
ParserData.MlyValue.VOID,p1,p2))
fun SIGNATURE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 54,(
ParserData.MlyValue.VOID,p1,p2))
fun STRUCT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 55,(
ParserData.MlyValue.VOID,p1,p2))
fun STRUCTURE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 56,(
ParserData.MlyValue.VOID,p1,p2))
fun WHERE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 57,(
ParserData.MlyValue.VOID,p1,p2))
fun SEAL (p1,p2) = Token.TOKEN (ParserData.LrTable.T 58,(
ParserData.MlyValue.VOID,p1,p2))
fun ZERO (p1,p2) = Token.TOKEN (ParserData.LrTable.T 59,(
ParserData.MlyValue.VOID,p1,p2))
fun DIGIT (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 60,(
ParserData.MlyValue.DIGIT (fn () => i),p1,p2))
fun NUMERIC (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 61,(
ParserData.MlyValue.NUMERIC (fn () => i),p1,p2))
fun INT (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 62,(
ParserData.MlyValue.INT (fn () => i),p1,p2))
fun HEXINT (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 63,(
ParserData.MlyValue.HEXINT (fn () => i),p1,p2))
fun WORD (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 64,(
ParserData.MlyValue.WORD (fn () => i),p1,p2))
fun HEXWORD (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 65,(
ParserData.MlyValue.HEXWORD (fn () => i),p1,p2))
fun REAL (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 66,(
ParserData.MlyValue.REAL (fn () => i),p1,p2))
fun STRING (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 67,(
ParserData.MlyValue.STRING (fn () => i),p1,p2))
fun CHAR (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 68,(
ParserData.MlyValue.CHAR (fn () => i),p1,p2))
fun ALPHA (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 69,(
ParserData.MlyValue.ALPHA (fn () => i),p1,p2))
fun SYMBOL (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 70,(
ParserData.MlyValue.SYMBOL (fn () => i),p1,p2))
fun STAR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 71,(
ParserData.MlyValue.VOID,p1,p2))
fun TYVAR (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 72,(
ParserData.MlyValue.TYVAR (fn () => i),p1,p2))
fun ETYVAR (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 73,(
ParserData.MlyValue.ETYVAR (fn () => i),p1,p2))
fun LONGID (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 74,(
ParserData.MlyValue.LONGID (fn () => i),p1,p2))
end
end
;
 functor LexerFn(structure Tokens : Parser_TOKENS) =
   struct
    structure UserDeclarations =
      struct
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML lexical analysis
 *
 * Definition, Sections 2.1-2.5, 3.1
 *
 * Notes:
 *   Since all lexical classes must be disjoint:
 *   - There is no single class ID, use ALPHA|SYMBOL|STAR|EQUALS.
 *   - There is no class LAB, use ALPHA|SYMBOL|NUMERIC|DIGIT|STAR.
 *   - ID does not contain `=' and `*', those are EQUALS and STAR.
 *   - LONGID does not contain unqualified ids (but allows for `=' and `*').
 *   - INT does not contain positive decimal integers without leading 0,
 *     and single DIGIT integers, those are in NUMERIC, DIGIT, and ZERO.
 *   - NUMERIC does not contain single digit numbers, those are in DIGIT.
 *   - DIGIT does not contain 0, that is ZERO.
 *
 *   The lexer uses a global variable to recognise nested comments, so it is
 *   not reentrant.
 *)


    open Tokens


    (* Types to match structure LEXER.UserDeclaration *)

    type ('a,'b) token = ('a,'b) Tokens.token
    type pos           = int
    type svalue        = Tokens.svalue
    type lexresult     = (svalue, pos) token



    (* Handling nested comments *)

    val nesting = ref 0		(* non-reentrant side-effect way :-P *)


    fun eof() =
	if !nesting = 0 then
	    Tokens.EOF(~1, ~1)
	else
	    raise Source.Error((0,0), "unclosed comment")



    (* Some helpers to create tokens *)

    open Tokens


    fun toLRPos(yypos, yytext) =
	let
	    val yypos = yypos - 2	(* bug in ML-Lex... *)
	in
	    (yypos, yypos + String.size yytext)
	end

    fun token(TOKEN, yypos, yytext) =
        TOKEN(toLRPos(yypos, yytext))

    fun tokenOf(TOKEN, toVal, yypos, yytext) =
	let
	    val i as (l,r) = toLRPos(yypos, yytext)
	in
	    TOKEN(toVal yytext, l, r)
	end

    fun error(yypos, yytext, s) =
	    raise Source.Error(toLRPos(yypos,yytext), s)

    fun invalid(yypos, yytext) =
	let
	    val s = "invalid character `" ^ String.toString yytext ^ "'"
	in
	    error(yypos, yytext, s)
	end



    (* Convert identifiers *)

    fun toId s = s

    fun toLongId s =
	let
	    fun split  []    = raise Fail "Lexer.toLongId: empty longid"
	      | split [x]    = ([],x)
	      | split(x::xs) = let val (ys,y) = split xs in (x::ys,y) end
	in
	    split(String.fields (fn c => c = #".") s)
	end


    (* Convert constants *)

    fun toInt s     = s
    fun toHexInt s  = String.substring(s, 2, String.size s-2)
    fun toWord s    = s
    fun toHexWord s = String.substring(s, 3, String.size s-3)
    fun toReal s    = s
    fun toString s  = String.substring(s, 1, String.size s-2)
    fun toChar s    = String.substring(s, 2, String.size s-3)


end (* end of user routines *)
exception LexError (* raised if illegal leaf action tried *)
structure Internal =
	struct

datatype yyfinstate = N of int
type statedata = {fin : yyfinstate list, trans: string}
(* transition & final state table *)
val tab = let
val s = [ 
 (0, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (1, 
"\005\005\005\005\005\005\005\005\005\238\239\238\238\238\005\005\
\\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\
\\238\180\226\211\180\180\180\209\207\206\205\180\204\202\199\180\
\\191\189\189\189\189\189\189\189\189\189\187\186\180\184\180\180\
\\180\025\025\025\025\025\025\025\025\025\025\025\025\025\025\025\
\\025\025\025\025\025\025\025\025\025\025\025\183\180\182\180\181\
\\180\166\025\162\153\134\126\025\120\108\025\025\101\025\095\085\
\\025\025\078\055\048\025\045\030\025\025\025\024\023\022\006\005\
\\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\
\\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\
\\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\
\\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\
\\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\
\\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\
\\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\
\\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005"
),
 (3, 
"\240\240\240\240\240\240\240\240\240\240\245\240\240\240\240\240\
\\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\
\\240\240\240\240\240\240\240\240\243\240\241\240\240\240\240\240\
\\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\
\\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\
\\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\
\\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\
\\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\
\\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\
\\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\
\\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\
\\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\
\\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\
\\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\
\\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\
\\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240\240"
),
 (6, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\007\000\007\007\007\007\000\000\000\007\007\000\007\000\007\
\\019\008\008\008\008\008\008\008\008\008\007\000\007\007\007\007\
\\007\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\007\000\007\000\
\\007\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\007\000\007\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (7, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\007\000\007\007\007\007\000\000\000\007\007\000\007\000\007\
\\000\000\000\000\000\000\000\000\000\000\007\000\007\007\007\007\
\\007\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\007\000\007\000\
\\007\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\007\000\007\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (8, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\013\000\
\\008\008\008\008\008\008\008\008\008\008\000\000\000\000\000\000\
\\000\000\000\000\000\009\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\009\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (9, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\012\012\012\012\012\012\012\012\012\012\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\010\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (10, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\011\011\011\011\011\011\011\011\011\011\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (12, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\012\012\012\012\012\012\012\012\012\012\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (13, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\014\014\014\014\014\014\014\014\014\014\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (14, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\014\014\014\014\014\014\014\014\014\014\000\000\000\000\000\000\
\\000\000\000\000\000\015\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\015\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (15, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\018\018\018\018\018\018\018\018\018\018\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\016\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (16, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\017\017\017\017\017\017\017\017\017\017\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (18, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\018\018\018\018\018\018\018\018\018\018\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (19, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\013\000\
\\008\008\008\008\008\008\008\008\008\008\000\000\000\000\000\000\
\\000\000\000\000\000\009\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\009\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\020\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (20, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\021\021\021\021\021\021\021\021\021\021\000\000\000\000\000\000\
\\000\021\021\021\021\021\021\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\021\021\021\021\021\021\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (25, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (27, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\028\000\028\028\028\028\000\000\000\028\028\000\028\000\028\
\\000\000\000\000\000\000\000\000\000\000\028\000\028\028\028\028\
\\028\029\029\029\029\029\029\029\029\029\029\029\029\029\029\029\
\\029\029\029\029\029\029\029\029\029\029\029\000\028\000\028\000\
\\028\029\029\029\029\029\029\029\029\029\029\029\029\029\029\029\
\\029\029\029\029\029\029\029\029\029\029\029\000\028\000\028\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (28, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\028\000\028\028\028\028\000\000\000\028\028\000\028\000\028\
\\000\000\000\000\000\000\000\000\000\000\028\000\028\028\028\028\
\\028\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\028\000\028\000\
\\028\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\028\000\028\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (29, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\029\000\000\000\000\000\000\027\000\
\\029\029\029\029\029\029\029\029\029\029\000\000\000\000\000\000\
\\000\029\029\029\029\029\029\029\029\029\029\029\029\029\029\029\
\\029\029\029\029\029\029\029\029\029\029\029\000\000\000\000\029\
\\000\029\029\029\029\029\029\029\029\029\029\029\029\029\029\029\
\\029\029\029\029\029\029\029\029\029\029\029\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (30, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\038\031\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (31, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\032\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (32, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\033\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (33, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\034\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (34, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\035\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (35, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\036\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (36, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\037\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (38, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\042\026\026\026\039\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (39, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\040\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (40, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\041\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (42, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\043\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (43, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\044\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (45, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\046\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (46, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\047\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (48, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\052\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\049\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (49, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\050\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (50, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\051\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (52, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\053\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (53, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\054\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (55, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\072\064\026\026\026\026\026\026\
\\026\026\026\026\056\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (56, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\057\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (57, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\058\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (58, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\059\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (59, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\060\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (60, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\061\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (61, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\062\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (62, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\063\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (64, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\065\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (65, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\066\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (66, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\067\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (67, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\068\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (68, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\069\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (69, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\070\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (70, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\071\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (72, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\073\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (73, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\074\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (74, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\075\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (75, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\076\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (76, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\077\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (78, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\081\026\026\026\079\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (79, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\080\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (81, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\082\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (82, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\083\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (83, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\084\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (85, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\094\026\026\026\026\026\026\026\026\026\
\\091\026\086\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (86, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\087\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (87, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\088\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (88, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\089\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (89, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\090\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (91, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\092\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (92, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\093\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (95, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\096\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (96, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\097\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (97, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\098\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (98, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\099\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (99, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\100\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (101, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\106\026\026\026\026\026\026\026\026\026\102\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (102, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\103\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (103, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\104\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (104, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\105\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (106, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\107\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (108, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\119\026\026\026\026\026\026\026\109\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (109, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\114\026\026\110\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (110, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\111\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (111, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\112\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (112, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\113\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (114, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\115\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (115, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\116\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (116, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\117\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (117, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\118\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (120, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\121\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (121, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\122\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (122, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\123\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (123, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\124\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (124, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\125\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (126, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\133\026\
\\026\026\026\026\026\127\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (127, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\128\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (128, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\129\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (129, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\130\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (130, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\131\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (131, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\132\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (134, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\150\026\148\026\
\\026\143\026\026\026\026\026\026\135\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (135, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\136\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (136, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\137\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (137, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\138\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (138, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\139\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (139, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\140\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (140, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\141\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (141, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\142\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (143, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\144\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (144, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\145\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (145, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\146\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (146, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\147\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (148, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\149\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (150, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\151\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (151, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\152\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (153, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\155\026\026\026\026\026\026\026\026\026\026\026\026\026\154\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (155, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\156\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (156, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\157\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (157, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\158\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (158, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\159\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (159, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\160\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (160, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\161\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (162, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\163\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (163, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\164\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (164, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\165\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (166, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\174\026\026\026\026\026\026\026\026\026\026\026\168\026\
\\026\026\026\167\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (168, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\169\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (169, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\170\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (170, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\171\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (171, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\172\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (172, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\173\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (174, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\175\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (175, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\176\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (176, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\177\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (177, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\178\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (178, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\026\000\000\000\000\000\000\027\000\
\\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\000\
\\000\026\026\026\026\026\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\026\
\\000\026\026\026\026\179\026\026\026\026\026\026\026\026\026\026\
\\026\026\026\026\026\026\026\026\026\026\026\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (184, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\007\000\007\007\007\007\000\000\000\007\007\000\007\000\007\
\\000\000\000\000\000\000\000\000\000\000\007\000\007\007\185\007\
\\007\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\007\000\007\000\
\\007\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\007\000\007\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (187, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\007\000\007\007\007\007\000\000\000\007\007\000\007\000\007\
\\000\000\000\000\000\000\000\000\000\000\007\000\007\007\188\007\
\\007\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\007\000\007\000\
\\007\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\007\000\007\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (189, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\013\000\
\\190\190\190\190\190\190\190\190\190\190\000\000\000\000\000\000\
\\000\000\000\000\000\009\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\009\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (191, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\013\000\
\\198\198\198\198\198\198\198\198\198\198\000\000\000\000\000\000\
\\000\000\000\000\000\009\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\009\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\194\192\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (192, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\193\193\193\193\193\193\193\193\193\193\000\000\000\000\000\000\
\\000\193\193\193\193\193\193\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\193\193\193\193\193\193\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (194, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\197\197\197\197\197\197\197\197\197\197\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\195\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (195, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\196\196\196\196\196\196\196\196\196\196\000\000\000\000\000\000\
\\000\196\196\196\196\196\196\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\196\196\196\196\196\196\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (197, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\197\197\197\197\197\197\197\197\197\197\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (198, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\013\000\
\\198\198\198\198\198\198\198\198\198\198\000\000\000\000\000\000\
\\000\000\000\000\000\009\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\009\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (199, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\200\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (200, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\201\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (202, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\007\000\007\007\007\007\000\000\000\007\007\000\007\000\007\
\\000\000\000\000\000\000\000\000\000\000\007\000\007\007\203\007\
\\007\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\007\000\007\000\
\\007\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\007\000\007\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (207, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\208\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (209, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\210\000\000\000\000\000\000\000\000\
\\210\210\210\210\210\210\210\210\210\210\000\000\000\000\000\000\
\\000\210\210\210\210\210\210\210\210\210\210\210\210\210\210\210\
\\210\210\210\210\210\210\210\210\210\210\210\000\000\000\000\210\
\\000\210\210\210\210\210\210\210\210\210\210\210\210\210\210\210\
\\210\210\210\210\210\210\210\210\210\210\210\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (211, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\007\212\007\007\007\007\000\000\000\007\007\000\007\000\007\
\\000\000\000\000\000\000\000\000\000\000\007\000\007\007\007\007\
\\007\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\007\000\007\000\
\\007\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\007\000\007\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (212, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\213\213\000\213\213\213\213\213\213\213\213\213\213\213\213\213\
\\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\
\\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\
\\213\213\213\213\213\213\213\213\213\213\213\213\217\213\213\213\
\\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\
\\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\000\
\\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\
\\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\
\\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\
\\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\
\\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\
\\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\
\\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\
\\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213"
),
 (213, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\216\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\214\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (214, 
"\000\000\000\000\000\000\000\000\000\215\215\215\215\215\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\215\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (215, 
"\000\000\000\000\000\000\000\000\000\215\215\215\215\215\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\215\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\213\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (217, 
"\000\000\000\000\000\000\000\000\000\225\225\225\225\225\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\225\000\213\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\223\223\223\223\223\223\223\223\223\223\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\213\000\222\000\
\\000\213\213\000\000\000\213\000\000\000\000\000\000\000\213\000\
\\000\000\213\000\213\218\213\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (218, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\219\219\219\219\219\219\219\219\219\219\000\000\000\000\000\000\
\\000\219\219\219\219\219\219\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\219\219\219\219\219\219\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (219, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\220\220\220\220\220\220\220\220\220\220\000\000\000\000\000\000\
\\000\220\220\220\220\220\220\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\220\220\220\220\220\220\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (220, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\221\221\221\221\221\221\221\221\221\221\000\000\000\000\000\000\
\\000\221\221\221\221\221\221\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\221\221\221\221\221\221\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (221, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\213\213\213\213\213\213\213\213\213\213\000\000\000\000\000\000\
\\000\213\213\213\213\213\213\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\213\213\213\213\213\213\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (222, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\
\\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\213\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (223, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\224\224\224\224\224\224\224\224\224\224\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (224, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\213\213\213\213\213\213\213\213\213\213\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (225, 
"\000\000\000\000\000\000\000\000\000\225\225\225\225\225\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\225\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\212\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (226, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\227\227\237\227\227\227\227\227\227\227\227\227\227\227\227\227\
\\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\
\\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\
\\227\227\227\227\227\227\227\227\227\227\227\227\228\227\227\227\
\\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\
\\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\000\
\\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\
\\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\
\\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\
\\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\
\\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\
\\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\
\\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\
\\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227"
),
 (228, 
"\000\000\000\000\000\000\000\000\000\236\236\236\236\236\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\236\000\227\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\234\234\234\234\234\234\234\234\234\234\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\227\000\233\000\
\\000\227\227\000\000\000\227\000\000\000\000\000\000\000\227\000\
\\000\000\227\000\227\229\227\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (229, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\230\230\230\230\230\230\230\230\230\230\000\000\000\000\000\000\
\\000\230\230\230\230\230\230\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\230\230\230\230\230\230\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (230, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\231\231\231\231\231\231\231\231\231\231\000\000\000\000\000\000\
\\000\231\231\231\231\231\231\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\231\231\231\231\231\231\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (231, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\232\232\232\232\232\232\232\232\232\232\000\000\000\000\000\000\
\\000\232\232\232\232\232\232\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\232\232\232\232\232\232\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (232, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\227\227\227\227\227\227\227\227\227\227\000\000\000\000\000\000\
\\000\227\227\227\227\227\227\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\227\227\227\227\227\227\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (233, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\
\\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\227\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (234, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\235\235\235\235\235\235\235\235\235\235\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (235, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\227\227\227\227\227\227\227\227\227\227\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (236, 
"\000\000\000\000\000\000\000\000\000\236\236\236\236\236\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\236\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\227\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (238, 
"\000\000\000\000\000\000\000\000\000\239\239\239\239\239\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\239\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (241, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\242\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
 (243, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\244\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
),
(0, "")]
fun f x = x 
val s = map f (rev (tl (rev s))) 
exception LexHackingError 
fun look ((j,x)::r, i) = if i = j then x else look(r, i) 
  | look ([], i) = raise LexHackingError
fun g {fin=x, trans=i} = {fin=x, trans=look(s,i)} 
in Vector.fromList(map g 
[{fin = [], trans = 0},
{fin = [], trans = 1},
{fin = [], trans = 1},
{fin = [], trans = 3},
{fin = [], trans = 3},
{fin = [(N 478)], trans = 0},
{fin = [(N 442),(N 478)], trans = 6},
{fin = [(N 442)], trans = 7},
{fin = [(N 295)], trans = 8},
{fin = [], trans = 9},
{fin = [], trans = 10},
{fin = [(N 344)], trans = 10},
{fin = [(N 344)], trans = 12},
{fin = [], trans = 13},
{fin = [(N 344)], trans = 14},
{fin = [], trans = 15},
{fin = [], trans = 16},
{fin = [(N 344)], trans = 16},
{fin = [(N 344)], trans = 18},
{fin = [(N 295)], trans = 19},
{fin = [], trans = 20},
{fin = [(N 305)], trans = 20},
{fin = [(N 43),(N 478)], trans = 0},
{fin = [(N 41),(N 442),(N 478)], trans = 7},
{fin = [(N 39),(N 478)], trans = 0},
{fin = [(N 439),(N 478)], trans = 25},
{fin = [(N 439)], trans = 25},
{fin = [], trans = 27},
{fin = [(N 461)], trans = 28},
{fin = [(N 461)], trans = 29},
{fin = [(N 439),(N 478)], trans = 30},
{fin = [(N 439)], trans = 31},
{fin = [(N 439)], trans = 32},
{fin = [(N 273),(N 439)], trans = 33},
{fin = [(N 439)], trans = 34},
{fin = [(N 439)], trans = 35},
{fin = [(N 439)], trans = 36},
{fin = [(N 282),(N 439)], trans = 25},
{fin = [(N 439)], trans = 38},
{fin = [(N 439)], trans = 39},
{fin = [(N 439)], trans = 40},
{fin = [(N 268),(N 439)], trans = 25},
{fin = [(N 439)], trans = 42},
{fin = [(N 439)], trans = 43},
{fin = [(N 262),(N 439)], trans = 25},
{fin = [(N 439),(N 478)], trans = 45},
{fin = [(N 439)], trans = 46},
{fin = [(N 256),(N 439)], trans = 25},
{fin = [(N 439),(N 478)], trans = 48},
{fin = [(N 439)], trans = 49},
{fin = [(N 439)], trans = 50},
{fin = [(N 252),(N 439)], trans = 25},
{fin = [(N 439)], trans = 52},
{fin = [(N 439)], trans = 53},
{fin = [(N 247),(N 439)], trans = 25},
{fin = [(N 439),(N 478)], trans = 55},
{fin = [(N 439)], trans = 56},
{fin = [(N 439)], trans = 57},
{fin = [(N 439)], trans = 58},
{fin = [(N 439)], trans = 59},
{fin = [(N 232),(N 439)], trans = 60},
{fin = [(N 439)], trans = 61},
{fin = [(N 439)], trans = 62},
{fin = [(N 242),(N 439)], trans = 25},
{fin = [(N 439)], trans = 64},
{fin = [(N 215),(N 439)], trans = 65},
{fin = [(N 439)], trans = 66},
{fin = [(N 439)], trans = 67},
{fin = [(N 439)], trans = 68},
{fin = [(N 439)], trans = 69},
{fin = [(N 439)], trans = 70},
{fin = [(N 225),(N 439)], trans = 25},
{fin = [(N 439)], trans = 72},
{fin = [(N 439)], trans = 73},
{fin = [(N 439)], trans = 74},
{fin = [(N 439)], trans = 75},
{fin = [(N 439)], trans = 76},
{fin = [(N 211),(N 439)], trans = 25},
{fin = [(N 439),(N 478)], trans = 78},
{fin = [(N 439)], trans = 79},
{fin = [(N 203),(N 439)], trans = 25},
{fin = [(N 439)], trans = 81},
{fin = [(N 439)], trans = 82},
{fin = [(N 439)], trans = 83},
{fin = [(N 199),(N 439)], trans = 25},
{fin = [(N 439),(N 478)], trans = 85},
{fin = [(N 439)], trans = 86},
{fin = [(N 439)], trans = 87},
{fin = [(N 439)], trans = 88},
{fin = [(N 439)], trans = 89},
{fin = [(N 193),(N 439)], trans = 25},
{fin = [(N 181),(N 439)], trans = 91},
{fin = [(N 439)], trans = 92},
{fin = [(N 186),(N 439)], trans = 25},
{fin = [(N 178),(N 439)], trans = 25},
{fin = [(N 439),(N 478)], trans = 95},
{fin = [(N 439)], trans = 96},
{fin = [(N 439)], trans = 97},
{fin = [(N 439)], trans = 98},
{fin = [(N 439)], trans = 99},
{fin = [(N 175),(N 439)], trans = 25},
{fin = [(N 439),(N 478)], trans = 101},
{fin = [(N 439)], trans = 102},
{fin = [(N 439)], trans = 103},
{fin = [(N 439)], trans = 104},
{fin = [(N 168),(N 439)], trans = 25},
{fin = [(N 439)], trans = 106},
{fin = [(N 162),(N 439)], trans = 25},
{fin = [(N 439),(N 478)], trans = 108},
{fin = [(N 137),(N 439)], trans = 109},
{fin = [(N 439)], trans = 110},
{fin = [(N 439)], trans = 111},
{fin = [(N 151),(N 439)], trans = 112},
{fin = [(N 158),(N 439)], trans = 25},
{fin = [(N 439)], trans = 114},
{fin = [(N 439)], trans = 115},
{fin = [(N 439)], trans = 116},
{fin = [(N 439)], trans = 117},
{fin = [(N 145),(N 439)], trans = 25},
{fin = [(N 134),(N 439)], trans = 25},
{fin = [(N 439),(N 478)], trans = 120},
{fin = [(N 439)], trans = 121},
{fin = [(N 439)], trans = 122},
{fin = [(N 439)], trans = 123},
{fin = [(N 439)], trans = 124},
{fin = [(N 131),(N 439)], trans = 25},
{fin = [(N 439),(N 478)], trans = 126},
{fin = [(N 439)], trans = 127},
{fin = [(N 116),(N 439)], trans = 128},
{fin = [(N 439)], trans = 129},
{fin = [(N 439)], trans = 130},
{fin = [(N 439)], trans = 131},
{fin = [(N 124),(N 439)], trans = 25},
{fin = [(N 112),(N 439)], trans = 25},
{fin = [(N 439),(N 478)], trans = 134},
{fin = [(N 439)], trans = 135},
{fin = [(N 439)], trans = 136},
{fin = [(N 439)], trans = 137},
{fin = [(N 439)], trans = 138},
{fin = [(N 439)], trans = 139},
{fin = [(N 439)], trans = 140},
{fin = [(N 439)], trans = 141},
{fin = [(N 109),(N 439)], trans = 25},
{fin = [(N 439)], trans = 143},
{fin = [(N 439)], trans = 144},
{fin = [(N 439)], trans = 145},
{fin = [(N 439)], trans = 146},
{fin = [(N 99),(N 439)], trans = 25},
{fin = [(N 439)], trans = 148},
{fin = [(N 92),(N 439)], trans = 25},
{fin = [(N 439)], trans = 150},
{fin = [(N 439)], trans = 151},
{fin = [(N 88),(N 439)], trans = 25},
{fin = [(N 439),(N 478)], trans = 153},
{fin = [(N 83),(N 439)], trans = 25},
{fin = [(N 439)], trans = 155},
{fin = [(N 439)], trans = 156},
{fin = [(N 439)], trans = 157},
{fin = [(N 439)], trans = 158},
{fin = [(N 439)], trans = 159},
{fin = [(N 439)], trans = 160},
{fin = [(N 80),(N 439)], trans = 25},
{fin = [(N 439),(N 478)], trans = 162},
{fin = [(N 439)], trans = 163},
{fin = [(N 439)], trans = 164},
{fin = [(N 71),(N 439)], trans = 25},
{fin = [(N 439),(N 478)], trans = 166},
{fin = [(N 66),(N 439)], trans = 25},
{fin = [(N 439)], trans = 168},
{fin = [(N 55),(N 439)], trans = 169},
{fin = [(N 439)], trans = 170},
{fin = [(N 439)], trans = 171},
{fin = [(N 439)], trans = 172},
{fin = [(N 63),(N 439)], trans = 25},
{fin = [(N 439)], trans = 174},
{fin = [(N 439)], trans = 175},
{fin = [(N 439)], trans = 176},
{fin = [(N 439)], trans = 177},
{fin = [(N 439)], trans = 178},
{fin = [(N 51),(N 439)], trans = 25},
{fin = [(N 442),(N 478)], trans = 7},
{fin = [(N 37),(N 478)], trans = 0},
{fin = [(N 35),(N 478)], trans = 0},
{fin = [(N 33),(N 478)], trans = 0},
{fin = [(N 28),(N 442),(N 478)], trans = 184},
{fin = [(N 31),(N 442)], trans = 7},
{fin = [(N 26),(N 478)], trans = 0},
{fin = [(N 21),(N 442),(N 478)], trans = 187},
{fin = [(N 24),(N 442)], trans = 7},
{fin = [(N 286),(N 289),(N 295),(N 478)], trans = 189},
{fin = [(N 289),(N 295)], trans = 189},
{fin = [(N 284),(N 295),(N 478)], trans = 191},
{fin = [], trans = 192},
{fin = [(N 305)], trans = 192},
{fin = [], trans = 194},
{fin = [], trans = 195},
{fin = [(N 316)], trans = 195},
{fin = [(N 310)], trans = 197},
{fin = [(N 295)], trans = 198},
{fin = [(N 478)], trans = 199},
{fin = [], trans = 200},
{fin = [(N 19)], trans = 0},
{fin = [(N 442),(N 478)], trans = 202},
{fin = [(N 15),(N 442)], trans = 7},
{fin = [(N 12),(N 478)], trans = 0},
{fin = [(N 10),(N 442),(N 478)], trans = 7},
{fin = [(N 8),(N 478)], trans = 0},
{fin = [(N 6),(N 478)], trans = 207},
{fin = [(N 464)], trans = 0},
{fin = [(N 434),(N 478)], trans = 209},
{fin = [(N 434)], trans = 209},
{fin = [(N 4),(N 442),(N 478)], trans = 211},
{fin = [], trans = 212},
{fin = [], trans = 213},
{fin = [], trans = 214},
{fin = [], trans = 215},
{fin = [(N 429)], trans = 0},
{fin = [], trans = 217},
{fin = [], trans = 218},
{fin = [], trans = 219},
{fin = [], trans = 220},
{fin = [], trans = 221},
{fin = [], trans = 222},
{fin = [], trans = 223},
{fin = [], trans = 224},
{fin = [], trans = 225},
{fin = [(N 476),(N 478)], trans = 226},
{fin = [], trans = 226},
{fin = [], trans = 228},
{fin = [], trans = 229},
{fin = [], trans = 230},
{fin = [], trans = 231},
{fin = [], trans = 232},
{fin = [], trans = 233},
{fin = [], trans = 234},
{fin = [], trans = 235},
{fin = [], trans = 236},
{fin = [(N 384)], trans = 0},
{fin = [(N 2),(N 478)], trans = 238},
{fin = [(N 2)], trans = 238},
{fin = [(N 472)], trans = 0},
{fin = [(N 472)], trans = 241},
{fin = [(N 470)], trans = 0},
{fin = [(N 472)], trans = 243},
{fin = [(N 467)], trans = 0},
{fin = [(N 474)], trans = 0}])
end
structure StartStates =
	struct
	datatype yystartstate = STARTSTATE of int

(* start state definitions *)

val COMMENT = STARTSTATE 3;
val INITIAL = STARTSTATE 1;

end
type result = UserDeclarations.lexresult
	exception LexerError (* raised if illegal leaf action tried *)
end

fun makeLexer yyinput =
let	val yygone0=1
	val yyb = ref "\n" 		(* buffer *)
	val yybl = ref 1		(*buffer length *)
	val yybufpos = ref 1		(* location of next character to use *)
	val yygone = ref yygone0	(* position in file of beginning of buffer *)
	val yydone = ref false		(* eof found yet? *)
	val yybegin = ref 1		(*Current 'start state' for lexer *)

	val YYBEGIN = fn (Internal.StartStates.STARTSTATE x) =>
		 yybegin := x

fun lex () : Internal.result =
let fun continue() = lex() in
  let fun scan (s,AcceptingLeaves : Internal.yyfinstate list list,l,i0) =
	let fun action (i,nil) = raise LexError
	| action (i,nil::l) = action (i-1,l)
	| action (i,(node::acts)::l) =
		case node of
		    Internal.N yyk => 
			(let val yytext = substring(!yyb,i0,i-i0)
			     val yypos = i0+ !yygone
			open UserDeclarations Internal.StartStates
 in (yybufpos := i; case yyk of 

			(* Application actions *)

  10 => ( token(STAR,      yypos, yytext) )
| 109 => ( token(EXCEPTION, yypos, yytext) )
| 112 => ( token(FN,        yypos, yytext) )
| 116 => ( token(FUN,       yypos, yytext) )
| 12 => ( token(COMMA,     yypos, yytext) )
| 124 => ( token(FUNCTOR,   yypos, yytext) )
| 131 => ( token(HANDLE,    yypos, yytext) )
| 134 => ( token(IF,        yypos, yytext) )
| 137 => ( token(IN,        yypos, yytext) )
| 145 => ( token(INCLUDE,   yypos, yytext) )
| 15 => ( token(ARROW,     yypos, yytext) )
| 151 => ( token(INFIX,     yypos, yytext) )
| 158 => ( token(INFIXR,    yypos, yytext) )
| 162 => ( token(LET,       yypos, yytext) )
| 168 => ( token(LOCAL,     yypos, yytext) )
| 175 => ( token(NONFIX,    yypos, yytext) )
| 178 => ( token(OF,        yypos, yytext) )
| 181 => ( token(OP,        yypos, yytext) )
| 186 => ( token(OPEN,      yypos, yytext) )
| 19 => ( token(DOTS,      yypos, yytext) )
| 193 => ( token(ORELSE,    yypos, yytext) )
| 199 => ( token(RAISE,     yypos, yytext) )
| 2 => ( continue() )
| 203 => ( token(REC,       yypos, yytext) )
| 21 => ( token(COLON,     yypos, yytext) )
| 211 => ( token(SHARING,   yypos, yytext) )
| 215 => ( token(SIG,       yypos, yytext) )
| 225 => ( token(SIGNATURE, yypos, yytext) )
| 232 => ( token(STRUCT,    yypos, yytext) )
| 24 => ( token(SEAL,      yypos, yytext) )
| 242 => ( token(STRUCTURE, yypos, yytext) )
| 247 => ( token(THEN,      yypos, yytext) )
| 252 => ( token(TYPE,      yypos, yytext) )
| 256 => ( token(VAL,       yypos, yytext) )
| 26 => ( token(SEMICOLON, yypos, yytext) )
| 262 => ( token(WHERE,     yypos, yytext) )
| 268 => ( token(WHILE,     yypos, yytext) )
| 273 => ( token(WITH,      yypos, yytext) )
| 28 => ( token(EQUALS,    yypos, yytext) )
| 282 => ( token(WITHTYPE,  yypos, yytext) )
| 284 => ( token  (ZERO,              yypos, yytext) )
| 286 => ( tokenOf(DIGIT,   toInt,    yypos, yytext) )
| 289 => ( tokenOf(NUMERIC, toInt,    yypos, yytext) )
| 295 => ( tokenOf(INT,     toInt,    yypos, yytext) )
| 305 => ( tokenOf(HEXINT,  toHexInt, yypos, yytext) )
| 31 => ( token(DARROW,    yypos, yytext) )
| 310 => ( tokenOf(WORD,    toWord,   yypos, yytext) )
| 316 => ( tokenOf(HEXWORD, toHexWord,yypos, yytext) )
| 33 => ( token(LBRACK,    yypos, yytext) )
| 344 => ( tokenOf(REAL,    toReal,   yypos, yytext) )
| 35 => ( token(RBRACK,    yypos, yytext) )
| 37 => ( token(UNDERBAR,  yypos, yytext) )
| 384 => ( tokenOf(STRING,  toString, yypos, yytext) )
| 39 => ( token(LBRACE,    yypos, yytext) )
| 4 => ( token(HASH,      yypos, yytext) )
| 41 => ( token(BAR,       yypos, yytext) )
| 429 => ( tokenOf(CHAR,    toChar,   yypos, yytext) )
| 43 => ( token(RBRACE,    yypos, yytext) )
| 434 => ( tokenOf(TYVAR,   toId,     yypos, yytext) )
| 439 => ( tokenOf(ALPHA,   toId,     yypos, yytext) )
| 442 => ( tokenOf(SYMBOL,  toId,     yypos, yytext) )
| 461 => ( tokenOf(LONGID,  toLongId, yypos, yytext) )
| 464 => ( nesting := 1 ; YYBEGIN COMMENT ; continue() )
| 467 => ( nesting := !nesting+1 ; continue() )
| 470 => ( nesting := !nesting-1 ;
			     if !nesting = 0 then YYBEGIN INITIAL else () ;
			     continue() )
| 472 => ( continue() )
| 474 => ( continue() )
| 476 => ( error(yypos, yytext, "invalid string") )
| 478 => ( invalid(yypos, yytext) )
| 51 => ( token(ABSTYPE,   yypos, yytext) )
| 55 => ( token(AND,       yypos, yytext) )
| 6 => ( token(LPAR,      yypos, yytext) )
| 63 => ( token(ANDALSO,   yypos, yytext) )
| 66 => ( token(AS,        yypos, yytext) )
| 71 => ( token(CASE,      yypos, yytext) )
| 8 => ( token(RPAR,      yypos, yytext) )
| 80 => ( token(DATATYPE,  yypos, yytext) )
| 83 => ( token(DO,        yypos, yytext) )
| 88 => ( token(ELSE,      yypos, yytext) )
| 92 => ( token(END,       yypos, yytext) )
| 99 => ( token(EQTYPE,    yypos, yytext) )
| _ => raise Internal.LexerError

		) end )

	val {fin,trans} = Vector.sub(Internal.tab, s)
	val NewAcceptingLeaves = fin::AcceptingLeaves
	in if l = !yybl then
	     if trans = #trans(Vector.sub(Internal.tab,0))
	       then action(l,NewAcceptingLeaves
) else	    let val newchars= if !yydone then "" else yyinput 1024
	    in if (size newchars)=0
		  then (yydone := true;
		        if (l=i0) then UserDeclarations.eof ()
		                  else action(l,NewAcceptingLeaves))
		  else (if i0=l then yyb := newchars
		     else yyb := substring(!yyb,i0,l-i0)^newchars;
		     yygone := !yygone+i0;
		     yybl := size (!yyb);
		     scan (s,AcceptingLeaves,l-i0,0))
	    end
	  else let val NewChar = Char.ord(String.sub(!yyb,l))
		val NewState = Char.ord(String.sub(trans,NewChar))
		in if NewState=0 then action(l,NewAcceptingLeaves)
		else scan(NewState,NewAcceptingLeaves,l+1,i0)
	end
	end
(*
	val start= if substring(!yyb,!yybufpos-1,1)="\n"
then !yybegin+1 else !yybegin
*)
	in scan(!yybegin (* start *),nil,!yybufpos,!yybufpos)
    end
end
  in lex
  end
end
;
(* queue-sig.sml
 *
 * COPYRIGHT (c) 1993 by AT&T Bell Laboratories.  See COPYRIGHT file for details.
 *
 * Imperative fifos
 *
 *)

signature QUEUE =
  sig
    type 'a queue

    exception Dequeue

    val mkQueue : unit -> 'a queue
	(* make a new queue *)
    val clear : 'a queue -> unit
	(* remove all elements *)
    val isEmpty : 'a queue -> bool
	(* test for empty queue *)
    val enqueue : 'a queue * 'a -> unit
	(* enqueue an element at the rear *)
    val dequeue : 'a queue -> 'a
	(* remove the front element (raise Dequeue if empty) *)
    val delete : ('a queue * ('a -> bool)) -> unit
	(* delete all elements satisfying the given predicate *)
    val head : 'a queue -> 'a
    val peek : 'a queue -> 'a option
    val length : 'a queue -> int
    val contents : 'a queue -> 'a list
    val app : ('a -> unit) -> 'a queue -> unit
    val map : ('a -> 'b) -> 'a queue -> 'b queue
    val foldl : ('a * 'b -> 'b) -> 'b -> 'a queue -> 'b
    val foldr : ('a * 'b -> 'b) -> 'b -> 'a queue -> 'b

  end
(* fifo-sig.sml
 *
 * COPYRIGHT (c) 1993 by AT&T Bell Laboratories.  See COPYRIGHT file for details.
 *
 * Applicative fifos
 *
 *)

signature FIFO =
  sig
    type 'a fifo

    exception Dequeue

    val empty : 'a fifo
    val isEmpty : 'a fifo -> bool
    val enqueue : 'a fifo * 'a -> 'a fifo
    val dequeue : 'a fifo -> 'a fifo * 'a
    val delete : ('a fifo * ('a -> bool)) -> 'a fifo
    val head : 'a fifo -> 'a
    val peek : 'a fifo -> 'a option
    val length : 'a fifo -> int
    val contents : 'a fifo -> 'a list
    val app : ('a -> unit) -> 'a fifo -> unit
    val map : ('a -> 'b) -> 'a fifo -> 'b fifo
    val foldl : ('a * 'b -> 'b) -> 'b -> 'a fifo -> 'b
    val foldr : ('a * 'b -> 'b) -> 'b -> 'a fifo -> 'b

  end (* FIFO *)
(* fifo.sml
 *
 * COPYRIGHT (c) 1993 by AT&T Bell Laboratories.  See COPYRIGHT file for details.
 *
 * Applicative fifos
 *
 *)

structure Fifo : FIFO =
  struct
    datatype 'a fifo = Q of {front: 'a list, rear: 'a list}

    exception Dequeue

    val empty = Q{front=[],rear=[]}

    fun isEmpty (Q{front=[],rear=[]}) = true
      | isEmpty _ = false

    fun enqueue (Q{front,rear},x) = Q{front=front,rear=(x::rear)}

    fun dequeue (Q{front=(hd::tl),rear}) = (Q{front=tl,rear=rear},hd)
      | dequeue (Q{rear=[],...}) = raise Dequeue
      | dequeue (Q{rear,...}) = dequeue(Q{front=rev rear,rear=[]})

    fun delete (Q{front, rear}, pred) = let
	  fun doFront [] = {front = doRear(rev rear), rear = []}
	    | doFront (x::r) = if (pred x)
		then {front = r, rear = rear}
		else let val {front, rear} = doFront r
		  in {front =  x :: front, rear = rear} end
	  and doRear [] = []
	    | doRear (x::r) = if (pred x) then r else x :: (doRear r)
	  in
	    Q(doFront front)
	  end

    fun peek (Q{front=(hd::_), ...}) = SOME hd
      | peek (Q{rear=[], ...}) = NONE
      | peek (Q{rear, ...}) = SOME(hd(rev rear))

    fun head (Q{front=(hd::_),...}) = hd
      | head (Q{rear=[],...}) = raise Dequeue
      | head (Q{rear,...}) = hd(rev rear)

    fun length (Q {rear,front}) = (List.length rear) + (List.length front)

    fun contents (Q {rear, front}) = (front @ (rev rear))

    fun app f (Q{front,rear}) = (List.app f front; List.app f (List.rev rear))
    fun map f (Q{front,rear}) = 
          Q{front = List.map f front, rear = rev(List.map f(rev rear))}
    fun foldl f b (Q{front,rear}) = List.foldr f (List.foldl f b front) rear
    fun foldr f b (Q{front,rear}) = List.foldr f (List.foldl f b rear) front

  end

(* queue.sml
 *
 * COPYRIGHT (c) 1993 by AT&T Bell Laboratories.  See COPYRIGHT file for details.
 *
 * Imperative fifos
 *
 *)

structure Queue :> QUEUE =
  struct
    type 'a queue = 'a Fifo.fifo ref

    exception Dequeue = Fifo.Dequeue

    fun mkQueue () = ref Fifo.empty

    fun clear q = (q := Fifo.empty)

    fun enqueue (q,x) = q := (Fifo.enqueue (!q, x))

    fun dequeue q = let 
          val (newq, x) = Fifo.dequeue (!q) 
          in
            q := newq;
            x
          end
  
    fun delete (q, pred) = (q := Fifo.delete (!q, pred))
    fun head q = Fifo.head (!q)
    fun peek q = Fifo.peek (!q)
    fun isEmpty q = Fifo.isEmpty (!q)
    fun length q = Fifo.length (!q)
    fun contents q = Fifo.contents (!q)
    fun app f q = Fifo.app f (!q)
    fun map f q = ref(Fifo.map f (!q))
    fun foldl f b q = Fifo.foldl f b (!q)
    fun foldr f b q = Fifo.foldr f b (!q)

  end
(* ML-Yacc Parser Generator (c) 1989 Andrew W. Appel, David R. Tarditi 
 *
 * $Log$
 * Revision 1.1  2000/11/24 13:32:28  rossberg
 * *** empty log message ***
 *
 * Revision 1.1.1.1  1997/01/14 01:38:04  george
 *   Version 109.24
 *
 * Revision 1.1.1.1  1996/01/31  16:01:42  george
 * Version 109
 * 
 *)

structure LrTable : LR_TABLE = 
    struct
	open Array
	infix 9 sub
	datatype ('a,'b) pairlist = EMPTY
				  | PAIR of 'a * 'b * ('a,'b) pairlist
	datatype term = T of int
	datatype nonterm = NT of int
	datatype state = STATE of int
	datatype action = SHIFT of state
			| REDUCE of int (* rulenum from grammar *)
			| ACCEPT
			| ERROR
	exception Goto of state * nonterm
	type table = {states: int, rules : int,initialState: state,
		      action: ((term,action) pairlist * action) array,
		      goto :  (nonterm,state) pairlist array}
	val numStates = fn ({states,...} : table) => states
	val numRules = fn ({rules,...} : table) => rules
	val describeActions =
	   fn ({action,...} : table) => 
	           fn (STATE s) => action sub s
	val describeGoto =
	   fn ({goto,...} : table) =>
	           fn (STATE s) => goto sub s
	fun findTerm (T term,row,default) =
	    let fun find (PAIR (T key,data,r)) =
		       if key < term then find r
		       else if key=term then data
		       else default
		   | find EMPTY = default
	    in find row
	    end
	fun findNonterm (NT nt,row) =
	    let fun find (PAIR (NT key,data,r)) =
		       if key < nt then find r
		       else if key=nt then SOME data
		       else NONE
		   | find EMPTY = NONE
	    in find row
	    end
	val action = fn ({action,...} : table) =>
		fn (STATE state,term) =>
		  let val (row,default) = action sub state
		  in findTerm(term,row,default)
		  end
	val goto = fn ({goto,...} : table) =>
			fn (a as (STATE state,nonterm)) =>
			  case findNonterm(nonterm,goto sub state)
			  of SOME state => state
			   | NONE => raise (Goto a)
	val initialState = fn ({initialState,...} : table) => initialState
	val mkLrTable = fn {actions,gotos,initialState,numStates,numRules} =>
	     ({action=actions,goto=gotos,
	       states=numStates,
	       rules=numRules,
               initialState=initialState} : table)
end;
(* ML-Yacc Parser Generator (c) 1989 Andrew W. Appel, David R. Tarditi 
 *
 * $Log$
 * Revision 1.1  2000/11/24 13:32:28  rossberg
 * *** empty log message ***
 *
 * Revision 1.2  1997/08/26 19:18:55  jhr
 *   Replaced used of "abstraction" with ":>".
 *
# Revision 1.1.1.1  1997/01/14  01:38:04  george
#   Version 109.24
#
 * Revision 1.1.1.1  1996/01/31  16:01:43  george
 * Version 109
 * 
 *)

(* Stream: a structure implementing a lazy stream.  The signature STREAM
   is found in base.sig *)

structure Stream :> STREAM =
struct
   datatype 'a str = EVAL of 'a * 'a str ref | UNEVAL of (unit->'a)

   type 'a stream = 'a str ref

   fun get s =
       case !s of EVAL t => t
	        | UNEVAL f =>
		  let val t = (f(), ref(UNEVAL f)) in s := EVAL t; t end

   fun streamify f = ref(UNEVAL f)
   fun cons(a,s) = ref(EVAL(a,s))

end;
(* ML-Yacc Parser Generator (c) 1989 Andrew W. Appel, David R. Tarditi 
 *
 * $Log$
 * Revision 1.1  2000/11/24 13:32:28  rossberg
 * *** empty log message ***
 *
 * Revision 1.2  1997/08/26 19:18:54  jhr
 *   Replaced used of "abstraction" with ":>".
 *
# Revision 1.1.1.1  1997/01/14  01:38:04  george
#   Version 109.24
#
 * Revision 1.3  1996/10/03  03:36:58  jhr
 * Qualified identifiers that are no-longer top-level (quot, rem, min, max).
 *
 * Revision 1.2  1996/02/26  15:02:29  george
 *    print no longer overloaded.
 *    use of makestring has been removed and replaced with Int.toString ..
 *    use of IO replaced with TextIO
 *
 * Revision 1.1.1.1  1996/01/31  16:01:42  george
 * Version 109
 * 
 *)

(* parser.sml:  This is a parser driver for LR tables with an error-recovery
   routine added to it.  The routine used is described in detail in this
   article:

	'A Practical Method for LR and LL Syntactic Error Diagnosis and
	 Recovery', by M. Burke and G. Fisher, ACM Transactions on
	 Programming Langauges and Systems, Vol. 9, No. 2, April 1987,
	 pp. 164-197.

    This program is an implementation is the partial, deferred method discussed
    in the article.  The algorithm and data structures used in the program
    are described below.  

    This program assumes that all semantic actions are delayed.  A semantic
    action should produce a function from unit -> value instead of producing the
    normal value.  The parser returns the semantic value on the top of the
    stack when accept is encountered.  The user can deconstruct this value
    and apply the unit -> value function in it to get the answer.

    It also assumes that the lexer is a lazy stream.

    Data Structures:
    ----------------
	
	* The parser:

	   The state stack has the type

		 (state * (semantic value * line # * line #)) list

	   The parser keeps a queue of (state stack * lexer pair).  A lexer pair
	 consists of a terminal * value pair and a lexer.  This allows the 
	 parser to reconstruct the states for terminals to the left of a
	 syntax error, and attempt to make error corrections there.

	   The queue consists of a pair of lists (x,y).  New additions to
	 the queue are cons'ed onto y.  The first element of x is the top
	 of the queue.  If x is nil, then y is reversed and used
	 in place of x.

    Algorithm:
    ----------

	* The steady-state parser:  

	    This parser keeps the length of the queue of state stacks at
	a steady state by always removing an element from the front when
	another element is placed on the end.

	    It has these arguments:

	   stack: current stack
	   queue: value of the queue
	   lexPair ((terminal,value),lex stream)

	When SHIFT is encountered, the state to shift to and the value are
	are pushed onto the state stack.  The state stack and lexPair are
	placed on the queue.  The front element of the queue is removed.

	When REDUCTION is encountered, the rule is applied to the current
	stack to yield a triple (nonterm,value,new stack).  A new
	stack is formed by adding (goto(top state of stack,nonterm),value)
	to the stack.

	When ACCEPT is encountered, the top value from the stack and the
	lexer are returned.

	When an ERROR is encountered, fixError is called.  FixError
	takes the arguments to the parser, fixes the error if possible and
        returns a new set of arguments.

	* The distance-parser:

	This parser includes an additional argument distance.  It pushes
	elements on the queue until it has parsed distance tokens, or an
	ACCEPT or ERROR occurs.  It returns a stack, lexer, the number of
	tokens left unparsed, a queue, and an action option.
*)

signature FIFO = 
  sig type 'a queue
      val empty : 'a queue
      exception Empty
      val get : 'a queue -> 'a * 'a queue
      val put : 'a * 'a queue -> 'a queue
  end

(* drt (12/15/89) -- the functor should be used in development work, but
   it wastes space in the release version.

functor ParserGen(structure LrTable : LR_TABLE
		  structure Stream : STREAM) : LR_PARSER =
*)

structure LrParser :> LR_PARSER =
   struct
      structure LrTable = LrTable
      structure Stream = Stream

      structure Token : TOKEN =
	struct
	    structure LrTable = LrTable
	    datatype ('a,'b) token = TOKEN of LrTable.term * ('a * 'b * 'b)
	    val sameToken = fn (TOKEN(t,_),TOKEN(t',_)) => t=t'
        end

      open LrTable
      open Token

      val DEBUG1 = false
      val DEBUG2 = false
      exception ParseError
      exception ParseImpossible of int

      structure Fifo :> FIFO =
        struct
	  type 'a queue = ('a list * 'a list)
	  val empty = (nil,nil)
	  exception Empty
	  fun get(a::x, y) = (a, (x,y))
	    | get(nil, nil) = raise Empty
	    | get(nil, y) = get(rev y, nil)
 	  fun put(a,(x,y)) = (x,a::y)
        end

      type ('a,'b) elem = (state * ('a * 'b * 'b))
      type ('a,'b) stack = ('a,'b) elem list
      type ('a,'b) lexv = ('a,'b) token
      type ('a,'b) lexpair = ('a,'b) lexv * (('a,'b) lexv Stream.stream)
      type ('a,'b) distanceParse =
		 ('a,'b) lexpair *
		 ('a,'b) stack * 
		 (('a,'b) stack * ('a,'b) lexpair) Fifo.queue *
		 int ->
		   ('a,'b) lexpair *
		   ('a,'b) stack * 
		   (('a,'b) stack * ('a,'b) lexpair) Fifo.queue *
		   int *
		   action option

      type ('a,'b) ecRecord =
	 {is_keyword : term -> bool,
          preferred_change : (term list * term list) list,
	  error : string * 'b * 'b -> unit,
	  errtermvalue : term -> 'a,
	  terms : term list,
	  showTerminal : term -> string,
	  noShift : term -> bool}

      local 
	 val print = fn s => TextIO.output(TextIO.stdOut,s)
	 val println = fn s => (print s; print "\n")
	 val showState = fn (STATE s) => "STATE " ^ (Int.toString s)
      in
        fun printStack(stack: ('a,'b) stack, n: int) =
         case stack
           of (state,_) :: rest =>
                 (print("\t" ^ Int.toString n ^ ": ");
                  println(showState state);
                  printStack(rest, n+1))
            | nil => ()
                
        fun prAction showTerminal
		 (stack as (state,_) :: _, next as (TOKEN (term,_),_), action) =
             (println "Parse: state stack:";
              printStack(stack, 0);
              print("       state="
                         ^ showState state	
                         ^ " next="
                         ^ showTerminal term
                         ^ " action="
                        );
              case action
                of SHIFT state => println ("SHIFT " ^ (showState state))
                 | REDUCE i => println ("REDUCE " ^ (Int.toString i))
                 | ERROR => println "ERROR"
		 | ACCEPT => println "ACCEPT")
        | prAction _ (_,_,action) = ()
     end

    (* ssParse: parser which maintains the queue of (state * lexvalues) in a
	steady-state.  It takes a table, showTerminal function, saction
	function, and fixError function.  It parses until an ACCEPT is
	encountered, or an exception is raised.  When an error is encountered,
	fixError is called with the arguments of parseStep (lexv,stack,and
	queue).  It returns the lexv, and a new stack and queue adjusted so
	that the lexv can be parsed *)
	
    val ssParse =
      fn (table,showTerminal,saction,fixError,arg) =>
	let val prAction = prAction showTerminal
	    val action = LrTable.action table
	    val goto = LrTable.goto table
	    fun parseStep(args as
			 (lexPair as (TOKEN (terminal, value as (_,leftPos,_)),
				      lexer
				      ),
			  stack as (state,_) :: _,
			  queue)) =
	      let val nextAction = action (state,terminal)
	          val _ = if DEBUG1 then prAction(stack,lexPair,nextAction)
			  else ()
	      in case nextAction
		 of SHIFT s =>
		  let val newStack = (s,value) :: stack
		      val newLexPair = Stream.get lexer
		      val (_,newQueue) =Fifo.get(Fifo.put((newStack,newLexPair),
							    queue))
		  in parseStep(newLexPair,(s,value)::stack,newQueue)
		  end
		 | REDUCE i =>
		     (case saction(i,leftPos,stack,arg)
		      of (nonterm,value,stack as (state,_) :: _) =>
		          parseStep(lexPair,(goto(state,nonterm),value)::stack,
				    queue)
		       | _ => raise (ParseImpossible 197))
		 | ERROR => parseStep(fixError args)
		 | ACCEPT => 
			(case stack
			 of (_,(topvalue,_,_)) :: _ =>
				let val (token,restLexer) = lexPair
				in (topvalue,Stream.cons(token,restLexer))
				end
			  | _ => raise (ParseImpossible 202))
	      end
	    | parseStep _ = raise (ParseImpossible 204)
	in parseStep
	end

    (*  distanceParse: parse until n tokens are shifted, or accept or
	error are encountered.  Takes a table, showTerminal function, and
	semantic action function.  Returns a parser which takes a lexPair
	(lex result * lexer), a state stack, a queue, and a distance
	(must be > 0) to parse.  The parser returns a new lex-value, a stack
	with the nth token shifted on top, a queue, a distance, and action
	option. *)

    val distanceParse =
      fn (table,showTerminal,saction,arg) =>
	let val prAction = prAction showTerminal
	    val action = LrTable.action table
	    val goto = LrTable.goto table
	    fun parseStep(lexPair,stack,queue,0) = (lexPair,stack,queue,0,NONE)
	      | parseStep(lexPair as (TOKEN (terminal, value as (_,leftPos,_)),
				      lexer
				     ),
			  stack as (state,_) :: _,
			  queue,distance) =
	      let val nextAction = action(state,terminal)
	          val _ = if DEBUG1 then prAction(stack,lexPair,nextAction)
			  else ()
	      in case nextAction
		 of SHIFT s =>
		  let val newStack = (s,value) :: stack
		      val newLexPair = Stream.get lexer
		  in parseStep(newLexPair,(s,value)::stack,
			       Fifo.put((newStack,newLexPair),queue),distance-1)
		  end
		 | REDUCE i =>
		    (case saction(i,leftPos,stack,arg)
		      of (nonterm,value,stack as (state,_) :: _) =>
		         parseStep(lexPair,(goto(state,nonterm),value)::stack,
				 queue,distance)
		      | _ => raise (ParseImpossible 240))
		 | ERROR => (lexPair,stack,queue,distance,SOME nextAction)
		 | ACCEPT => (lexPair,stack,queue,distance,SOME nextAction)
	      end
	   | parseStep _ = raise (ParseImpossible 242)
	in parseStep : ('_a,'_b) distanceParse 
	end

(* mkFixError: function to create fixError function which adjusts parser state
   so that parse may continue in the presence of an error *)

fun mkFixError({is_keyword,terms,errtermvalue,
	      preferred_change,noShift,
	      showTerminal,error,...} : ('_a,'_b) ecRecord,
	     distanceParse : ('_a,'_b) distanceParse,
	     minAdvance,maxAdvance) 

            (lexv as (TOKEN (term,value as (_,leftPos,_)),_),stack,queue) =
    let val _ = if DEBUG2 then
			error("syntax error found at " ^ (showTerminal term),
			      leftPos,leftPos)
		else ()

        fun tokAt(t,p) = TOKEN(t,(errtermvalue t,p,p))

	val minDelta = 3

	(* pull all the state * lexv elements from the queue *)

	val stateList = 
	   let fun f q = let val (elem,newQueue) = Fifo.get q
			 in elem :: (f newQueue)
			 end handle Fifo.Empty => nil
	   in f queue
	   end

	(* now number elements of stateList, giving distance from
	   error token *)

	val (_, numStateList) =
	      List.foldr (fn (a,(num,r)) => (num+1,(a,num)::r)) (0, []) stateList

	(* Represent the set of potential changes as a linked list.

	   Values of datatype Change hold information about a potential change.

	   oper = oper to be applied
	   pos = the # of the element in stateList that would be altered.
	   distance = the number of tokens beyond the error token which the
	     change allows us to parse.
	   new = new terminal * value pair at that point
	   orig = original terminal * value pair at the point being changed.
	 *)

	datatype ('a,'b) change = CHANGE of
	   {pos : int, distance : int, leftPos: 'b, rightPos: 'b,
	    new : ('a,'b) lexv list, orig : ('a,'b) lexv list}


         val showTerms = concat o map (fn TOKEN(t,_) => " " ^ showTerminal t)

	 val printChange = fn c =>
	  let val CHANGE {distance,new,orig,pos,...} = c
	  in (print ("{distance= " ^ (Int.toString distance));
	      print (",orig ="); print(showTerms orig);
	      print (",new ="); print(showTerms new);
	      print (",pos= " ^ (Int.toString pos));
	      print "}\n")
	  end

	val printChangeList = app printChange

(* parse: given a lexPair, a stack, and the distance from the error
   token, return the distance past the error token that we are able to parse.*)

	fun parse (lexPair,stack,queuePos : int) =
	    case distanceParse(lexPair,stack,Fifo.empty,queuePos+maxAdvance+1)
             of (_,_,_,distance,SOME ACCEPT) => 
		        if maxAdvance-distance-1 >= 0 
			    then maxAdvance 
			    else maxAdvance-distance-1
	      | (_,_,_,distance,_) => maxAdvance - distance - 1

(* catList: concatenate results of scanning list *)

	fun catList l f = List.foldr (fn(a,r)=> f a @ r) [] l

        fun keywordsDelta new = if List.exists (fn(TOKEN(t,_))=>is_keyword t) new
	               then minDelta else 0

        fun tryChange{lex,stack,pos,leftPos,rightPos,orig,new} =
	     let val lex' = List.foldr (fn (t',p)=>(t',Stream.cons p)) lex new
		 val distance = parse(lex',stack,pos+length new-length orig)
	      in if distance >= minAdvance + keywordsDelta new 
		   then [CHANGE{pos=pos,leftPos=leftPos,rightPos=rightPos,
				distance=distance,orig=orig,new=new}] 
		   else []
	     end


(* tryDelete: Try to delete n terminals.
              Return single-element [success] or nil.
	      Do not delete unshiftable terminals. *)


    fun tryDelete n ((stack,lexPair as (TOKEN(term,(_,l,r)),_)),qPos) =
	let fun del(0,accum,left,right,lexPair) =
	          tryChange{lex=lexPair,stack=stack,
			    pos=qPos,leftPos=left,rightPos=right,
			    orig=rev accum, new=[]}
	      | del(n,accum,left,right,(tok as TOKEN(term,(_,_,r)),lexer)) =
		   if noShift term then []
		   else del(n-1,tok::accum,left,r,Stream.get lexer)
         in del(n,[],l,r,lexPair)
        end

(* tryInsert: try to insert tokens before the current terminal;
       return a list of the successes  *)

        fun tryInsert((stack,lexPair as (TOKEN(_,(_,l,_)),_)),queuePos) =
	       catList terms (fn t =>
		 tryChange{lex=lexPair,stack=stack,
			   pos=queuePos,orig=[],new=[tokAt(t,l)],
			   leftPos=l,rightPos=l})
			      
(* trySubst: try to substitute tokens for the current terminal;
       return a list of the successes  *)

        fun trySubst ((stack,lexPair as (orig as TOKEN (term,(_,l,r)),lexer)),
		      queuePos) =
	      if noShift term then []
	      else
		  catList terms (fn t =>
		      tryChange{lex=Stream.get lexer,stack=stack,
				pos=queuePos,
				leftPos=l,rightPos=r,orig=[orig],
				new=[tokAt(t,r)]})

     (* do_delete(toks,lexPair) tries to delete tokens "toks" from "lexPair".
         If it succeeds, returns SOME(toks',l,r,lp), where
	     toks' is the actual tokens (with positions and values) deleted,
	     (l,r) are the (leftmost,rightmost) position of toks', 
	     lp is what remains of the stream after deletion 
     *)
        fun do_delete(nil,lp as (TOKEN(_,(_,l,_)),_)) = SOME(nil,l,l,lp)
          | do_delete([t],(tok as TOKEN(t',(_,l,r)),lp')) =
	       if t=t'
		   then SOME([tok],l,r,Stream.get lp')
                   else NONE
          | do_delete(t::rest,(tok as TOKEN(t',(_,l,r)),lp')) =
	       if t=t'
		   then case do_delete(rest,Stream.get lp')
                         of SOME(deleted,l',r',lp'') =>
			       SOME(tok::deleted,l,r',lp'')
			  | NONE => NONE
		   else NONE
			     
        fun tryPreferred((stack,lexPair),queuePos) =
	    catList preferred_change (fn (delete,insert) =>
	       if List.exists noShift delete then [] (* should give warning at
						 parser-generation time *)
               else case do_delete(delete,lexPair)
                     of SOME(deleted,l,r,lp) => 
			    tryChange{lex=lp,stack=stack,pos=queuePos,
				      leftPos=l,rightPos=r,orig=deleted,
				      new=map (fn t=>(tokAt(t,r))) insert}
		      | NONE => [])

	val changes = catList numStateList tryPreferred @
	                catList numStateList tryInsert @
			  catList numStateList trySubst @
			    catList numStateList (tryDelete 1) @
			      catList numStateList (tryDelete 2) @
			        catList numStateList (tryDelete 3)

	val findMaxDist = fn l => 
	  foldr (fn (CHANGE {distance,...},high) => Int.max(distance,high)) 0 l

(* maxDist: max distance past error taken that we could parse *)

	val maxDist = findMaxDist changes

(* remove changes which did not parse maxDist tokens past the error token *)

        val changes = catList changes 
	      (fn(c as CHANGE{distance,...}) => 
		  if distance=maxDist then [c] else [])

      in case changes 
	  of (l as change :: _) =>
	      let fun print_msg (CHANGE {new,orig,leftPos,rightPos,...}) =
		  let val s = 
		      case (orig,new)
			  of (_::_,[]) => "deleting " ^ (showTerms orig)
	                   | ([],_::_) => "inserting " ^ (showTerms new)
			   | _ => "replacing " ^ (showTerms orig) ^
				 " with " ^ (showTerms new)
		  in error ("syntax error: " ^ s,leftPos,rightPos)
		  end
		   
		  val _ = 
		      (if length l > 1 andalso DEBUG2 then
			   (print "multiple fixes possible; could fix it by:\n";
			    app print_msg l;
			    print "chosen correction:\n")
		       else ();
		       print_msg change)

		  (* findNth: find nth queue entry from the error
		   entry.  Returns the Nth queue entry and the  portion of
		   the queue from the beginning to the nth-1 entry.  The
		   error entry is at the end of the queue.

		   Examples:

		   queue = a b c d e
		   findNth 0 = (e,a b c d)
		   findNth 1 =  (d,a b c)
		   *)

		  val findNth = fn n =>
		      let fun f (h::t,0) = (h,rev t)
			    | f (h::t,n) = f(t,n-1)
			    | f (nil,_) = let exception FindNth
					  in raise FindNth
					  end
		      in f (rev stateList,n)
		      end
		
		  val CHANGE {pos,orig,new,...} = change
		  val (last,queueFront) = findNth pos
		  val (stack,lexPair) = last

		  val lp1 = foldl(fn (_,(_,r)) => Stream.get r) lexPair orig
		  val lp2 = foldr(fn(t,r)=>(t,Stream.cons r)) lp1 new

		  val restQueue = 
		      Fifo.put((stack,lp2),
			       foldl Fifo.put Fifo.empty queueFront)

		  val (lexPair,stack,queue,_,_) =
		      distanceParse(lp2,stack,restQueue,pos)

	      in (lexPair,stack,queue)
	      end
	| nil => (error("syntax error found at " ^ (showTerminal term),
			leftPos,leftPos); raise ParseError)
    end

   val parse = fn {arg,table,lexer,saction,void,lookahead,
		   ec=ec as {showTerminal,...} : ('_a,'_b) ecRecord} =>
	let val distance = 15   (* defer distance tokens *)
	    val minAdvance = 1  (* must parse at least 1 token past error *)
	    val maxAdvance = Int.max(lookahead,0)(* max distance for parse check *)
	    val lexPair = Stream.get lexer
	    val (TOKEN (_,(_,leftPos,_)),_) = lexPair
	    val startStack = [(initialState table,(void,leftPos,leftPos))]
	    val startQueue = Fifo.put((startStack,lexPair),Fifo.empty)
	    val distanceParse = distanceParse(table,showTerminal,saction,arg)
	    val fixError = mkFixError(ec,distanceParse,minAdvance,maxAdvance)
	    val ssParse = ssParse(table,showTerminal,saction,fixError,arg)
	    fun loop (lexPair,stack,queue,_,SOME ACCEPT) =
		   ssParse(lexPair,stack,queue)
	      | loop (lexPair,stack,queue,0,_) = ssParse(lexPair,stack,queue)
	      | loop (lexPair,stack,queue,distance,SOME ERROR) =
		 let val (lexPair,stack,queue) = fixError(lexPair,stack,queue)
		 in loop (distanceParse(lexPair,stack,queue,distance))
		 end
	      | loop _ = let exception ParseInternal
			 in raise ParseInternal
			 end
	in loop (distanceParse(lexPair,startStack,startQueue,distance))
	end
 end;

(* drt (12/15/89) -- needed only when the code above is functorized

structure LrParser = ParserGen(structure LrTable=LrTable
			     structure Stream=Stream);
*)
(*
 * (c) Andreas Rossberg 2001-2007
 *
 * Line/column counting
 *)

functor LineAwareLexer(
	structure Lexer: LEXER
		  where type UserDeclarations.pos = int
		  and   type ('a,'b) UserDeclarations.token =
			     ('a,'b) LrParser.Token.token
	exception Error of (int * int) * string
) =
struct

    structure UserDeclarations =
    struct
	open Lexer.UserDeclarations
	type pos = Source.pos
    end

    exception Error' = Error
    exception Error of Source.region * string

    fun makeLexer yyinput =
	let
	    val lin  = ref 1
	    val col  = ref 0
	    val pos  = ref 0
	    val buf  = ref ""		(* current buffer *)
	    val off  = ref 0		(* offset to start of current buffer *)
	    val que  = Queue.mkQueue ()	(* buffer queue *)
	    val offE = ref 0		(* end offset *)

	    fun count(i, i', lin, col) =
		if i = i' then
		    (lin,col)
		else (case String.sub(!buf, i)
		    of #"\n" => count(i+1, i', lin+1, 0)
		     | #"\t" => count(i+1, i', lin, col+8 - col mod 8)
		     |  _    => count(i+1, i', lin, col+1)
		) handle Subscript => (* end of current buffer *)
		let
		    val n = String.size(!buf)
		    val (buf',off') = Queue.dequeue que
		in
		    buf := buf';
		    off := off';
		    count(0, i'-n, lin, col)
		end handle Dequeue => (* end of file token *)
		    ( buf := ""
		    ; off := !offE
		    ; (lin,col)
		    )

	    fun transform(pos1, pos2) =
		let
		    val n0 = !off
		    val pos1' as (l1,c1) = count(!pos-n0, pos1-n0, !lin, !col)
		    val n0 = !off
		    val pos2' as (l2,c2) = count(pos1-n0, pos2-n0, l1, c1)
		in
		    lin := l2;
		    col := c2;
		    pos := pos2;
		    (pos1',pos2')
		end

	    fun yyinput' n =
		let
		    val s = yyinput n
		in
		    Queue.enqueue(que, (s, !offE));
		    offE := !offE + String.size s;
		    s
		end

	    val lexer = Lexer.makeLexer yyinput'
	in
	    fn () =>
		let
		    val LrParser.Token.TOKEN(term, (svalue,pos1,pos2)) = lexer()
		    val (pos1', pos2') = transform(pos1, pos2)
		in
		    LrParser.Token.TOKEN(term, (svalue, pos1', pos2'))
		end
		handle Error'(position, e) => raise Error(transform position, e)
	end
end
(* ML-Yacc Parser Generator (c) 1989 Andrew W. Appel, David R. Tarditi 
 *
 * $Log$
 * Revision 1.1  2000/11/24 13:32:28  rossberg
 * *** empty log message ***
 *
 * Revision 1.1.1.1  1997/01/14 01:38:04  george
 *   Version 109.24
 *
 * Revision 1.1.1.1  1996/01/31  16:01:42  george
 * Version 109
 * 
 *)

(* functor Join creates a user parser by putting together a Lexer structure,
   an LrValues structure, and a polymorphic parser structure.  Note that
   the Lexer and LrValues structure must share the type pos (i.e. the type
   of line numbers), the type svalues for semantic values, and the type
   of tokens.
*)

functor Join(structure ParserData: PARSER_DATA
	     structure Lex : LEXER
	     structure LrParser : LR_PARSER
	     sharing type ParserData.LrTable.state = LrParser.LrTable.state
	     sharing type ParserData.LrTable.term = LrParser.LrTable.term
	     sharing type ParserData.LrTable.nonterm = LrParser.LrTable.nonterm
	     sharing type ParserData.LrTable.table = LrParser.LrTable.table
	     sharing type ParserData.Token.token = LrParser.Token.token
	     sharing type Lex.UserDeclarations.svalue = ParserData.svalue
	     sharing type Lex.UserDeclarations.pos = ParserData.pos
	     sharing type Lex.UserDeclarations.token = ParserData.Token.token)
		 : PARSER =
struct
    structure Token = ParserData.Token
    structure Stream = LrParser.Stream
 
    exception ParseError = LrParser.ParseError

    type arg = ParserData.arg
    type pos = ParserData.pos
    type result = ParserData.result
    type svalue = ParserData.svalue
    val makeLexer = LrParser.Stream.streamify o Lex.makeLexer
    val parse = fn (lookahead,lexer,error,arg) =>
	(fn (a,b) => (ParserData.Actions.extract a,b))
     (LrParser.parse {table = ParserData.table,
	        lexer=lexer,
		lookahead=lookahead,
		saction = ParserData.Actions.actions,
		arg=arg,
		void= ParserData.Actions.void,
	        ec = {is_keyword = ParserData.EC.is_keyword,
		      noShift = ParserData.EC.noShift,
		      preferred_change = ParserData.EC.preferred_change,
		      errtermvalue = ParserData.EC.errtermvalue,
		      error=error,
		      showTerminal = ParserData.EC.showTerminal,
		      terms = ParserData.EC.terms}}
      )
     val sameToken = Token.sameToken
end

(* functor JoinWithArg creates a variant of the parser structure produced 
   above.  In this case, the makeLexer take an additional argument before
   yielding a value of type unit -> (svalue,pos) token
 *)

functor JoinWithArg(structure ParserData: PARSER_DATA
	     structure Lex : ARG_LEXER
	     structure LrParser : LR_PARSER
	     sharing type ParserData.LrTable.state = LrParser.LrTable.state
	     sharing type ParserData.LrTable.term = LrParser.LrTable.term
	     sharing type ParserData.LrTable.nonterm = LrParser.LrTable.nonterm
	     sharing type ParserData.LrTable.table = LrParser.LrTable.table
	     sharing type ParserData.Token.token = LrParser.Token.token
	     sharing type Lex.UserDeclarations.svalue = ParserData.svalue
	     sharing type Lex.UserDeclarations.pos = ParserData.pos
	     sharing type Lex.UserDeclarations.token = ParserData.Token.token)
		 : ARG_PARSER  =
struct
    structure Token = ParserData.Token
    structure Stream = LrParser.Stream

    exception ParseError = LrParser.ParseError

    type arg = ParserData.arg
    type lexarg = Lex.UserDeclarations.arg
    type pos = ParserData.pos
    type result = ParserData.result
    type svalue = ParserData.svalue

    val makeLexer = fn s => fn arg =>
		 LrParser.Stream.streamify (Lex.makeLexer s arg)
    val parse = fn (lookahead,lexer,error,arg) =>
	(fn (a,b) => (ParserData.Actions.extract a,b))
     (LrParser.parse {table = ParserData.table,
	        lexer=lexer,
		lookahead=lookahead,
		saction = ParserData.Actions.actions,
		arg=arg,
		void= ParserData.Actions.void,
	        ec = {is_keyword = ParserData.EC.is_keyword,
		      noShift = ParserData.EC.noShift,
		      preferred_change = ParserData.EC.preferred_change,
		      errtermvalue = ParserData.EC.errtermvalue,
		      error=error,
		      showTerminal = ParserData.EC.showTerminal,
		      terms = ParserData.EC.terms}}
      )
    val sameToken = Token.sameToken
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML syntactic analysis
 *)

signature PARSE =
sig
    (* Import *)

    type source  = Source.source
    type InfEnv  = Infix.InfEnv
    type Program = GrammarProgram.Program

    (* Export *)

    val parse : InfEnv * source * string option -> InfEnv * Program
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML syntactic analysis
 *)

structure Parse : PARSE =
struct
    (* Import *)

    type source  = Source.source
    type InfEnv  = Infix.InfEnv
    type Program = GrammarProgram.Program


    (* Build Yacc parser *)

    structure LrVals = LrValsFn(structure Token      = LrParser.Token)
    structure Lexer  = LexerFn (structure Tokens     = LrVals.Tokens)
    structure Lexer' = LineAwareLexer(structure Lexer = Lexer
				      exception Error = Source.Error)
    structure Parser = Join    (structure LrParser   = LrParser
				structure ParserData = LrVals.ParserData
				structure Lex        = Lexer')


    (* The actual parsing function *)

    fun parse(J, source, filename) =
	let
	    val yyread = ref false
	    fun yyinput _ =
		if !yyread then
		    ""
		else
		    ( yyread := true; source )

	    val lexer = Parser.makeLexer yyinput

	    fun onError(s, pos1, pos2) =
		Error.error({file = filename, region = (pos1,pos2)}, s)

	    fun I(left, right) : Source.info =
		{ file = filename,
		  region = (left, if right = (0,0) then left else right) }

	    val ((program,J'),_) =
		Parser.parse(0, lexer, onError, (I, J))
		handle Lexer'.Error(region, e) =>
		    Error.error({file = filename, region = region}, e)
	in
	    (J',program)
	end
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML syntactic restrictions for modules
 *
 * Definition, Section 3.5
 *)

signature SYNTACTIC_RESTRICTIONS_MODULE =
sig
    (* Import *)

    type Basis		= BindingObjectsModule.Basis
    type TopDec		= GrammarModule.TopDec

    (* Export *)

    val checkTopDec :	Basis * TopDec -> Basis
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML syntactic restrictions for the core
 *
 * Definition, Section 2.9
 *
 * Notes:
 *   - The "syntactic restrictions" defined in the Definition are not purely
 *     syntactic. E.g. the restriction that valbinds may not bind the same vid
 *     twice [2nd bullet] cannot be checked without proper binding analysis,
 *     to compute identifier status.
 *   - Also, checking of type variable shadowing [last bullet] is a global
 *     check dependent on context. Moreover, it requires the transformation from
 *     Section 4.6 to be done first.
 *   - The Definition contains a bug -- an important syntactic restriction
 *     is missing:
 *       "Any tyvar occuring on the right side of a typbind or datbind of the
 *        form tyvarseq tycon = ... must occur in tyvarseq."
 *)

signature SYNTACTIC_RESTRICTIONS_CORE =
sig
    (* Import *)

    type VId			= IdsCore.VId
    type 'a VIdMap		= 'a IdsCore.VIdMap
    type 'a TyConMap		= 'a IdsCore.TyConMap
    type 'a StrIdMap		= 'a IdsCore.StrIdMap

    type Dec			= GrammarCore.Dec
    type Ty			= GrammarCore.Ty
    type TyVarseq		= GrammarCore.TyVarseq

    type TyVarSet		= BindingObjectsCore.TyVarSet
    type Env			= BindingObjectsCore.Env
    type Context		= BindingObjectsCore.Context


    (* Export *)

    val checkDec :		Context * Dec -> Env
    val checkTy :		Ty -> TyVarSet
    val checkTyVarseq :		TyVarseq -> TyVarSet

    val validBindVId :		VId -> bool
    val validConBindVId :	VId -> bool
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML scope of type variables
 *
 * Definition, Section 4.6
 *)

signature SCOPE_TYVARS =
sig
    (* Import *)

    type ValBind  = GrammarCore.ValBind
    type TyVarSet = TyVarSet.set

    (* Operation *)

    val unguardedTyVars : ValBind -> TyVarSet
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML scope of type variables
 *
 * Definition, Section 4.6
 *)

structure ScopeTyVars : SCOPE_TYVARS =
struct
    (* Import *)

    open GrammarCore
    type TyVarSet = TyVarSet.set


    (* Helpers *)

    val op+ = TyVarSet.union

    fun ? tyvarsX  NONE    = TyVarSet.empty
      | ? tyvarsX (SOME x) = tyvarsX x


    (* Operation *)

    fun unguardedTyVarsAtExp(SCONAtExp(_, _)) =
	    TyVarSet.empty
      | unguardedTyVarsAtExp(IDAtExp(_, _, _)) =
	    TyVarSet.empty
      | unguardedTyVarsAtExp(RECORDAtExp(_, exprow_opt)) =
	    ?unguardedTyVarsExpRow exprow_opt
      | unguardedTyVarsAtExp(LETAtExp(_, dec, exp)) =
	    unguardedTyVarsDec dec + unguardedTyVarsExp exp
      | unguardedTyVarsAtExp(PARAtExp(_, exp)) =
	    unguardedTyVarsExp exp

    and unguardedTyVarsExpRow(ExpRow(_, lab, exp, exprow_opt)) =
	    unguardedTyVarsExp exp + ?unguardedTyVarsExpRow exprow_opt

    and unguardedTyVarsExp(ATExp(_, atexp)) =
	    unguardedTyVarsAtExp atexp
      | unguardedTyVarsExp(APPExp(_, exp, atexp)) =
	    unguardedTyVarsExp exp + unguardedTyVarsAtExp atexp
      | unguardedTyVarsExp(COLONExp(_, exp, ty)) =
	    unguardedTyVarsExp exp + unguardedTyVarsTy ty
      | unguardedTyVarsExp(HANDLEExp(_, exp, match)) =
	    unguardedTyVarsExp exp + unguardedTyVarsMatch match
      | unguardedTyVarsExp(RAISEExp(_, exp)) =
	    unguardedTyVarsExp exp
      | unguardedTyVarsExp(FNExp(_, match)) =
	    unguardedTyVarsMatch match

    and unguardedTyVarsMatch(Match(_, mrule, match_opt)) =
	    unguardedTyVarsMrule mrule + ?unguardedTyVarsMatch match_opt

    and unguardedTyVarsMrule(Mrule(_, pat, exp)) =
	    unguardedTyVarsPat pat + unguardedTyVarsExp exp

    and unguardedTyVarsDec(VALDec(_, _, _)) =
	    TyVarSet.empty
      | unguardedTyVarsDec(TYPEDec(_, _)) =
	    TyVarSet.empty
      | unguardedTyVarsDec(DATATYPEDec(_, _)) =
	    TyVarSet.empty
      | unguardedTyVarsDec(DATATYPE2Dec(_, _, _)) =
	    TyVarSet.empty
      | unguardedTyVarsDec(ABSTYPEDec(_, datbind, dec)) =
	    unguardedTyVarsDec dec
      | unguardedTyVarsDec(EXCEPTIONDec(_, exbind)) =
	    unguardedTyVarsExBind exbind
      | unguardedTyVarsDec(LOCALDec(_, dec1, dec2)) =
	    unguardedTyVarsDec dec1 + unguardedTyVarsDec dec2
      | unguardedTyVarsDec(OPENDec(_, _)) =
	    TyVarSet.empty
      | unguardedTyVarsDec(EMPTYDec(_)) =
	    TyVarSet.empty
      | unguardedTyVarsDec(SEQDec(_, dec1, dec2)) =
	    unguardedTyVarsDec dec1 + unguardedTyVarsDec dec2

    and unguardedTyVarsValBind(PLAINValBind(_, pat, exp, valbind_opt)) =
	    unguardedTyVarsPat pat + unguardedTyVarsExp exp +
	    ?unguardedTyVarsValBind valbind_opt
      | unguardedTyVarsValBind(RECValBind(_, valbind)) =
	    unguardedTyVarsValBind valbind

    and unguardedTyVarsExBind(NEWExBind(_, _, vid, ty_opt, exbind_opt)) =
	    ?unguardedTyVarsTy ty_opt + ?unguardedTyVarsExBind exbind_opt
      | unguardedTyVarsExBind(EQUALExBind(_, _, vid, _, longvid, exbind_opt)) =
	    ?unguardedTyVarsExBind exbind_opt

    and unguardedTyVarsAtPat(WILDCARDAtPat(_)) =
	    TyVarSet.empty
      | unguardedTyVarsAtPat(SCONAtPat(_, _)) =
	    TyVarSet.empty
      | unguardedTyVarsAtPat(IDAtPat(_, _, _)) =
	    TyVarSet.empty
      | unguardedTyVarsAtPat(RECORDAtPat(_, patrow_opt)) =
	    ?unguardedTyVarsPatRow patrow_opt
      | unguardedTyVarsAtPat(PARAtPat(_, pat)) =
	    unguardedTyVarsPat pat

    and unguardedTyVarsPatRow(DOTSPatRow(_)) = TyVarSet.empty
      | unguardedTyVarsPatRow(FIELDPatRow(_, lab, pat, patrow_opt)) =
	    unguardedTyVarsPat pat + ?unguardedTyVarsPatRow patrow_opt

    and unguardedTyVarsPat(ATPat(_, atpat)) =
	    unguardedTyVarsAtPat atpat
      | unguardedTyVarsPat(CONPat(_, _, longvid, atpat)) =
	    unguardedTyVarsAtPat atpat
      | unguardedTyVarsPat(COLONPat(_, pat, ty)) =
	    unguardedTyVarsPat pat + unguardedTyVarsTy ty
      | unguardedTyVarsPat(ASPat(_, _, vid, ty_opt, pat)) =
	    ?unguardedTyVarsTy ty_opt + unguardedTyVarsPat pat

    and unguardedTyVarsTy(VARTy(_, tyvar)) = TyVarSet.singleton tyvar
      | unguardedTyVarsTy(RECORDTy(_, tyrow_opt)) =
	    ?unguardedTyVarsTyRow tyrow_opt
      | unguardedTyVarsTy(CONTy(_, tyseq, longtycon)) =
	    unguardedTyVarsTyseq tyseq
      | unguardedTyVarsTy(ARROWTy(_, ty, ty')) =
	    unguardedTyVarsTy ty + unguardedTyVarsTy ty'
      | unguardedTyVarsTy(PARTy(_, ty)) =
	    unguardedTyVarsTy ty

    and unguardedTyVarsTyRow(TyRow(_, lab, ty, tyrow_opt)) =
	    unguardedTyVarsTy ty + ?unguardedTyVarsTyRow tyrow_opt

    and unguardedTyVarsTyseq(Tyseq(_, tys)) =
	    List.foldl (fn(ty,U) => unguardedTyVarsTy ty + U) TyVarSet.empty tys


    (* Export *)

    val unguardedTyVars = unguardedTyVarsValBind
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML context for binding analysis
 *)

signature BINDING_CONTEXT =
sig
    (* Import *)

    type VId		= IdsCore.VId
    type TyCon		= IdsCore.TyCon
    type StrId		= IdsCore.StrId
    type longVId	= IdsCore.longVId
    type longTyCon	= IdsCore.longTyCon
    type longStrId	= IdsCore.longStrId

    type TyVarSet	= BindingObjectsCore.TyVarSet
    type TyStr		= BindingObjectsCore.TyStr
    type TyEnv		= BindingObjectsCore.TyEnv
    type ValStr		= BindingObjectsCore.ValStr
    type ValEnv		= BindingObjectsCore.ValEnv
    type Env		= BindingObjectsCore.Env
    type Context	= BindingObjectsCore.Context


    (* Operations *)

    val Uof :		Context -> TyVarSet
    val Eof :		Context -> Env

    val plusU :		Context * TyVarSet -> Context
    val plusE :		Context * Env      -> Context
    val plusVE :	Context * ValEnv   -> Context
    val plusTE :	Context * TyEnv    -> Context
    val plusVEandTE :	Context * (ValEnv * TyEnv) -> Context

    val findLongVId :	Context * longVId   -> ValStr option
    val findLongTyCon :	Context * longTyCon -> TyStr option
    val findLongStrId :	Context * longStrId -> Env option
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML context for binding analysis
 *)

structure BindingContext : BINDING_CONTEXT =
struct
    (* Import *)

    open IdsCore
    open BindingObjectsCore

    (* Projections [Section 4.3] *)

    fun Uof (U,E) = U
    fun Eof (U,E) = E

    (* Modification [Section 4.3] *)

    infix plus plusU plusVE plusTE plusVEandTE plusE

    val op plus = BindingEnv.plus

    fun (U,E) plusU U'  = (TyVarSet.union(U, U'), E)
    fun (U,E) plusE E'	= (U, E plus E')
    fun (U,E) plusVE VE	= (U, E plus BindingEnv.fromVE VE)
    fun (U,E) plusTE TE	= (U, E plus BindingEnv.fromTE TE)
    fun (U,E) plusVEandTE (VE,TE) = (U, E plus BindingEnv.fromVEandTE (VE,TE))

    (* Application (lookup) [Section 4.3] *)

    fun findLongVId  ((U,E), longvid)   = BindingEnv.findLongVId(E, longvid)
    fun findLongTyCon((U,E), longtycon) = BindingEnv.findLongTyCon(E, longtycon)
    fun findLongStrId((U,E), longstrid) = BindingEnv.findLongStrId(E, longstrid)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML syntactic restrictions for the core
 *
 * Definition, Section 2.9
 *
 * Notes:
 *   - The "syntactic restrictions" defined in the Definition are not purely
 *     syntactic. E.g. the restriction that valbinds may not bind the same vid
 *     twice [2nd bullet] cannot be checked without proper binding analysis,
 *     to compute identifier status.
 *   - Also, checking of type variable shadowing [last bullet] is a global
 *     check dependent on context. Moreover, it requires the transformation from
 *     Section 4.6 to be done first.
 *   - The Definition contains a bug -- an important syntactic restriction
 *     is missing:
 *       "Any tyvar occuring on the right side of a typbind or datbind of the
 *        form tyvarseq tycon = ... must occur in tyvarseq."
 *)

structure SyntacticRestrictionsCore : SYNTACTIC_RESTRICTIONS_CORE =
struct
    (* Import *)

    open GrammarCore
    open BindingObjectsCore
    open Error


    (* Helpers for context modification *)

    open BindingContext
    val plus = BindingEnv.plus

    infix plus plusU plusVE plusTE plusVEandTE plusE


    (* Checking restriction for vids in binding [Section 2.9, 5th bullet] *)

    fun validBindVId vid =
	    vid <> VId.fromString "true" andalso
	    vid <> VId.fromString "false" andalso
	    vid <> VId.fromString "nil" andalso
	    vid <> VId.fromString "::"  andalso
	    vid <> VId.fromString "ref"

    fun validConBindVId vid =
	    validBindVId vid andalso
	    vid <> VId.fromString "it"


    (* Type variable sequences *)

    fun checkTyVarseq(TyVarseq(I, tyvars)) =
	(* [Section 2.9, 3rd bullet; Section 3.5, 3rd bullet] *)
	let
	    fun check(U, []) = U
	      | check(U, tyvar::tyvars) =
		    if TyVarSet.member(U, tyvar) then
			errorTyVar(I, "duplicate type variable ", tyvar)
		    else
			check(TyVarSet.add(U, tyvar), tyvars)
	in
	    check(TyVarSet.empty, tyvars)
	end


    (* Atomic Expressions *)

    fun checkAtExp(C, SCONAtExp(I, scon)) =
	    ()

      | checkAtExp(C, IDAtExp(I, _, longvid)) =
	    ()

      | checkAtExp(C, RECORDAtExp(I, exprow_opt)) =
	let
	    val labs = case exprow_opt
			 of NONE        => LabSet.empty
			  | SOME exprow => checkExpRow(C, exprow)
	in
	    ()
	end

      | checkAtExp(C, LETAtExp(I, dec, exp)) =
	let
	    val E = checkDec(C, dec)
	in
	    checkExp(C plusE E, exp)
	end

      | checkAtExp(C, PARAtExp(I, exp)) =
	let
	    val () = checkExp(C, exp)
	in
	    ()
	end


    (* Expression Rows *)

    and checkExpRow(C, ExpRow(I, lab, exp, exprow_opt)) =
	let
	    val ()   = checkExp(C, exp)
	    val labs = case exprow_opt
			 of NONE        => LabSet.empty
			  | SOME exprow => checkExpRow(C, exprow)
	in
	    (* [Section 2.9, 1st bullet] *)
	    if LabSet.member(labs, lab) then
		errorLab(I, "duplicate label ", lab)
	    else
		LabSet.add(labs, lab)
	end


    (* Expressions *)

    and checkExp(C, ATExp(I, atexp)) =
	let
	    val () = checkAtExp(C, atexp)
	in
	    ()
	end

      | checkExp(C, APPExp(I, exp, atexp)) =
	let
	    val () = checkExp(C, exp)
	    val () = checkAtExp(C, atexp)
	in
	    ()
	end

      | checkExp(C, COLONExp(I, exp, ty)) =
	let
	    val () = checkExp(C, exp)
	    val U  = checkTy ty
	in
	    ()
	end

      | checkExp(C, HANDLEExp(I, exp, match)) =
	let
	    val () = checkExp(C, exp)
	    val () = checkMatch(C, match)
	in
	    ()
	end

      | checkExp(C, RAISEExp(I, exp)) =
	let
	    val () = checkExp(C, exp)
	in
	    ()
	end

      | checkExp(C, FNExp(I, match)) =
	let
	    val () = checkMatch(C, match)
	in
	    ()
	end


    (* Matches *)

    and checkMatch(C, Match(I, mrule, match_opt)) =
	let
	    val () = checkMrule(C, mrule)
	    val () = case match_opt
		       of NONE       => ()
			| SOME match => checkMatch(C, match)
	in
	    ()
	end


    (* Match rules *)

    and checkMrule(C, Mrule(I, pat, exp)) =
	let
	    val VE = checkPat(C, pat)
	    val () = checkExp(C plusVE VE, exp)
	in
	    ()
	end


    (* Declarations *)

    and checkDec(C, VALDec(I, tyvarseq, valbind)) =
	let
	    val U' = checkTyVarseq tyvarseq
	    (* Collect implicitly bound tyvars [Section 4.6] *)
	    val U  = TyVarSet.union(U',
			TyVarSet.difference(ScopeTyVars.unguardedTyVars valbind,
					    Uof C))
	    val VE = checkValBind(C plusU U, valbind)
	in
	    if not(TyVarSet.isEmpty(TyVarSet.intersection(Uof C, U))) then
		(* [Section 2.9, last bullet] *)
		error(I, "some type variables shadow previous ones")
	    else
		BindingEnv.fromVE VE
	end

      | checkDec(C, TYPEDec(I, typbind)) =
	let
	    val TE = checkTypBind typbind
	in
	    BindingEnv.fromTE TE
	end

      | checkDec(C, DATATYPEDec(I, datbind)) =
	let
	    val (VE,TE) = checkDatBind datbind
	in
	    BindingEnv.fromVEandTE(VE,TE)
	end

      | checkDec(C, DATATYPE2Dec(I, tycon, longtycon)) =
	let
	    val VE = case findLongTyCon(C, longtycon)
		       of SOME VE => VE
			| NONE    => VIdMap.empty (* actually an error *)
	    val TE = TyConMap.singleton(tycon, VE)
	in
	    BindingEnv.fromVEandTE(VE,TE)
	end

      | checkDec(C, ABSTYPEDec(I, datbind, dec)) =
	let
	    val (VE,TE) = checkDatBind datbind
	    val    E    = checkDec(C plusVEandTE (VE,TE), dec)
	in
	    E
	end

      | checkDec(C, EXCEPTIONDec(I, exbind)) =
	let
	    val VE = checkExBind exbind
	in
	    BindingEnv.fromVE VE
	end

      | checkDec(C, LOCALDec(I, dec1, dec2)) =
	let
	    val E1 = checkDec(C, dec1)
	    val E2 = checkDec(C plusE E1, dec2)
	in
	    E2
	end

      | checkDec(C, OPENDec(I, longstrids)) =
	let
	    val Es =
		List.map
		    (fn longstrid =>
			case findLongStrId(C, longstrid)
			  of SOME E => E
			   | NONE => BindingEnv.empty) (* actually an error *)
		    longstrids
	in
	    List.foldl (op plus) BindingEnv.empty Es
	end

      | checkDec(C, EMPTYDec(I)) =
	    BindingEnv.empty

      | checkDec(C, SEQDec(I, dec1, dec2)) =
	let
	    val E1 = checkDec(C, dec1)
	    val E2 = checkDec(C plusE E1, dec2)
	in
	    E1 plus E2
	end


    (* Value Bindings *)

    and checkValBind(C, PLAINValBind(I, pat, exp, valbind_opt)) =
	let
	    val VE  = checkPat(C, pat)
	    val ()  = checkExp(C, exp)
	    val VE' = case valbind_opt
			of NONE         => VIdMap.empty
			 | SOME valbind => checkValBind(C, valbind)
	in
	    VIdMap.appi (fn(vid,_) =>
		if validBindVId vid then () else
		(* [Section 2.9, 5th bullet] *)
		errorVId(I, "illegal rebinding of identifier ", vid)
	    ) VE;
	    VIdMap.unionWithi
		(fn(vid,_,_) =>
		    (* [Section 2.9, 2nd bullet] *)
		    errorVId(I, "duplicate variable ", vid))
		(VE,VE')
	end

      | checkValBind(C, RECValBind(I, valbind)) =
	let
	    val VE1 = lhsRecValBind valbind
	    val VE  = checkValBind(C plusVE VE1, valbind)
	in
	    VE
	end


    (* Type Bindings *)

    and checkTypBind(TypBind(I, tyvarseq, tycon, ty, typbind_opt)) =
	let
	    val U1 = checkTyVarseq tyvarseq
	    val U2 = checkTy ty
	    val TE = case typbind_opt
		       of NONE         => TyConMap.empty
			| SOME typbind => checkTypBind typbind
	in
	    if not(TyVarSet.isSubset(U2, U1)) then
		(* Restriction missing in the Definition! *)
		error(I, "free type variables in type binding")
	    else if TyConMap.inDomain(TE, tycon) then
		(* Syntactic restriction [Section 2.9, 2nd bullet] *)
		errorTyCon(I, "duplicate type constructor ", tycon)
	    else
		TyConMap.insert(TE, tycon, VIdMap.empty)
	end


    (* Datatype Bindings *)

    and checkDatBind(DatBind(I, tyvarseq, tycon, conbind, datbind_opt)) =
	let
	    val  U1     = checkTyVarseq tyvarseq
	    val (U2,VE) = checkConBind conbind
	    val(VE',TE') = case datbind_opt
			     of NONE         => ( VIdMap.empty, TyConMap.empty )
			      | SOME datbind => checkDatBind datbind
	in
	    if not(TyVarSet.isSubset(U2, U1)) then
		(* Restriction missing in Definition! *)
		error(I, "free type variables in datatype binding")
	    else if TyConMap.inDomain(TE', tycon) then
		(* [Section 2.9, 2nd bullet] *)
		errorTyCon(I, "duplicate type constructor ", tycon)
	    else
	    ( VIdMap.unionWithi (fn(vid,_,_) =>
		(* [Section 2.9, 2nd bullet] *)
		errorVId(I, "duplicate data constructor ", vid)) (VE,VE')
	    , TyConMap.insert(TE', tycon, VE)
	    )
	end


    (* Constructor Bindings *)

    and checkConBind(ConBind(I, _, vid, ty_opt, conbind_opt)) =
	let
	    val  U      = case ty_opt
			    of NONE    => TyVarSet.empty
			     | SOME ty => checkTy ty
	    val (U',VE) = case conbind_opt
			    of NONE         => ( TyVarSet.empty, VIdMap.empty )
			     | SOME conbind => checkConBind conbind
	in
	    if VIdMap.inDomain(VE, vid) then
		(* [Section 2.9, 2nd bullet] *)
		errorVId(I, "duplicate data constructor ", vid)
	    else if not(validConBindVId vid) then
		(* [Section 2.9, 5th bullet] *)
		errorVId(I, "illegal rebinding of identifier ", vid)
	    else
		( TyVarSet.union(U, U'), VIdMap.insert(VE, vid, IdStatus.c) )
	end


    (* Exception Bindings *)

    and checkExBind(NEWExBind(I, _, vid, ty_opt, exbind_opt)) =
	let
	    val U  = case ty_opt
			 of NONE    => TyVarSet.empty
			  | SOME ty => checkTy ty
	    val VE = case exbind_opt
		       of NONE        => VIdMap.empty
		        | SOME exbind => checkExBind exbind
	in
	    if VIdMap.inDomain(VE, vid) then
		(* [Section 2.9, 2nd bullet] *)
		errorVId(I, "duplicate exception constructor ", vid)
	    else if not(validConBindVId vid) then
		(* [Section 2.9, 5th bullet] *)
		errorVId(I, "illegal rebinding of identifier ", vid)
	    else
		VIdMap.insert(VE, vid, IdStatus.e)
	end

      | checkExBind(EQUALExBind(I, _, vid, _, longvid, exbind_opt)) =
	let
	    val VE = case exbind_opt
		       of NONE        => VIdMap.empty
		        | SOME exbind => checkExBind exbind
	in
	    if VIdMap.inDomain(VE, vid) then
		(* [Section 2.9, 2nd bullet] *)
		errorVId(I, "duplicate exception constructor ", vid)
	    else
		VIdMap.insert(VE, vid, IdStatus.e)
	end


    (* Atomic Patterns *)

    and checkAtPat(C, WILDCARDAtPat(I)) =
	    VIdMap.empty

      | checkAtPat(C, SCONAtPat(I, scon)) =
	(case scon
	   of SCon.REAL _ =>
	      (* [Section 2.9, 6th bullet] *)
	      error(I, "real constant in pattern")
	    | _ =>
	      VIdMap.empty
	)

      | checkAtPat(C, IDAtPat(I, _, longvid)) =
	let
	    val (strids,vid) = LongVId.explode longvid
	in
	    if List.null strids andalso
	       ( case findLongVId(C, longvid)
		   of NONE    => true
		    | SOME is => is = IdStatus.v )
	    then
		VIdMap.singleton(vid, IdStatus.v)
	    else
		VIdMap.empty
	end

      | checkAtPat(C, RECORDAtPat(I, patrow_opt)) =
	let
	    val (VE,labs) = case patrow_opt
			      of NONE        => ( VIdMap.empty, LabSet.empty )
			       | SOME patrow => checkPatRow(C, patrow)
	in
	    VE
	end

      | checkAtPat(C, PARAtPat(I, pat)) =
	let
	    val VE = checkPat(C, pat)
	in
	    VE
	end


    (* Pattern Rows *)

    and checkPatRow(C, DOTSPatRow(I)) =
	    ( VIdMap.empty, LabSet.empty )

      | checkPatRow(C, FIELDPatRow(I, lab, pat, patrow_opt)) =
	let
	    val  VE        = checkPat(C, pat)
	    val (VE',labs) = case patrow_opt
			       of NONE        => ( VIdMap.empty, LabSet.empty )
				| SOME patrow => checkPatRow(C, patrow)
	in
	    if LabSet.member(labs, lab) then
		(* [Section 2.9, 1st bullet] *)
		errorLab(I, "duplicate label ", lab)
	    else
		( VIdMap.unionWithi (fn(vid,_,_) =>
		    (* [Section 2.9, 2nd bullet] *)
		    errorVId(I, "duplicate variable ", vid)) (VE,VE')
		, LabSet.add(labs, lab)
		)
	end


    (* Patterns *)

    and checkPat(C, ATPat(I, atpat)) =
	let
	    val VE = checkAtPat(C, atpat)
	in
	    VE
	end

      | checkPat(C, CONPat(I, _, longvid, atpat)) =
	let
	    val VE = checkAtPat(C, atpat)
	in
	    VE
	end

      | checkPat(C, COLONPat(I, pat, ty)) =
	let
	    val VE = checkPat(C, pat)
	    val U  = checkTy ty
	in
	    VE
	end

      | checkPat(C, ASPat(I, _, vid, ty_opt, pat)) =
	let
	    val VE = checkPat(C, pat)
	    val U  = case ty_opt
		       of NONE    => TyVarSet.empty
			| SOME ty => checkTy ty
	in
	    if VIdMap.inDomain(VE, vid) then
		(* [Section 2.9, 2nd bullet] *)
		errorVId(I, "duplicate variable ", vid)
	    else
		VIdMap.insert(VE, vid, IdStatus.v)
	end


    (* Type Expressions *)

    and checkTy(VARTy(I, tyvar)) =
	    TyVarSet.singleton tyvar

      | checkTy(RECORDTy(I, tyrow_opt)) =
	let
	    val (U,labs) = case tyrow_opt
			     of NONE       => ( TyVarSet.empty, LabSet.empty )
			      | SOME tyrow => checkTyRow tyrow
	in
	    U
	end

      | checkTy(CONTy(I, tyseq, longtycon)) =
	let
	    val Tyseq(_,tys) = tyseq
	    val Us = List.map checkTy tys
	in
	    List.foldl TyVarSet.union TyVarSet.empty Us
	end

      | checkTy(ARROWTy(I, ty, ty')) =
	let
	    val U  = checkTy ty
	    val U' = checkTy ty'
	in
	    TyVarSet.union(U, U')
	end

      | checkTy(PARTy(I, ty)) =
	let
	    val U = checkTy ty
	in
	    U
	end


    (* Type-expression Rows *)

    and checkTyRow(TyRow(I, lab, ty, tyrow_opt)) =
	let
	    val  U        = checkTy ty
	    val (U',labs) = case tyrow_opt
			      of NONE       => ( TyVarSet.empty, LabSet.empty )
			       | SOME tyrow => checkTyRow tyrow
	in
	    if LabSet.member(labs, lab) then
		(* [Section 2.9, 1st bullet] *)
		errorLab(I, "duplicate label ", lab)
	    else
		( TyVarSet.union(U, U'), LabSet.add(labs, lab) )
	end



    (* Build tentative VE from LHSs of recursive valbind *)

    and lhsRecValBind(PLAINValBind(I, pat, exp, valbind_opt)) =
	let
	    val VE  = lhsRecValBindPat pat
	    val VE' = case valbind_opt
			of NONE         => VIdMap.empty
			 | SOME valbind => lhsRecValBind valbind
	in
	    case exp
	      of FNExp _ => VIdMap.unionWith #2 (VE,VE')
	       | _ =>
		(* [Section 2.9, 4th bullet] *)
		error(I, "illegal expression within recursive value binding")
	end

      | lhsRecValBind(RECValBind(I, valbind)) =
	    lhsRecValBind valbind

    and lhsRecValBindPat(ATPat(I, atpat)) =
	    lhsRecValBindAtPat atpat

      | lhsRecValBindPat(CONPat(I, _, longvid, atpat)) =
	    lhsRecValBindAtPat atpat

      | lhsRecValBindPat(COLONPat(I, pat, ty)) =
	    lhsRecValBindPat pat

      | lhsRecValBindPat(ASPat(I, _, vid, ty_opt, pat)) =
	    VIdMap.insert(lhsRecValBindPat pat, vid, IdStatus.v)

    and lhsRecValBindAtPat(WILDCARDAtPat(I)) =
	    VIdMap.empty

      | lhsRecValBindAtPat(SCONAtPat(I, scon)) =
	    VIdMap.empty

      | lhsRecValBindAtPat(IDAtPat(I, _, longvid)) =
	   (case LongVId.explode longvid
	      of ([], vid) => VIdMap.singleton(vid, IdStatus.v)
	       | _         => VIdMap.empty
	   )

      | lhsRecValBindAtPat(RECORDAtPat(I, patrow_opt)) =
	   (case patrow_opt
	      of NONE        => VIdMap.empty
	       | SOME patrow => lhsRecValBindPatRow patrow
	   )

      | lhsRecValBindAtPat(PARAtPat(I, pat)) =
	    lhsRecValBindPat pat

    and lhsRecValBindPatRow(DOTSPatRow(I)) =
	    VIdMap.empty

      | lhsRecValBindPatRow(FIELDPatRow(I, lab, pat, patrow_opt)) =
	let
	    val VE  = lhsRecValBindPat pat
	in
	    case patrow_opt
	      of NONE        => VE
	       | SOME patrow =>
		 VIdMap.unionWith #2 (VE, lhsRecValBindPatRow patrow)
	end
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML syntactic restrictions for modules
 *
 * Definition, Section 3.5
 *)

structure SyntacticRestrictionsModule : SYNTACTIC_RESTRICTIONS_MODULE =
struct
    (* Import *)

    open GrammarModule
    open BindingObjectsModule
    open Error


    (* Helpers for basis modification *)

    val empty  = BindingBasis.plus
    val plus   = BindingBasis.plus
    val plusG  = BindingBasis.plusG
    val plusF  = BindingBasis.plusF
    val plusE  = BindingBasis.plusE
    val plusSE = BindingBasis.plusSE

    infix plus plusG plusF plusE plusSE


    (* Inference rules [Section 5.7] *)


    (* Structure Expressions *)

    fun checkStrExp(B, STRUCTStrExp(I, strdec)) =
	let
	    val E = checkStrDec(B, strdec)
	in
	    E
	end

      | checkStrExp(B, IDStrExp(I, longstrid)) =
	let
	    val E = case BindingBasis.findLongStrId(B, longstrid)
		      of SOME E => E
		       | NONE   => BindingEnv.empty (* actually an error *)
	in
	    E
	end

      | checkStrExp(B, COLONStrExp(I, strexp, sigexp)) =
	let
	    val E1 = checkStrExp(B, strexp)
	    val E2 = checkSigExp(B, sigexp)
	in
	    E2
	end

      | checkStrExp(B, SEALStrExp(I, strexp, sigexp)) =
	let
	    val E1 = checkStrExp(B, strexp)
	    val E2 = checkSigExp(B, sigexp)
	in
	    E2
	end

      | checkStrExp(B, APPStrExp(I, funid, strexp)) =
	let
	    val E1 = checkStrExp(B, strexp)
	    val E2 = case BindingBasis.findFunId(B, funid)
		       of SOME E => E
		        | NONE   => BindingEnv.empty (* actually an error *)
	in
	    E2
	end

      | checkStrExp(B, LETStrExp(I, strdec, strexp)) =
	let
	    val E1 = checkStrDec(B, strdec)
	    val E2 = checkStrExp(B plusE E1, strexp)
	in
	    E2
	end


    (* Structure-level Declarations *)

    and checkStrDec(B, DECStrDec(I, dec)) =
	let
	    val E = SyntacticRestrictionsCore.checkDec(BindingBasis.Cof B, dec)
	in
	    E
	end

      | checkStrDec(B, STRUCTUREStrDec(I, strbind)) =
	let
	    val SE = checkStrBind(B, strbind)
	in
	    BindingEnv.fromSE SE
	end

      | checkStrDec(B, LOCALStrDec(I, strdec1, strdec2)) =
	let
	    val E1 = checkStrDec(B, strdec1)
	    val E2 = checkStrDec(B plusE E1, strdec2)
	in
	    E2
	end

      | checkStrDec(B, EMPTYStrDec(I)) =
	    BindingEnv.empty

      | checkStrDec(B, SEQStrDec(I, strdec1, strdec2)) =
	let
	    val E1 = checkStrDec(B, strdec1)
	    val E2 = checkStrDec(B plusE E1, strdec2)
	in
	    BindingEnv.plus(E1, E2)
	end


    (* Structure Bindings *)

    and checkStrBind(B, StrBind(I, strid, strexp, strbind_opt)) =
	let
	    val E  = checkStrExp(B, strexp)
	    val SE = case strbind_opt
		       of NONE         => StrIdMap.empty
		        | SOME strbind => checkStrBind(B, strbind)
	in
	    if StrIdMap.inDomain(SE, strid) then
		(* [Section 3.5, 1st bullet] *)
		errorStrId(I, "duplicate structure identifier ", strid)
	    else
		StrIdMap.insert(SE, strid, E)
	end


    (* Signature Expressions *)

    and checkSigExp(B, SIGSigExp(I, spec)) =
	let
	    val E = checkSpec(B, spec)
	in
	    E
	end

      | checkSigExp(B, IDSigExp(I, sigid)) =
	let
	    val E = case BindingBasis.findSigId(B, sigid)
		      of SOME E => E
		       | NONE   => BindingEnv.empty (* actually an error *)
	in
	    E
	end

      | checkSigExp(B, WHERETYPESigExp(I, sigexp, tyvarseq, longtycon, ty)) =
	let
	    val E  = checkSigExp(B, sigexp)
	    val U1 = SyntacticRestrictionsCore.checkTyVarseq tyvarseq
	    val U2 = SyntacticRestrictionsCore.checkTy ty
	in
	    if not(TyVarSet.isSubset(U2, U1)) then
		(* [Section 3.5, 4th bullet] *)
		error(I, "free type variables in type realisation")
	    else
		E
	end


    (* Signature Declarations *)

    and checkSigDec(B, SigDec(I, sigbind)) =
	let
	    val G = checkSigBind(B, sigbind)
	in
	    G
	end


    (* Signature Bindings *)

    and checkSigBind(B, SigBind(I, sigid, sigexp, sigbind_opt)) =
	let
	    val E = checkSigExp(B, sigexp)
	    val G = case sigbind_opt
		      of NONE         => SigIdMap.empty
		       | SOME sigbind => checkSigBind(B, sigbind)
	in
	    if SigIdMap.inDomain(G, sigid) then
		(* [Section 3.5, 1st bullet] *)
		errorSigId(I, "duplicate signature identifier ", sigid)
	    else
		SigIdMap.insert(G, sigid, E)
	end


    (* Specifications *)

    and checkSpec(B, VALSpec(I, valdesc)) =
	let
	    val VE = checkValDesc valdesc
	in
	    BindingEnv.fromVE VE
	end

      | checkSpec(B, TYPESpec(I, typdesc)) =
	let
	    val TE = checkTypDesc typdesc
	in
	    BindingEnv.fromTE TE
	end

      | checkSpec(B, EQTYPESpec(I, typdesc)) =
	let
	    val TE = checkTypDesc typdesc
	in
	    BindingEnv.fromTE TE
	end

      | checkSpec(B, DATATYPESpec(I, datdesc)) =
	let
	    val (VE,TE) = checkDatDesc datdesc
	in
	    BindingEnv.fromVEandTE(VE,TE)
	end

      | checkSpec(B, DATATYPE2Spec(I, tycon, longtycon)) =
	let
	    val VE = case BindingBasis.findLongTyCon(B, longtycon)
		       of SOME VE => VE
			| NONE    => VIdMap.empty (* actually an error *)
	    val TE = TyConMap.singleton(tycon, VE)
	in
	    BindingEnv.fromVEandTE(VE,TE)
	end

      | checkSpec(B, EXCEPTIONSpec(I, exdesc)) =
	let
	    val VE = checkExDesc exdesc
	in
	    BindingEnv.fromVE VE
	end

      | checkSpec(B, STRUCTURESpec(I, strdesc)) =
	let
	    val SE = checkStrDesc(B, strdesc)
	in
	    BindingEnv.fromSE SE
	end

      | checkSpec(B, INCLUDESpec(I, sigexp)) =
	let
	    val E = checkSigExp(B, sigexp)
	in
	    E
	end

      | checkSpec(B, EMPTYSpec(I)) =
	    BindingEnv.empty

      | checkSpec(B, SEQSpec(I, spec1, spec2)) =
	let
	    val E1 = checkSpec(B, spec1)
	    val E2 = checkSpec(B plusE E1, spec2)
	in
	    BindingEnv.plus(E1, E2)
	end

      | checkSpec(B, SHARINGTYPESpec(I, spec, longtycons)) =
	let
	    val E = checkSpec(B, spec)
	in
	    E
	end

      | checkSpec(B, SHARINGSpec(I, spec, longstrids)) =
	(* [Appendix A] *)
	let
	    val E = checkSpec(B, spec)
	in
	    E
	end


    (* Value Descriptions *)

    and checkValDesc(ValDesc(I, vid, ty, valdesc_opt)) =
	let
	    val U  = SyntacticRestrictionsCore.checkTy ty
	    val VE = case valdesc_opt
		       of NONE         => VIdMap.empty
			| SOME valdesc => checkValDesc valdesc
	in
	    if VIdMap.inDomain(VE, vid) then
		(* [Section 3.5, 2nd bullet] *)
		errorVId(I, "duplicate variable ", vid)
	    else if not(SyntacticRestrictionsCore.validBindVId vid) then
		(* [Section 3.5, 5th bullet] *)
		errorVId(I, "illegal specification of identifier ", vid)
	    else
		VIdMap.insert(VE, vid, IdStatus.v)
	end


    (* Type Descriptions *)

    and checkTypDesc(TypDesc(I, tyvarseq, tycon, typdesc_opt)) =
	let
	    val U  = SyntacticRestrictionsCore.checkTyVarseq tyvarseq
	    val TE = case typdesc_opt
		       of NONE         => TyConMap.empty
			| SOME typdesc => checkTypDesc typdesc
	in
	    if TyConMap.inDomain(TE, tycon) then
		(* [Section 3.5, 2nd bullet] *)
		errorTyCon(I, "duplicate type constructor ", tycon)
	    else
		TyConMap.insert(TE, tycon, VIdMap.empty)
	end


    (* Datatype Descriptions *)

    and checkDatDesc(DatDesc(I, tyvarseq, tycon, condesc, datdesc_opt)) =
	let
	    val  U1      = SyntacticRestrictionsCore.checkTyVarseq tyvarseq
	    val (U2,VE)  = checkConDesc condesc
	    val(VE',TE') = case datdesc_opt
			     of NONE         => ( VIdMap.empty, TyConMap.empty )
			      | SOME datdesc => checkDatDesc datdesc
	in
	    if TyConMap.inDomain(TE', tycon) then
		(* [Section 3.5, 2nd bullet] *)
		errorTyCon(I, "duplicate type constructor ", tycon)
	    else if not(TyVarSet.isSubset(U2, U1)) then
		(* [Section 3.5,4th bullet]*)
		error(I, "free type variables in datatype description")
	    else
	    ( VIdMap.unionWithi (fn(vid,_,_) =>
		(* [Section 3.5, 2nd bullet] *)
		errorVId(I, "duplicate data cnstructor ", vid)) (VE,VE')
	    , TyConMap.insert(TE', tycon, VE)
	    )
	end


    (* Constructor Descriptions *)

    and checkConDesc(ConDesc(I, vid, ty_opt, condesc_opt)) =
	let
	    val  U      = case ty_opt
			    of NONE    => TyVarSet.empty
			     | SOME ty => SyntacticRestrictionsCore.checkTy ty
	    val (U',VE) = case condesc_opt
			    of NONE         => ( TyVarSet.empty, VIdMap.empty )
			     | SOME condesc => checkConDesc condesc
	in
	    if VIdMap.inDomain(VE, vid) then
		(* [Section 3.5, 2nd bullet] *)
		errorVId(I, "duplicate data constructor ", vid)
	    else if not(SyntacticRestrictionsCore.validConBindVId vid) then
		(* [Section 3.5, 5th bullet] *)
		errorVId(I, "illegal specifiation of identifier ", vid)
	    else
		( TyVarSet.union(U, U'), VIdMap.insert(VE, vid, IdStatus.c) )
	end


    (* Exception Description *)

    and checkExDesc(ExDesc(I, vid, ty_opt, exdesc_opt)) =
	let
	    val U  = case ty_opt
		       of NONE    => TyVarSet.empty
			| SOME ty => SyntacticRestrictionsCore.checkTy ty
	    val VE = case exdesc_opt
		       of NONE        => VIdMap.empty
			| SOME exdesc => checkExDesc exdesc
	in
	    if VIdMap.inDomain(VE, vid) then
		(* [Section 3.5, 2nd bullet] *)
		errorVId(I, "duplicate exception constructor ", vid)
	    else if not(SyntacticRestrictionsCore.validConBindVId vid) then
		(* [Section 3.5, 5th bullet] *)
		errorVId(I, "illegal specification of identifier ", vid)
	    else
		VIdMap.insert(VE, vid, IdStatus.e)
	end


    (* Structure Descriptions *)

    and checkStrDesc(B, StrDesc(I, strid, sigexp, strdesc_opt)) =
	let
	    val E  = checkSigExp(B, sigexp)
	    val SE = case strdesc_opt
		       of NONE         => StrIdMap.empty
		        | SOME strdesc => checkStrDesc(B, strdesc)
	in
	    if StrIdMap.inDomain(SE, strid) then
		(* [Section 3.5, 2nd bullet] *)
		errorStrId(I, "duplicate structure identifier ", strid)
	    else
		StrIdMap.insert(SE, strid, E)
	end


    (* Functor Declarations *)

    and checkFunDec(B, FunDec(I, funbind)) =
	let
	    val F = checkFunBind(B, funbind)
	in
	    F
	end


    (* Functor Bindings *)

    and checkFunBind(B, FunBind(I, funid, strid, sigexp, strexp, funbind_opt)) =
	let
	    val E1 = checkSigExp(B, sigexp)
	    val E2 = checkStrExp(B plusSE StrIdMap.singleton(strid, E1), strexp)
	    val F  = case funbind_opt
		       of NONE         => FunIdMap.empty
			| SOME funbind => checkFunBind(B, funbind)
	in
	    if FunIdMap.inDomain(F, funid) then
		(* [Section 3.5, 1st bullet] *)
		errorFunId(I, "duplicate functor identifier ", funid)
	    else
		FunIdMap.insert(F, funid, E2)
	end


    (* Top-level Declarations *)

    and checkTopDec(B, STRDECTopDec(I, strdec, topdec_opt)) =
	let
	    val E   = checkStrDec(B, strdec)
	    val B'  = case topdec_opt
			of NONE        => BindingBasis.empty
			 | SOME topdec => checkTopDec(B plusE E, topdec)
	in
	    BindingBasis.fromE E plus B'
	end

      | checkTopDec(B, SIGDECTopDec(I, sigdec, topdec_opt)) =
	let
	    val G   = checkSigDec(B, sigdec)
	    val B'  = case topdec_opt
			of NONE        => BindingBasis.empty
			 | SOME topdec => checkTopDec(B plusG G, topdec)
	in
	    BindingBasis.fromG G plus B'
	end

      | checkTopDec(B, FUNDECTopDec(I, fundec, topdec_opt)) =
	let
	    val F   = checkFunDec(B, fundec)
	    val B'  = case topdec_opt
			of NONE        => BindingBasis.empty
			 | SOME topdec => checkTopDec(B plusF F, topdec)
	in
	    BindingBasis.fromF F plus B'
	end
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML syntactic restrictions for programs
 *)

signature SYNTACTIC_RESTRICTIONS_PROGRAM =
sig
    (* Import *)

    type Basis		= SyntacticRestrictionsModule.Basis
    type Program	= GrammarProgram.Program

    (* Export *)

    val checkProgram :	Basis * Program -> Basis
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML syntactic restrictions for programs
 *)

structure SyntacticRestrictionsProgram : SYNTACTIC_RESTRICTIONS_PROGRAM =
struct
    (* Import *)

    open GrammarProgram

    type Basis = SyntacticRestrictionsModule.Basis


    (* Operation *)

    fun checkProgram(B, Program(I, topdec, program_opt)) =
	let
	    val B1  = SyntacticRestrictionsModule.checkTopDec(B, topdec)
	    val B'  = BindingBasis.plus(B, B1)
	    val B'' = case program_opt
			of NONE         => B'
			 | SOME program => checkProgram(B', program)
	in
	    B''
	end
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML combined basis
 *
 * Definition, Section 8
 *)

signature BASIS =
sig
    (* Import *)

    type StaticBasis  = StaticObjectsModule.Basis		(* [B_STAT] *)
    type DynamicBasis = DynamicObjectsModule.Basis		(* [B_DYN] *)


    (* Type [Section 8] *)

    type Basis = StaticBasis * DynamicBasis			(* [B] *)


    (* Operations *)

    val B_STATof :	Basis -> StaticBasis
    val B_DYNof :	Basis -> DynamicBasis

    val oplus :		Basis * Basis -> Basis
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML combined basis
 *
 * Definition, Section 8
 *)

structure Basis :> BASIS =
struct
    (* Import *)

    type StaticBasis  = StaticObjectsModule.Basis		(* [B_STAT] *)
    type DynamicBasis = DynamicObjectsModule.Basis		(* [B_DYN] *)


    (* Type [Section 8] *)

    type Basis = StaticBasis * DynamicBasis			(* [B] *)


    (* Projections *)

    fun B_STATof (B_STAT,B_DYN) = B_STAT
    fun B_DYNof  (B_STAT,B_DYN) = B_DYN


    (* Modification [Section 4.3] *)

    infix oplus

    fun (B_STAT,B_DYN) oplus (B_STAT',B_DYN') =
	    ( StaticBasis.plus(B_STAT, B_STAT')
	    , DynamicBasis.plus(B_DYN, B_DYN')
	    )
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML programs
 *
 * Definition, Section 8
 *
 * Note:
 *     State is passed as reference and modified via side effects. This way
 *     expanding out the state and exception convention in the inference rules
 *     of modules and core can be avoided. Note that the state therefore
 *     never is returned.
 *)

signature PROGRAM =
sig
    (* Import *)

    type Program      = GrammarProgram.Program
    type StaticBasis  = Basis.StaticBasis
    type DynamicBasis = Basis.DynamicBasis
    type Basis        = Basis.Basis
    type State        = State.State


    (* Export *)

    val execProgram : bool -> State ref * Basis * Program -> Basis
    val elabProgram : bool -> StaticBasis * Program -> StaticBasis
    val evalProgram : bool -> State ref * DynamicBasis * Program -> DynamicBasis
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML core evaluation
 *
 * Definition, Section 6.7
 *
 * Notes:
 *   - State is passed as reference and modified via side effects. This way
 *     expanding out of the state and exception convention in the inference
 *     rules can be avoided (would really be a pain). Note that the state
 *     therefore never is returned.
 *   - Doing so, we can model the exception convention using exceptions.
 *)

signature EVAL_CORE =
sig
    (* Import types *)

    type Dec   = GrammarCore.Dec
    type Env   = DynamicObjectsCore.Env
    type State = DynamicObjectsCore.State

    (* Export *)

    val evalDec : State ref * Env * Dec -> Env
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML basic values
 *
 * Definition, Section 6.4
 *)

signature BASVAL =
sig
    (* Import *)

    type BasVal	= DynamicObjectsCore.BasVal
    type Val	= DynamicObjectsCore.Val


    (* Operations *)

    exception TypeError of string

    val APPLY :    BasVal * Val -> Val (* / Pack *)

    val toString : BasVal -> string
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML basic values
 *
 * Definition, Section 6.4
 *)

structure BasVal :> BASVAL =
struct
    (* Import *)

    open DynamicObjectsCore


    (* Conversions *)

    fun toString b = b


    (* Application of basic values *)

    exception TypeError = Library.TypeError

    fun fromBool b = VId(VId.fromString(if b then "true" else "false"))

    fun APPLY("=", v) =
	(case Val.toPair v
	   of SOME vv => (fromBool(Val.equal vv) handle Domain =>
			  raise TypeError "equality type expected")
	    | NONE    => raise TypeError "pair expected"
	)
      | APPLY(b, v) = Library.APPLY(b, v)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML core evaluation
 *
 * Definition, Sections 6.7 and 6.2
 *
 * Notes:
 *   - State is passed as reference and modified via side effects. This way
 *     expanding out the state and exception convention in the inference rules
 *     can be avoided (would really be a pain). Note that the state therefore
 *     never is returned.
 *   - Doing so, we can model the exception convention using exceptions.
 *     Rules of the form A |- phrase => A'/p therefore turn into
 *     A |- phrase => A'.
 *   - We only pass the state where necessary.
 *)

structure EvalCore : EVAL_CORE =
struct
    (* Import *)

    open GrammarCore
    open DynamicObjectsCore
    open Error


    (* Helpers for environment modification *)

    val plus        = DynamicEnv.plus
    val plusVE      = DynamicEnv.plusVE
    val plusTE      = DynamicEnv.plusTE
    val plusVEandTE = DynamicEnv.plusVEandTE

    infix plus plusVE plusTE plusVEandTE


    (* Evaluating special constants [Section 6.2] *)

    fun valSCon(SCon.INT(b, sc, ref t_opt)) =
	  SVal(SVal.INT(Library.intFromString(b, sc, t_opt)))
      | valSCon(SCon.WORD(b, sc, ref t_opt)) =
	  SVal(SVal.WORD(Library.wordFromString(b, sc, t_opt)))
      | valSCon(SCon.CHAR(sc, ref t_opt)) =
	  SVal(SVal.CHAR(Library.charFromString(sc, t_opt)))
      | valSCon(SCon.STRING(sc, ref t_opt)) =
	  SVal(SVal.STRING(Library.stringFromString(sc, t_opt)))
      | valSCon(SCon.REAL(sc, ref t_opt)) =
	  SVal(SVal.REAL(Library.realFromString(sc, t_opt)))


    (* Inference rules [Section 6.7] *)

    (* Atomic Expressions *)

    fun evalAtExp(s,E, SCONAtExp(I, scon)) =
	(* [Rule 90] *)
	(valSCon scon handle Overflow =>
	    error(I, "runtime error: special constant out of range")
	)

      | evalAtExp(s,E, IDAtExp(I, _, longvid)) =
	(* [Rule 91] *)
	let
	    val (v,is) = case DynamicEnv.findLongVId(E, longvid)
			   of SOME valstr => valstr
			    | NONE =>
			      errorLongVId(I, "runtime error: \
					      \unknown identifier ", longvid)
	in
	    v
	end

      | evalAtExp(s,E, RECORDAtExp(I, exprow_opt)) =
	(* [Rule 92] *)
	let
	    val r = case exprow_opt
		      of NONE        => LabMap.empty
		       | SOME exprow => evalExpRow(s,E, exprow)
	in
	    Record r
	end

      | evalAtExp(s,E, LETAtExp(I, dec, exp)) =
	(* [Rule 93] *)
	let
	    val E' = evalDec(s,E, dec)
	    val v  = evalExp(s,E plus E', exp)
	in
	    v
	end

      | evalAtExp(s,E, PARAtExp(I, exp)) =
	(* [Rule 94] *)
	let
	    val v = evalExp(s,E, exp)
	in
	    v
	end


    (* Expression Rows *)

    and evalExpRow(s,E, ExpRow(I, lab, exp, exprow_opt)) =
	(* [Rule 95] *)
	let
	    val v = evalExp(s,E, exp)
	    val r = case exprow_opt
		      of NONE        => LabMap.empty
		       | SOME exprow => evalExpRow(s,E, exprow)
	in
	    LabMap.insert(r, lab, v)
	end


    (* Expressions *)

    and evalExp(s,E, ATExp(I, atexp)) =
	(* [Rule 96] *)
	let
	    val v = evalAtExp(s,E, atexp)
	in
	    v
	end

      | evalExp(s,E, APPExp(I, exp, atexp)) =
	(* [Rules 97 to 103] *)
	let
	    val v1 = evalExp(s,E, exp)
	    val v  = evalAtExp(s,E, atexp)
	in
	    case v1
	      of VId vid =>
		 if vid = VId.fromString "ref" then
		     (* [Rule 99] *)
		     let
		         val a = Addr.addr()
		     in
			 s := State.insertAddr(!s, a, v);
			 Addr a
		     end
		 else
		     (* [Rule 97] *)
		     VIdVal (vid,v)

	       | ExVal(ExName en) =>
		 (* [Rule 98] *)
		 ExVal(ExNameVal(en,v))

	       | Assign =>
		 (* [Rule 100] *)
		 (case Val.toPair v
		    of SOME(Addr a, v) =>
			( s := State.insertAddr(!s, a, v)
			; Record LabMap.empty
			)
		     | _ => error(I, "runtime type error: address expected")
		 )

	       | BasVal b =>
		 (* [Rule 101] *)
		 (BasVal.APPLY(b, v) handle BasVal.TypeError s =>
		     error(I, "runtime type error: " ^ s))

	       | FcnClosure(match,E',VE) =>
		 (* [Rule 102] *)
		 (let
		     val v' = evalMatch(s,E' plusVE DynamicEnv.Rec VE, v, match)
		  in
		     v'
		  end
		  handle FAIL =>
		     (* [Rule 103] *)
		     raise Pack(ExName InitialDynamicEnv.enMatch)
		 )
	       | _ =>
		 error(I, "runtime type error: applicative value expected")
	end

      | evalExp(s,E, COLONExp(I, exp, _)) =
	(* Omitted [Section 6.1] *)
	evalExp(s,E, exp)

      | evalExp(s,E, HANDLEExp(I, exp, match)) =
	(* [Rule 104 to 106] *)
	(let
	    val v = evalExp(s,E, exp)
	 in
	    (* [Rule 104] *)
	    v
	 end
	 handle Pack e =>
	    let
		val v = evalMatch(s,E,ExVal e, match)
	    in
		(* [Rule 105] *)
		v
	    end
	    handle FAIL =>
		(* [Rule 106] *)
		raise Pack e
	)

      | evalExp(s,E, RAISEExp(I, exp)) =
	(* [Rule 107] *)
	let
	    val e = case evalExp(s,E, exp)
		      of ExVal e => e
		       | _ => error(I, "runtime type error: \
				       \exception value expected")
	in
	    raise Pack e
	end

      | evalExp(s,E, FNExp(I, match)) =
	(* [Rule 108] *)
	FcnClosure(match,E,VIdMap.empty)


    (* Matches *)

    and evalMatch(s,E,v, Match(I, mrule, match_opt)) =
	(* [Rules 109 to 111] *)
	let
	    val v' = evalMrule(s,E,v, mrule)
	in
	    (* [Rule 109] *)
	    v'
	end
	handle FAIL =>
	    case match_opt
	      of NONE =>
		 (* [Rule 110] *)
		 raise FAIL

	       | SOME match =>
		 (* [Rule 111] *)
		 let
		     val v' = evalMatch(s,E,v, match)
		 in
		     v'
		 end


    (* Match rules *)

    and evalMrule(s,E,v, Mrule(I, pat, exp)) =
	(* [Rules 112 and 113] *)
	let
	    val VE = evalPat(s,E,v, pat)
	    (* [Rule 112] *)
	    val v' = evalExp(s,E plusVE VE, exp)
	in
	    v'
	end
	(* FAIL on evalPat propagates through [Rule 113] *)


    (* Declarations *)

    and evalDec(s,E, VALDec(I, tyvarseq, valbind)) =
	(* [Rule 114] *)
	let
	    val VE = evalValBind(s,E, valbind)
	in
	    DynamicEnv.fromVE VE
	end

      | evalDec(s,E, TYPEDec(I, typbind)) =
	(* [Rule 115] *)
	let
	    val TE = evalTypBind(typbind)
	in
	    DynamicEnv.fromTE TE
	end

      | evalDec(s,E, DATATYPEDec(I, datbind)) =
	(* [Rule 116] *)
	let
	    val (VE,TE) = evalDatBind(datbind)
	in
	    DynamicEnv.fromVEandTE(VE,TE)
	end

      | evalDec(s,E, DATATYPE2Dec(I, tycon, longtycon)) =
	(* [Rule 117] *)
	let
	    val VE = case DynamicEnv.findLongTyCon(E, longtycon)
		       of SOME VE => VE
			| NONE =>
			  errorLongTyCon(I, "runtime error: unknown type ",
					    longtycon)
	in
	    DynamicEnv.fromVEandTE(VE, TyConMap.singleton(tycon, VE))
	end

      | evalDec(s,E, ABSTYPEDec(I, datbind, dec)) =
	(* [Rule 118] *)
	let
	    val (VE,TE) = evalDatBind(datbind)
	    val    E'   = evalDec(s,E plusVEandTE (VE,TE), dec)
	in
	    E'
	end

      | evalDec(s,E, EXCEPTIONDec(I, exbind)) =
	(* [Rule 119] *)
	let
	    val VE = evalExBind(s,E, exbind)
	in
	    DynamicEnv.fromVE VE
	end

      | evalDec(s,E, LOCALDec(I, dec1, dec2)) =
	(* [Rule 120] *)
	let
	    val E1 = evalDec(s,E, dec1)
	    val E2 = evalDec(s,E plus E1, dec2)
	in
	    E2
	end

      | evalDec(s,E, OPENDec(I, longstrids)) =
	(* [Rule 121] *)
	let
	    val Es =
		List.map
		    (fn longstrid =>
			case DynamicEnv.findLongStrId(E, longstrid)
			  of SOME E => E
			   | NONE =>
			     errorLongStrId(I, "runtime error: unknown \
					       \structure ", longstrid) )
		    longstrids
	in
	    List.foldl DynamicEnv.plus DynamicEnv.empty Es
	end

      | evalDec(s,E, EMPTYDec(I)) =
	(* [Rule 122] *)
	DynamicEnv.empty

      | evalDec(s,E, SEQDec(I, dec1, dec2)) =
	(* [Rule 123] *)
	let
	    val E1 = evalDec(s,E, dec1)
	    val E2 = evalDec(s,E plus E1, dec2)
	in
	    E1 plus E2
	end


    (* Value Bindings *)

    and evalValBind(s,E, PLAINValBind(I, pat, exp, valbind_opt)) =
	(* [Rule 124 and 125] *)
	(let
	    val v   = evalExp(s,E, exp)
	    val VE  = evalPat(s,E,v, pat)
	    (* [Rule 124] *)
	    val VE' = case valbind_opt
			of NONE         => VIdMap.empty
			 | SOME valbind => evalValBind(s,E, valbind)
	 in
	    VIdMap.unionWith #2 (VE, VE')
	 end
	 handle FAIL =>
	    (* [Rule 125] *)
	    raise Pack(ExName InitialDynamicEnv.enBind)
	)

      | evalValBind(s,E, RECValBind(I, valbind)) =
	(* [Rule 126] *)
	let
	    val VE = evalValBind(s,E, valbind)
	in
	    DynamicEnv.Rec VE
	end


    (* Type Bindings *)

    and evalTypBind(TypBind(I, tyvarseq, tycon, ty, typbind_opt)) =
	(* [Rule 127] *)
	let
	    val TE = case typbind_opt
		       of NONE         => TyConMap.empty
			| SOME typbind => evalTypBind(typbind)
	in
	    TyConMap.insert(TE, tycon, VIdMap.empty)
	end


    (* Datatype Bindings *)

    and evalDatBind(DatBind(I, tyvarseq, tycon, conbind, datbind_opt)) =
	(* [Rule 128] *)
	let
	    val  VE       = evalConBind(conbind)
	    val (VE',TE') = case datbind_opt
			      of NONE          => ( VIdMap.empty, TyConMap.empty )
			       | SOME datbind' => evalDatBind(datbind')
	in
	    ( VIdMap.unionWith #2 (VE, VE')
	    , TyConMap.insert(TE', tycon, VE)
	    )
	end


    (* Constructor Bindings *)

    and evalConBind(ConBind(I, _, vid, _, conbind_opt)) =
	(* [Rule 129] *)
	let
	    val VE = case conbind_opt
		       of NONE         => VIdMap.empty
			| SOME conbind => evalConBind(conbind)
	in
	    VIdMap.insert(VE, vid, (VId vid,IdStatus.c))
	end


    (* Exception Bindings *)

    and evalExBind(s,E, NEWExBind(I, _, vid, _, exbind_opt)) =
	(* [Rule 130] *)
	let
	    val en = ExName.exname vid
	    val VE = case exbind_opt
		       of NONE        => VIdMap.empty
			| SOME exbind => evalExBind(s,E, exbind)
	in
	    s := State.insertExName(!s, en);
	    VIdMap.insert(VE, vid, (ExVal(ExName en),IdStatus.e))
	end

      | evalExBind(s,E, EQUALExBind(I, _, vid, _, longvid, exbind_opt)) =
	(* [Rule 131] *)
	let
	    val en = case DynamicEnv.findLongVId(E, longvid)
		       of SOME(en,IdStatus.e) => en
			| SOME _ =>
			  errorLongVId(I, "runtime error: non-exception \
					  \identifier ", longvid)
			| NONE =>
			  errorLongVId(I, "runtime error: unknown identifier ",
					  longvid)
	    val VE = case exbind_opt
		       of NONE        => VIdMap.empty
			| SOME exbind => evalExBind(s,E, exbind)
	in
	    VIdMap.insert(VE, vid, (en,IdStatus.e))
	end


    (* Atomic Patterns *)

    and evalAtPat(s,E,v, WILDCARDAtPat(I)) =
	(* [Rule 132] *)
	VIdMap.empty

      | evalAtPat(s,E,v, SCONAtPat(I, scon)) =
	(* [Rule 133 and 134] *)
	((if Val.equal(v, valSCon scon) then
	   (* [Rule 133] *)
	   VIdMap.empty
	else
	   (* [Rule 134] *)
	   raise FAIL
	) handle Overflow =>
	    error(I, "runtime error: special constant out of range")
	)

      | evalAtPat(s,E,v, IDAtPat(I, _, longvid)) =
	(* [Rule 135 to 137] *)
	let
	    val (strids,vid) = LongVId.explode longvid
	in
	    if List.null strids andalso
	       ( case DynamicEnv.findVId(E, vid)
		   of NONE       => true
		    | SOME(_,is) => is = IdStatus.v )
	    then
		(* [Rule 135] *)
		VIdMap.singleton(vid, (v,IdStatus.v))
	    else
		let
		    val (v',is) =
			case DynamicEnv.findLongVId(E, longvid)
			  of SOME valstr => valstr
			   | NONE => errorLongVId(I, "runtime error: \
						     \unknown constructor ",
						  longvid)
		in
		    if is = IdStatus.v then
			errorLongVId(I, "runtime error: non-constructor ",
					longvid)
		    else if Val.equal(v, v') then
			(* [Rule 136] *)
			VIdMap.empty
		    else
			(* [Rule 137] *)
			raise FAIL
		end
	end

      | evalAtPat(s,E,v, RECORDAtPat(I, patrow_opt)) =
	(* [Rule 138] *)
	let
	    val r  = case v
		       of Record r => r
		        | _ =>
			  error(I, "runtime type error: record expected")

	    val VE = case patrow_opt
		       of NONE        =>
			  if LabMap.isEmpty r then
			     VIdMap.empty
			  else
			     error(I, "runtime type error: \
				      \empty record expected")

			| SOME patrow =>
			      evalPatRow(s,E,r, patrow)
	in
	    VE
	end

      | evalAtPat(s,E,v, PARAtPat(I, pat)) =
	(* [Rule 139] *)
	let
	    val VE = evalPat(s,E,v, pat)
	in
	    VE
	end


    (* Pattern Rows *)

    and evalPatRow(s,E,r, DOTSPatRow(I)) =
	(* [Rule 140] *)
	VIdMap.empty

      | evalPatRow(s,E,r, FIELDPatRow(I, lab, pat, patrow_opt)) =
	(* [Rule 141 and 142] *)
	let
	    val v   = case LabMap.find(r, lab)
		        of SOME v => v
		         | _ => errorLab(I, "runtime type error: \
					    \unmatched label ", lab)
	    val VE  = evalPat(s,E,v, pat)
	    (* FAIL on evalPat propagates through [Rule 142] *)
	    (* [Rule 141] *)
	    val VE' = case patrow_opt
			of NONE        => VIdMap.empty
			 | SOME patrow => evalPatRow(s,E,r, patrow)
	in
	    VIdMap.unionWithi #2 (VE, VE')
	end


    (* Patterns *)

    and evalPat(s,E,v, ATPat(I, atpat)) =
	(* [Rule 143] *)
	let
	    val VE = evalAtPat(s,E,v, atpat)
	in
	    VE
	end

      | evalPat(s,E,v, CONPat(I, _, longvid, atpat)) =
	(* [Rules 144 to 148] *)
	let
	    val (strids,vid) = LongVId.explode longvid
	in
	    if List.null strids andalso vid = VId.fromString "ref" then
		case v
		  of Addr a =>
		     (* [Rule 148] *)
		     let
			 val v =  case State.findAddr(!s, a)
				    of SOME v => v
				     | NONE   =>
				       raise Fail "EvalCore.evalPat: \
						  \invalid address"
			 val VE = evalAtPat(s,E,v, atpat)
		     in
			 VE
		     end
		   | _ =>
			 error(I, "runtime type error: address expected")
	    else
		case DynamicEnv.findLongVId(E, longvid)
		  of SOME(VId vid, IdStatus.c) =>
		     (case v
			of VIdVal(vid',v') =>
			   if vid = vid' then
			       (* [Rule 144] *)
			       let
				   val VE = evalAtPat(s,E,v', atpat)
			       in
				   VE
			       end
			   else
			       (* [Rule 145] *)
			       raise FAIL
			 | _ =>
			       (* [Rule 145] *)
			       raise FAIL
		     )
		   | SOME(ExVal(ExName en), IdStatus.e) =>
		     (case v
			of ExVal(ExNameVal(en',v')) =>
	        	   if en = en' then
			       (* [Rule 146] *)
			       let
				   val VE = evalAtPat(s,E,v', atpat)
			       in
				   VE
			       end
			   else
			       (* [Rule 147] *)
			       raise FAIL
			 | _ =>
			       (* [Rule 147] *)
			       raise FAIL
		     )
		   | _ =>
			error(I, "runtime type error: constructor expected")
	end

      | evalPat(s,E,v, COLONPat(I, pat, _)) =
	(* Omitted [Section 6.1] *)
	evalPat(s,E,v, pat)

      | evalPat(s,E,v, ASPat(I, _, vid, _, pat)) =
	(* [Rule 149] *)
	let
	    val VE = evalPat(s,E,v, pat)
	in
	    VIdMap.insert(VE, vid, (v,IdStatus.v))
	end
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML modules evaluation
 *
 * Definition, Section 7.3
 *
 * Notes:
 *   - State is passed as reference and modified via side effects. This way
 *     expanding out the state and exception convention in the inference rules
 *     can be avoided (would really be a pain). Note that the state therefore
 *     never is returned.
 *   - Doing so, we can model the exception convention using exceptions.
 *)

signature EVAL_MODULE =
sig
    (* Import types *)

    type TopDec = GrammarModule.TopDec
    type Basis  = DynamicObjectsModule.Basis
    type State  = EvalCore.State


    (* Export *)

    val evalTopDec :  State ref * Basis * TopDec -> Basis
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML interface basis
 *
 * Definition, Section 7.2
 *)

signature INTBASIS =
sig
    (* Import types *)

    type longTyCon	= IdsCore.longTyCon
    type SigId		= IdsModule.SigId

    type Int		= DynamicObjectsModule.Int
    type ValInt		= DynamicObjectsModule.ValInt
    type Basis		= DynamicObjectsModule.Basis
    type IntBasis	= DynamicObjectsModule.IntBasis


    (* Operations *)

    val Inter :		Basis -> IntBasis

    val plusI :		IntBasis * Int -> IntBasis

    val findSigId :	IntBasis * SigId     -> Int option
    val findLongTyCon :	IntBasis * longTyCon -> ValInt option
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML interface basis
 *
 * Definition, Section 7.2
 *)

structure IntBasis :> INTBASIS =
struct
    (* Import types *)

    open IdsCore
    open IdsModule
    open DynamicObjectsModule


    (* Injections [Section 7.2] *)

    fun Inter (F,G,E) = (G, Inter.Inter E)


    (* Modifications [Sections 4.3 and 7.2] *)

    infix plusI

    fun (G,I) plusI I'  = ( G, Inter.plus(I,I') )


    (* Application (lookup) [Sections 7.2 and 4.3] *)

    fun findSigId((G,I), sigid) = SigIdMap.find(G, sigid)

    fun findLongTyCon((G,I), longtycon) = Inter.findLongTyCon(I, longtycon)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML modules evaluation
 *
 * Definition, Section 7.3
 *
 * Notes:
 *   - State is passed as reference and modified via side effects. This way
 *     expanding out the state and exception convention in the inference rules
 *     can be avoided (would really be a pain). Note that the state therefore
 *     never is returned.
 *   - Doing so, we can model the exception convention using exceptions.
 *     Rules of the form A |- phrase => A'/p therefore turn into
 *     A |- phrase => A'.
 *   - We only pass the state where necessary, ie. strexp, strdec, strbind, and
 *     topdec (compare note in [Section 7.3]).
 *   - There is a typo in the Definition in rule 182: both occurances of IB
 *     should be replaced by B.
 *   - The rules for toplevel declarations are all wrong: in the conclusions,
 *     the result right of the arrow must be B' <+ B''> instead of B'<'> in
 *     all three rules.
 *)

structure EvalModule : EVAL_MODULE =
struct
    (* Import *)

    open GrammarModule
    open DynamicObjectsModule
    open Error

    type State = EvalCore.State


    (* Helpers for basis modification *)

    val plus    = DynamicBasis.plus
    val plusSE  = DynamicBasis.plusSE
    val plusG   = DynamicBasis.plusG
    val plusF   = DynamicBasis.plusF
    val plusE   = DynamicBasis.plusE

    infix plus plusG plusF plusE plusSE



    (* Inference rules [Section 7.3] *)


    (* Structure Expressions *)

    fun evalStrExp(s,B, STRUCTStrExp(I, strdec)) =
	(* [Rule 150] *)
	let
	    val E = evalStrDec(s,B, strdec)
	in
	    E
	end

      | evalStrExp(s,B, IDStrExp(I, longstrid)) =
	(* [Rule 151] *)
	let
	    val E = case DynamicBasis.findLongStrId(B, longstrid)
		      of SOME E => E
		       | NONE =>
			 errorLongStrId(I, "runtime error: unknown structure ",
					   longstrid)
	in
	    E
	end

      | evalStrExp(s,B, COLONStrExp(I, strexp, sigexp)) =
	(* [Rule 152] *)
	let
	    val E = evalStrExp(s,B, strexp)
	    val I = evalSigExp(IntBasis.Inter B, sigexp)
	in
	    Inter.cutdown(E, I)
	end

      | evalStrExp(s,B, SEALStrExp(_, strexp, sigexp)) =
	(* [Rule 153] *)
	let
	    val E = evalStrExp(s,B, strexp)
	    val I = evalSigExp(IntBasis.Inter B, sigexp)
	in
	    Inter.cutdown(E, I)
	end

      | evalStrExp(s,B, APPStrExp(I', funid, strexp)) =
	(* [Rule 154] *)
	let
	    val FunctorClosure((strid, I), strexp', B') =
		      case DynamicBasis.findFunId(B, funid)
			of SOME funcclos => funcclos
			 | NONE => errorFunId(I', "runtime error: \
						  \unknown functor ", funid)
	    val E  = evalStrExp(s,B, strexp)
	    val E' = evalStrExp(
			s,
			B' plusSE StrIdMap.singleton(strid, Inter.cutdown(E,I)),
			strexp')
	in
	    E'
	end

      | evalStrExp(s,B, LETStrExp(I, strdec, strexp)) =
	(* [Rule 155] *)
	let
	    val E  = evalStrDec(s,B, strdec)
	    val E' = evalStrExp(s,B plusE E, strexp)
	in
	    E'
	end


    (* Structure-level Declarations *)

    and evalStrDec(s,B, DECStrDec(I, dec)) =
	(* [Rule 156] *)
	let
	    val E' = EvalCore.evalDec(s,DynamicBasis.Eof B, dec)
	in
	    E'
	end

      | evalStrDec(s,B, STRUCTUREStrDec(I, strbind)) =
	(* [Rule 157] *)
	let
	    val SE = evalStrBind(s,B, strbind)
	in
	    DynamicEnv.fromSE SE
	end

      | evalStrDec(s,B, LOCALStrDec(I, strdec1, strdec2)) =
	(* [Rule 158] *)
	let
	    val E1 = evalStrDec(s,B, strdec1)
	    val E2 = evalStrDec(s,B plusE E1, strdec2)
	in
	    E2
	end

      | evalStrDec(s,B, EMPTYStrDec(I)) =
	(* [Rule 159] *)
	DynamicEnv.empty

      | evalStrDec(s,B, SEQStrDec(I, strdec1, strdec2)) =
	(* [Rule 160] *)
	let
	    val E1 = evalStrDec(s,B, strdec1)
	    val E2 = evalStrDec(s,B plusE E1, strdec2)
	in
	    DynamicEnv.plus(E1, E2)
	end


    (* Structure Bindings *)

    and evalStrBind(s,B, StrBind(I, strid, strexp, strbind_opt)) =
	(* [Rule 161] *)
	let
	    val E  = evalStrExp(s,B, strexp)
	    val SE = case strbind_opt
		       of NONE         => StrIdMap.empty
		        | SOME strbind => evalStrBind(s,B, strbind)
	in
	    StrIdMap.insert(SE, strid, E)
	end


    (* Signature Expressions *)

    and evalSigExp(IB, SIGSigExp(I, spec)) =
	(* [Rule 162] *)
	let
	    val I = evalSpec(IB, spec)
	in
	    I
	end

      | evalSigExp(IB, IDSigExp(I, sigid)) =
	(* [Rule 163] *)
	let
	    val I = case IntBasis.findSigId(IB, sigid)
		      of SOME I => I
		       | NONE   => errorSigId(I, "runtime error: unknown \
						 \signature ",sigid)
	in
	    I
	end

      | evalSigExp(IB, WHERETYPESigExp(I, sigexp, _, _, _)) =
	(* Omitted [Section 7.1] *)
	evalSigExp(IB, sigexp)


    (* Signature Declarations *)

    and evalSigDec(IB, SigDec(I, sigbind)) =
	(* [Rule 164] *)
	let
	    val G = evalSigBind(IB, sigbind)
	in
	    G
	end


    (* Signature Bindings *)

    and evalSigBind(IB, SigBind(I, sigid, sigexp, sigbind_opt)) =
	(* [Rule 165] *)
	let
	    val I = evalSigExp(IB, sigexp)
	    val G = case sigbind_opt
		      of NONE         => SigIdMap.empty
		       | SOME sigbind => evalSigBind(IB, sigbind)
	in
	    SigIdMap.insert(G, sigid, I)
	end


    (* Specifications *)

    and evalSpec(IB, VALSpec(I, valdesc)) =
	(* [Rule 166] *)
	let
	    val VI = evalValDesc(valdesc)
	in
	    Inter.fromVI VI
	end

      | evalSpec(IB, TYPESpec(I, typdesc)) =
	(* [Rule 167] *)
	let
	    val TI = evalTypDesc(typdesc)
	in
	    Inter.fromTI TI
	end

      | evalSpec(IB, EQTYPESpec(I, typdesc)) =
	(* [Rule 168] *)
	let
	    val TI = evalTypDesc(typdesc)
	in
	    Inter.fromTI TI
	end

      | evalSpec(IB, DATATYPESpec(I, datdesc)) =
	(* [Rule 169] *)
	let
	    val (VI,TI) = evalDatDesc(datdesc)
	in
	    Inter.fromVIandTI(VI,TI)
	end

      | evalSpec(IB, DATATYPE2Spec(I, tycon, longtycon)) =
	(* [Rule 170] *)
	let
	    val VI = case IntBasis.findLongTyCon(IB, longtycon)
		       of SOME VI => VI
			| NONE => errorLongTyCon(I, "runtime error: \
						    \unknown type ", longtycon)
	    val TI = TyConMap.singleton(tycon, VI)
	in
	    Inter.fromVIandTI(VI,TI)
	end

      | evalSpec(IB, EXCEPTIONSpec(I, exdesc)) =
	(* [Rule 171] *)
	let
	    val VI = evalExDesc(exdesc)
	in
	    Inter.fromVI VI
	end

      | evalSpec(IB, STRUCTURESpec(I, strdesc)) =
	(* [Rule 172] *)
	let
	    val SI = evalStrDesc(IB, strdesc)
	in
	    Inter.fromSI SI
	end

      | evalSpec(IB, INCLUDESpec(I, sigexp)) =
	(* [Rule 173] *)
	let
	    val I = evalSigExp(IB, sigexp)
	in
	    I
	end

      | evalSpec(IB, EMPTYSpec(I)) =
	(* [Rule 174] *)
	Inter.empty

      | evalSpec(IB, SEQSpec(I, spec1, spec2)) =
	(* [Rule 175] *)
	let
	    val I1 = evalSpec(IB, spec1)
	    val I2 = evalSpec(IntBasis.plusI(IB, I1), spec2)
	in
	    Inter.plus(I1,I2)
	end

      | evalSpec(IB, SHARINGTYPESpec(I, spec, longtycons)) =
	(* Omitted [Section 7.1] *)
	evalSpec(IB, spec)

      | evalSpec(IB, SHARINGSpec(I, spec, longstrids)) =
	(* Omitted [Section 7.1] *)
	evalSpec(IB, spec)


    (* Value Descriptions *)

    and evalValDesc(ValDesc(I, vid, _, valdesc_opt)) =
	(* [Rule 176] *)
	let
	    val VI = case valdesc_opt
		       of NONE         => VIdMap.empty
			| SOME valdesc => evalValDesc(valdesc)
	in
	    VIdMap.insert(VI, vid, IdStatus.v)
	end


    (* Type Descriptions *)

    and evalTypDesc(TypDesc(I, tyvarseq, tycon, typdesc_opt)) =
	(* [Rule 177] *)
	let
	    val TI = case typdesc_opt
		       of NONE         => TyConMap.empty
			| SOME typdesc => evalTypDesc(typdesc)
	in
	    TyConMap.insert(TI, tycon, VIdMap.empty)
	end


    (* Datatype Descriptions *)

    and evalDatDesc(DatDesc(I, tyvarseq, tycon, condesc, datdesc_opt)) =
	(* [Rule 178] *)
	let
	    val  VI       = evalConDesc(condesc)
	    val (VI',TI') = case datdesc_opt
			      of NONE          => ( VIdMap.empty, TyConMap.empty )
			       | SOME datdesc' => evalDatDesc(datdesc')
	in
	    ( VIdMap.unionWith #2 (VI, VI')
	    , TyConMap.insert(TI', tycon, VI)
	    )
	end


    (* Constructor Descriptions *)

    and evalConDesc(ConDesc(I, vid, _, condesc_opt)) =
	(* [Rule 179] *)
	let
	    val VI = case condesc_opt
		       of NONE         => VIdMap.empty
			| SOME condesc => evalConDesc(condesc)
	in
	    VIdMap.insert(VI, vid, IdStatus.c)
	end


    (* Exception Description *)

    and evalExDesc(ExDesc(I, vid, _, exdesc_opt)) =
	(* [Rule 180] *)
	let
	    val VI = case exdesc_opt
		       of NONE        => VIdMap.empty
			| SOME exdesc => evalExDesc(exdesc)
	in
	    VIdMap.insert(VI, vid, IdStatus.e)
	end


    (* Structure Descriptions *)

    and evalStrDesc(IB, StrDesc(I, strid, sigexp, strdesc_opt)) =
	(* [Rule 181] *)
	let
	    val I  = evalSigExp(IB, sigexp)
	    val SI = case strdesc_opt
		       of NONE         => StrIdMap.empty
		        | SOME strdesc => evalStrDesc(IB, strdesc)
	in
	    StrIdMap.insert(SI, strid, I)
	end


    (* Functor Bindings *)

    and evalFunBind(B, FunBind(I, funid, strid, sigexp, strexp, funbind_opt)) =
	(* [Rule 182] *)
	(* Note that there is a typo in this rule. *)
	let
	    val I  = evalSigExp(IntBasis.Inter B, sigexp)
	    val F  = case funbind_opt
		       of NONE         => FunIdMap.empty
			| SOME funbind => evalFunBind(B, funbind)
	in
	    FunIdMap.insert(F, funid, FunctorClosure((strid,I),strexp,B))
	end


    (* Functor Declarations *)

    and evalFunDec(B, FunDec(I, funbind)) =
	(* [Rule 183] *)
	let
	    val F = evalFunBind(B, funbind)
	in
	    F
	end


    (* Top-level Declarations *)

    and evalTopDec(s,B, STRDECTopDec(I, strdec, topdec_opt)) =
	(* [Rule 184] *)
	(* Note the mistake in the conclusion of this rule. *)
	let
	    val E   = evalStrDec(s,B, strdec)
	    val B'  = DynamicBasis.fromE E
	    val B'' = case topdec_opt
			of NONE        => DynamicBasis.empty
			 | SOME topdec => evalTopDec(s,B plus B', topdec)
	in
	    B' plus B''
	end

      | evalTopDec(s,B, SIGDECTopDec(I, sigdec, topdec_opt)) =
	(* [Rule 185] *)
	(* Note the mistake in the conclusion of this rule. *)
	let
	    val G   = evalSigDec(IntBasis.Inter B, sigdec)
	    val B'  = DynamicBasis.fromG G
	    val B'' = case topdec_opt
			of NONE        => DynamicBasis.empty
			 | SOME topdec => evalTopDec(s,B plus B', topdec)
	in
	    B' plus B''
	end

      | evalTopDec(s,B, FUNDECTopDec(I, fundec, topdec_opt)) =
	(* [Rule 186] *)
	(* Note the mistake in the conclusion of this rule. *)
	let
	    val F   = evalFunDec(B, fundec)
	    val B'  = DynamicBasis.fromF F
	    val B'' = case topdec_opt
			of NONE        => DynamicBasis.empty
			 | SOME topdec => evalTopDec(s,B plus B', topdec)
	in
	    B' plus B''
	end
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * A generic pretty printer.
 *
 * Based on:
 *    Philip Wadler. "A prettier printer"
 *    http://cm.bell-labs.com/cm/cs/who/wadler/
 * and Christian Lindig's port to OCaml.
 *
 * The semantics has been extended to allow 4 different kinds of
 * groups (`boxes'), 2 modes of nesting, and varying break representations.
 * This is no more easily described by an algebra though, and the `below'
 * combinator looses optimality.
 *)

signature PRETTY_PRINT =
sig
    type doc

    val empty :		doc			(* empty document *)
    val break :		doc			(* space or line break *)
    val ebreak :	doc			(* empty or line break *)
    val text :		string -> doc		(* raw text *)

    val ^^ :		doc * doc -> doc	(* concatenation *)
    val ^/^ :		doc * doc -> doc	(* concatenation with break *)

    val hbox :		doc -> doc		(* horizontal box *)
    val vbox :		doc -> doc		(* vertical box *)
    val fbox :		doc -> doc		(* fill box (h and v) *)
    val abox :		doc -> doc		(* auto box (h or v) *)

    val nest :		int -> doc -> doc	(* indentation by k char's *)
    val below :		doc -> doc		(* keep current indentation *)

    val isEmpty :	doc -> bool

    val toString :	doc * int -> string
    val output :	TextIO.outstream * doc * int -> unit
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * A generic pretty printer.
 *
 * Based on:
 *    Philip Wadler. "A prettier printer"
 *    http://cm.bell-labs.com/cm/cs/who/wadler/
 * and Christian Lindig's port to OCaml.
 *
 * The semantics has been extended to allow 4 different kinds of
 * groups (`boxes'), 2 modes of nesting, and varying break representations.
 *)

structure PrettyPrint :> PRETTY_PRINT =
struct
    (* Types *)

    datatype mode = H | V | F | A

    datatype doc =
	  EMPTY
	| BREAK of string
	| TEXT  of string
	| CONS  of doc * doc
	| BOX   of mode * doc
	| NEST  of int * doc
	| BELOW of doc

    datatype prim =
	  PTEXT of string
	| PLINE of int


    (* Interface operators *)

    infixr ^^ ^/^

    val empty	= EMPTY
    val break	= BREAK " "
    val ebreak	= BREAK ""
    val text	= TEXT

    fun x ^^ EMPTY	= x
      | EMPTY ^^ y	= y
      | x ^^ y		= CONS(x, y)

    fun x ^/^ EMPTY	= x
      | EMPTY ^/^ y	= y
      | x ^/^ y		= CONS(x, CONS(break, y))

    fun below EMPTY	= EMPTY
      | below x		= BELOW x

    fun hbox EMPTY	= EMPTY
      | hbox x		= BOX(H, x)

    fun vbox EMPTY	= EMPTY
      | vbox x		= BOX(V, x)

    fun fbox EMPTY	= EMPTY
      | fbox x		= BOX(F, x)

    fun abox EMPTY	= EMPTY
      | abox x		= BOX(A, x)

    fun nest k EMPTY	= EMPTY
      | nest k x	= NEST(k, x)


    fun isEmpty EMPTY	= true
      | isEmpty _	= false


    (* Check whether the first line of a document fits into remaining characters *)

    (* We abuse the mode A (which can never occur in the lists passed to
     * fits) to flag breaks which occur inside swallowed vboxes.
     *)

    fun fits(w, z) =
	w >= 0 andalso
	case z
	  of []			=> true
	   | (i,m,EMPTY)::z	=> fits(w, z)
	   | (i,m,CONS(x,y))::z	=> fits(w, (i,m,x)::(i,m,y)::z)
	   | (i,m,TEXT s)::z	=> fits(w - String.size s, z)
	   | (i,H,BREAK s)::z	=> fits(w - String.size s, z)
	   | (i,A,BREAK s)::z	=> false
	   | (i,m,BREAK s)::z	=> true
	   | (i,V,BOX(H,x))::z	=> fits(w, (i,H,x)::z)
	   | (i,V,BOX(n,x))::z	=> fits(w, (i,V,x)::z)
	   | (i,m,BOX(V,x))::z	=> fits(w, (i,A,x)::z)
	   | (i,m,BOX(n,x))::z	=> fits(w, (i,H,x)::z)
	   | (i,m,NEST(j,x))::z	=> fits(w, (i,m,x)::z)
	   | (i,m,BELOW x)::z	=> fits(w, (i,m,x)::z)


    (* Layout *)

    fun best(w, k, z, a) =
	case z
	  of []			=> List.rev a
	   | (i,m,EMPTY)::z	=> best(w, k, z, a)
	   | (i,m,CONS(x,y))::z	=> best(w, k, (i,m,x)::(i,m,y)::z, a)
	   | (i,m,TEXT s)::z	=> best(w, k + String.size s, z, PTEXT(s)::a)
	   | (i,H,BREAK s)::z	=> horizontal(w, k, s, z, a)
	   | (i,V,BREAK s)::z	=> vertical(w, i, z, a)
	   | (i,F,BREAK s)::z	=> if fits(w - k - String.size s, z)
				   then horizontal(w, k, s, z, a)
				   else vertical(w, i, z, a)
	   | (i,A,BREAK s)::z	=> raise Fail "PrettyPrint.best"
	   | (i,m,BOX(A,x))::z	=> if fits(w - k, (i,H,x)::z)
				   then best(w, k, (i,H,x)::z, a)
				   else best(w, k, (i,V,x)::z, a)
	   | (i,m,BOX(n,x))::z	=> best(w, k, (i,n,x)::z, a)
	   | (i,m,NEST(j,x))::z	=> best(w, k, (i+j,m,x)::z, a)
	   | (i,m,BELOW x)::z	=> best(w, k, (k,m,x)::z, a)

    and horizontal(w, k, s, z, a) =
	    best(w, k + String.size s, z, PTEXT(s)::a)

    and vertical(w, i, z, a) =
	    best(w, i, z, PLINE(i)::a)


    fun layout(doc, w) = best(w, 0, [(0,V,doc)], [])



    (* Convert a document *)

    fun primToString(PTEXT s) = s
      | primToString(PLINE i) = 
	    String.implode(#"\n" :: List.tabulate(i, fn _ => #" "))

    val toString = String.concat o List.map primToString o layout



    (* Output a document directly (is MUCH faster!) *)

    fun loop 0 f = ()
      | loop n f = ( f() ; loop (n-1) f )

    fun outputPrim os (PTEXT s) = TextIO.output(os, s)
      | outputPrim os (PLINE i) =
	    ( TextIO.output1(os, #"\n")
	    ; loop i (fn() => TextIO.output1(os, #" "))
	    )

    fun output(os, doc, w) = List.app (outputPrim os) (layout(doc, w))
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML pretty printing of values
 *)

signature PP_VAL =
sig
    type Val   = DynamicObjectsCore.Val
    type ExVal = DynamicObjectsCore.ExVal
    type State = DynamicObjectsCore.State

    val ppVal :   State * Val -> PrettyPrint.doc
    val ppExVal : State * ExVal -> PrettyPrint.doc
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML miscellaneous pretty printing helpers
 *)

signature PP_MISC =
sig
    type doc = PrettyPrint.doc

    val nest :		doc -> doc

    val paren :		doc -> doc
    val brace :		doc -> doc
    val brack :		doc -> doc
    val comment :	doc -> doc

    val parenAt :	int -> int * doc -> doc

    val ppCommaList :	('a -> doc) -> 'a list -> doc
    val ppStarList :	('a -> doc) -> 'a list -> doc
    val ppSeq :		('a -> doc) -> 'a list -> doc
    val ppSeqPrec :	(int -> 'a -> doc) -> int -> 'a list -> doc
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML miscellaneous pretty printing helpers
 *)

structure PPMisc : PP_MISC =
struct
    (* Import *)

    open PrettyPrint

    infixr ^^ ^/^


    (* Some PP combinators *)

    val nest			= nest 2

    fun paren doc		= text "(" ^^ fbox(below doc) ^^ text ")"
    fun brace doc		= text "{" ^^ fbox(below doc) ^^ text "}"
    fun brack doc		= text "[" ^^ fbox(below doc) ^^ text "]"
    fun comment doc		= text "(* " ^^ fbox(below doc) ^^ text " *)"

    fun parenAt p (p',doc)	= if p' > p then paren doc else doc

    fun ppCommaList ppX   []    = empty
      | ppCommaList ppX   [x]   = ppX x
      | ppCommaList ppX (x::xs) = ppX x ^^ text "," ^/^ ppCommaList ppX xs

    fun ppStarList ppX   []     = empty
      | ppStarList ppX   [x]    = ppX x
      | ppStarList ppX (x::xs)  = hbox(ppX x ^/^ text "*") ^/^ ppStarList ppX xs

    fun ppSeqPrec ppXPrec n []  = empty
      | ppSeqPrec ppXPrec n [x] = ppXPrec n x
      | ppSeqPrec ppXPrec n  xs = paren(ppCommaList (ppXPrec 0) xs)

    fun ppSeq ppX		= ppSeqPrec (fn _ => ppX) 0
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML pretty printing of values
 *)

structure PPVal : PP_VAL =
struct
    (* Import *)

    open DynamicObjectsCore
    open PrettyPrint
    open PPMisc

    infixr ^^ ^/^


    (* Simple objects *)

    val ppFn        = text "_fn"

    fun ppLab lab   = text(Lab.toString lab)
    fun ppVId vid   = text(VId.toString vid)
    fun ppExName en = text(ExName.toString en)
    fun ppSVal sv   = text(SVal.toString sv)


    (* Precedences *)

    val topPrec   = 0
    val applyPrec = 1
    val atomPrec  = 2


    (* Values *)

    fun ppVal  (s, v) = fbox(below(nest(ppValPrec (topPrec, s) v)))
    and ppExVal(s, e) = fbox(below(nest(ppExValPrec (topPrec, s) e)))

    and ppValPrec (p, s) (Assign) =
	    ppFn

      | ppValPrec (p, s) (SVal sv) =
	    ppSVal sv

      | ppValPrec (p, s) (BasVal b) =
	    ppFn

      | ppValPrec (p, s) (VId vid) =
	    ppVId vid

      | ppValPrec (p, s) (v as VIdVal(vid, v')) =
	(case Val.toList v
	   of SOME vs => brack(ppCommaList (ppValPrec (topPrec, s)) vs)
	    | NONE    =>
	      let
		  val doc = ppVId vid ^/^ ppValPrec (applyPrec+1, s) v'
	      in
		  parenAt applyPrec (p, doc)
	      end
	)

      | ppValPrec (p, s) (ExVal e) =
	    ppExValPrec (p, s) e

      | ppValPrec (p, s) (Record r) =
	let
	    fun isTuple(   [],     n) = n > 2
	      | isTuple(lab::labs, n) =
		    lab = Lab.fromInt n andalso isTuple(labs, n+1)

	    val labvs     = LabMap.listItemsi r
	    val (labs,vs) = ListPair.unzip labvs
	in
	    if List.null labs then
		text "()"
	    else if isTuple(labs, 1) then
		paren(ppCommaList (ppValPrec (topPrec, s)) vs)
	    else
		brace(ppCommaList (ppLabVal s) labvs)
	end

      | ppValPrec (p, s) (Addr a) =
	let
	    val v   = case State.findAddr(s, a)
			of SOME v => v
			 | NONE   => raise Fail "PPVal.ppVal: invalid address"

	    val doc = text "ref" ^/^ ppValPrec (applyPrec+1, s) v
	in
	    parenAt applyPrec (p, doc)
	end

      | ppValPrec (p, s) (FcnClosure _) =
	    ppFn


    and ppLabVal s (lab, v) =
	    abox(nest(
		hbox(
		    ppLab lab ^/^
		    text "="
		) ^/^
		ppVal(s, v)
	    ))


    and ppExValPrec (p, s) (ExName en) =
	    ppExName en

      | ppExValPrec (p, s) (ExNameVal(en, v)) =
	let
	    val doc = ppExName en ^/^ ppValPrec (applyPrec+1, s) v
	in
	    parenAt applyPrec (p, doc)
	end
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML pretty printing of the static basis
 *)

signature PP_STATIC_BASIS =
sig
    type Basis  = StaticObjectsModule.Basis
    type SigEnv = StaticObjectsModule.SigEnv
    type FunEnv = StaticObjectsModule.FunEnv

    val ppBasis :   Basis  -> PrettyPrint.doc
    val ppSigEnv :  SigEnv -> PrettyPrint.doc
    val ppFunEnv :  FunEnv -> PrettyPrint.doc
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML pretty printing of the static environment
 *)

signature PP_STATIC_ENV =
sig
    type ValEnv		= StaticObjectsCore.ValEnv
    type TyEnv		= StaticObjectsCore.TyEnv
    type Env		= StaticObjectsCore.Env
    type TyNameSet	= StaticObjectsCore.TyNameSet

    val ppEnv :		Env -> PrettyPrint.doc
    val ppSig :		TyNameSet * Env -> PrettyPrint.doc
    val ppTyNameSet :	TyNameSet -> PrettyPrint.doc

    val ppTyEnv :	TyNameSet * TyEnv -> PrettyPrint.doc
    val ppExEnv :	ValEnv -> PrettyPrint.doc
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML pretty printing of types and type schemes
 *)

signature PP_TYPE =
sig
    type Type       = StaticObjectsCore.Type
    type TypeScheme = StaticObjectsCore.TypeScheme

    val ppType :	Type -> PrettyPrint.doc
    val ppTypeScheme :	TypeScheme -> PrettyPrint.doc
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML pretty printing of types and type schemes
 *)

structure PPType : PP_TYPE =
struct
    (* Import *)

    open StaticObjectsCore
    open PrettyPrint
    open PPMisc

    infixr ^^ ^/^


    (* Simple objects *)

    fun ppLab lab     = text(Lab.toString lab)
    fun ppTyVar alpha = text(TyVar.toString alpha)
    fun ppTyName t    = text(TyName.toString t)

    fun ppOverloadingClass O =
	let
	    val T  = OverloadingClass.set O
	    val t  = OverloadingClass.default O
	    val ts = t :: TyNameSet.listItems(TyNameSet.delete(T,t))
	in
	    brack(ppCommaList ppTyName ts)
	end


    fun ppRowVar NONE    = empty
      | ppRowVar(SOME _) = text "," ^/^ text "..."


    (* Precedences *)

    val topPrec   = 0
    val arrowPrec = 1
    val starPrec  = 2
    val applyPrec = 3
    val atomPrec  = 4

    (* Types *)

    fun ppType tau = fbox(below(nest(ppTypePrec topPrec tau)))

    and ppTypePrec p (ref tau')        = ppType'Prec p tau'

    and ppType'Prec p (TyVar(alpha))   = ppTyVar alpha

      | ppType'Prec p (RowType(rho,r)) =
	let
	    fun isTuple(   [],     n) = n > 2
	      | isTuple(lab::labs, n) =
		    lab = Lab.fromInt n andalso isTuple(labs, n+1)

	    val labtaus     = LabMap.listItemsi rho
	    val (labs,taus) = ListPair.unzip labtaus
	in
	    if not(Option.isSome r) andalso List.null labs then
		text "unit"
	    else if not(Option.isSome r) andalso isTuple(labs, 1) then
		let
		    val doc = fbox(below(nest(
				  ppStarList (ppTypePrec(starPrec+1)) taus
			      )))
		in
		    parenAt starPrec (p, doc)
		end
	    else
		brace(ppCommaList ppLabType labtaus ^^ ppRowVar r)
	end

      | ppType'Prec p (FunType(tau1,tau2)) =
	let
	    val doc = fbox(below(nest(
			  ppTypePrec (arrowPrec+1) tau1 ^/^
			  text "->" ^/^
			  ppTypePrec arrowPrec tau2
		      )))
	in
	    parenAt arrowPrec (p, doc)
	end

      | ppType'Prec p (ConsType(taus,t)) =
	    fbox(below(nest(
		ppSeqPrec ppTypePrec applyPrec taus ^/^ ppTyName t
	    )))

      | ppType'Prec p (Undetermined{stamp,eq,...}) =
	    text((if eq then "''" else "'") ^ Stamp.toString stamp)

      | ppType'Prec p (Overloaded(O)) =
	    text "'" ^^ ppOverloadingClass O

      | ppType'Prec p (Determined(tau)) =
	    ppTypePrec p tau

    and ppLabType(lab, tau) =
	    fbox(below(nest(
		hbox(
		    ppLab lab ^/^
		    text ":"
		) ^/^
		ppType tau
	    )))


    (* Type schemes *)

    fun ppTypeScheme sigma =
	let
	    val (alphas,tau) = TypeScheme.normalise sigma
	in
	    ppType tau
	end
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML pretty printing of the static environment
 *)

structure PPStaticEnv : PP_STATIC_ENV =
struct
    (* Import *)

    open StaticObjectsCore
    open PrettyPrint
    open PPMisc

    infixr ^^ ^/^


    (* Simple objects *)

    fun ppVId vid     = text(VId.toString vid)
    fun ppTyCon tycon = text(TyCon.toString tycon)
    fun ppTyVar alpha = text(TyVar.toString alpha)
    fun ppStrId strid = text(StrId.toString strid)

    fun ppTyName t    = text(TyName.toString t)


    (* Environments *)

    fun ppConTypeScheme (_, ref(FunType(tau,_))) =
	    text "of" ^/^ PPType.ppType tau

      | ppConTypeScheme _ = empty


    fun ppValEnv VE =
	VIdMap.foldri
	    (fn(vid, (sigma,IdStatus.v), doc) =>
		fbox(nest(
		    hbox(
			text "val" ^/^
			ppVId vid ^/^
			text ":"
		    ) ^/^
		    PPType.ppTypeScheme sigma
		)) ^/^
		doc

	     | (vid, (sigma,_), doc) => doc
	    )
	    empty VE

    fun ppExEnv VE =
	VIdMap.foldri
	    (fn(vid, (sigma,IdStatus.e), doc) =>
		fbox(nest(
		    hbox(
			text "exception" ^/^
			ppVId vid
		    ) ^/^
		    ppConTypeScheme sigma
		)) ^/^
		doc

	     | (vid, (sigma,_), doc) => doc
	    )
	    empty VE

    fun ppConEnv VE =
	VIdMap.foldri
	    (fn(vid, (sigma,_), doc) =>
		fbox(nest(
		    ppVId vid ^/^
		    hbox(
			ppConTypeScheme sigma ^/^
			(if isEmpty doc then empty else text "|")
		    )
		)) ^/^
		doc
	    )
	    empty VE


    fun absTy(T, tycon, theta) =
	case TypeFcn.toTyName theta
	  of NONE    => NONE
	   | SOME t  => if TyName.toString t = TyCon.toString tycon
			andalso TyNameSet.member(T, t) then
			    SOME(TyName.admitsEquality t)
			else
			    NONE

    fun ppAbsTyEnv(T,TE) =
	TyConMap.foldri
	    (fn(tycon, (theta as (alphas,tau), VE), doc) =>
		if VIdMap.isEmpty VE then
		case absTy(T, tycon, theta)
		 of NONE    => doc
		  | SOME eq =>
		    fbox(nest(
			hbox(
			    text(if eq then "eqtype" else "type") ^/^
			    ppSeq ppTyVar alphas ^/^
			    ppTyCon tycon
			)
		    )) ^/^
		    doc
		else
		    doc
	    )
	    empty TE

    fun ppSynTyEnv(T,TE) =
	TyConMap.foldri
	    (fn(tycon, (theta as (alphas,tau), VE), doc) =>
		if VIdMap.isEmpty VE
		andalso not(isSome(absTy(T, tycon, theta))) then
		    fbox(nest(
			hbox(
			    text "type" ^/^
			    ppSeq ppTyVar alphas ^/^
			    ppTyCon tycon ^/^
			    text "="
			) ^/^
			PPType.ppType tau
		    )) ^/^
		    doc
		else
		    doc
	    )
	    empty TE

    fun ppDataTyEnv TE =
	TyConMap.foldri
	    (fn(tycon, ((alphas,tau),VE), doc) =>
		if VIdMap.isEmpty VE then
		    doc
		else
		    fbox(nest(
		        hbox(
			    text "datatype" ^/^
			    ppSeq ppTyVar alphas ^/^
			    ppTyCon tycon ^/^
			    text "="
			) ^/^
			abox(
			    ppConEnv VE
			)
		    )) ^/^
		    doc
	    )
	    empty TE

    fun ppTyEnv(T,TE) =
	    vbox(
		ppAbsTyEnv(T,TE) ^/^
		ppSynTyEnv(T,TE) ^/^
		ppDataTyEnv TE
	    )

    fun ppStrEnv(T,SE) =
	StrIdMap.foldri
	    (fn(strid, E, doc) =>
		fbox(nest(
		    hbox(
			text "structure" ^/^
			ppStrId strid ^/^
			text ":"
		    ) ^/^
		    ppSig' false (T,E)
		)) ^/^
		doc
	    )
	    empty SE

    and ppEnv'(T, Env(SE,TE,VE)) =
	    vbox(
		ppStrEnv(T,SE) ^/^
		ppTyEnv(T,TE) ^/^
		ppExEnv VE ^/^
		ppValEnv VE
	    )

    and ppEnv E = ppEnv'(TyNameSet.empty, E)


    (* Signatures *)

    and ppTyNameSet T =
	if TyNameSet.isEmpty T then
	    empty
	else
	    comment(ppCommaList ppTyName (TyNameSet.listItems T))

    and ppSig' withT (T,E) =
	    abox(below(
		nest(
		    hbox(
			text "sig" ^/^
			(if withT then ppTyNameSet T else empty)
		    ) ^/^
		    vbox(
			ppEnv'(T, E)
		    )
		) ^/^
		text "end"
	    ))

    fun ppSig Sigma = ppSig' true Sigma
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML pretty printing of the static basis
 *)

structure PPStaticBasis : PP_STATIC_BASIS =
struct
    (* Import *)

    type Basis  = StaticObjectsModule.Basis
    type SigEnv = StaticObjectsModule.SigEnv
    type FunEnv = StaticObjectsModule.FunEnv

    open PrettyPrint
    open PPMisc

    infixr ^^ ^/^


    (* Simple objects *)

    fun ppSigId sigid = text(SigId.toString sigid)
    fun ppFunId funid = text(FunId.toString funid)


    (* Environments *)

    fun ppSigEnv G =
	SigIdMap.foldri
	    (fn(sigid, Sigma, doc) =>
		fbox(nest(
		    hbox(
			text "signature" ^/^
			ppSigId sigid ^/^
			text "="
		    ) ^/^
		    PPStaticEnv.ppSig Sigma
		)) ^/^
		doc
	    )
	    empty G

    fun ppFunEnv F =
	FunIdMap.foldri
	    (fn(funid, (T,(E,Sigma)), doc) =>
		fbox(nest(
		    hbox(
			text "functor" ^/^
			ppFunId funid
		    ) ^^ ebreak ^^
		    fbox(nest(
			text "(" ^^
			hbox(
			    text "Arg" ^/^
			    text ":"
			) ^/^
			PPStaticEnv.ppSig(T,E) ^^
			text ")"
		    )) ^/^
		    text ":" ^/^
		    PPStaticEnv.ppSig Sigma
		)) ^/^
		doc
	    )
	    empty F


    (* Basis *)

    fun ppBasis (T,F,G,E) =
	    vbox(
		PPStaticEnv.ppTyNameSet T ^/^
		ppSigEnv G ^/^
		ppFunEnv F ^/^
		PPStaticEnv.ppEnv E ^/^
		text ""
	    )
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML pretty printing of the dynamic basis
 *)

signature PP_DYNAMIC_BASIS =
sig
    type Basis = DynamicObjectsModule.Basis
    type State = DynamicObjectsCore.State

    val ppBasis : State * Basis -> PrettyPrint.doc
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML pretty printing of the dynamic environment
 *)

signature PP_DYNAMIC_ENV =
sig
    type Env   = DynamicObjectsCore.Env
    type State = DynamicObjectsCore.State

    val ppEnv : State * Env -> PrettyPrint.doc
    val ppStr : State * Env -> PrettyPrint.doc
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML pretty printing of the dynamic environment
 *)

structure PPDynamicEnv : PP_DYNAMIC_ENV =
struct
    (* Import *)

    type Env   = DynamicObjectsCore.Env
    type State = DynamicObjectsCore.State

    open PrettyPrint
    open PPMisc

    infixr ^^ ^/^


    (* Simple objects *)

    fun ppVId vid     = text(VId.toString vid)
    fun ppTyCon tycon = text(TyCon.toString tycon)
    fun ppStrId strid = text(StrId.toString strid)


    (* Environments *)

    fun ppValEnv(s, VE) =
	VIdMap.foldri
	    (fn(vid, (v,IdStatus.v), doc) =>
		fbox(nest(
		    hbox(
			text "val" ^/^
			ppVId vid ^/^
			text "="
		    ) ^/^
		    PPVal.ppVal(s, v)
		)) ^/^
		doc

	     | (vid, (v,_), doc) => doc
	    )
	    empty VE

    fun ppExEnv VE =
	VIdMap.foldri
	    (fn(vid, (v,IdStatus.e), doc) =>
		hbox(
		    text "exception" ^/^
		    ppVId vid
		) ^/^
		doc

	     | (vid, (v,_), doc) => doc
	    )
	    empty VE

    fun ppConEnv VE =
	VIdMap.foldli
	    (fn(vid, (v,IdStatus.c), doc) =>
		hbox(
		    text "con" ^/^
		    ppVId vid
		) ^/^
		doc

	     | (vid, (v,_), doc) => doc
	    )
	    empty VE

    fun ppTyEnv(s, TE) =
	TyConMap.foldri
	    (fn(tycon, VE, doc) =>
		fbox(nest(
		    hbox(
			text "type" ^/^
			ppTyCon tycon
		    )
		)) ^/^
		doc
	    )
	    empty TE

    fun ppStrEnv(s, SE) =
	StrIdMap.foldri
	    (fn(strid, S, doc) =>
		fbox(nest(
		    hbox(
			text "structure" ^/^
			ppStrId strid ^/^
			text "="
		    ) ^/^
		    ppStr(s, S)
		)) ^/^
		doc
	    )
	    empty SE

    and ppEnv(s, DynamicObjectsCore.Env(SE,TE,VE)) =
	    vbox(
		ppStrEnv(s, SE) ^/^
		ppTyEnv(s, TE) ^/^
		ppConEnv VE ^/^
		ppExEnv  VE ^/^
		ppValEnv(s, VE)
	    )


    (* Structures *)

    and ppStr(s, E) =
	    abox(below(
		nest(
		    text "struct" ^/^
		    vbox(
			ppEnv(s, E)
		    )
		) ^/^
		text "end"
	    ))
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML pretty printing of the dynamic basis
 *)

structure PPDynamicBasis : PP_DYNAMIC_BASIS =
struct
    (* Import *)

    type Basis = DynamicObjectsModule.Basis
    type State = DynamicObjectsCore.State

    open PrettyPrint

    infixr ^^ ^/^


    (* Simple objects *)

    fun ppFunId funid = text(FunId.toString funid)
    fun ppSigId sigid = text(SigId.toString sigid)


    (* Environments *)

    fun ppFunEnv F =
	FunIdMap.foldri
	    (fn(funid, _, doc) =>
		hbox(
		    text "functor" ^/^
		    ppFunId funid
		) ^/^
		doc
	    )
	    empty F

    fun ppSigEnv G =
	SigIdMap.foldri
	    (fn(sigid, _, doc) =>
		hbox(
		    text "signature" ^/^
		    ppSigId sigid
		) ^/^
		doc
	    )
	    empty G


    (* Basis *)

    fun ppBasis(s, (F,G,E)) =
	    vbox(
		ppSigEnv G ^/^
		ppFunEnv F ^/^
		PPDynamicEnv.ppEnv(s, E) ^/^
		text ""
	    )
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML pretty printing of the combined basis
 *)

signature PP_BASIS =
sig
    type Basis = Basis.Basis
    type State = DynamicObjectsCore.State

    val ppBasis : State * Basis -> PrettyPrint.doc
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML pretty printing of the combined static/dynamic environment
 *)

signature PP_ENV =
sig
    type Env   = StaticObjectsCore.Env * DynamicObjectsCore.Env
    type State = DynamicObjectsCore.State

    val ppEnv : State * Env -> PrettyPrint.doc
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML pretty printing of the combined static/dynamic environment
 *)

structure PPEnv : PP_ENV =
struct
    (* Import *)

    type Env   = StaticObjectsCore.Env * DynamicObjectsCore.Env
    type State = DynamicObjectsCore.State

    open PrettyPrint
    open PPMisc

    infixr ^^ ^/^


    (* Simple objects *)

    fun ppVId vid     = text(VId.toString vid)
    fun ppStrId strid = text(StrId.toString strid)


    (* Environments *)

    fun ppValEnv(s, (VE_STAT,VE_DYN)) =
	VIdMap.foldri
	    (fn(vid, (sigma,IdStatus.v), doc) =>
	     let
		val (v,is) = valOf(VIdMap.find(VE_DYN, vid))
	     in
		fbox(nest(
		    hbox(
			text "val" ^/^
			ppVId vid
		    ) ^/^
		    text "=" ^/^
		    PPVal.ppVal(s, v) ^/^
		    text ":" ^/^
		    PPType.ppTypeScheme sigma
		)) ^/^
		doc
	     end

	     | (vid, (sigma,_), doc) => doc
	    )
	    empty VE_STAT

    fun ppStrEnv(s, T, (SE_STAT,SE_DYN)) =
	StrIdMap.foldri
	    (fn(strid, E_STAT, doc) =>
	     let
		val E_DYN = valOf(StrIdMap.find(SE_DYN, strid))
	     in
		fbox(nest(
		    hbox(
			text "structure" ^/^
			ppStrId strid ^/^
			text "="
		    ) ^/^
		    ppStr (s, T, (E_STAT,E_DYN))
		)) ^/^
		doc
	     end
	    )
	    empty SE_STAT

    and ppEnv'(s, T, (StaticObjectsCore.Env(SE_STAT,TE_STAT,VE_STAT),
		      DynamicObjectsCore.Env(SE_DYN, TE_DYN, VE_DYN))) =
	    vbox(
		ppStrEnv(s, T, (SE_STAT,SE_DYN)) ^/^
		PPStaticEnv.ppTyEnv(T,TE_STAT) ^/^
		PPStaticEnv.ppExEnv VE_STAT ^/^
		ppValEnv(s, (VE_STAT,VE_DYN))
	    )

    and ppEnv(s, (E_STAT, E_DYN)) =
	    ppEnv'(s, TyNameSet.empty, (E_STAT,E_DYN))


    (* Structures *)

    and ppStr(s, T, E) =
	    abox(below(
		nest(
		    text "struct" ^/^
		    vbox(
			ppEnv'(s, T, E)
		    )
		) ^/^
		text "end"
	    ))
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML pretty printing of the combined basis
 *)

structure PPBasis : PP_BASIS =
struct
    (* Import *)

    type Basis = Basis.Basis
    type State = DynamicObjectsCore.State

    open PrettyPrint

    infixr ^^ ^/^


    (* Basis *)

    fun ppBasis (s, ((T,F_STAT,G_STAT,E_STAT), (F_DYN,G_DYN,E_DYN))) =
	    vbox(
		PPStaticBasis.ppSigEnv G_STAT ^/^
		PPStaticBasis.ppFunEnv F_STAT ^/^
		PPEnv.ppEnv(s, (E_STAT,E_DYN)) ^/^
		text ""
	    )
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML modules elaboration
 *
 * Definition, Sections 5.7 and 3.5
 *)

signature ELAB_MODULE =
sig
    (* Import types *)

    type TopDec = GrammarModule.TopDec
    type Basis  = StaticObjectsModule.Basis

    (* Export *)

    val elabTopDec : Basis * TopDec -> Basis
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML core elaboration
 *
 * Definition, Sections 4.10, 4.11, 4.6, 4.7, 2.9
 *
 * Notes:
 *   - Elaboration also checks the further restrictions [Section 4.11].
 *   - To implement the 3rd restriction in 4.11 elabDec is passed an
 *     additional boolean argument to recognise being on the toplevel.
 *)

signature ELAB_CORE =
sig
    (* Import *)

    type VId		= IdsCore.VId
    type TyVar		= IdsCore.TyVar

    type Dec		= GrammarCore.Dec
    type Ty		= GrammarCore.Ty
    type TyVarseq	= GrammarCore.TyVarseq

    type TyVarSet	= StaticObjectsCore.TyVarSet
    type Type		= StaticObjectsCore.Type
    type Env		= StaticObjectsCore.Env
    type Context	= StaticObjectsCore.Context


    (* Export *)

    val elabDec :	bool -> Context * Dec -> Env
    val elabTy :	Context * Ty -> Type
    val tyvars :	TyVarseq -> TyVarSet * TyVar list
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML closure of value environments
 *
 * Definition, Section 4.7 and 4.8
 *)

signature CLOS =
sig
    (* Import *)

    type ValBind = GrammarCore.ValBind
    type ValEnv  = StaticObjectsCore.ValEnv
    type Context = StaticObjectsCore.Context


    (* Operation *)

    val Clos :  Context * ValBind -> ValEnv -> ValEnv
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML closure of value environments
 *
 * Definition, Section 4.7 and 4.8
 *)

structure Clos : CLOS =
struct
    (* Import *)

    open GrammarCore
    open StaticObjectsCore


    (* Check whether a pattern binds an identifier *)

    fun ? bindsX(NONE,   vid) = false
      | ? bindsX(SOME x, vid) = bindsX(x, vid)

    fun bindsAtPat(WILDCARDAtPat(_), vid)   = false
      | bindsAtPat(SCONAtPat(_, scon), vid) = false
      | bindsAtPat(IDAtPat(_, _, longvid), vid) =
	let
	    val (strids,vid') = LongVId.explode longvid
	in
	    List.null strids andalso vid = vid'
	end
      | bindsAtPat(RECORDAtPat(_, patrow_opt), vid) =
	    ?bindsPatRow(patrow_opt, vid)
      | bindsAtPat(PARAtPat(_, pat), vid) = bindsPat(pat, vid)

    and bindsPatRow(DOTSPatRow(_), vid) = false
      | bindsPatRow(FIELDPatRow(_, lab, pat, patrow_opt), vid) =
	    bindsPat(pat, vid) orelse ?bindsPatRow(patrow_opt, vid)

    and bindsPat(ATPat(_, atpat), vid)              = bindsAtPat(atpat, vid)
      | bindsPat(CONPat(_, _, longvid, atpat), vid) = bindsAtPat(atpat, vid)
      | bindsPat(COLONPat(_, pat, ty), vid)         = bindsPat(pat, vid)
      | bindsPat(ASPat(_, _, vid', ty_opt, pat), vid) =
	    vid = vid' orelse bindsPat(pat, vid)


    (* Non-expansive expressions [Section 4.7] *)

    fun ? isNonExpansiveX C  NONE    = true
      | ? isNonExpansiveX C (SOME x) = isNonExpansiveX C x

    fun isNonExpansiveAtExp C (SCONAtExp(_, scon))         = true
      | isNonExpansiveAtExp C (IDAtExp(_, _, longvid))     = true
      | isNonExpansiveAtExp C (RECORDAtExp(_, exprow_opt)) =
	    ?isNonExpansiveExpRow C exprow_opt
      | isNonExpansiveAtExp C (PARAtExp(_, exp)) = isNonExpansiveExp C exp
      | isNonExpansiveAtExp C  _                 = false

    and isNonExpansiveExpRow C (ExpRow(_, lab, exp, exprow_opt)) =
	    isNonExpansiveExp C exp andalso ?isNonExpansiveExpRow C exprow_opt

    and isNonExpansiveExp C (ATExp(_, atexp)) = isNonExpansiveAtExp C atexp
      | isNonExpansiveExp C (APPExp(_, exp, atexp)) =
	    isConExp C exp andalso isNonExpansiveAtExp C atexp
      | isNonExpansiveExp C (COLONExp(_, exp, ty))  = isNonExpansiveExp C exp
      | isNonExpansiveExp C (FNExp(_, match))       = true
      | isNonExpansiveExp C  _                      = false

    and isConAtExp C (PARAtExp(_, exp))       = isConExp C exp
      | isConAtExp C (IDAtExp(_, _, longvid)) =
	    LongVId.explode longvid <> ([],VId.fromString "ref") andalso
	    (case Context.findLongVId(C, longvid)
	       of SOME(_,is) => is = IdStatus.c orelse is = IdStatus.e
		| NONE       => false
	    )
      | isConAtExp C  _                            = false

    and isConExp C (ATExp(_, atexp))                  = isConAtExp C atexp
      | isConExp C (COLONExp(_, ATExp(_, atexp), ty)) = isConAtExp C atexp
      | isConExp C  _                                 = false


    (* Closure [Section 4.8] *)

    fun hasNonExpansiveRHS C (vid, PLAINValBind(I, pat, exp, valbind_opt)) =
	if bindsPat(pat, vid) then
	    isNonExpansiveExp C exp
	else
	    hasNonExpansiveRHS C (vid, valOf valbind_opt)

      | hasNonExpansiveRHS C (vid, RECValBind _) =
	    (* A rec valbind can only contain functions. *)
	    true

    fun Clos (C,valbind) VE =
	let
	    val tyvarsC = Context.tyvars C
	    val undetsC = Context.undetermined C

	    fun ClosType vid tau =
		if hasNonExpansiveRHS C (vid, valbind) then
		    let
			val tyvars =
			    TyVarSet.difference(Type.tyvars tau, tyvarsC)
			val undets =
			    StampMap.difference(Type.undetermined tau, undetsC)
			val tyvars' = StampMap.map TyVar.invent undets
			val det     = StampMap.map Type.fromTyVar tyvars'
		    in
			(TyVarSet.listItems tyvars @ StampMap.listItems tyvars',
			 Type.determine det tau)
		    end
		else
		    ( [], tau )
	in
	    VIdMap.mapi (fn(vid, ((_,tau),is)) => (ClosType vid tau, is)) VE
	end
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML consistency of patterns and matches
 *
 * Definition, Section 4.11
 *
 * Note:
 *     The requirement to check for irredundancy of matches is a `bug' in the
 *     definition since this cannot be checked in general for two reasons:
 *
 *     (1) There may be (hidden) aliasing of exception constructors.
 *         Consequently, we only detect redundant exception constructors
 *         if they are denoted by the same longvid.
 *
 *     (2) There is no requirement of consistency for constructors in
 *         sharing specifications or type realisations (actually, we
 *         consider this a serious bug). For example,
 *		datatype t1 = A | B
 *		datatype t2 = C
 *		sharing type t1 = t2
 *         is a legal specification. This allows a mix of the constructors
 *         to appear in matches, rendering the terms of irredundancy and
 *         exhaustiveness meaningless. We make no attempt to detect this,
 *         so generated warnings may or may not make sense in that situation.
 *)

signature CHECK_PATTERN =
sig
    (* Import *)

    type Pat   = GrammarCore.Pat
    type Match = GrammarCore.Match
    type Env   = StaticEnv.Env


    (* Operations *)

    val checkPat :	Env * Pat   -> unit
    val checkMatch :	Env * Match -> unit
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML consistency of patterns and matches
 *
 * Definition, Section 4.11
 *
 * Note:
 *   - We represent special constants in a normalised form as strings.
 *   - The requirement to check for irredundancy of matches is a `bug' in the
 *     definition since this cannot be checked in general for two reasons:
 *
 *     (1) There may be (hidden) aliasing of exception constructors.
 *         Consequently, we only detect redundant exception constructors
 *         if they are denoted by the same longvid.
 *
 *     (2) There is no requirement of consistency for constructors in
 *         sharing specifications or type realisations (actually, we
 *         consider this a serious bug). For example,
 *		datatype t1 = A | B
 *		datatype t2 = C
 *		sharing type t1 = t2
 *         is a legal specification. This allows a mix of the constructors
 *         to appear in matches, rendering the terms of irredundancy and
 *         exhaustiveness meaningless. We make no attempt to detect this,
 *         so generated warnings may or may not make sense in that situation.
 *)

structure CheckPattern : CHECK_PATTERN =
struct
    (* Import *)

    type SCon    = SCon.SCon
    type Lab     = Lab.Lab
    type VId     = VId.Id
    type longVId = LongVId.longId
    type Pat     = GrammarCore.Pat
    type Match   = GrammarCore.Match
    type Env     = StaticEnv.Env

    open GrammarCore



    (*
     * Algorithm loosely based on:
     *    Peter Sestoft.
     *         "ML pattern matching compilation and partial evaluation",
     *    in: Dagstuhl Seminar on Partial Evaluation,
     *        Lecture Notes in Computer Science, Springer-Verlag 1996
     *)


    (* Value description *)

    structure SConSet'   = FinSetFn(type ord_key = string
				    val  compare = String.compare)
    structure VIdSet     = FinSetFn(type ord_key = VId.Id
				    val  compare = VId.compare)
    structure LongVIdSet = FinSetFn(type ord_key = LongVId.longId
				    val  compare = LongVId.compare)

    type SCon'         = string
    type SConSet'      = SConSet'.set
    type VIdSet        = VIdSet.set
    type LongVIdSet    = LongVIdSet.set
    type 'a LabMap     = 'a LabMap.map

    datatype description =
	  ANY
	| SCON      of SCon'
	| NOT_SCON  of SConSet'
	| EXCON     of longVId * description option
	| NOT_EXCON of LongVIdSet
	| CON       of VId * description option
	| NOT_CON   of VIdSet
	| RECORD    of description LabMap

    datatype context =
	  EXCON'  of context * longVId
	| CON'    of context * VId
	| RECORD' of context * description LabMap * Lab * PatRow option
	| MATCH'  of Source.info * Match option

    (* Normalise special constants *)

    fun normalise(SCon.INT(b, sc, ref t_opt)) =
	  Library.intToString(Library.intFromString(b, sc, t_opt))
      | normalise(SCon.WORD(b, sc, ref t_opt)) =
	  Library.wordToString(Library.wordFromString(b, sc, t_opt))
      | normalise(SCon.CHAR(sc, ref t_opt)) =
	  Library.charToString(Library.charFromString(sc, t_opt))
      | normalise(SCon.STRING(sc, ref t_opt)) =
	  Library.stringToString(Library.stringFromString(sc, t_opt))
      | normalise(SCon.REAL(sc, ref t_opt)) =
	  Library.realToString(Library.realFromString(sc, t_opt))

    fun span scon = case SCon.tyname scon of NONE   => 0
					   | SOME t => Library.span t


    (* Result type for static matching *)

    structure RegionSet = FinSetFn(type ord_key = Source.info
				   val compare  = Source.compare)

    type sets = {matches : RegionSet.set, reached : RegionSet.set}

    type result = sets * bool

    val emptySets = {matches = RegionSet.empty, reached = RegionSet.empty}

    fun update({matches, reached}, l, f) =
	let
	    val m = ref matches
	    val r = ref reached
	    val sets = {matches = m, reached = r}
	    val r = l sets
	in
	    r := f(!r);
	    {matches = !m, reached = !r}
	end

    fun extend(sets, l, I) =
	    update(sets, l, fn R => RegionSet.add(R, I))

    fun branch((sets1, exhaustive1) : result, (sets2, exhaustive2) : result) =
	    ( {matches = RegionSet.union(#matches sets1, #matches sets2),
	       reached = RegionSet.union(#reached sets1, #reached sets2)},
	     exhaustive1 andalso exhaustive2 )


    (* Static pattern matching *)

    fun matchMatch(E, desc, Match(_, mrule, match_opt), sets) =
	    matchMrule(E, desc, mrule, match_opt, sets)


    and matchMrule(E, desc, Mrule(I, pat, exp), match_opt, sets) =
	    matchPat(E, desc, pat, MATCH'(I, match_opt),
		     extend(sets, #matches, I))


    and matchAtPat(E, desc, atpat, context, sets) =
	case atpat
	  of WILDCARDAtPat(_) =>
		succeed(E, desc, context, sets)

	   | SCONAtPat(_, scon) =>
		matchSCon(E, desc, normalise scon, span scon, context, sets)

	   | IDAtPat(_, _, longvid) =>
	       (case StaticEnv.findLongVId(E, longvid)
		  of NONE =>
			succeed(E, desc, context, sets)

		   | SOME(sigma, IdStatus.v) =>
			succeed(E, desc, context, sets)

		   | SOME(sigma, IdStatus.e) =>
			matchExCon(E, desc, longvid, NONE, context, sets)

		   | SOME((_,tau), IdStatus.c) =>
		     let
			val vid  = LongVId.toId longvid
			val span = TyName.span(Type.tyname(Type.range tau))
		     in
			matchCon(E, desc, vid, span, NONE, context, sets)
		     end
	       )

	   | RECORDAtPat(_, patrow_opt) =>
		matchPatRowOpt(E, desc, patrow_opt, context, sets)

	   | PARAtPat(_, pat) =>
		matchPat(E, desc, pat, context, sets)


    and matchPat(E, desc, pat, context, sets) =
	case pat
	  of ATPat(_, atpat) =>
		matchAtPat(E, desc, atpat, context, sets)

	   | CONPat(_, _, longvid, atpat) =>
	       (case StaticEnv.findLongVId(E, longvid)
		  of SOME(sigma, IdStatus.e) =>
			matchExCon(E, desc, longvid, SOME atpat, context, sets)

		   | SOME((_,tau), IdStatus.c) =>
		     let
			val vid  = LongVId.toId longvid
			val span = TyName.span(Type.tyname(Type.range tau))
		     in
			matchCon(E, desc, vid, span, SOME atpat, context, sets)
		     end

		   | _ => raise Fail "CheckPattern.matchPat: \
				     \invalid constructed pattern"
	       )

	  | COLONPat(_, pat, ty) =>
		matchPat(E, desc, pat, context, sets)

	  | ASPat(I, _, _, _, pat) =>
		matchPat(E, desc, pat, context, sets)


    and matchPatRowOpt(E, desc, patrow_opt, context, sets) =
	let
	    val descs = case desc
			  of ANY          => LabMap.empty
			   | RECORD descs => descs
			   | _ => raise Fail "CheckPattern.matchAtPat: \
					     \invalid record pattern"
	in
	    case patrow_opt
	      of SOME(FIELDPatRow(_, lab, pat, patrow_opt')) =>
		 let
		     val desc' = case LabMap.find(descs, lab)
				   of NONE       => ANY
				    | SOME desc' => desc'
		 in
		     matchPat(E, desc', pat,
			      RECORD'(context, descs, lab, patrow_opt'), sets)
		 end

	       | _ =>
		 succeed(E, RECORD(descs), context, sets)
	end


    and matchSCon(E, desc, scon, span, context, sets) =
	let
	    val descSucc       = SCON scon
	    fun descFail scons = NOT_SCON(SConSet'.add(scons, scon))
	in
	    case desc
	      of ANY =>
		 branch(succeed(E, descSucc, context, sets),
			fail(E, descFail SConSet'.empty, context, sets)
		       )

	       | SCON scon' =>
		 if scon = scon' then
		     succeed(E, desc, context, sets)
		 else
		     fail(E, desc, context, sets)

	       | NOT_SCON scons =>
		 if SConSet'.member(scons, scon) then
		     fail(E, desc, context, sets)
		 else if SConSet'.numItems scons = span - 1 then
		     succeed(E, descSucc, context, sets)
		 else
		     branch(succeed(E, descSucc, context, sets),
			    fail(E, descFail scons, context, sets)
			   )

	       | _ => raise Fail "CheckPattern.matchSCon: type error"
	end


    and matchExCon(E, desc, longvid, atpat_opt, context, sets) =
	let
	    val context' = EXCON'(context, longvid)
	    val descSucc = EXCON(longvid, NONE)
	    fun descFail longvids =
		NOT_EXCON(LongVIdSet.add(longvids, longvid))
	in
	    case desc
	      of ANY =>
		 branch(matchArgOpt(E, descSucc, SOME ANY, atpat_opt,
				    context, context', sets),
			fail(E, descFail LongVIdSet.empty, context, sets)
		       )

	       | EXCON(longvid', desc_opt) =>
		 if longvid = longvid' then
		     matchArgOpt(E, descSucc, desc_opt, atpat_opt,
				 context, context', sets)
		 else
		     fail(E, desc, context, sets)

	       | NOT_EXCON longvids =>
		 if LongVIdSet.member(longvids, longvid) then
		     fail(E, desc, context, sets)
		 else
		     branch(matchArgOpt(E, descSucc, SOME ANY, atpat_opt,
					context, context', sets),
			    fail(E, descFail longvids, context, sets)
			   )

	       | _ => raise Fail "CheckPattern.matchExCon: type error"
	end


    and matchCon(E, desc, vid, span, atpat_opt, context, sets) =
	let
	    val context'      = CON'(context, vid)
	    val descSucc      = CON(vid, NONE)
	    fun descFail vids = NOT_CON(VIdSet.add(vids, vid))
	in
	    case desc
	      of ANY =>
		 if span = 1 then
		     matchArgOpt(E, descSucc, SOME ANY, atpat_opt,
				 context, context', sets)
		 else
		     branch(matchArgOpt(E, descSucc, SOME ANY, atpat_opt,
					context, context', sets),
			    fail(E, descFail VIdSet.empty, context, sets)
			   )

	       | CON(vid', desc_opt) =>
		 if vid = vid' then
		     matchArgOpt(E, descSucc, desc_opt, atpat_opt,
				 context, context', sets)
		 else
		     fail(E, desc, context, sets)

	       | NOT_CON vids =>
		 if VIdSet.member(vids, vid) then
		     fail(E, desc, context, sets)
		 else if VIdSet.numItems vids = span - 1 then
		     matchArgOpt(E, descSucc, SOME ANY, atpat_opt,
				 context, context', sets)
		 else
		     branch(matchArgOpt(E, descSucc, SOME ANY, atpat_opt,
					context, context', sets),
			    fail(E, descFail vids, context, sets)
			   )

	       | _ => raise Fail "CheckPattern.matchCon: type error"
	end


    and matchArgOpt(E, desc, desc_opt, atpat_opt, context, context', sets) =
	case atpat_opt
	  of NONE =>
		succeed(E, desc, context, sets)

	   | SOME atpat =>
		matchAtPat(E, valOf desc_opt, atpat, context', sets)


    and succeed(E, desc, EXCON'(context, longvid), sets) =
	    succeed(E, EXCON(longvid, SOME desc), context, sets)

      | succeed(E, desc, CON'(context, vid), sets) =
	    succeed(E, CON(vid, SOME desc), context, sets)

      | succeed(E, desc, RECORD'(context, descs, lab, patrow_opt), sets) =
	    matchPatRowOpt(E, RECORD(LabMap.insert(descs, lab, desc)),
			   patrow_opt, context, sets)

      | succeed(E, desc, MATCH'(I, match_opt), sets) =
	    skip(match_opt, extend(sets, #reached, I))


    and skip (SOME(Match(_, mrule, match_opt)), sets) =
	    skip(match_opt, extend(sets, #matches, infoMrule mrule))

      | skip (NONE, sets) =
	    ( sets, true )


    and fail(E, desc, EXCON'(context, longvid), sets) =
	    fail(E, EXCON(longvid, SOME desc), context, sets)

      | fail(E, desc, CON'(context, vid), sets) =
	    fail(E, CON(vid, SOME desc), context, sets)

      | fail(E, desc, RECORD'(context, descs, lab, patrow_opt), sets) =
	    fail(E, RECORD(LabMap.insert(descs, lab, desc)), context, sets)

      | fail(E, desc, MATCH'(I, SOME match), sets) =
	    matchMatch(E, desc, match, sets)

      | fail(E, desc, MATCH'(I, NONE), sets) =
	    ( sets, false )


    (* Checking matches [Section 4.11, item 2; RFC conjunctive patterns;
     *                   RFC disjunctive patterns] *)

    fun checkMatch(E, match) =
	let
	    val (sets, exhaustive) = matchMatch(E, ANY, match, emptySets)
	    val unreached = RegionSet.difference(#matches sets, #reached sets)
	in
	    RegionSet.app (fn I => Error.warning(I, "redundant match rule"))
	        unreached;
	    if exhaustive then () else
		Error.warning(infoMatch match, "match not exhaustive")
	end



    (* Checking single patterns [Section 4.11, item 3] *)

    fun checkPat(E, pat) =
	let
	    val (_, exhaustive) =
		matchPat(E, ANY, pat, MATCH'(Source.nowhere, NONE), emptySets)
	in
	    if exhaustive then () else
		Error.warning(infoPat pat, "pattern not exhaustive")
	end
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML core elaboration
 *
 * Definition, Sections 4.10, 4.11, 4.6, 4.7, 2.9
 *
 * Notes:
 *   - Elaboration also checks the further restrictions [Section 4.11].
 *   - To implement overloading resolution and checks for flexible records,
 *     we accumulate lists of unresolved types at each value declaration.
 *     This requires an additional argument to most elab functions.
 *   - To implement the 3rd restriction in 4.11 some elab functions are
 *     passed an additional boolean argument to recognise being on the toplevel.
 *   - The definition says that overloaded types get defaulted if the
 *     "surrounding text" does not resolve it. It leaves some freedom to
 *     how large this context may be. We choose the innermost value binding.
 *   - The definition states that "the program context" must determine the
 *     exact type of flexible records, but it does not say how large this
 *     context may be either. Again we choose the innermost surrounding value
 *     declaration.
 *)

structure ElabCore : ELAB_CORE =
struct
    (* Import *)

    open GrammarCore
    open StaticObjectsCore
    open Error


    (* Helpers for context modification *)

    val plus         = StaticEnv.plus
    val plusU        = Context.plusU
    val plusVE       = Context.plusVE
    val oplusE       = Context.oplusE
    val oplusTE      = Context.oplusTE
    val oplusVEandTE = Context.oplusVEandTE

    infix plusU plusVE oplusE oplusTE oplusVEandTE



    (* Treating tyvarseqs *)

    fun tyvars(TyVarseq(I, tyvars)) =
	let
	    fun collect(     [],       U) = U
	      | collect(tyvar::tyvars, U) =
		    collect(tyvars, TyVarSet.add(U, tyvar))
	in
	    ( collect(tyvars, TyVarSet.empty), tyvars )
	end


    (* Management of unresolved types [Sections 4.11 and Appendix E] *)

    type unresolved = Info * Type * SCon option

    fun instance (I,utaus) sigma =
	let
	    val (taus,tau) = TypeScheme.instance sigma
	in
	    utaus := List.map (fn tau => (I, tau, NONE))
			      (List.filter Type.isOverloaded taus) @ !utaus;
	    tau
	end

    fun resolve(I, tau, sc_opt) =
	( Type.resolve tau handle Type.Flexible =>
	      (* Further restriction [Section 4.11, item 1] *)
	      error(I, "unresolved flexible record type")
	; case sc_opt
	    of NONE    => ()
	     | SOME sc =>
	       let
	           val t = Type.tyname tau
	       in
		   case sc
		     of SCon.INT(b, s, r) =>
			( Library.intFromString(b, s, SOME t) ; r := SOME t )
		      | SCon.WORD(b, s, r) =>
			( Library.wordFromString(b, s, SOME t) ; r := SOME t )
		      | SCon.CHAR(s, r) =>
			( Library.charFromString(s, SOME t) ; r := SOME t )
		      | SCon.STRING(s, r) =>
			( Library.stringFromString(s, SOME t) ; r := SOME t )
		      | SCon.REAL(s, r) =>
			( Library.realFromString(s, SOME t) ; r := SOME t )
	       end
	       handle Overflow =>
		     (* [Section E.1] *)
		     error(I, "special constant out of range")
	)



    (* Typing special constants [Section 4.1, Appendix E.1] *)

    fun typeSCon (utaus, I) scon =
	let
	    val tau = typeSCon' scon
	in
	    utaus := (I, tau, SOME scon) :: !utaus;
	    tau
	end

    and typeSCon'(SCon.INT _)    = Type.fromOverloadingClass Library.Int
      | typeSCon'(SCon.WORD _)   = Type.fromOverloadingClass Library.Word
      | typeSCon'(SCon.CHAR _)   = Type.fromOverloadingClass Library.Char
      | typeSCon'(SCon.STRING _) = Type.fromOverloadingClass Library.String
      | typeSCon'(SCon.REAL _)   = Type.fromOverloadingClass Library.Real



    (* Inference rules [Section 4.10] *)


    (* Atomic Expressions *)

    fun elabAtExp (utaus, fnmatches) (C, SCONAtExp(I, scon)) =
	(* [Rule 1] *)
	typeSCon (utaus, I) scon

      | elabAtExp (utaus, fnmatches) (C, IDAtExp(I, _, longvid)) =
	(* [Rule 2] *)
	let
	    val (sigma,is) = case Context.findLongVId(C, longvid)
			       of SOME valstr => valstr
			        | NONE =>
				  errorLongVId(I, "unknown identifier ",longvid)
	    val tau = instance (I,utaus) sigma
	in
	    tau
	end

      | elabAtExp (utaus, fnmatches) (C, RECORDAtExp(I, exprow_opt)) =
	(* [Rule 3] *)
	let
	    val rho = case exprow_opt
			of NONE        => Type.emptyRow
			 | SOME exprow => elabExpRow (utaus, fnmatches)
						     (C, exprow)
	in
	    Type.fromRowType rho
	end

      | elabAtExp (utaus, fnmatches) (C, LETAtExp(I, dec, exp)) =
	(* [Rule 4] *)
	let
	    val E   = elabDec false (C, dec)
	    val tau = elabExp (utaus, fnmatches) (C oplusE E, exp)
	in
	    if TyNameSet.isSubset(Type.tynames tau, Context.Tof C) then
		tau
	    else
		error(I, "escaping local type name in let expression")
	end

      | elabAtExp (utaus, fnmatches) (C, PARAtExp(I, exp)) =
	(* [Rule 5] *)
	let
	    val tau = elabExp (utaus, fnmatches) (C, exp)
	in
	    tau
	end


    (* Expression Rows *)

    and elabExpRow (utaus, fnmatches) (C, ExpRow(I, lab, exp, exprow_opt)) =
	(* [Rule 6] *)
	let
	    val tau = elabExp (utaus, fnmatches) (C, exp)
	    val rho = case exprow_opt
			of NONE        => Type.emptyRow
			 | SOME exprow => elabExpRow (utaus, fnmatches)
						     (C, exprow)
	in
	    Type.insertRow(rho, lab, tau)
	end


    (* Expressions *)

    and elabExp (utaus, fnmatches) (C, ATExp(I, atexp)) =
	(* [Rule 7] *)
	let
	    val tau = elabAtExp (utaus, fnmatches) (C, atexp)
	in
	    tau
	end

      | elabExp (utaus, fnmatches) (C, APPExp(I, exp, atexp)) =
	(* [Rule 8] *)
	let
	    val tau1 = elabExp (utaus, fnmatches) (C, exp)
	    val tau' = elabAtExp (utaus, fnmatches) (C, atexp)
	    val tau  = Type.guess false
	in
	    Type.unify(tau1, Type.fromFunType(tau',tau))
	    handle Type.Unify => error(I, "type mismatch on application");
	    tau
	end

      | elabExp (utaus, fnmatches) (C, COLONExp(I, exp, ty)) =
	(* [Rule 9] *)
	let
	    val tau1 = elabExp (utaus, fnmatches) (C, exp)
	    val tau  = elabTy(C, ty)
	in
	    Type.unify(tau1,tau)
	    handle Type.Unify =>
		   error(I, "expression does not match annotation");
	    tau
	end

      | elabExp (utaus, fnmatches) (C, HANDLEExp(I, exp, match)) =
	(* [Rule 10] *)
	let
	    val tau1 = elabExp (utaus, fnmatches) (C, exp)
	    val tau2 = elabMatch (utaus, fnmatches) (C, match)
	in
	    Type.unify(Type.fromFunType(InitialStaticEnv.tauExn, tau1), tau2)
	    handle Type.Unify =>
		   error(I, "type mismatch in handler");
	    tau1
	end

      | elabExp (utaus, fnmatches) (C, RAISEExp(I, exp)) =
	(* [Rule 11] *)
	let
	    val tau1 = elabExp (utaus, fnmatches) (C, exp)
	in
	    Type.unify(tau1, InitialStaticEnv.tauExn)
	    handle Type.Unify =>
		   error(I, "raised expression is not an exception");
	    Type.guess false
	end

      | elabExp (utaus, fnmatches) (C, FNExp(I, match)) =
	(* [Rule 12] *)
	let
	    val tau = elabMatch (utaus, fnmatches) (C, match)
	in
	    (* Further restriction [Section 4.11, item 2] *)
	    fnmatches := (Context.Eof C, match) :: !fnmatches;
	    tau
	end


    (* Matches *)

    and elabMatch (utaus, fnmatches) (C, Match(I, mrule, match_opt)) =
	(* [Rule 13] *)
	let
	    val tau = elabMrule (utaus, fnmatches) (C, mrule)
	in
	    case match_opt
	      of NONE       => tau
	       | SOME match =>
		 let
		     val tau2 = elabMatch (utaus, fnmatches) (C, match)
		 in
		     Type.unify(tau, tau2)
		     handle Type.Unify =>
			    error(I, "type mismatch between different matches");
		     tau
		 end
	end


    (* Match rules *)

    and elabMrule (utaus, fnmatches) (C, Mrule(I, pat, exp)) =
	(* [Rule 14] *)
	let
	    val (VE,tau) = elabPat utaus (C, pat)
	    val  tau'    = elabExp (utaus, fnmatches) (C plusVE VE, exp)
	in
	    if TyNameSet.isSubset(StaticEnv.tynamesVE VE, Context.Tof C) then
		Type.fromFunType(tau,tau')
	    else
		(* Side condition is always ensured by stamping. *)
		error(I, "inconsistent type names")
	end


    (* Declarations *)

    and elabDec toplevel (C, VALDec(I, tyvarseq, valbind)) =
	(* [Rule 15] *)
	let
	    val U' = #1(tyvars(tyvarseq))
		     (* Collect implicitly bound tyvars [Section 4.6] *)
	    val U  = TyVarSet.union(U',
			TyVarSet.difference(ScopeTyVars.unguardedTyVars valbind,
					    Context.Uof C))
	    val utaus     = ref []
	    val fnmatches = ref []
	    val VE        = elabValBind (toplevel, utaus, fnmatches)
					(C plusU U, valbind)
	    val _         = List.app resolve (!utaus)
	    val VE'       = Clos.Clos (C,valbind) VE
	in
	    (* Further restriction [Section 4.11, item 2] *)
	    List.app CheckPattern.checkMatch (!fnmatches);
	    if TyVarSet.isEmpty(
			TyVarSet.intersection(U, StaticEnv.tyvarsVE VE')) then
		StaticEnv.fromVE VE'
	    else
		error(I, "some explicit type variables cannot be generalised")
	end

      | elabDec toplevel (C, TYPEDec(I, typbind)) =
	(* [Rule 16] *)
	let
	    val TE = elabTypBind(C, typbind)
	in
	    StaticEnv.fromTE TE
	end

      | elabDec toplevel (C, DATATYPEDec(I, datbind)) =
	(* [Rule 17] *)
	let
	    val      TE1  = lhsDatBind datbind
	    val (VE2,TE2) = elabDatBind(C oplusTE TE1, datbind)
	    val (TE, VE)  = StaticEnv.maximiseEquality(TE2,VE2)
	in
	    if List.all (fn(t,VE') =>
	 		    not(TyNameSet.member(Context.Tof C,
						 valOf(TypeFcn.toTyName t))))
			(TyConMap.listItems TE) then
		StaticEnv.fromVEandTE(VE,TE)
	    else
		(* Side condition is always ensured by stamping. *)
		error(I, "inconsistent type names")
	end

      | elabDec toplevel (C, DATATYPE2Dec(I, tycon, longtycon)) =
	(* [Rule 18] *)
	let
	    val (theta,VE) = case Context.findLongTyCon(C, longtycon)
			      of SOME tystr => tystr
			       | NONE =>
				 errorLongTyCon(I, "unknown type ", longtycon)
	    val  TE        = TyConMap.singleton(tycon, (theta,VE))
	in
	    StaticEnv.fromVEandTE(VE,TE)
	end

      | elabDec toplevel (C, ABSTYPEDec(I, datbind, dec)) =
	(* [Rule 19] *)
	let
	    val      TE1  = lhsDatBind datbind
	    val (VE2,TE2) = elabDatBind(C oplusTE TE1, datbind)
	    val (TE, VE)  = StaticEnv.maximiseEquality(TE2,VE2)
	    val    E      = elabDec false (C oplusVEandTE (VE,TE), dec)
	in
	    if List.all (fn(t,VE') =>
			    not(TyNameSet.member(Context.Tof C,
						 valOf(TypeFcn.toTyName t))))
			(TyConMap.listItems TE) then
		StaticEnv.Abs(TE,E)
	    else
		(* Side condition is always ensured by stamping. *)
		error(I, "inconsistent type names")
	end

      | elabDec toplevel (C, EXCEPTIONDec(I, exbind)) =
	(* [Rule 20] *)
	let
	    val VE = elabExBind(C, exbind)
	in
	    StaticEnv.fromVE VE
	end

      | elabDec toplevel (C, LOCALDec(I, dec1, dec2)) =
	(* [Rule 21] *)
	let
	    val E1 = elabDec false (C, dec1)
	    val E2 = elabDec false (C oplusE E1, dec2)
	in
	    E2
	end

      | elabDec toplevel (C, OPENDec(I, longstrids)) =
	(* [Rule 22] *)
	let
	    val Es =
		List.map
		    (fn longstrid =>
			case Context.findLongStrId(C, longstrid)
			  of SOME E => E
			   | NONE =>
			     errorLongStrId(I, "unknown structure ", longstrid))
		    longstrids
	in
	    List.foldr StaticEnv.plus StaticEnv.empty Es
	end

      | elabDec toplevel (C, EMPTYDec(I)) =
	(* [Rule 23] *)
	StaticEnv.empty

      | elabDec toplevel (C, SEQDec(I, dec1, dec2)) =
	(* [Rule 24] *)
	let
	    val E1 = elabDec toplevel (C, dec1)
	    val E2 = elabDec toplevel (C oplusE E1, dec2)
	in
	    StaticEnv.plus(E1, E2)
	end


    (* Value Bindings *)

    and elabValBind (toplevel, utaus, fnmatches)
		    (C, PLAINValBind(I, pat, exp, valbind_opt)) =
	(* [Rule 25] *)
	let
	    val (VE,tau1) = elabPat utaus (C, pat)
	    val     tau2  = elabExp (utaus, fnmatches) (C, exp)
	    val  VE'      = case valbind_opt
			      of NONE         => VIdMap.empty
			       | SOME valbind =>
				 elabValBind (toplevel, utaus, fnmatches)
					     (C, valbind)
	in
	    Type.unify(tau1,tau2)
	    handle Type.Unify =>
		   error(I, "type mismatch between pattern and expression");
	    if toplevel then () else
		(* Further restriction [Section 4.11, item 3] *)
		CheckPattern.checkPat(Context.Eof C, pat);
	    VIdMap.unionWith #2 (VE,VE')
	end

      | elabValBind (toplevel, utaus, fnmatches) (C, RECValBind(I, valbind)) =
	(* [Rule 26] *)
	let
	    val VE1 = lhsRecValBind valbind
	    val VE  = elabValBind (toplevel, utaus, fnmatches)
				  (C plusVE VE1, valbind)
	in
	    if not(StaticEnv.equalsVE(VE1, VE)) then
		error(I, "type mismatch in recursive binding")
	    else if TyNameSet.isSubset(StaticEnv.tynamesVE VE,
				       Context.Tof C) then
		VE
	    else
		(* Side condition is always ensured by construction. *)
		error(I, "invalid introduction of type names")
	end


    (* Type Bindings *)

    and elabTypBind(C, TypBind(I, tyvarseq, tycon, ty, typbind_opt)) =
	(* [Rule 27] *)
	let
	    val (U,alphas) = tyvars tyvarseq
	    val tau        = elabTy(C, ty)
	    val TE         = case typbind_opt
			       of NONE         => TyConMap.empty
				| SOME typbind => elabTypBind(C, typbind)
	in
	    TyConMap.insert(TE, tycon, ((alphas,tau),VIdMap.empty))
	end


    (* Datatype Bindings *)

    and elabDatBind(C, DatBind(I, tyvarseq, tycon, conbind, datbind_opt)) =
	(* [Rule 28, part 2] *)
	let
	    val (U,alphas)   = tyvars tyvarseq
	    val (alphas,tau) = case Context.findTyCon(C, tycon)
				 of SOME(theta,VE) => theta
				  | NONE => (* lhsDatBind inserted it! *)
				    raise Fail "ElabCore.elabDatBind: \
						\tycon not pre-bound"
	    val VE       = elabConBind(C,tau, conbind)
	    val(VE',TE') = case datbind_opt
			     of NONE         => ( VIdMap.empty, TyConMap.empty )
			      | SOME datbind =>
				let
				    val  t = valOf(TypeFcn.toTyName(alphas,tau))
				    val (VE',TE') = elabDatBind(C, datbind)
				in
				    if List.all (fn(t',VE'') =>
						t <> valOf(TypeFcn.toTyName t'))
					 	(TyConMap.listItems TE') then
					(VE',TE')
				    else
					(* Side condition is always ensured
					 * by stamping. *)
					error(I, "inconsistent type names")
				end
	    val ClosVE   = StaticEnv.Clos VE
	in
	    ( VIdMap.unionWith #2 (ClosVE,VE')
	    , TyConMap.insert(TE', tycon, ((alphas,tau),ClosVE))
	    )
	end


    (* Constructor Bindings *)

    and elabConBind(C,tau, ConBind(I, _, vid, ty_opt, conbind_opt)) =
	(* [Rule 29] *)
	let
	    val tau1 = case ty_opt
			 of NONE    => tau
			  | SOME ty =>
			    let
				val tau' = elabTy(C, ty)
			    in
			        Type.fromFunType(tau',tau)
			    end
	    val VE   = case conbind_opt
			 of NONE         => VIdMap.empty
			  | SOME conbind => elabConBind(C,tau, conbind)
	in
	    VIdMap.insert(VE, vid, (([],tau1),IdStatus.c))
	end


    (* Exception Bindings *)

    and elabExBind(C, NEWExBind(I, _, vid, ty_opt, exbind_opt)) =
	(* [Rule 30] *)
	let
	    val tau1 = case ty_opt
			 of NONE    => InitialStaticEnv.tauExn
			  | SOME ty =>
			    let
				val tau = elabTy(C, ty)
			    in
			        Type.fromFunType(tau, InitialStaticEnv.tauExn)
			    end
	    val VE   = case exbind_opt
			 of NONE        => VIdMap.empty
			  | SOME exbind => elabExBind(C, exbind)
	in
	    VIdMap.insert(VE, vid, (([],tau1),IdStatus.e))
	end

      | elabExBind(C, EQUALExBind(I, _, vid, _, longvid, exbind_opt)) =
	(* [Rule 31] *)
	let
	    val tau = case Context.findLongVId(C, longvid)
		        of SOME(([],tau),IdStatus.e) => tau
			 | SOME _ =>
			   errorLongVId(I, "non-exception identifier ", longvid)
			 | NONE =>
			   errorLongVId(I, "unknown identifier ", longvid)
	    val VE  = case exbind_opt
			of NONE        => VIdMap.empty
			 | SOME exbind => elabExBind(C, exbind)
	in
	    VIdMap.insert(VE, vid, (([],tau),IdStatus.e))
	end


    (* Atomic Patterns *)

    and elabAtPat utaus (C, WILDCARDAtPat(I)) =
	(* [Rule 32] *)
	( VIdMap.empty, Type.guess false )

      | elabAtPat utaus (C, SCONAtPat(I, scon)) =
	(* [Rule 33] *)
	( VIdMap.empty, typeSCon (utaus,I) scon )

      | elabAtPat utaus (C, IDAtPat(I, _, longvid)) =
	(* [Rule 34 and 35] *)
	let
	    val (strids,vid) = LongVId.explode longvid
	in
	    if List.null strids andalso
	       ( case Context.findVId(C, vid)
		   of NONE           => true
		    | SOME(sigma,is) => is = IdStatus.v )
	    then
		(* [Rule 34] *)
		let
		    val tau = Type.guess false
		in
		    ( VIdMap.singleton(vid, (([],tau),IdStatus.v))
		    , tau )
		end
	    else
		(* [Rule 35] *)
		let
		    val (sigma,is) = case Context.findLongVId(C, longvid)
				       of SOME valstr => valstr
				        | NONE =>
					  errorLongVId(I,"unknown constructor ",
							 longvid)
		    val  tau       = instance (I,utaus) sigma
		in
		    if is = IdStatus.v then
			error(I, "non-constructor long identifier in pattern")
		    else case !tau
		      of ConsType _ => 
			 ( VIdMap.empty, tau )
		       | _ => 
			 error(I, "missing constructor argument in pattern")
		end
	end

      | elabAtPat utaus (C, RECORDAtPat(I, patrow_opt)) =
	(* [Rule 36] *)
	let
	    val (VE,rho) = case patrow_opt
			     of NONE        => (VIdMap.empty, Type.emptyRow)
			      | SOME patrow => elabPatRow utaus (C, patrow)
	    val tau = Type.fromRowType rho
	in
	    utaus := (I, tau, NONE) :: !utaus;
	    (VE, tau)
	end

      | elabAtPat utaus (C, PARAtPat(I, pat)) =
	(* [Rule 37] *)
	let
	    val (VE,tau) = elabPat utaus (C, pat)
	in
	    (VE,tau)
	end


    (* Pattern Rows *)

    and elabPatRow utaus (C, DOTSPatRow(I)) =
	(* [Rule 38] *)
	( VIdMap.empty, Type.guessRow() )

      | elabPatRow utaus (C, FIELDPatRow(I, lab, pat, patrow_opt)) =
	(* [Rule 39] *)
	let
	    val (VE,tau)  = elabPat utaus (C, pat)
	    val (VE',rho) = case patrow_opt
			      of NONE        => (VIdMap.empty, Type.emptyRow)
			       | SOME patrow => elabPatRow utaus (C, patrow)
	in
	    ( VIdMap.unionWithi (fn(vid,_,_) =>
		    errorVId(I, "duplicate variable ", vid)) (VE,VE')
	    , Type.insertRow(rho, lab, tau)
	    )
	end


    (* Patterns *)

    and elabPat utaus (C, ATPat(I, atpat)) =
	(* [Rule 40] *)
	let
	    val (VE,tau) = elabAtPat utaus (C, atpat)
	in
	    (VE,tau)
	end

      | elabPat utaus (C, CONPat(I, _, longvid, atpat)) =
	(* [Rule 41] *)
	let
	    val (sigma,is) = case Context.findLongVId(C, longvid)
			       of SOME valstr => valstr
			        | NONE =>
				errorLongVId(I, "unknown constructor ", longvid)
	    val _          = if is <> IdStatus.v then () else
				errorLongVId(I, "non-constructor ", longvid)
	    val (tau',tau) = case !(instance (I,utaus) sigma)
			       of FunType(tau',tau) => (tau', tau)
			        | _ =>
				errorLongVId(I,"misplaced nullary constructor ",
						longvid)
	    val (VE,tau'2)  = elabAtPat utaus (C, atpat)
	in
	    Type.unify(tau',tau'2)
	    handle Type.Unify =>
		   error(I, "type mismatch in constructor pattern");
	    (VE,tau)
	end

      | elabPat utaus (C, COLONPat(I, pat, ty)) =
	(* [Rule 42] *)
	let
	    val (VE,tau1) = elabPat utaus (C, pat)
	    val     tau   = elabTy(C, ty)
	in
	    Type.unify(tau1,tau)
	    handle Type.Unify => error(I, "pattern does not match annotation");
	    (VE,tau)
	end

      | elabPat utaus (C, ASPat(I, _, vid, ty_opt, pat)) =
	(* [Rule 43] *)
	let
	    val (VE1,tau1) = elabPat utaus (C, pat)
	    val (VE, tau)  =
		case ty_opt
		  of NONE    => (VE1,tau1)
		   | SOME ty =>
		     let
			 val tau = elabTy(C, ty)
		     in
			 Type.unify(tau1,tau)
			 handle Type.Unify =>
				error(I, "pattern does not match annotation");
			 (VE1,tau)
		     end
	in
	    if VIdMap.inDomain(VE, vid) then
		errorVId(I, "duplicate variable ", vid)
	    else
		( VIdMap.insert(VE, vid, (([],tau),IdStatus.v)), tau )
	end


    (* Type Expressions *)

    and elabTy(C, VARTy(I, tyvar)) =
	(* [Rule 44] *)
	let
	    val alpha = tyvar
	in
	    Type.fromTyVar alpha
	end

      | elabTy(C, RECORDTy(I, tyrow_opt)) =
	(* [Rule 45] *)
	let
	    val rho = case tyrow_opt
			of NONE       => Type.emptyRow
			 | SOME tyrow => elabTyRow(C, tyrow)
	in
	    Type.fromRowType rho
	end

      | elabTy(C, CONTy(I, tyseq, longtycon)) =
	(* [Rule 46] *)
	let
	    val Tyseq(I',tys) = tyseq
	    val k             = List.length tys
	    val taus          = List.map (fn ty => elabTy(C, ty)) tys
	    val (theta,VE)    =
		case Context.findLongTyCon(C, longtycon)
		  of SOME tystr => tystr
		   | NONE =>
		     errorLongTyCon(I, "unknown type constructor ", longtycon)
	in
	    TypeFcn.apply(taus, theta)
	    handle TypeFcn.Apply =>
		errorLongTyCon(I, "arity mismatch at type application ",
				  longtycon)
	end

      | elabTy(C, ARROWTy(I, ty, ty')) =
	(* [Rule 47] *)
	let
	    val tau  = elabTy(C, ty)
	    val tau' = elabTy(C, ty')
	in
	    Type.fromFunType(tau,tau')
	end

      | elabTy(C, PARTy(I, ty)) =
	(* [Rule 48] *)
	let
	    val tau = elabTy(C, ty)
	in
	    tau
	end


    (* Type-expression Rows *)

    and elabTyRow(C, TyRow(I, lab, ty, tyrow_opt)) =
	(* [Rule 49] *)
	let
	    val tau = elabTy(C, ty)
	    val rho = case tyrow_opt
			of NONE       => Type.emptyRow
			 | SOME tyrow => elabTyRow(C, tyrow)
	in
	    Type.insertRow(rho, lab, tau)
	end



    (* Build tentative VE from LHSs of recursive valbind *)

    and lhsRecValBind(PLAINValBind(I, pat, exp, valbind_opt)) =
	let
	    val VE  = lhsRecValBindPat pat
	    val VE' = case valbind_opt
			of NONE         => VIdMap.empty
			 | SOME valbind => lhsRecValBind valbind
	in
	    VIdMap.unionWith #2 (VE,VE')
	end

      | lhsRecValBind(RECValBind(I, valbind)) =
	    lhsRecValBind valbind

    and lhsRecValBindPat(ATPat(I, atpat)) =
	    lhsRecValBindAtPat atpat

      | lhsRecValBindPat(CONPat(I, _, longvid, atpat)) =
	    lhsRecValBindAtPat atpat

      | lhsRecValBindPat(COLONPat(I, pat, ty)) =
	    lhsRecValBindPat pat

      | lhsRecValBindPat(ASPat(I, _, vid, ty_opt, pat)) =
	let
	    val VE = lhsRecValBindPat pat
	in
	    VIdMap.insert(VE, vid, (([], Type.guess false), IdStatus.v))
	end

    and lhsRecValBindAtPat(WILDCARDAtPat(I)) =
	    VIdMap.empty

      | lhsRecValBindAtPat(SCONAtPat(I, scon)) =
	    VIdMap.empty

      | lhsRecValBindAtPat(IDAtPat(I, _, longvid)) =
	   (case LongVId.explode longvid
	      of ([], vid) =>
	 	 VIdMap.singleton(vid, (([], Type.guess false), IdStatus.v))
	       | _ => VIdMap.empty
	   )

      | lhsRecValBindAtPat(RECORDAtPat(I, patrow_opt)) =
	   (case patrow_opt
	      of NONE        => VIdMap.empty
	       | SOME patrow => lhsRecValBindPatRow patrow
	   )

      | lhsRecValBindAtPat(PARAtPat(I, pat)) =
	    lhsRecValBindPat pat

    and lhsRecValBindPatRow(DOTSPatRow(I)) =
	    VIdMap.empty

      | lhsRecValBindPatRow(FIELDPatRow(I, lab, pat, patrow_opt)) =
	let
	    val VE = lhsRecValBindPat pat
	in
	    case patrow_opt
	      of NONE        => VE
	       | SOME patrow =>
		 let
		     val VE' = lhsRecValBindPatRow patrow
		 in
		     VIdMap.unionWith #2 (VE,VE')
		 end
	end



    (* Build tentative TE from LHSs of datbind *)

    and lhsDatBind(DatBind(I, tyvarseq, tycon, conbind, datbind_opt)) =
	(* [Rule 28, part 1] *)
	let
	    val (U,alphas) = tyvars tyvarseq
	    val k          = List.length alphas
	    val span       = lhsConBind conbind
	    val t          = TyName.tyname(TyCon.toString tycon, k, true, span)
	    val tau        = Type.fromConsType(List.map Type.fromTyVar alphas,t)
	    val TE'        = case datbind_opt
			       of NONE         => TyConMap.empty
				| SOME datbind => lhsDatBind datbind
	in
	    TyConMap.insert(TE', tycon, ((alphas,tau), VIdMap.empty))
	end

    and lhsConBind(ConBind(I, _, vid, ty_opt, conbind_opt)) =
	case conbind_opt
	  of NONE         => 1
	   | SOME conbind => 1 + lhsConBind conbind
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML modules elaboration
 *
 * Definition, Sections 5.7 and 3.5
 *
 * Notes:
 *   - To implement the 3rd restriction in 4.11 some elab functions are
 *     passed an additional boolean argument to recognise being on the toplevel.
 *   - Another bug in the Definition -- rules 64 and 78 need additional
 *     side conditions to ensure well-formedness of the constructed realisation:
 *     (64) t in TyName^(k)
 *     (78) t_i in TyName^(k), i = 1..n
 *)

structure ElabModule : ELAB_MODULE =
struct
    (* Import *)

    open GrammarModule
    open StaticObjectsModule
    open StaticObjectsCore
    open Error


    (* Helpers for basis modification *)

    val plus    = StaticBasis.plus
    val plusT   = StaticBasis.plusT
    val oplusSE = StaticBasis.oplusSE
    val oplusG  = StaticBasis.oplusG
    val oplusF  = StaticBasis.oplusF
    val oplusE  = StaticBasis.oplusE

    infix plus plusT oplusG oplusF oplusE oplusSE


    (* Inference rules [Section 5.7] *)


    (* Structure Expressions *)

    fun elabStrExp(B, STRUCTStrExp(I, strdec)) =
	(* [Rule 50] *)
	let
	    val E = elabStrDec false (B, strdec)
	in
	    E
	end

      | elabStrExp(B, IDStrExp(I, longstrid)) =
	(* [Rule 51] *)
	let
	    val E = case StaticBasis.findLongStrId(B, longstrid)
		      of SOME E => E
		       | NONE =>
			 errorLongStrId(I, "unknown structure ", longstrid)
	in
	    E
	end

      | elabStrExp(B, COLONStrExp(I, strexp, sigexp)) =
	(* [Rule 52] *)
	let
	    val E      = elabStrExp(B, strexp)
	    val Sigma  = elabSigExp(B, sigexp)
	    val (E',_) = Sig.match(E, Sigma)
			 handle Sig.Match =>
				error(I, "structure does not match constraint")
	in
	    E'
	end

      | elabStrExp(B, SEALStrExp(I, strexp, sigexp)) =
	(* [Rule 53] *)
	let
	    val E       = elabStrExp(B, strexp)
	    val (T',E') = Sig.rename(elabSigExp(B, sigexp))
	    val (E'',_) = Sig.match(E, (T',E'))
			  handle Sig.Match =>
				 error(I, "structure does not match constraint")
	in
	    if TyNameSet.isEmpty
		    (TyNameSet.intersection(T', StaticBasis.tynames B)) then
		E'
	    else
		(* Side condition is always ensured by renaming. *)
		error(I, "inconsistent type names")
	end

      | elabStrExp(B, APPStrExp(I, funid, strexp)) =
	(* [Rule 54] *)
	let
	    val E = elabStrExp(B, strexp)
	    val (T1,(E1,(T1',E1'))) =
		      case StaticBasis.findFunId(B, funid)
			of SOME Phi => Phi
			 | NONE     => errorFunId(I, "unknown functor ", funid)
	    val (E'',phi) = Sig.match(E, (T1,E1))
			    handle Sig.Match =>
				error(I, "structure does not match constraint")
	    val (T',E')   = Sig.rename (T1', StaticEnv.realise phi E1')
	in
	    if TyNameSet.isEmpty
		(TyNameSet.intersection(TyNameSet.union(StaticEnv.tynames E,
							StaticBasis.tynames B),
					T')) then
		E'
	    else
		(* Side condition is always ensured by renaming. *)
		error(I, "inconsistent type names")
	end

      | elabStrExp(B, LETStrExp(I, strdec, strexp)) =
	(* [Rule 55] *)
	let
	    val E1 = elabStrDec false (B, strdec)
	    val E2 = elabStrExp(B oplusE E1, strexp)
	in
	    E2
	end


    (* Structure-level Declarations *)

    and elabStrDec toplevel (B, DECStrDec(I, dec)) =
	(* [Rule 56] *)
	let
	    val E = ElabCore.elabDec toplevel (StaticBasis.Cof B, dec)
	in
	    E
	end

      | elabStrDec toplevel (B, STRUCTUREStrDec(I, strbind)) =
	(* [Rule 57] *)
	let
	    val SE = elabStrBind(B, strbind)
	in
	    StaticEnv.fromSE SE
	end

      | elabStrDec toplevel (B, LOCALStrDec(I, strdec1, strdec2)) =
	(* [Rule 58] *)
	let
	    val E1 = elabStrDec false (B, strdec1)
	    val E2 = elabStrDec false (B oplusE E1, strdec2)
	in
	    E2
	end

      | elabStrDec toplevel (B, EMPTYStrDec(I)) =
	(* [Rule 59] *)
	StaticEnv.empty

      | elabStrDec toplevel (B, SEQStrDec(I, strdec1, strdec2)) =
	(* [Rule 60] *)
	let
	    val E1 = elabStrDec toplevel (B, strdec1)
	    val E2 = elabStrDec toplevel (B oplusE E1, strdec2)
	in
	    StaticEnv.plus(E1,E2)
	end


    (* Structure Bindings *)

    and elabStrBind(B, StrBind(I, strid, strexp, strbind_opt)) =
	(* [Rule 61] *)
	let
	    val E  = elabStrExp(B, strexp)
	    val SE = case strbind_opt
		       of NONE         => StrIdMap.empty
		        | SOME strbind =>
			  elabStrBind(B plusT StaticEnv.tynames E, strbind)
	in
	    StrIdMap.insert(SE, strid, E)
	end


    (* Signature Expressions *)

    and elabSigExpE(B, SIGSigExp(I, spec)) =
	(* [Rule 62] *)
	let
	    val E = elabSpec(B, spec)
	in
	    E
	end

      | elabSigExpE(B, IDSigExp(I, sigid)) =
	(* [Rule 63] *)
	let
	    val (T,E) = case StaticBasis.findSigId(B, sigid)
			  of SOME Sigma => Sig.rename Sigma
			   | NONE => errorSigId(I, "unknown signature ",sigid)
	in
	    E
	end

      | elabSigExpE(B, WHERETYPESigExp(I, sigexp, tyvarseq, longtycon, ty)) =
	(* [Rule 64] *)
	let
	    val E      = elabSigExpE(B, sigexp)
	    val (U,alphas) = ElabCore.tyvars tyvarseq
	    val tau    = ElabCore.elabTy(StaticBasis.Cof B, ty)
	    val t      = case StaticEnv.findLongTyCon(E,longtycon)
			   of NONE =>
			      errorLongTyCon(I, "unknown type ", longtycon)
			    | SOME(theta,VE) =>
			 case TypeFcn.toTyName theta
			   of NONE =>
			      errorLongTyCon(I, "non-flexible type ", longtycon)
			    | SOME t => t
	    val  _     = if not(TyNameSet.member(StaticBasis.Tof B, t)) then ()
			 else errorLongTyCon(I, "rigid type ", longtycon)
	    val phi    = TyNameMap.singleton(t, (alphas,tau))
	    val  _     = if not(TyName.admitsEquality t)
			 orelse TypeFcn.admitsEquality (alphas,tau) then () else
			  error(I, "type realisation does not respect equality")
	    val  _     = if TyName.arity t = List.length alphas then () else
			  error(I, "type realisation does not respect arity")
	    val E'     = StaticEnv.realise phi E
	    val  _     = if StaticEnv.isWellFormed E' then () else
			  error(I, "type realisation does not respect datatype")
	in
	    E'
	end

    and elabSigExp(B, sigexp) =
	(* [Rule 65] *)
	let
	    val E = elabSigExpE(B, sigexp)
	    val T = TyNameSet.difference(StaticEnv.tynames E, StaticBasis.Tof B)
	in
	    (T,E)
	end


    (* Signature Declarations *)

    and elabSigDec(B, SigDec(I, sigbind)) =
	(* [Rule 66] *)
	let
	    val G = elabSigBind(B, sigbind)
	in
	    G
	end


    (* Signature Bindings *)

    and elabSigBind(B, SigBind(I, sigid, sigexp, sigbind_opt)) =
	(* [Rule 67] *)
	let
	    val Sigma = elabSigExp(B, sigexp)
	    val G     = case sigbind_opt
			  of NONE         => SigIdMap.empty
			   | SOME sigbind => elabSigBind(B, sigbind)
	in
	    SigIdMap.insert(G, sigid, Sigma)
	end


    (* Specifications *)

    and elabSpec(B, VALSpec(I, valdesc)) =
	(* [Rule 68] *)
	let
	    val VE = elabValDesc(StaticBasis.Cof B, valdesc)
	in
	    StaticEnv.fromVE(StaticEnv.Clos VE)
	end

      | elabSpec(B, TYPESpec(I, typdesc)) =
	(* [Rule 69] *)
	let
	    val TE = elabTypDesc false (StaticBasis.Cof B, typdesc)
	in
	    if List.all (fn(t,VE) =>
			 not(TyName.admitsEquality(valOf(TypeFcn.toTyName t))))
			(TyConMap.listItems TE) then
		StaticEnv.fromTE TE
	    else
		(* Side condition is always ensured by elabTypDesc false. *)
		error(I, "inconsistent type names")
	end

      | elabSpec(B, EQTYPESpec(I, typdesc)) =
	(* [Rule 70] *)
	let
	    val TE = elabTypDesc true (StaticBasis.Cof B, typdesc)
	in
	    if List.all (fn(t,VE) =>
			 TyName.admitsEquality(valOf(TypeFcn.toTyName t)))
			(TyConMap.listItems TE) then
		StaticEnv.fromTE TE
	    else
		(* Side condition is always ensured by elabTypDesc true. *)
		error(I, "inconsistent type names")
	end

      | elabSpec(B, DATATYPESpec(I, datdesc)) =
	(* [Rule 71] *)
	let
	    val      TE1  = lhsDatDesc datdesc
	    val (VE2,TE2) = elabDatDesc(Context.oplusTE(StaticBasis.Cof B,TE1),
					datdesc)
	    val (TE, VE)  = StaticEnv.maximiseEquality(TE2,VE2)
	in
	    if List.all (fn(t,VE') =>
			    not(TyNameSet.member(StaticBasis.tynames B,
						 valOf(TypeFcn.toTyName t))))
			(TyConMap.listItems TE) then
		StaticEnv.fromVEandTE(VE,TE)
	    else
		(* Side condition is always ensured by stamping. *)
		error(I, "inconsistent type names")
	end

      | elabSpec(B, DATATYPE2Spec(I, tycon, longtycon)) =
	(* [Rule 72] *)
	let
	    val (theta,VE) = case StaticBasis.findLongTyCon(B, longtycon)
			      of SOME tystr => tystr
			       | NONE =>
				 errorLongTyCon(I, "unknown type ", longtycon)
	    val  TE        = TyConMap.singleton(tycon, (theta,VE))
	in
	    StaticEnv.fromVEandTE(VE,TE)
	end

      | elabSpec(B, EXCEPTIONSpec(I, exdesc)) =
	(* [Rule 73] *)
	let
	    val VE = elabExDesc(StaticBasis.Cof B, exdesc)
	in
	    StaticEnv.fromVE VE
	end

      | elabSpec(B, STRUCTURESpec(I, strdesc)) =
	(* [Rule 74] *)
	let
	    val SE = elabStrDesc(B, strdesc)
	in
	    StaticEnv.fromSE SE
	end

      | elabSpec(B, INCLUDESpec(I, sigexp)) =
	(* [Rule 75] *)
	let
	    val E = elabSigExpE(B, sigexp)
	in
	    E
	end

      | elabSpec(B, EMPTYSpec(I)) =
	(* [Rule 76] *)
	StaticEnv.empty

      | elabSpec(B, SEQSpec(I, spec1, spec2)) =
	(* [Rule 77] *)
	let
	    val E1 = elabSpec(B, spec1)
	    val E2 = elabSpec(B oplusE E1, spec2)
	    val _  = if StaticEnv.disjoint(E1,E2) then () else
		     error(I, "duplicate specifications in signature")
	in
	    StaticEnv.plus(E1,E2)
	end

      | elabSpec(B, SHARINGTYPESpec(I, spec, longtycons)) =
	(* [Rule 78] *)
	let
	    val E  = elabSpec(B, spec)
	    val ts =
		List.map
		(fn longtycon =>
		 case StaticEnv.findLongTyCon(E, longtycon)
		   of NONE =>
			errorLongTyCon(I, "unknown type ", longtycon)
		    | SOME(theta,VE) =>
		 case TypeFcn.toTyName theta
		   of NONE =>
			errorLongTyCon(I, "non-flexible type ", longtycon)
		    | SOME t =>
		      if TyNameSet.member(StaticBasis.Tof B, t) then
			errorLongTyCon(I, "rigid type ", longtycon)
		      else
			t
		)
		longtycons
	    val arity    = TyName.arity(List.hd ts)
	    val equality = List.exists TyName.admitsEquality ts
	    val span  = List.foldl
				(fn(t, span) => Int.max(TyName.span t, span))
				0 ts
	    val t     = TyName.tyname(TyName.toString(List.hd ts),
				      arity, equality, span)
	    val  _    = if List.all (fn ti => TyName.arity ti = arity) ts
			then () else
			  error(I, "type sharing does not respect arity")
	    val phi   = List.foldl
			    (fn(ti, phi) =>
				TyNameMap.insert(phi, ti, TypeFcn.fromTyName t))
			    TyNameMap.empty ts
	in
	    if TyNameSet.isEmpty
		(TyNameSet.intersection(TyNameSet.fromList ts,
					StaticBasis.tynames B)) then
		StaticEnv.realise phi E
	    else
		(* Side condition is always ensured by stamping. *)
		error(I, "inconsistent type names")
	end

      | elabSpec(B, SHARINGSpec(I, spec, longstrids)) =
	(* [Appendix A] *)
	let
	    fun shareFlexibleTyName(t1, t2, phi) =
		let
		    val t = TyName.tyname(TyName.toString t1, TyName.arity t1,
					  TyName.admitsEquality t1 orelse
					      TyName.admitsEquality t2,
					  Int.max(TyName.span t1,
						  TyName.span t2))
		    val theta = TypeFcn.fromTyName t
		in
		    TyNameMap.insert(TyNameMap.insert(phi,
			t1, theta),
			t2, theta)
		end

	    fun shareTE(TE1, TE2, phi) =
		TyConMap.foldli
		    (fn(tycon, (theta1,VE1), phi) =>
			case TyConMap.find(TE2, tycon)
			  of NONE             => phi
			   | SOME(theta2,VE2) =>
			case (TypeFcn.toTyName(TypeFcn.realise phi theta1),
			      TypeFcn.toTyName(TypeFcn.realise phi theta2))
			  of (SOME t1, SOME t2) =>
			     if TyNameSet.member(StaticBasis.Tof B, t1)
			     orelse TyNameSet.member(StaticBasis.Tof B,t2) then
				errorTyCon(I, "structure contains rigid type ",
					      tycon)
			     else
				shareFlexibleTyName(t1, t2, phi)
			   | _ =>
			     errorTyCon(I, "structure contains non-flexible \
					   \type ", tycon)
		    )
		    phi TE1

	    fun shareSE(SE1, SE2, phi) =
		StrIdMap.foldli
		    (fn(strid, E1, phi) =>
			case StrIdMap.find(SE2, strid)
			  of NONE    => phi
			   | SOME E2 => shareE(E1, E2, phi)
		    )
		    phi SE1

	    and shareE(Env(SE1,TE1,VE1), Env(SE2,TE2,VE2), phi) =
		let
		    val phi'  = shareTE(TE1, TE2, phi)
		    val phi'' = shareSE(SE1, SE2, phi')
		in
		    phi''
		end

	    fun share1(E1,   [],   phi) = phi
	      | share1(E1, E2::Es, phi) =
		let
		    val phi' = shareE(E1, E2, phi)
		in
		    share1(E1, Es, phi')
		end

	    fun shareAll( [],   phi) = phi
	      | shareAll(E::Es, phi) =
		let
		    val phi' = share1(E, Es, phi)
		in
		    shareAll(Es, phi')
		end

	    val E   = elabSpec(B, spec)
	    val Es  = List.map
			(fn longstrid =>
			 case StaticEnv.findLongStrId(E, longstrid)
			   of SOME E' => E'
			    | NONE =>
			      errorLongStrId(I, "unknown structure ", longstrid)
			) longstrids
	    val phi = shareAll(Es, TyNameMap.empty)
	in
	    if TyNameSet.isEmpty
		   (TyNameSet.intersection
			(TyNameSet.addList(TyNameSet.empty,
					   TyNameMap.listKeys phi),
			 StaticBasis.tynames B)) then
		StaticEnv.realise phi E
	    else
		(* Side condition is always ensured by stamping. *)
		error(I, "inconsistent type names")
	end


    (* Value Descriptions *)

    and elabValDesc(C, ValDesc(I, vid, ty, valdesc_opt)) =
	(* [Rule 79] *)
	let
	    val tau = ElabCore.elabTy(C, ty)
	    val VE  = case valdesc_opt
			of NONE         => VIdMap.empty
			 | SOME valdesc => elabValDesc(C, valdesc)
	in
	    VIdMap.insert(VE, vid, (([],tau),IdStatus.v))
	end


    (* Type Descriptions *)

    and elabTypDesc eq (C, TypDesc(I, tyvarseq, tycon, typdesc_opt)) =
	(* [Rule 80] *)
	let
	    val alphas = #2(ElabCore.tyvars tyvarseq)
	    val k      = List.length alphas
	    val t      = TyName.tyname(TyCon.toString tycon, k, eq, 0)
	    val TE     = case typdesc_opt
			   of NONE         => TyConMap.empty
			    | SOME typdesc => 
				let
				    val TE = elabTypDesc eq (C, typdesc)
				in
				    if TyNameSet.member(StaticEnv.tynamesTE TE,
							t) then
					(* Side condition is always ensured
					 * by stamping. *)
					error(I, "inconsistent type names")
				    else
					TE
				end
	    val tau    = Type.fromConsType (List.map Type.fromTyVar alphas, t)
	in
	    TyConMap.insert(TE, tycon, ((alphas,tau), VIdMap.empty))
	end


    (* Datatype Descriptions *)

    and elabDatDesc(C, DatDesc(I, tyvarseq, tycon, condesc, datdesc_opt)) =
	(* [Rule 81, part 2] *)
	let
	    val (U,alphas)   = ElabCore.tyvars tyvarseq
	    val (alphas,tau) = case Context.findTyCon(C, tycon)
				 of SOME(theta,VE) => theta
				  | NONE => (* lhsDatDesc inserted it! *)
				    raise Fail "ElabCore.elabDatDesc: \
						\tycon not pre-bound"
	    val VE       = elabConDesc(C,tau, condesc)
	    val(VE',TE') = case datdesc_opt
			     of NONE         => ( VIdMap.empty, TyConMap.empty )
			      | SOME datdesc =>
				let
				    val  t = valOf(TypeFcn.toTyName(alphas,tau))
				    val (VE',TE') = elabDatDesc(C, datdesc)
				in
				    if List.all (fn(t',VE'') =>
						t <> valOf(TypeFcn.toTyName t'))
					 	(TyConMap.listItems TE') then
					(VE',TE')
				    else
					(* Side condition is always ensured
					 * by stamping. *)
					error(I, "inconsistent type names")
				end
	    val ClosVE   = StaticEnv.Clos VE
	in
	    ( VIdMap.unionWith #2 (ClosVE,VE')
	    , TyConMap.insert(TE', tycon, ((alphas,tau),ClosVE))
	    )
	end


    (* Constructor Descriptions *)

    and elabConDesc(C,tau, ConDesc(I, vid, ty_opt, condesc_opt)) =
	(* [Rule 82] *)
	let
	    val tau1 = case ty_opt
			 of NONE    => tau
			  | SOME ty =>
			    let
				val tau' = ElabCore.elabTy(C, ty)
			    in
			        Type.fromFunType(tau',tau)
			    end
	    val VE   = case condesc_opt
			 of NONE         => VIdMap.empty
			  | SOME condesc => elabConDesc(C,tau, condesc)
	in
	    VIdMap.insert(VE, vid, (([],tau1),IdStatus.c))
	end


    (* Exception Description *)

    and elabExDesc(C, ExDesc(I, vid, ty_opt, exdesc_opt)) =
	(* [Rule 83] *)
	let
	    val tau1 = case ty_opt
			 of NONE    => InitialStaticEnv.tauExn
			  | SOME ty =>
			    let
				val tau = ElabCore.elabTy(C, ty)
				val  _  = if TyVarSet.isEmpty(Type.tyvars tau)
					  then () else
					  error(I, "free type variables \
						   \in exception description")
			    in
			        Type.fromFunType(tau, InitialStaticEnv.tauExn)
			    end
	    val VE   = case exdesc_opt
			 of NONE        => VIdMap.empty
			  | SOME exdesc => elabExDesc(C, exdesc)
	in
	    VIdMap.insert(VE, vid, (([],tau1),IdStatus.e))
	end


    (* Structure Descriptions *)

    and elabStrDesc(B, StrDesc(I, strid, sigexp, strdesc_opt)) =
	(* [Rule 84] *)
	let
	    val E  = elabSigExpE(B, sigexp)
	    val SE = case strdesc_opt
		       of NONE         => StrIdMap.empty
		        | SOME strdesc =>
			  elabStrDesc(B plusT StaticEnv.tynames E, strdesc)
	in
	    StrIdMap.insert(SE, strid, E)
	end


    (* Functor Declarations *)

    and elabFunDec(B, FunDec(I, funbind)) =
	(* [Rule 85] *)
	let
	    val F = elabFunBind(B, funbind)
	in
	    F
	end


    (* Functor Bindings *)

    and elabFunBind(B, FunBind(I, funid, strid, sigexp, strexp, funbind_opt)) =
	(* [Rule 86] *)
	let
	    val (T,E) = elabSigExp(B, sigexp)
	    val  E'   = elabStrExp(
			   B oplusSE StrIdMap.singleton(strid, E),
			   strexp)
	    val T'    = TyNameSet.difference(StaticEnv.tynames E',
				TyNameSet.union(StaticBasis.Tof B, T))
	    val F     = case funbind_opt
			  of NONE         => FunIdMap.empty
			   | SOME funbind => elabFunBind(B, funbind)
	in
	    if not(TyNameSet.isEmpty
			(TyNameSet.intersection(T, StaticBasis.tynames B))) then
		(* Side condition is always ensured by stamping. *)
		error(I, "inconsistent type names")
	    else
		FunIdMap.insert(F, funid, (T,(E,(T',E'))))
	end


    (* Top-level Declarations *)

    and elabTopDec(B, STRDECTopDec(I, strdec, topdec_opt)) =
	(* [Rule 87] *)
	let
	    val E   = elabStrDec true (B, strdec)
	    val B'  = case topdec_opt
			of NONE        => StaticBasis.empty
			 | SOME topdec => elabTopDec(B oplusE E, topdec)
	    val B'' = StaticBasis.plus
			(StaticBasis.fromTandE(StaticEnv.tynames E, E), B')
	in
	    if not(TyVarSet.isEmpty(StaticBasis.tyvars B'')) then
		error(I, "free type variables on top-level")
	    else if not(StampMap.isEmpty(StaticBasis.undetermined B'')) then
		error(I, "undetermined types on top-level")
	    else
		B''
	end

      | elabTopDec(B, SIGDECTopDec(I, sigdec, topdec_opt)) =
	(* [Rule 88] *)
	let
	    val G   = elabSigDec(B, sigdec)
	    val B'  = case topdec_opt
			of NONE        => StaticBasis.empty
			 | SOME topdec => elabTopDec(B oplusG G, topdec)
	    val B'' = StaticBasis.plus
			(StaticBasis.fromTandG(StaticBasis.tynamesG G, G), B')
	in
	    B''
	end

      | elabTopDec(B, FUNDECTopDec(I, fundec, topdec_opt)) =
	(* [Rule 89] *)
	let
	    val F   = elabFunDec(B, fundec)
	    val B'  = case topdec_opt
			of NONE        => StaticBasis.empty
			 | SOME topdec => elabTopDec(B oplusF F, topdec)
	    val B'' = StaticBasis.plus
			(StaticBasis.fromTandF(StaticBasis.tynamesF F, F), B')
	in
	    if not(TyVarSet.isEmpty(StaticBasis.tyvars B'')) then
		error(I, "free type variables on top-level")
	    else if not(StampMap.isEmpty(StaticBasis.undetermined B'')) then
		error(I, "undetermined types on top-level")
	    else
		B''
	end



    (* Build tentative TE from LHSs of datdesc *)

    and lhsDatDesc(DatDesc(I, tyvarseq, tycon, condesc, datdesc_opt)) =
	(* [Rule 81, part 1] *)
	let
	    val (U,alphas) = ElabCore.tyvars tyvarseq
	    val k          = List.length alphas
	    val span       = lhsConDesc condesc
	    val t          = TyName.tyname(TyCon.toString tycon, k, true, span)
	    val tau        = Type.fromConsType(List.map Type.fromTyVar alphas,t)
	    val TE'        = case datdesc_opt
			       of NONE         => TyConMap.empty
				| SOME datdesc => lhsDatDesc datdesc
	in
	    TyConMap.insert(TE', tycon, ((alphas,tau), VIdMap.empty))
	end

    and lhsConDesc(ConDesc(I, vid, ty_opt, condesc_opt)) =
	case condesc_opt
	  of NONE         => 1
	   | SOME condesc => 1 + lhsConDesc condesc
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML programs
 *
 * Definition, Section 8
 *
 * Note:
 *     State is passed as reference and modified via side effects. This way
 *     expanding out the state and exception convention in the inference rules
 *     of modules and core can be avoided. Note that the state therefore
 *     never is returned.
 *)

structure Program : PROGRAM =
struct
    (* Import *)

    open GrammarProgram
    open Basis
    type State = EvalModule.State


    (* Helpers for output *)

    val width = 79

    fun printException(s, e) =
	( TextIO.output(TextIO.stdOut, "Uncaught exception: ")
	; PrettyPrint.output(TextIO.stdOut, PPVal.ppExVal(s, e), width)
	; TextIO.output1(TextIO.stdOut, #"\n")
	; TextIO.flushOut TextIO.stdOut
	)

    fun printStaticBasis B_STAT =
	( PrettyPrint.output(TextIO.stdOut, PPStaticBasis.ppBasis B_STAT,
			     width)
	; TextIO.flushOut TextIO.stdOut
	)

    fun printDynamicBasis(s, B_DYN) =
	( PrettyPrint.output(TextIO.stdOut, PPDynamicBasis.ppBasis(s, B_DYN),
			     width)
	; TextIO.flushOut TextIO.stdOut
	)

    fun printBasis(s, B) =
	( PrettyPrint.output(TextIO.stdOut, PPBasis.ppBasis(s, B), width)
	; TextIO.flushOut TextIO.stdOut
	)


    (* Helpers for basis modification *)

    val oplus = Basis.oplus

    infix oplus


    (* Inference rules [Section 8] *)

    fun execProgram echo (s,B, Program(I, topdec, program_opt)) =
	(* [Rules 187 to 189] *)
	let
	    val B_STAT1 = ElabModule.elabTopDec(Basis.B_STATof B, topdec)
	    val B_DYN1  = EvalModule.evalTopDec(s,Basis.B_DYNof B, topdec)
	    (* [Rule 189] *)
	    val _       = if echo then printBasis(!s, (B_STAT1,B_DYN1)) else ()
	    val B'      = B oplus (B_STAT1,B_DYN1)
	    val B''     = case program_opt
			    of NONE         => B'
			     | SOME program => execProgram echo (s,B', program)
	in
	    B''
	end
	handle Error.Error =>
	       (* [Rule 187] *)
	       let
		   val B' = case program_opt
			      of NONE         => B
			       | SOME program => execProgram echo (s,B, program)
	       in
		   B'
	       end

	     | DynamicObjectsCore.Pack e =>
	       (* [Rule 188] *)
	       let
		   val _  = printException(!s, e)
		   val B' = case program_opt
			      of NONE         => B
			       | SOME program => execProgram echo (s,B, program)
	       in
		   B'
	       end


    (* Elaboration only *)

    fun elabProgram echo (B_STAT, Program(I, topdec, program_opt)) =
	let
	    val B_STAT1  = ElabModule.elabTopDec(B_STAT, topdec)
	    val  _       = if echo then printStaticBasis B_STAT1 else ()
	    val B_STAT'  = StaticBasis.plus(B_STAT, B_STAT1)
	    val B_STAT'' = case program_opt
			     of NONE         => B_STAT'
			      | SOME program =>
				   elabProgram echo (B_STAT', program)
	in
	    B_STAT''
	end
	handle Error.Error =>
	       B_STAT


    (* Evaluation only *)

    fun evalProgram echo (s,B_DYN, Program(I, topdec, program_opt)) =
	let
	    val B_DYN1  = EvalModule.evalTopDec(s,B_DYN, topdec)
	    val  _      = if echo then printDynamicBasis(!s, B_DYN1) else ()
	    val B_DYN'  = DynamicBasis.plus(B_DYN, B_DYN1)
	    val B_DYN'' = case program_opt
			    of NONE         => B_DYN'
			     | SOME program =>
				  evalProgram echo (s,B_DYN', program)
	in
	    B_DYN''
	end
	handle Error.Error =>
	       (* Runtime error *)
	       let
		   val B_DYN' = case program_opt
				  of NONE         => B_DYN
				   | SOME program =>
					evalProgram echo (s,B_DYN, program)
	       in
		   B_DYN'
	       end

	     | DynamicObjectsCore.Pack e =>
	       let
		   val  _     = printException(!s, e)
		   val B_DYN' = case program_opt
				  of NONE         => B_DYN
				   | SOME program =>
					evalProgram echo (s,B_DYN, program)
	       in
		   B_DYN'
	       end
end;
(*
 * (c) Andreas Rossberg 2007
 *
 * Printer for abstract program grammar
 *)

signature PP_PROGRAM =
sig
    type Program = GrammarProgram.Program

    val ppProgram : TextIO.outstream * int * Program -> unit
end;
(*
 * (c) Andreas Rossberg 2007
 *
 * Auxiliaries for printing grammar
 *)

signature PP_GRAMMAR =
sig
    type outstream = TextIO.outstream

    val ppAtom : outstream * int * string * string -> unit
    val ppElem : outstream * int * string * Source.info *
		     (outstream * int -> unit) list -> unit

    val sub :    (outstream * int * 'a -> unit) -> 'a ->
		     outstream * int -> unit
    val subs :   (outstream * int * 'a -> unit) -> 'a list ->
		     outstream * int -> unit
    val subo :   (outstream * int * 'a -> unit) -> 'a option ->
		     outstream * int -> unit
end;
(*
 * (c) Andreas Rossberg 2007
 *
 *)

structure PPGrammar : PP_GRAMMAR =
struct
    open TextIO

    fun ppIndent(out, i) = output(out, CharVector.tabulate(2*i, fn _ => #" "))
    fun ppBegin out      = output(out, "(")
    fun ppEnd out        = output(out, ")")

    fun ppInfo(out, {file, region = ((l1,c1), (l2,c2))}) =
	( case file of NONE   => ()
		     | SOME f => (output(out, f); output(out, ":")) 
	; output(out, Int.toString l1)
	; output(out, ".")
	; output(out, Int.toString c1)
	; output(out, "-")
	; output(out, Int.toString l2)
	; output(out, ".")
	; output(out, Int.toString c2)
	)

    fun ppHead'(out, i, s, I_opt) =
	( ppIndent(out, i)
	; ppBegin out
	; output(out, s)
	; case I_opt of NONE   => ()
		      | SOME I => (output(out, " "); ppInfo(out, I))
	)

    fun ppFoot'(out, i, I_opt) =
	( ppEnd out
	; output(out, "\n")
	)

    fun ppHead(out, i, s, I) = (ppHead'(out, i, s, SOME I); output(out, "\n"))
    fun ppFoot(out, i, I)    = (ppIndent(out, i); ppFoot'(out, i, SOME I))
    fun ppHeadFoot(out, i, s, I) =
	(ppHead'(out, i, s, SOME I); ppFoot'(out, i, SOME I))

    fun ppAtom(out, i, s1, s2) =
	( ppHead'(out, i, s1, NONE)
	; output(out, " ")
	; output(out, s2)
	; ppFoot'(out, i, NONE)
	)

    fun ppElem(out, i, s, I, []) =
	  ppHeadFoot(out, i, s, I)
      | ppElem(out, i, s, I, subs) =
	( ppHead(out, i, s, I)
	; List.app (fn pp => pp(out, i+1)) subs
	; ppFoot(out, i, I)
	)

    fun ppOpt ppX (out, i, NONE)    = ()
      | ppOpt ppX (out, i, SOME x) = ppX(out, i, x)

    fun sub ppX x (out, i)      = ppX(out, i, x)
    fun subs ppX xs (out, i)    = List.app (fn x => ppX(out, i, x)) xs
    fun subo ppX x_opt (out, i) = ppOpt ppX (out, i, x_opt)
end;
(*
 * (c) Andreas Rossberg 2007
 *
 * Printer for abstract module grammar
 *)

signature PP_MODULE =
sig
    type TopDec = GrammarModule.TopDec

    val ppTopDec : TextIO.outstream * int * TopDec -> unit
end;
(*
 * (c) Andreas Rossberg 2007
 *
 * Printer for abstract core grammar
 *)

signature PP_CORE =
sig
    type VId        = IdsCore.VId
    type TyCon      = IdsCore.TyCon
    type StrId      = IdsCore.StrId
    type longVId    = IdsCore.longVId
    type longTyCon  = IdsCore.longTyCon
    type longStrId  = IdsCore.longStrId
    type TyVarseq   = GrammarCore.TyVarseq
    type Ty         = GrammarCore.Ty
    type Dec        = GrammarCore.Dec

    val ppVId :       TextIO.outstream * int * VId -> unit
    val ppTyCon :     TextIO.outstream * int * TyCon -> unit
    val ppStrId :     TextIO.outstream * int * StrId -> unit
    val ppLongVId :   TextIO.outstream * int * longVId -> unit
    val ppLongTyCon : TextIO.outstream * int * longTyCon -> unit
    val ppLongStrId : TextIO.outstream * int * longStrId -> unit
    val ppTyVarseq :  TextIO.outstream * int * TyVarseq -> unit

    val ppTy :        TextIO.outstream * int * Ty -> unit
    val ppDec :       TextIO.outstream * int * Dec -> unit
end;
(*
 * (c) Andreas Rossberg 2007
 *
 * Printer for abstract core grammar
 *)

structure PPCore : PP_CORE =
struct
    (* Import *)

    open GrammarCore
    open PPGrammar


    (* Special constants *)

    fun ppSCon(out, i, scon) =
	let
	    val tag = case scon of SCon.INT _    => "INT"
				 | SCon.WORD _   => "WORD"
				 | SCon.STRING _ => "STRING"
				 | SCon.CHAR _   => "CHAR"
				 | SCon.REAL _   => "REAL"
	in
	    ppAtom(out, i, tag ^ "SCon", SCon.toString scon)
	end


    (* Identifiers *)

    fun ppLab(out, i, lab)     = ppAtom(out, i, "Lab", Lab.toString lab)
    fun ppVId(out, i, vid)     = ppAtom(out, i, "VId", VId.toString vid)
    fun ppTyVar(out, i, tyvar) = ppAtom(out, i, "TyVar", TyVar.toString tyvar)
    fun ppTyCon(out, i, tycon) = ppAtom(out, i, "TyCon", TyCon.toString tycon)
    fun ppStrId(out, i, strid) = ppAtom(out, i, "StrId", StrId.toString strid)

    fun ppLongVId(out, i, longvid) =
	    ppAtom(out, i, "LongVId", LongVId.toString longvid)
    fun ppLongTyCon(out, i, longtycon) =
	    ppAtom(out, i, "LongTyCon", LongTyCon.toString longtycon)
    fun ppLongStrId(out, i, longstrid) =
	    ppAtom(out, i, "LongStrId", LongStrId.toString longstrid)


    (* Expressions *)

    fun ppAtExp(out, i, SCONAtExp(I, scon)) =
	    ppElem(out, i, "SCONAtExp", I,
		   [sub ppSCon scon])
      | ppAtExp(out, i, IDAtExp(I, _, longvid)) =
	    ppElem(out, i, "IDAtExp", I,
		   [sub ppLongVId longvid])
      | ppAtExp(out, i, RECORDAtExp(I, exprow_opt)) =
	    ppElem(out, i, "RECORDAtExp", I,
		   [subo ppExpRow exprow_opt])
      | ppAtExp(out, i, LETAtExp(I, dec, exp)) =
	    ppElem(out, i, "LETAtExp", I,
		   [sub ppDec dec, sub ppExp exp])
      | ppAtExp(out, i, PARAtExp(I, exp)) =
	    ppElem(out, i, "PARAtExp", I,
		   [sub ppExp exp])

    and ppExpRow(out, i, ExpRow(I, lab, exp, exprow_opt)) =
	    ppElem(out, i, "ExpRow", I,
		   [sub ppLab lab, sub ppExp exp, subo ppExpRow exprow_opt])

    and ppExp(out, i, ATExp(I, atexp)) =
	    ppElem(out, i, "ATExp", I,
		   [sub ppAtExp atexp])
      | ppExp(out, i, APPExp(I, exp, atexp)) =
	    ppElem(out, i, "APPExp", I,
		   [sub ppExp exp, sub ppAtExp atexp])
      | ppExp(out, i, COLONExp(I, exp, ty)) =
	    ppElem(out, i, "COLONExp", I,
		   [sub ppExp exp, sub ppTy ty])
      | ppExp(out, i, HANDLEExp(I, exp, match)) =
	    ppElem(out, i, "HANDLEExp", I,
		   [sub ppExp exp, sub ppMatch match])
      | ppExp(out, i, RAISEExp(I, exp)) =
	    ppElem(out, i, "RAISEExp", I,
		   [sub ppExp exp])
      | ppExp(out, i, FNExp(I, match)) =
	    ppElem(out, i, "FNExp", I,
		   [sub ppMatch match])


    (* Matches *)

    and ppMatch(out, i, Match(I, mrule, match_opt)) =
	    ppElem(out, i, "Match", I,
		   [sub ppMrule mrule, subo ppMatch match_opt])

    and ppMrule(out, i, Mrule(I, pat, exp)) =
	    ppElem(out, i, "Mrule", I,
		   [sub ppPat pat, sub ppExp exp])


    (* Declarations *)

    and ppDec(out, i, VALDec(I, tyvarseq, valbind)) =
	    ppElem(out, i, "VALDec", I,
		   [sub ppTyVarseq tyvarseq, sub ppValBind valbind])
      | ppDec(out, i, TYPEDec(I, typbind)) =
	    ppElem(out, i, "TYPEDec", I,
		   [sub ppTypBind typbind])
      | ppDec(out, i, DATATYPEDec(I, datbind)) =
	    ppElem(out, i, "DATATYPEDec", I,
		   [sub ppDatBind datbind])
      | ppDec(out, i, DATATYPE2Dec(I, tycon, longtycon)) =
	    ppElem(out, i, "DATATYPE2Dec", I,
		   [sub ppTyCon tycon, sub ppLongTyCon longtycon])
      | ppDec(out, i, ABSTYPEDec(I, datbind, dec)) =
	    ppElem(out, i, "ABSTYPEDec", I,
		   [sub ppDatBind datbind, sub ppDec dec])
      | ppDec(out, i, EXCEPTIONDec(I, exbind)) =
	    ppElem(out, i, "EXCEPTIONDec", I,
		   [sub ppExBind exbind])
      | ppDec(out, i, LOCALDec(I, dec1, dec2)) =
	    ppElem(out, i, "LOCALDec", I,
		   [sub ppDec dec1, sub ppDec dec2])
      | ppDec(out, i, OPENDec(I, longstrids)) =
	    ppElem(out, i, "OPENDec", I,
		   [subs ppLongStrId longstrids])
      | ppDec(out, i, EMPTYDec(I)) =
	    ppElem(out, i, "EMPTYDec", I, [])
      | ppDec(out, i, SEQDec(I, dec1, dec2)) =
	    ppElem(out, i, "SEQDec", I,
		   [sub ppDec dec1, sub ppDec dec2])

    and ppValBind(out, i, PLAINValBind(I, pat, exp, valbind_opt)) =
	    ppElem(out, i, "PLAINValBind", I,
		   [sub ppPat pat, sub ppExp exp, subo ppValBind valbind_opt])
      | ppValBind(out, i, RECValBind(I, valbind)) =
	    ppElem(out, i, "RECValBind", I,
		   [sub ppValBind valbind])
    and ppTypBind(out, i, TypBind(I, tyvarseq, tycon, ty, typbind_opt)) =
	    ppElem(out, i, "TypBind", I,
		   [sub ppTyVarseq tyvarseq, sub ppTyCon tycon, sub ppTy ty,
		    subo ppTypBind typbind_opt])
    and ppDatBind(out, i, DatBind(I, tyvarseq, tycon, conbind, datbind_opt)) =
	    ppElem(out, i, "DatBind", I,
		   [sub ppTyVarseq tyvarseq, sub ppTyCon tycon,
		    sub ppConBind conbind, subo ppDatBind datbind_opt])
    and ppConBind(out, i, ConBind(I, _, vid, ty_opt, conbind_opt)) =
	    ppElem(out, i, "ConBind", I,
		   [sub ppVId vid, subo ppTy ty_opt,
		    subo ppConBind conbind_opt])
    and ppExBind(out, i, NEWExBind(I, _, vid, ty_opt, exbind_opt)) =
	    ppElem(out, i, "NEWExBind", I,
		   [sub ppVId vid, subo ppTy ty_opt,
		    subo ppExBind exbind_opt])
      | ppExBind(out, i, EQUALExBind(I, _, vid, _, longvid, exbind_opt)) =
	    ppElem(out, i, "EQUALExBind", I,
		   [sub ppVId vid, sub ppLongVId longvid,
		    subo ppExBind exbind_opt])


    (* Patterns *)

    and ppAtPat(out, i, WILDCARDAtPat(I)) =
	    ppElem(out, i, "WILDCARDAtPat", I, [])
      | ppAtPat(out, i, SCONAtPat(I, scon)) =
	    ppElem(out, i, "SCONAtPat", I,
		   [sub ppSCon scon])
      | ppAtPat(out, i, IDAtPat(I, _, longvid)) =
	    ppElem(out, i, "IDAtPat", I,
		   [sub ppLongVId longvid])
      | ppAtPat(out, i, RECORDAtPat(I, patrow_opt)) =
	    ppElem(out, i, "RECORDAtPat", I,
		   [subo ppPatRow patrow_opt])
      | ppAtPat(out, i, PARAtPat(I, pat)) =
	    ppElem(out, i, "PARAtPat", I,
		   [sub ppPat pat])

    and ppPatRow(out, i, DOTSPatRow(I)) =
	    ppElem(out, i, "DOTSPatRow", I, [])
      | ppPatRow(out, i, FIELDPatRow(I, lab, pat, patrow_opt)) =
	    ppElem(out, i, "FIELDPatRow", I,
		   [sub ppLab lab, sub ppPat pat, subo ppPatRow patrow_opt])

    and ppPat(out, i, ATPat(I, atpat)) =
	    ppElem(out, i, "ATPat", I,
		   [sub ppAtPat atpat])
      | ppPat(out, i, CONPat(I, _, longvid, atpat)) =
	    ppElem(out, i, "CONPat", I,
		   [sub ppLongVId longvid, sub ppAtPat atpat])
      | ppPat(out, i, COLONPat(I, pat, ty)) =
	    ppElem(out, i, "COLONPat", I,
		   [sub ppPat pat, sub ppTy ty])
      | ppPat(out, i, ASPat(I, _, vid, ty_opt, pat)) =
	    ppElem(out, i, "ASPat", I,
		   [sub ppVId vid, subo ppTy ty_opt, sub ppPat pat])


    (* Type expressions *)

    and ppTy(out, i, VARTy(I, tyvar)) =
	    ppElem(out, i, "VARTy", I,
		   [sub ppTyVar tyvar])
      | ppTy(out, i, RECORDTy(I, tyrow_opt)) =
	    ppElem(out, i, "RECORDTy", I,
		   [subo ppTyRow tyrow_opt])
      | ppTy(out, i, CONTy(I, tyseq, longtycon)) =
	    ppElem(out, i, "CONTy", I,
		   [sub ppTyseq tyseq, sub ppLongTyCon longtycon])
      | ppTy(out, i, ARROWTy(I, ty1, ty2)) =
	    ppElem(out, i, "ARROWTy", I,
		   [sub ppTy ty1, sub ppTy ty2])
      | ppTy(out, i, PARTy(I, ty)) =
	    ppElem(out, i, "PARTy", I,
		   [sub ppTy ty])

    and ppTyRow(out, i, TyRow(I, lab, ty, tyrow_opt)) =
	    ppElem(out, i, "TyRow", I,
		   [sub ppLab lab, sub ppTy ty, subo ppTyRow tyrow_opt])


    (* Sequences *)

    and ppTyseq(out, i, Tyseq(I, tys)) =
	    ppElem(out, i, "Tyseq", I,
		   [subs ppTy tys])

    and ppTyVarseq(out, i, TyVarseq(I, tyvars)) =
	    ppElem(out, i, "TyVarseq", I,
		   [subs ppTyVar tyvars])
end;
(*
 * (c) Andreas Rossberg 2007
 *
 * Printer for abstract module grammar
 *)

structure PPModule : PP_MODULE =
struct
    (* Import *)

    open GrammarModule
    open PPGrammar


    (* Identifiers *)

    fun ppSigId(out, i, sigid) = ppAtom(out, i, "SigId", SigId.toString sigid)
    fun ppFunId(out, i, funid) = ppAtom(out, i, "FunId", FunId.toString funid)


    (* Structures *)

    fun ppStrExp(out, i, STRUCTStrExp(I, strdec)) =
	    ppElem(out, i, "STRUCTStrExp", I,
		   [sub ppStrDec strdec])
      | ppStrExp(out, i, IDStrExp(I, longstrid)) =
	    ppElem(out, i, "IDStrExp", I,
		   [sub PPCore.ppLongStrId longstrid])
      | ppStrExp(out, i, COLONStrExp(I, strexp, sigexp)) =
	    ppElem(out, i, "COLONStrExp", I,
		   [sub ppStrExp strexp, sub ppSigExp sigexp])
      | ppStrExp(out, i, SEALStrExp(I, strexp, sigexp)) =
	    ppElem(out, i, "SEALStrExp", I,
		   [sub ppStrExp strexp, sub ppSigExp sigexp])
      | ppStrExp(out, i, APPStrExp(I, funid, strexp)) =
	    ppElem(out, i, "APPStrExp", I,
		   [sub ppFunId funid, sub ppStrExp strexp])
      | ppStrExp(out, i, LETStrExp(I, strdec, strexp)) =
	    ppElem(out, i, "LETStrExp", I,
		   [sub ppStrDec strdec, sub ppStrExp strexp])

    and ppStrDec(out, i, DECStrDec(I, dec)) =
	    ppElem(out, i, "DECStrDec", I,
		   [sub PPCore.ppDec dec])
      | ppStrDec(out, i, STRUCTUREStrDec(I, strbind)) =
	    ppElem(out, i, "STRUCTUREStrDec", I,
		   [sub ppStrBind strbind])
      | ppStrDec(out, i, LOCALStrDec(I, strdec1, strdec2)) =
	    ppElem(out, i, "LOCALStrDec", I,
		   [sub ppStrDec strdec1, sub ppStrDec strdec2])
      | ppStrDec(out, i, EMPTYStrDec(I)) =
	    ppElem(out, i, "EMPTYStrDec", I, [])
      | ppStrDec(out, i, SEQStrDec(I, strdec1, strdec2)) =
	    ppElem(out, i, "SEQStrDec", I,
		   [sub ppStrDec strdec1, sub ppStrDec strdec2])

    and ppStrBind(out, i, StrBind(I, strid, strexp, strbind_opt)) =
	    ppElem(out, i, "StrBind", I,
		   [sub PPCore.ppStrId strid, sub ppStrExp strexp,
		    subo ppStrBind strbind_opt])


    (* Signatures *)

    and ppSigExp(out, i, SIGSigExp(I, spec)) =
	    ppElem(out, i, "SIGSigExp", I,
		   [sub ppSpec spec])
      | ppSigExp(out, i, IDSigExp(I, sigid)) =
	    ppElem(out, i, "IDSigExp", I,
		   [sub ppSigId sigid])
      | ppSigExp(out, i, WHERETYPESigExp(I, sigexp, tyvarseq, longtycon, ty)) =
	    ppElem(out, i, "WHERETYPESigExp", I,
		   [sub ppSigExp sigexp, sub PPCore.ppTyVarseq tyvarseq,
		    sub PPCore.ppLongTyCon longtycon, sub PPCore.ppTy ty])

    and ppSigDec(out, i, SigDec(I, sigbind)) =
	    ppElem(out, i, "SigDec", I,
		   [sub ppSigBind sigbind])

    and ppSigBind(out, i, SigBind(I, sigid, sigexp, sigbind_opt)) =
	    ppElem(out, i, "SigBind", I,
		   [sub ppSigId sigid, sub ppSigExp sigexp,
		    subo ppSigBind sigbind_opt])


    (* Specifications *)

    and ppSpec(out, i, VALSpec(I, valdesc)) =
	    ppElem(out, i, "VALSpec", I,
		   [sub ppValDesc valdesc])
      | ppSpec(out, i, TYPESpec(I, typdesc)) =
	    ppElem(out, i, "TYPESpec", I,
		   [sub ppTypDesc typdesc])
      | ppSpec(out, i, EQTYPESpec(I, typdesc)) =
	    ppElem(out, i, "EQTYPESpec", I,
		   [sub ppTypDesc typdesc])
      | ppSpec(out, i, DATATYPESpec(I, datdesc)) =
	    ppElem(out, i, "DATATYPESpec", I,
		   [sub ppDatDesc datdesc])
      | ppSpec(out, i, DATATYPE2Spec(I, tycon, longtycon)) =
	    ppElem(out, i, "DATATYPE2Spec", I,
		   [sub PPCore.ppTyCon tycon, sub PPCore.ppLongTyCon longtycon])
      | ppSpec(out, i, EXCEPTIONSpec(I, exdesc)) =
	    ppElem(out, i, "EXCEPTIONSpec", I,
		   [sub ppExDesc exdesc])
      | ppSpec(out, i, STRUCTURESpec(I, strdesc)) =
	    ppElem(out, i, "STRUCTURESpec", I,
		   [sub ppStrDesc strdesc])
      | ppSpec(out, i, INCLUDESpec(I, sigexp)) =
	    ppElem(out, i, "INCLUDESpec", I,
		   [sub ppSigExp sigexp])
      | ppSpec(out, i, EMPTYSpec(I)) =
	    ppElem(out, i, "EMPTYSpec", I, [])
      | ppSpec(out, i, SEQSpec(I, spec1, spec2)) =
	    ppElem(out, i, "SEQSpec", I,
		   [sub ppSpec spec1, sub ppSpec spec2])
      | ppSpec(out, i, SHARINGTYPESpec(I, spec, longtycons)) =
	    ppElem(out, i, "SHARINGTYPESpec", I,
		   [sub ppSpec spec, subs PPCore.ppLongTyCon longtycons])
      | ppSpec(out, i, SHARINGSpec(I, spec, longstrids)) =
	    ppElem(out, i, "SHARINGSpec", I,
		   [sub ppSpec spec, subs PPCore.ppLongStrId longstrids])

    and ppValDesc(out, i, ValDesc(I, vid, ty, valdesc_opt)) =
	    ppElem(out, i, "ValDesc", I,
		   [sub PPCore.ppVId vid, sub PPCore.ppTy ty,
		    subo ppValDesc valdesc_opt])
    and ppTypDesc(out, i, TypDesc(I, tyvarseq, tycon, typdesc_opt)) =
	    ppElem(out, i, "TypDec", I,
		   [sub PPCore.ppTyVarseq tyvarseq, sub PPCore.ppTyCon tycon,
		    subo ppTypDesc typdesc_opt])
    and ppDatDesc(out, i, DatDesc(I, tyvarseq, tycon, condesc, datdesc_opt)) =
	    ppElem(out, i, "DatDesc", I,
		   [sub PPCore.ppTyVarseq tyvarseq, sub PPCore.ppTyCon tycon,
		    sub ppConDesc condesc, subo ppDatDesc datdesc_opt])
    and ppConDesc(out, i, ConDesc(I, vid, ty_opt, condesc_opt)) =
	    ppElem(out, i, "ConDesc", I,
		   [sub PPCore.ppVId vid, subo PPCore.ppTy ty_opt,
		    subo ppConDesc condesc_opt])
    and ppExDesc(out, i, ExDesc(I, vid, ty_opt, exdesc_opt)) =
	    ppElem(out, i, "ExDesc", I,
		   [sub PPCore.ppVId vid, subo PPCore.ppTy ty_opt,
		    subo ppExDesc exdesc_opt])
    and ppStrDesc(out, i, StrDesc(I, strid, sigexp, strdesc_opt)) =
	    ppElem(out, i, "StrDesc", I,
		   [sub PPCore.ppStrId strid, sub ppSigExp sigexp,
		    subo ppStrDesc strdesc_opt])


    (* Functors *)

    and ppFunDec(out, i, FunDec(I, funbind)) =
	    ppElem(out, i, "FunDec", I,
		   [sub ppFunBind funbind])

    and ppFunBind(out, i, FunBind(I, funid, strid, sigexp, strexp,
				  funbind_opt)) =
	    ppElem(out, i, "FunBind", I,
		   [sub ppFunId funid, sub PPCore.ppStrId strid,
		    sub ppSigExp sigexp, sub ppStrExp strexp,
		    subo ppFunBind funbind_opt])


    (* Top-level declarations *)

    and ppTopDec(out, i, STRDECTopDec(I, strdec, topdec_opt)) =
	    ppElem(out, i, "STRDECTopDec", I,
		   [sub ppStrDec strdec, subo ppTopDec topdec_opt])
      | ppTopDec(out, i, SIGDECTopDec(I, sigdec, topdec_opt)) =
	    ppElem(out, i, "SIGDECTopDec", I,
		   [sub ppSigDec sigdec, subo ppTopDec topdec_opt])
      | ppTopDec(out, i, FUNDECTopDec(I, fundec, topdec_opt)) =
	    ppElem(out, i, "FUNDECTopDec", I,
		   [sub ppFunDec fundec, subo ppTopDec topdec_opt])
end;
(*
 * (c) Andreas Rossberg 2007
 *
 * Printer for abstract program grammar
 *)


structure PPProgram : PP_PROGRAM =
struct
    (* Import *)

    open GrammarProgram
    open PPGrammar


    (* Programs *)

    fun ppProgram(out, i, Program(I, topdec, program_opt)) =
	    ppElem(out, i, "Program", I,
		   [sub PPModule.ppTopDec topdec, subo ppProgram program_opt])
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML implementation main structure
 *)

structure Sml : SML =
struct
    (* Initial arguments *)

    type arg = (Infix.InfEnv * BindingBasis.Basis) * Basis.Basis * Program.State

    val J0          = InitialInfixEnv.J0
    val B0_BIND     = StaticBasis.toBindingBasis InitialStaticBasis.B0
    val B0_BIND'    = StaticBasis.toBindingBasis Library.B0_STAT
    val B0_STAT     = InitialStaticBasis.B0
    val B0_DYN      = InitialDynamicBasis.B0
    val B0          = (B0_STAT, B0_DYN)
    val B0'         = (Library.B0_STAT, Library.B0_DYN)
    val s0          = InitialDynamicBasis.s0
    val s0'         = Library.s0

    val initialArg  = ((J0,B0_BIND), B0, s0)
    val initialArg' = ((J0,B0_BIND'), B0', s0')


    (* Parsing only *)

    val checkProgram = SyntacticRestrictionsProgram.checkProgram

    fun parseArg (JB,B,s) = JB

    fun parse (J, B_BIND) (filenameOpt, source) =
	let
	    val (J',program) = Parse.parse(J, source, filenameOpt)
	    val  B_BIND'     = checkProgram(B_BIND, program)
	    val  _           = PPProgram.ppProgram(TextIO.stdOut, 0, program)
	in
	    (J', B_BIND')
	end


    (* Parsing and elaboration *)

    fun elabArg ((J,B_BIND), (B_STAT,B_DYN), s) = (J, B_BIND, B_STAT)

    fun elab (J, B_BIND, B_STAT) (filenameOpt, source) =
	let
	    val (J',program) = Parse.parse(J, source, filenameOpt)
	    val  B_BIND'     = checkProgram(B_BIND, program)
	    val  B_STAT'     = Program.elabProgram true (B_STAT, program)
	in
	    (J', B_BIND', B_STAT')
	end


    (* Parsing and evaluation *)

    fun evalArg ((J,B_BIND), (B_STAT,B_DYN), s) = (J, B_BIND, B_DYN, s)

    fun eval (J, B_BIND, B_DYN, s) (filenameOpt, source) =
	let
	    val (J',program) = Parse.parse(J, source, filenameOpt)
	    val  B_BIND'     = checkProgram(B_BIND, program)
	    val  s'          = ref s
	    val  B_DYN'      = Program.evalProgram true (s', B_DYN, program)
	in
	    (J', B_BIND', B_DYN', !s')
	end


    (* Parsing, elaboration, and evaluation *)

    fun execArg arg = arg

    fun exec' echo ((J,B_BIND), B, s) (filenameOpt, source) =
	let
	    val (J',program) = Parse.parse(J, source, filenameOpt)
	    val  B_BIND'     = checkProgram(B_BIND, program)
	    val  s'          = ref s
	    val  B'          = Program.execProgram echo (s', B, program)
	in
	    ((J',B_BIND'), B', !s' )
	end

    val exec = exec' true


    (* Process the `use' queue *)

    fun uses fromFile (process, arg) =
	case Use.extract()
	  of NONE      => arg
	   | SOME name => uses fromFile (process, fromFile (process, arg) name)


    (* Processing of strings *)

    fun fromString'' fromUsedFile (process, arg) (filenameOpt, source) =
	let
	    val arg' = process arg (filenameOpt, source)
		       handle Error.Error => arg	(* Syntax error *)
	in
	    uses fromUsedFile (process, arg')
	end

    fun fromString' fromUsedFile (process, arg) source =
	fromString'' fromUsedFile (process, arg) (NONE, source)

    fun fromInput' fromUsedFile (process, arg) (n, source) =
	fromString'' fromUsedFile (process, arg)
		     (SOME("(input "^Int.toString n^")"), source)


    (* Processing of files *)

    fun fromFile' fromUsedFile (process, arg) name =
	let
	    val file   = TextIO.openIn name
	    val source = TextIO.inputAll file ^ ";"
	    val _      = TextIO.closeIn file
	    val dir    = OS.FileSys.getDir()
	    val dir'   = case OS.Path.dir name of ""   => OS.Path.currentArc
						| dir' => dir'
	in
	    OS.FileSys.chDir dir';
	    fromString'' fromUsedFile (process, arg) (SOME name, source)
	    before OS.FileSys.chDir dir
	end
	handle IO.Io _ =>
	    ( TextIO.output(TextIO.stdErr, name ^ ": read error\n") ; arg )

    fun fromFileLogged (process, arg) name =
	( TextIO.output(TextIO.stdOut, "[processing " ^ name ^ "]\n")
	; TextIO.flushOut TextIO.stdOut
	; fromFile' fromFileLogged (process, arg) name
	)

    fun fromString args    = fromString' fromFileLogged args
    fun fromInput args     = fromInput' fromFileLogged args
    fun fromFile args      = fromFile' fromFileLogged args
    fun fromFileQuiet args = fromFile' fromFileQuiet args


    (* Processing several files mentioned in a list file *)

    fun fromFiles (process, initialArg) names =
	List.foldl (fn (name, arg) =>
		    fromFileLogged (process, initialArg) name) initialArg names


    (* Session *)

    fun fromSession (process, initialArg) =
	let
	    fun inputLines prompt =
		let
		    val _    = TextIO.output(TextIO.stdOut, prompt)
		    val _    = TextIO.flushOut TextIO.stdOut
		    val line = TextIO.inputLine TextIO.stdIn
		in
		    case line
		      of NONE      => nil
		       | SOME "\n" => "\n" :: inputLines "  "
		       | SOME text =>
			 if String.sub(text, String.size text - 2) = #";" then
			     text :: nil
			 else
			     text :: inputLines "  "
		end

	    fun loop(n, arg) =
		case inputLines "- "
		  of nil   => ()
		   | lines => loop(n+1, fromInput (process, arg)
						  (n, String.concat lines))
	in
	    loop(1, initialArg)
	end


    (* Install library *)

    val basisPath = ref "basis"

    fun loadLib() =
	( TextIO.output(TextIO.stdOut, "[loading standard basis library]\n")
	; TextIO.flushOut TextIO.stdOut
	; fromFileQuiet (exec' false, initialArg')
			(OS.Path.joinDirFile{dir  = !basisPath,
					     file = Library.file})
	)
	handle IO.Io _ =>
	( TextIO.output(TextIO.stdOut, "[library not found]\n")
	; initialArg
	)

    val libRef = ref(NONE : arg option)

    fun lib() =	case !libRef of SOME arg => arg
			      | NONE => ( libRef := SOME(loadLib()); lib() )


    (* Plumbing *)

    fun processString (f, arg) s   = ignore(fromString (f, arg(lib())) s)
    fun processFile   (f, arg) s   = ignore(fromFile (f, arg(lib())) s)
    fun processFiles  (f, arg) s   = ignore(fromFiles (f, arg(lib())) s)
    fun processSession (f, arg) () = fromSession(f, arg(lib()))

    val parseString  = processString(parse, parseArg)
    val elabString   = processString(elab, elabArg)
    val evalString   = processString(eval, evalArg)
    val execString   = processString(exec, execArg)

    val parseFile    = processFile(parse, parseArg)
    val elabFile     = processFile(elab, elabArg)
    val evalFile     = processFile(eval, evalArg)
    val execFile     = processFile(exec, execArg)

    val parseFiles   = processFiles(parse, parseArg)
    val elabFiles    = processFiles(elab,  elabArg)
    val evalFiles    = processFiles(eval,  evalArg)
    val execFiles    = processFiles(exec,  execArg)

    val parseSession = processSession(parse, parseArg)
    val elabSession  = processSession(elab,  elabArg)
    val evalSession  = processSession(eval,  evalArg)
    val execSession  = processSession(exec,  execArg)
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML implementation stand-alone
 *)

signature MAIN =
sig
    val version : string
    val main :    unit -> 'a
end;
(*
 * (c) Andreas Rossberg 1999-2007
 *
 * Standard ML implementation stand-alone
 *)

structure Main : MAIN =
struct
    val version = "1.3.1"

    fun usage() =
	( TextIO.output(TextIO.stdErr,
	    "Usage: hamlet [-<mode>] [file ...]\n\
	    \where <mode> is one of:\n\
	    \  h   help:       print this message\n\
	    \  p   parse mode: just parse input\n\
	    \  l   elab mode:  parse and elaborate\n\
	    \  v   eval mode:  parse and evaluate (no type checking!)\n\
	    \  x   exec mode:  parse, elaborate, and evaluate (default)\n"
	  )
	; TextIO.flushOut TextIO.stdErr
	; OS.Process.failure
	)

    fun start mode process =
	( TextIO.print("HaMLet " ^ version ^
		       " - To Be Or Not To Be Standard ML\n")
	; case mode of NONE => () | SOME s => TextIO.print("[" ^ s ^ " mode]\n")
	; process()
	; TextIO.print "\n"
	; OS.Process.success
	)

    fun expand(name, names) =
	if String.size name = 0 orelse String.sub(name, 0) <> #"@" then
	    name::names
	else
	    let
		val file    = TextIO.openIn(String.extract(name, 1, NONE))
		val content = TextIO.inputAll file
		val _       = TextIO.closeIn file
	    in
		List.foldr expand names (String.tokens Char.isSpace content)
	    end

    fun run process names =
	( process (List.foldr expand [] names)
	; OS.Process.success
	)
	handle IO.Io _ =>
	( TextIO.output(TextIO.stdOut, "I/O error\n")
	; OS.Process.failure
	)

    fun main' ["-h"]        = ( usage() ; OS.Process.success )
      | main' ["-p"]        = start (SOME "Parsing") Sml.parseSession
      | main' ["-l"]        = start (SOME "Elaboration") Sml.elabSession
      | main' ["-v"]        = start (SOME "Evaluation") Sml.evalSession
      | main' ["-x"]        = start NONE Sml.execSession
      | main' []            = start NONE Sml.execSession
      | main' ("-p"::names) = run Sml.parseFiles names
      | main' ("-l"::names) = run Sml.elabFiles names
      | main' ("-v"::names) = run Sml.evalFiles names
      | main' ("-x"::names) = run Sml.execFiles names
      | main' names         = run Sml.execFiles names

    fun main() =
	let
	    val homeDir = case OS.Path.dir(CommandLine.name())
			    of ""  => OS.Path.currentArc
			     | dir => dir
	in
	    Sml.basisPath := OS.Path.joinDirFile{dir=homeDir, file="basis"};
	    OS.Process.exit(main'(CommandLine.arguments()))
	end
	handle exn =>
	( TextIO.output(TextIO.stdErr, "hamlet: unhandled internal exception " ^
					General.exnName exn ^ "\n")
	; OS.Process.exit OS.Process.failure
	)
end;
val _ = Main.main()
